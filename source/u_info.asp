﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<%
' 验证用户
call CheckUserLogin("")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>用户信息中心 </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
dim action,errormsg
action=getForm("action","")
errormsg=""
if action="save" then
    myuser.firstname=TurnSql(getForm("firstname",""))
	myuser.lastname=TurnSql(getForm("lastname",""))
	myuser.sex=TurnSql(getForm("sex",""))
	myuser.city=getForm("city",0)
	myuser.state=getForm("state",0)
	myuser.country=getForm("country",44)
	myuser.address=TurnSql(getForm("address",""))
	myuser.postcode=TurnSql(getForm("postcode",""))
	myuser.mobile=TurnSql(getForm("mobile",""))
	myuser.phone=TurnSql(getForm("phone",""))
	myuser.fax=TurnSql(getForm("fax",""))
	myuser.homepage=TurnSql(getForm("homepage",""))
	
	myuser.update()		
	ErrorMsg="操作完成!!"		
end if
%>
<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td width="180">
                    <!--#include file="u_left.asp"-->
                </td>
                <td width="8"></td>
                <td>
                    <form name="webForm" id="webForm" class="appnitro" action="" method="post">
                        <input type="hidden" name="action" value="save" />
                        <ul>
                            <% if ErrorMsg<>"" then  %><li class="imsg">
                                <%=ErrorMsg %>
                            </li>
                            <% end if %>
                            <!--<li>
                                <label class="description" for="email">
                                    Email <font style="color: Red;">*</font>
                                </label>
                                <div>
                                    <input id="email" name="email" class="element text medium" value="<%=myuser.email %>" type="text"
                                        maxlength="100" />
                                </div>
                                <p class="guidelines">
                                    请填写有效的Email地址<br />
                                    Email 用于取回密码及网站相关通知</p>
                            </li>
                            <li>
                                <hr />
                            </li>-->
                            <li>
                                <label class="description" for="firstname">
                                    真实姓名(truename)
                                </label>
                                <div>
                                    <input id="firstname" name="firstname"  value="<%=myuser.firstname %>" onFocus="this.select();" class="element text small"
                                        type="text" maxlength="100" />
                                    <input id="lastname" name="lastname"  value="<%=myuser.lastname %>"  onfocus="this.select();" class="element text small"
                                        type="text" maxlength="100" />
                                </div>
                            </li>
                            <li>
                                <label class="description" for="sex1">
                                    性别(Sex)
                                </label>
                                <div>
                                    <input id="sex1" name="sex" class="element radio" type="radio" value="男" />
                                    <label class="choice" for="sex1">
                                        男</label>
                                    <input id="sex2" name="sex" class="element radio" type="radio" value="女" />
                                    <label class="choice" for="sex2">
                                        女</label>
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.sex, "<%=myuser.sex %>", "男");
                                        </script>
                                </div>
                            </li>
                            <!--<li>
                                <label class="description" for="country">
                                    Country
                                </label>
                                <div>
                                    <select id="country" name="country" class="element select small">
                                        <option value="0">-- Select --</option>
                                    </select>
                                    <script type="text/javascript">
                                        InitArea(areaArray, webForm.country, 0);
                                        $.setElementValue(webForm.country, "<%=myuser.country %>", "0");
                                        </script>
                                </div>
                            </li>-->
                            <li>
                                <label class="description" for="city">
                                    所在地区(State&City)
                                </label>
                                <div>
                                    <% if conOpenSelectRegion=1 then %>
                                    <select id="state" name="state" class="element select small" onChange="InitArea(areaArray,webForm.city,this.value);">
                                        <option value="0">-- 请选择省份 --</option>
                                    </select>
                                    <select id="city" name="city" class="element select small">
                                        <option value="0">-- 请选择城市 --</option>
                                    </select>
                                    <script type="text/javascript">
                                        InitArea(areaArray, webForm.state, 44);
                                        $.setElementValue(webForm.state, "<%=myuser.state %>", "0");
                                        $.setElementValue(webForm.city, "<%=myuser.city %>", "0");
                                        </script>
                                    <% else %>
                                        <input id="state" name="state" value="<%=myuser.state %>"  class="element text small" type="text" maxlength="100" />
                        <input id="city" name="city" value="<%=myuser.city %>"  class="element text small"  type="text" maxlength="100" />
                                    <% end if %>
                                </div>
                            </li>
                            <li>
                                <label class="description" for="address">
                                    详细地址(address)
                                </label>
                                <div>
                                    <textarea class="element textarea small" id="address" name="address"><%=myuser.address %></textarea>
                                </div>
                            </li>
                            <li>
                                <label class="description" for="postcode">
                                    邮编
                                </label>
                                <div>
                                    <input id="postcode" name="postcode" value="<%=myuser.postcode %>" class="element text small" type="text" maxlength="20" />
                                </div>
                            </li>
                            <li>
                                <label class="description" for="phone">
                                    联系电话
                                </label>
                                <div>
                                    <input id="phone" name="phone" value="<%=myuser.phone %>" class="element text medium" type="text" maxlength="20" />
                                </div>
                                <p class="guidelines">
                                    请输入主要联系电话,固话主填写区号(如:020-81688168)</p>
                            </li>
                            <li>
                                <label class="description" for="mobile">
                                    手机号
                                </label>
                                <div>
                                    <input id="mobile" name="mobile" value="<%=myuser.mobile %>" class="element text medium" type="text" maxlength="20" />
                                </div>
                            </li>
                            <li class="buttons">
                                <input class="button" type="submit" value=" 提 交 " />
                            </li>
                        </ul>
                    </form>
                </td>
            </tr>
        </table>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
