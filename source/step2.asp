﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>结算中心 </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
if conOrderForUser=1 then
	' 验证用户
	call CheckUserLogin("")
end if

dim region
set region=new regionobj

dim myshopcart
set myshopcart=new shoppingcartsObj
myshopcart.checkcartid "",""

set delivery=new deliveryObj
delivery.getInfoByCookies("Cart")

dim id,action,rurl
id=getquerystring("id",0)
action=getquerystring("action","")
rurl=getquerystring("rurl","step3.asp")

%>
<%
ErrorMsg=""

delivery.deliveryObj(id)
    
if delivery.firstname="" then
    delivery.firstname="姓"
    delivery.lastname="名"
end if
if action="del" and id<>"0" then
    conn.execute "delete from delivery where userid=" & myuser.id & " and  id =" & id
    call topage("step2.asp?rurl=" & rurl)
end if

if action="go" and not delivery.isnull() then
    delivery.Save2Cookies("Cart")
    call topage(rurl)
end if

if action="save" then
    delivery.userid=myuser.id
    delivery.firstname=getform("firstname","")
    delivery.lastname=getform("lastname","")
    delivery.city=getform("city","")
    delivery.state=getform("state","")
    delivery.country=getform("country","44")
    delivery.address=getform("address","")
    delivery.postcode=getform("postcode","")
    delivery.phone=getform("phone","")
    delivery.mobile=getform("mobile","")
    delivery.email=getform("email","")    
    delivery.Save2Cookies("Cart") '  保存到购物车Cookies
    
    if not myuser.isnull then
        if delivery.isnull then
            delivery.Add()
        else
            delivery.Update()
        end if
    end if
    call topage(rurl)
end if
%>

<script type="text/javascript">
    function HandleAddress(ID, action) {
        location.href = "?id=" + ID + "&action=" + action + "&rurl=<%=rurl %>";
    }   
</script>

<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <div style="font-size: 14px; font-weight: bolder; margin-bottom: 20px;width:100%;">
            订购流程:<span>1.我的购物车</span> => <span style="color: Red;">2.填写订购信息</span> => <span>3.确认订购信息</span>
            => <span>4.完成订购</span>
        </div>
        <!--<div style="padding: 6px 10px 6px; width: 100%; text-align:left;">
            <input type="button" class="button" value="<< 上一步" onclick="window.location='step1.asp';" /></div>-->
        <% set rs=delivery.load("","where a.userid=" & myuser.id,"order by a.id") %>
        <% if not rs.eof then %>
        <div style=" margin-bottom:6px; background:#7f0019; padding:6px; color:White;">我的地址簿</div>
        <div style="border: solid 1px #ddd; margin-bottom: 10px; height: 20px; width: :100%;
            padding: 12px;">
            <% for i=1 to rs.RecordCount %>
            <div style="width: 48%; float: left; line-height: 120%;">
                <input type="button" class="button" value="配送到此地址" style="padding: 3px 4px 2px;"
                    onclick="HandleAddress('<%=rs.fields("id") %>','go')" /><br />
                <br />
                <%=rs.fields("firstname") %>
                <%=rs.fields("lastname") %>
                <br />
                <% region.regionObj(rs.fields("country")):response.Write region.name  %>
                <br />
                <% if conOpenSelectRegion=1 then %>
                <% region.regionObj(rs.fields("state")):response.Write region.name  %>&nbsp;
                <% region.regionObj(rs.fields("city")):response.Write region.name  %>&nbsp;
                <% else %>
                <%=rs.fields("state") %>
                <%=rs.fields("city") %>
                <% end if %><br />
                <%=rs.fields("address") %><br />
                <%=rs.fields("postcode") %><br />
                <%=rs.fields("email") %><br />
                <%=rs.fields("phone") %><br />
                <%=rs.fields("mobile") %><br />
                <a href="javascript:HandleAddress('<%=rs.fields("id") %>','');" class="b">修改</a>&nbsp;&nbsp;<a
                    href="javascript:if(confirm('确定要删除吗?'))HandleAddress('<%=rs.fields("id") %>','del');"
                    class="b">删除</a>
            </div>
            <%  rs.movenext %>
            <% next %>
        </div>
        <% end if %>
        <% rs.close:set rs=nothing %>
        <div style=" margin-bottom:6px; background:#7f0019; padding:6px; color:White;">请输入新的配送地址</div>
        <script>
            function checkForm(form) {
                if (!$.Validator["require"].test(form.firstname.value)) {
	                alert("请输入收货人姓名！");
	                form.firstname.select();
	                return false;
                }
                if (!$.Validator["chinese"].test(form.firstname.value)) {
	                alert("收货人姓名必须是中文！");
	                form.firstname.select();
	                return false;
	            }
	            if (!$.Validator["require"].test(form.city.value)) {
	                alert("请选择城市！");
	                form.city.focus();
	                return false;
	            }
                if (!$.Validator["require"].test(form.address.value)) {
	                alert("请输入详细地址！");
	                form.address.select();
	                return false;
                }
                if (!$.Validator["require"].test(form.postcode.value)) {
	                alert("请输入邮政编码！");
	                form.postcode.select();
	                return false;
                }
                if (!$.Validator["email"].test(form.email.value)) {
                    alert("请输入电子邮件！");
	                form.email.select();
	                return false;
	            }
	        }
                
                </script>
        <form name="webForm" id="webForm" class="appnitro" action="?action=save&id=<%=delivery.id %>&rurl=<%=rurl %>"
            method="post" onSubmit="return checkForm(this);">
            <ul>
                <% if ErrorMsg<>"" then  %><li class="imsg">
                    <%=ErrorMsg %>
                </li>
                <% end if %>
                
                <li>
                    <label class="description" for="firstname">
                        收货人姓名 <font style="color:Red;">*</font>
                    </label>
                    <div>
                        <input id="firstname" name="firstname" value="<%=delivery.firstname %>" onFocus="this.select();"
                            class="element text small" type="text" maxlength="100" />
                        <input id="lastname" name="lastname" value="<%=delivery.lastname %>" onFocus="this.select();"
                            class="element text small" type="text" maxlength="100" />
                         <p class="guidelines">
                        收货人姓名必须是中文</p>
                    </div>
                </li>
                <!--<li>
                                <label class="description" for="country">
                                    Country
                                </label>
                                <div>
                                    <select id="country" name="country" class="element select small">
                        <option value="0">-- Select --</option>
                        </select>
                        <script type="text/javascript">
                                        InitArea(areaArray, webForm.country, 0);
                                        $.setElementValue(webForm.country, "<%=delivery.country %>", "0");
                                        </script>
                                </div>
                            </li>-->
                <li>
                    <label class="description" for="city">
                        所在地区(State&City) <font style="color:Red;">*</font>
                    </label>
                    <div>
                        <% if conOpenSelectRegion=1 then %>
                        <select id="state" name="state" class="element select small" onChange="InitArea(areaArray,webForm.city,this.value);">
                            <option value="0">-- 请选择省份 --</option>
                        </select>
                        <select id="city" name="city" class="element select small">
                            <option value="0">-- 请选择城市 --</option>
                        </select>

                        <script type="text/javascript">
                            InitArea(areaArray, webForm.state, 44);
                            $.setElementValue(webForm.state, "<%=delivery.state %>", "0");
                            $.setElementValue(webForm.city, "<%=delivery.city %>", "0");
                        </script>

                        <% else %>
                        <input id="state" name="state" value="<%=delivery.state %>" onFocus="this.select();"
                            class="element text small" type="text" maxlength="100" />
                        <input id="city" name="city" value="<%=delivery.city %>" onFocus="this.select();"
                            class="element text small" type="text" maxlength="100" />
                        <% end if %>
                    </div>
                </li>
                <li>
                    <label class="description" for="address">
                        地址(Address) <font style="color:Red;">*</font>
                    </label>
                    <div>
                        <textarea class="element textarea small" id="address" name="address"><%=delivery.address %></textarea>
                    </div>
                </li>
                <li>
                    <label class="description" for="postcode">
                        邮政编码(PostCode)
                    </label>
                    <div>
                        <input id="postcode" name="postcode" class="element text small" value="<%=delivery.postcode %>"  type="text" maxlength="20" />
                    </div>
                </li>
                <li>
                    <label class="description" for="phone">
                        联系电话(Tel/Mobile) <font style="color:Red;">*</font>
                    </label>
                    <div>
                        <input id="phone" name="phone" class="element text medium" value="<%=delivery.phone %>"  type="text" maxlength="100" />
                    </div>
                </li>
                <li>
                    <label class="description" for="email">
                        电子邮箱(E-mail) <font style="color:Red;">*</font>
                    </label>
                    <div>
                        <input id="email" name="email" class="element text medium" value="<%=delivery.email %>"  type="text" maxlength="100" />
                        <p class="guidelines">
                        请输入常用的电子邮箱地址</p>
                    </div>
                </li>
                <!--<li>
                    <label class="description" for="mobile">
                        手机(Mobile)
                    </label>
                    <div>
                        <input id="mobile" name="mobile" class="element text medium" value="<%=delivery.mobile %>"  type="text" maxlength="100" />
                    </div>
                    
                </li>-->
                <li class="buttons">
                    <input class="button" type="submit" value="使用此地址" />
                </li>
            </ul>
        </form>
        
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
