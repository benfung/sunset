﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/MailObject.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>操作完成,下单成功 </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
if conOrderForUser=1 then
	' 验证用户
	call CheckUserLogin("")
end if
%>

<%
dim orderform,ordernum,email
ordernum=turnsql(getquerystring("ordernum","0"))
email=getquerystring("email","")
if not Validator("email",email) or ordernum="" then
	call alert("操作错误,回到购物车!","step1.asp")
end if
set orderform=new ordersObj
orderform.orderformObjByNE ordernum,email
%>

<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <div style="font-size: 14px; font-weight: bolder; margin-bottom: 20px;width:100%;">
            订购流程:<span>1.我的购物车</span> => <span >2.填写订购信息</span> => <span >3.确认订购信息</span>
            => <span style="color: Red;">4.完成订购</span>
        </div>
            <div style="width:90%; text-align:center;">
            <div style="color: #6b0303; width:100%; text-align: center; font-size: 160%; font-weight: bolder;
                margin-bottom: 10px;">
                恭喜您，订单提交成功了！</div>
            <div style="text-align: center;
                margin-bottom: 10px; line-height:200%;">
                订单号：<%=orderform.ordernum %><br />
                支付金额：<%=conCurrencyUnit %><%=FormatNumber(orderform.moneytotal) %><br />
                <span style="text-align:center;color: #6b0303; ">在确认支付成功后，可为您发货。</span>
            </div>
            <form action="pay/default.asp" method="post" name="webForm">
            <input type="hidden" name="orderid" value="<%=orderform.id %>" />
            <div style="text-align: center; margin-bottom: 120px;">
                <input type="submit" class="button" value="立即支付" /></div>
            </form>
            <div style=" line-height:180%;">
                您可以在“<a href="checkorder.asp" class="b" target="_blank">订单查询</a>”中输入您的邮箱及订单号进行订单状态查询,如果您是会员则可以在“<a href="u_orders.asp" class="b">我的帐户</a>”中查看或取消订单。<br />
                成功订购但未支付的订单将保留24小时，你可以在24小时内完成支付。
            </div>
            <hr style="width:100%;border: dashed 1px #ddd;" size="0" />
            <div style="width:100%; line-height:180%;">
                感谢您对我们的信任，如在购物过程中有意见或建议请及时告诉我们。<a href="feedback.asp" class="b" target="_blank">提意见</a>
            </div>
            </div>
        
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
