﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
      -->
    </script>
</head>
<%
dim AppPath
AppPath=Server.MapPath("../")
%>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                空间占用情况
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">基本属性</td>
                        <td class="formTabMsg">&nbsp;</td>
                    </tr>
                </table>
                <div class="div1" id="Tab1_Box">
                    <div class="div2">
                        <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                            <tr>
                                <td class="td01">占用总计：</td>
                                <td class="td02b">
                                   <%=FormatSize(CountSizeByPath(AppPath))%>
                                </td>
                            </tr> 
                            <%                                
                            dim fso,subFolders,subsize,totalsize
    set fso=Server.CreateObject("Scripting.FileSystemObject")
    set subFolders=fso.getfolder(AppPath).SubFolders
    totalsize=CountSizeByPath(AppPath)
    For Each f in subFolders
     subsize=CountSizeByPath(Server.MapPath("../"&f.name))
                            %>
                            <tr>
                                <td class="td01"><%=f.name %>：</td>
                                <td class="td02b">
                                   <span style="width:<%=subsize/totalsize*200 %>px; background:green;"></span>&nbsp;<%=FormatSize(subsize)%>
                                </td>
                            </tr>  
                            <%
                            next
                            set fso=nothing
                            %>   
                        </table>
                    </div>
                </div>                
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

<%
 	
 	' 计算目录点用空间(包括子目录)
 	function CountSizeByPath(path)
 	    dim fso,Result
 	    Result=0
 	    set fso=Server.CreateObject("Scripting.FileSystemObject")
 	    Result=fso.getfolder(path).Size 	    
 	    set fso=nothing
 	    CountSizeByPath=Result
 	end function
 	
 	
 	' 格式化数值
 	function FormatSize(si)
 	    dim Result
 	    Result=si & "&nbsp;Byte"
 	    if si>1024 then
 		   si=(si/1024)
 		   Result=formatnumber(si,2) & "&nbsp;KB"
 		end if
 		if si>1024 then
 		   si=(si/1024)
 		   Result=formatnumber(si,2) & "&nbsp;MB"
 		end if
 		if si>1024 then
 		   si=(si/1024)
 		   Result=formatnumber(si,2) & "&nbsp;GB"
 		end if
 		FormatSize=Result
 	end function
%>