﻿<meta   name="Robots"   content="none" />
<script language="javascript" src="../content/js/jquery.js" type="text/javascript" ></script>
<script language="javascript" src="../content/js/jquery.myext.js" type="text/javascript"></script>
<script language="javascript" src="../content/js/swfobject.js" type="text/javascript"></script>
<script language="javascript" src="../content/js/popcalendar.js" type="text/javascript"></script>
<script language="javascript" src="../content/js/swfupload.js" type="text/javascript"></script>

<script language="javascript">
    //var swfu;
    // 初始上传控件
    // 例:UploadInit("spanButtonPlaceHolder", $.FileType["Image"], "图片文件", 0,500, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete)
    // 参数:
    // bid: 安钮所在标签ID
    // t: 文件类型,多个用分号(;)隔开,如(*.jpg)
    // td: 说明,如(图片,All Files)
    // l: 限制数量,0为不限
    // si:文件大小,单位(KB),0表示文件大小无限制,可用的单位有B,KB,MB,GB
    // h_1: 添加队列后处理函数,如(fileQueued)
    // h_2: 队列错误
    // h_3: 对话框关闭时
    // h_4: 上传开始时
    // h_5: 上传时，一般用于进度条
    // h_6: 上传错误时
    // h_7: 上传成功时
    // h_8: 上传完成时
    // isthumb: 是否生成缩略图(true or false)
    /////////////////////////////////////////////////////////////////////
    function UploadInit(bid, t, td, l, si, h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, ipID, isthumb,uproID) {
        var settings = {
            flash_url: "../content/plugIn/swfupload.swf",
            upload_url: "../up.asp?u=u&isthumb=" + isthumb,
            file_post_name:"file",
            post_params: { "u": "u" },
            file_size_limit: si,
            file_types: t,
            file_types_description: td,
            file_upload_limit: l,
            file_queue_limit: l,
            custom_settings: {
                InputObject: $("#" + ipID)[0],
                progressTarget: $("#" + uproID)[0],
                cancelButtonId: "btnCancel"
            },
            //debug: true,

            // Button settings
            button_image_url: "images/BtnUpload.png",
            button_width: 61,
            button_height: 22,
            button_placeholder_id: bid,

            // The event handler functions are defined in handlers.js
            file_queued_handler: h_1,
            file_queue_error_handler: h_2,
            file_dialog_complete_handler: h_3,
            upload_start_handler: h_4,
            upload_progress_handler: h_5,
            upload_error_handler: h_6,
            upload_success_handler: h_7,
            upload_complete_handler: h_8
        };
        return new SWFUpload(settings);
    }

    function fileQueued(file) {
        //this.startUpload();
    }
    function fileQueueError(file, errorCode, message) {
        alert(message);
    }
    function fileDialogComplete() {
        if (this.getStats().files_queued > 0) {
            this.startUpload();
        }
        //alert("fileDialogComplete");

    }
    function uploadStart() {
        // 显款进度条
        if (this.customSettings.progressTarget != null) {
            this.customSettings.progressTarget.style.display = "block";
            this.customSettings.progressTarget.childNodes[0].innerHTML = "";
        }
    }
    function uploadProgress(file, bytesLoaded, bytesTotal) {
        // 进度条
        if (this.customSettings.progressTarget != null) {
            this.customSettings.progressTarget.childNodes[0].innerHTML = "<b>" + file.name + "</b>  <span style=\"color:Green;\">" + Math.ceil((bytesLoaded / bytesTotal) * 100) +"%</span>";
        }
    }
    function uploadError(file, errorCode, message) {
        // 关闭进度条
        if (this.customSettings.progressTarget != null) this.customSettings.progressTarget.style.display = "none";
        alert(message);
    }
    function uploadSuccess(file, server) {
        var statuscode = server.trim().split("&")[0];
        var msg = server.trim().split("&")[1];
        if (statuscode == 0) {
            this.customSettings.InputObject.value = msg;
            this.customSettings.InputObject.fireEvent("onchange");
        }
        else {
            alert(file.name + "\r\n\r\n" + msg);
        }
    }
    function uploadComplete(file) {
        // 关闭进度条
        if (this.customSettings.progressTarget != null) this.customSettings.progressTarget.style.display = "none";
        if (this.getStats().files_queued > 0) {
            this.startUpload();
        }
    }

    function DelFile(file, inputObject) {
        if (confirm("确定要删除?")) {
            $.ajax({
                type: "POST",
                url: "ajax/delfile.asp?",
                data: "d=d&f=../" + file,
                success: function(msg) {
                    alert(msg);
                    if (msg == "删除成功" && inputObject != null) inputObject.value = "";
                }
            });
            return true;
        } else {
            return false;
        }
    }
    
</script>

<script type="text/javascript">
    var cObj;
    // 对象ID构成规则为:表名_字段名_类型_ID值(如:news_sort_int_12)
    //                                       (如:news_title_str_12)
    function bgUpdate(obj) {
        var objid = obj.id;
        var table = objid.split("_")[0];
        var field = objid.split("_")[1];
        var ty = objid.split("_")[2];
        var id = objid.split("_")[3];
        obj.style.color = "red";
        cObj = obj;
        $.ajax({
            type: "POST",
            url: "ajax/update.asp?",
            data: "action=save&id=" + id + "&table=" + table + "&field=" + field + "&ty=" + ty + "&value=" + obj.value,
            success: function(msg) { if (msg > 0) { cObj.style.color = "Green"; } cObj = null; }
        });
    }
        
        
    </script>