﻿<%@  language="VBScript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>管理系统</title>    
    <!--#include file="headmeta.asp"-->
    <style type="text/css">
        .back_020web { background-image: url('image/titlebg.gif'); color: #000; }
        .table_020web { background-color: #A4B6D7; }
        .td_020web { background-color: #F2F8FF; }
        .tr_020web { background-color: #ECF5FF; }
        .t1 { font: 12px '宋体'; color: #000; }
        .t2 { font: 12px '宋体'; color: #fff; }
        .t3 { font: 12px '宋体'; color: #ff0; }
        .t4 { font: 12px '宋体'; color: #800000; }
        .t5 { font: 12px '宋体'; color: #191970; }
        .weiqun:hover { font-unline: yes; font-family: '宋体'; color: #fff; text-decoration: underline; background-color: #ccc; }
        td { font-size: 12px; }
        a:link,a:visited { color: #000000; text-decoration: none; }
        a:hover { color: #000000; text-decoration: underline; }
        a.b:link,a.b:visited { color: blue; text-decoration: none; }
        a.b:hover { color: blue; text-decoration: underline; }
    </style>
</head>
<body>
    <br>
    <table cellpadding="2" cellspacing="1" border="0" width="95%" align="center" class="a2">
        <tr>
            <td class="a1" height="25" align="center">
                <strong>网站管理系统</strong></td>
        </tr>
        <tr class="a4">
            <td align="center">
                <div align="center">
                    <table width="95%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <br>
                                <strong>网站管理系统</strong><br>
                                升级功能：1、加了背景音乐播放器管理系统 2、加了QQ客服管理系统 3，修改模版风格管理系统 4，加了右键菜单，访问统计 后台主要功能如下：<br>
                                一、系统管理：管理员管理，可以新增管理员及修改管理员密码；数据库备份，为保证您的数据安全本系统采用了数据库备份功能；上传文件管理，管理你增加产品时上传的图片及其他文件。
                                <br>
                                二、企业信息：可设置修改企业的各类信息及介绍。
                                <br>
                                三、产品管理：产品类别新增修改管理，产品添加修改以及产品的审核。<br>
                                四、订单管理：查看订单的详细信息及订单处理。
                                <br>
                                五、下载中心：可分类增加各种文件，如驱动和技术文档等文件的下载。<br>
                                六、会员管理：查看修改删除会员资料，及锁定解锁功能。可在线给会员发信！
                                <br>
                                七、新闻管理：能分大类和小类新闻，不再受新闻栏目的限制。
                                <br>
                                八、留言管理：管理信息反馈及注册会员的留言，注册会员的留言可在线回复，未注册会员可使用在线发信功能给于答复。
                                <br>
                                九、荣誉管理：新增修改企业荣誉栏目的信息。新增修改企业形象栏目的信息。
                                <br>
                                十、人才管理：发布修改招聘信息，人才策略栏目管理，应聘管理。
                                <br>
                                十一、营销网络：修改营销网络栏目的信息。
                                <br>
                                十二、调查管理：发布修改新调查。
                                <br>
                                十三、邮件列表：在线发送邮件。
                                <br>
                                十四、友情链接：新增修改友情链接,支持图片友情链接。<br>
                                十五、模板管理：可以自己选定现存的模板也可以导入第三方的模板。
                                <br>
                                <br>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table cellpadding="2" cellspacing="1" border="0" width="95%" align="center" class="a2">
        <tr>
            <td class="a1" height="25" align="center">
                <strong>UBB 标 签</strong></td>
        </tr>
        <tr class="a4">
            <td align="center">
                <div align="center">
                    <br>
                    <table cellspacing="0" cellpadding="0" width="100%" align="center" bgcolor="#fffff7"
                        border="0">
                        <tr>
                            <td width="100%">
                                <table style="border-collapse: collapse" cellspacing="1" cellpadding="2" width="96%"
                                    align="center" bgcolor="#999999" border="0">
                                    <tr bgcolor="#6699cc">
                                        <td align="middle" colspan="2" height="22">
                                            <div align="center">
                                                <font color="#ffffff">网站管理系统支持简单的UBB代码：UBB是一种比HTML更安全的网页代码。你可以参照下面的格式来使用它！</font></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="498" bgcolor="#ffffff">
                                            <ul>
                                                <font color="#000000"><font color="#990000">[quote]</font>这个标签是用来做为引用所设置的，如果你有什么内容是引用自别的地方，请加上这个标签！<font
                                                    color="#990000">[/quote]</font> </font>
                                            </ul>
                                        </td>
                                        <td width="244" bgcolor="#ffffff">
                                            <font color="#000000">
                                                <hr color="#990000" noshade size="1">
                                                <blockquote>
                                                    这个标签是用来做为引用所设置的，如果你有什么内容是引用自别的地方，请加上这个标签！</blockquote>
                                                <hr color="#990000" noshade size="1">
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="498" bgcolor="#ffffff">
                                            <ul>
                                                <font color="#000000"><font color="#990000">[code]</font>
                                                    <br>
                                                    unless ( eq "") {
                                                    <br>
                                                    print "错误的管理密码";
                                                    <br>
                                                    &amp;unlock;
                                                    <br>
                                                    exit;
                                                    <br>
                                                    }<br>
                                                    <font color="#990000">[/code]</font> </font>
                                            </ul>
                                        </td>
                                        <td width="244" bgcolor="#ffffff">
                                            <blockquote>
                                                <font color="#000000">代码：
                                                    <hr color="#990000" noshade size="1">
                                                    unless ( eq "") {
                                                    <br>
                                                    print "错误的管理密码";
                                                    <br>
                                                    &amp;unlock;
                                                    <br>
                                                    exit;
                                                    <br>
                                                    }
                                                    <hr color="#990000" noshade size="1">
                                                </font>
                                            </blockquote>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff">
                                            <font color="#000000"><font color="#990000">[url]</font><a href="http://www.abc.com/">http://www.abc.com</a><font
                                                color="#990000">[/url]</font> </font>
                                        </td>
                                        <td align="middle" width="244" bgcolor="#ffffff">
                                            <font color="#000000"><a href="http://www.abc.com/" target="_blank">http://www.abc.com</a></font></td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff">
                                            <font color="#000000"><font color="#990000">[url=</font>http://www.abc.com/<font
                                                color="#990000">]有限公司[/url]</font> </font>
                                        </td>
                                        <td align="middle" width="244" bgcolor="#ffffff">
                                            <font color="#000000"><font color="#990000"><a href="http://www.abc.com" target="_blank">
                                            </a>有限公司</font></font></td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff">
                                            <font color="#000000"><font color="#990000">[email=tech@abc.com]</font>写信给我<font
                                                color="#990000">[/email]</font> </font>
                                        </td>
                                        <td align="middle" width="244" bgcolor="#ffffff">
                                            <font color="#000000"><a href="mailto:tech@abc.com">写信给我</a></font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff">
                                            <font color="#000000"><font color="#990000">[email]</font><font color="#000000"><a
                                                href="mailto:tech@abc.com">tech</a></font><font color="#990000">@abc.com[/email]</font>
                                            </font>
                                        </td>
                                        <td align="middle" width="244" bgcolor="#ffffff">
                                            <font color="#000000"><a href="mailto:tech@abc.com">tech@</a></font><a href="mailto:tech@abc.com"><font
                                                color="#0099ff">abc.com</font></a></td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff">
                                            <font color="#000000"><font color="#990000">[b]</font>文字加粗体效果<font color="#990000">[/b]</font>
                                            </font>
                                        </td>
                                        <td align="middle" width="244" bgcolor="#ffffff">
                                            <font color="#000000"><b>文字加粗体效果</b></font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff">
                                            <font color="#000000"><font color="#990000">[i]</font>文字加倾斜效果<font color="#990000">[/i]</font>
                                            </font>
                                        </td>
                                        <td align="middle" width="244" bgcolor="#ffffff">
                                            <font color="#000000"><i>文字加倾斜效果</i></font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff">
                                            <font color="#000000"><font color="#990000">[u]</font>文字加下划线效果<font color="#990000">[/u]</font>
                                            </font>
                                        </td>
                                        <td align="middle" width="244" bgcolor="#ffffff">
                                            <font color="#000000"><u>文字加下划线效果</u></font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff">
                                            <font color="#000000"><font color="#990000">[size=4]</font>改变文字大小<font color="#990000">[/size]</font>
                                            </font>
                                        </td>
                                        <td align="middle" width="244" bgcolor="#ffffff">
                                            <font color="#000000"><font size="4">改变文字大小</font> </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff" height="12">
                                            <font color="#000000"><font color="#990000">[color=red]</font>改变文字颜色<font color="#990000">[/color]</font>
                                            </font>
                                        </td>
                                        <td width="244" bgcolor="#ffffff" height="12">
                                            <font color="#000000"><font color="red">改变文字颜色</font></font></td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff" height="12">
                                            [move]滚动文字特效[/move]</td>
                                        <td width="244" bgcolor="#ffffff" height="12">
                                            <marquee scrollamount="3">
                                                滚动文字特效</marquee>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff" height="12">
                                            [fly]飞行文字特效[/fly]</td>
                                        <td width="244" bgcolor="#ffffff" height="12">
                                            <marquee scrollamount="3" behavior="alternate" width="90%">
                                                飞行文字特效</marquee>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="middle" width="498" bgcolor="#ffffff" height="12">
                                            <font color="#000000"><font color="#990000">[img]</font>http://www.xxx.com/xxx.gif<font
                                                color="#990000">[/img]</font> </font>
                                        </td>
                                        <td width="244" bgcolor="#ffffff" height="12">
                                            贴图
                                        </td>
                                    </tr>
                                    </TBODY></table>
                            </td>
                        </tr>
                        <tr bgcolor="#fffff7">
                            <td width="100%" bgcolor="#fffff7">
                                <div align="center">
                                    <br>
                                    以上列表中的颜色可以是ＲＧＢ值，也可以是英文代码，如可以如下表示：<font color="#ff0000">[color=red]</font>彩色文字<font
                                        color="#ff0000"> [/color]</font><br>
                                    以上的"red"可以是：<br>
                                </div>
                                <div align="center">
                                    <table cellspacing="0" bordercolordark="#ffffff" cellpadding="0" width="96%" bordercolorlight="#000000"
                                        border="1">
                                        <tbody>
                                            <tr>
                                                <td align="middle" width="31%" height="22">
                                                    Black</td>
                                                <td align="middle" width="35%" height="22">
                                                    #000000</td>
                                                <td align="middle" width="34%" height="22">
                                                    黑色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Silver</td>
                                                <td align="middle" width="35%">
                                                    #C0C0C0</td>
                                                <td align="middle" width="34%">
                                                    银色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Gray</td>
                                                <td align="middle" width="35%">
                                                    #808080</td>
                                                <td align="middle" width="34%">
                                                    灰色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    White</td>
                                                <td align="middle" width="35%">
                                                    #FFFFFF</td>
                                                <td align="middle" width="34%">
                                                    白色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Maroon</td>
                                                <td align="middle" width="35%">
                                                    #800000</td>
                                                <td align="middle" width="34%">
                                                    茶色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Red</td>
                                                <td align="middle" width="35%">
                                                    #FF0000</td>
                                                <td align="middle" width="34%">
                                                    红色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Purple</td>
                                                <td align="middle" width="35%">
                                                    #800080</td>
                                                <td align="middle" width="34%">
                                                    紫色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Fuchsia</td>
                                                <td align="middle" width="35%">
                                                    #FF00FF</td>
                                                <td align="middle" width="34%">
                                                    紫红</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Green</td>
                                                <td align="middle" width="35%">
                                                    #008000</td>
                                                <td align="middle" width="34%">
                                                    绿色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Lime</td>
                                                <td align="middle" width="35%">
                                                    #00FF00</td>
                                                <td align="middle" width="34%">
                                                    亮绿</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Olive</td>
                                                <td align="middle" width="35%">
                                                    #808000</td>
                                                <td align="middle" width="34%">
                                                    橄榄</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Yellow</td>
                                                <td align="middle" width="35%">
                                                    #FFFF00</td>
                                                <td align="middle" width="34%">
                                                    黄色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Navy</td>
                                                <td align="middle" width="35%">
                                                    #000080</td>
                                                <td align="middle" width="34%">
                                                    深蓝</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Blue</td>
                                                <td align="middle" width="35%">
                                                    #0000FF</td>
                                                <td align="middle" width="34%">
                                                    蓝色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Teal</td>
                                                <td align="middle" width="35%">
                                                    #008080</td>
                                                <td align="middle" width="34%">
                                                    青色</td>
                                            </tr>
                                            <tr>
                                                <td align="middle" width="31%">
                                                    Aqua</td>
                                                <td align="middle" width="35%">
                                                    #00FFFF</td>
                                                <td align="middle" width="34%">
                                                    浅绿</td>
                                            </tr>
                                    </table>
                                    <br>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <br>
</body>
</html>
