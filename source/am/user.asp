﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 

dim action,idset,idArray,where,username,status,lockuser,userlevel
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

userlevel=getquerystring("userlevel","")
status=getquerystring("status","")
lockuser=getquerystring("lockuser","")
title=getquerystring("title","")
filePath="?title=" & title & "&status=" & status & "&lockuser=" & lockuser & "&userlevel=" & userlevel & "&"

CurrentPage=Cint(getquerystring("pageno","1"))

set myuser=new myuserObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>

</head>
<%
' 批量操作
if action="del" then    
    conn.execute "delete from myuser where id in (" & idset & ")"       
end if

if action="pass" then    
    conn.execute "update myuser set status=1 where id in (" & idset & ")"
end if

if action="unpass" then    
    conn.execute "update myuser set status=0 where id in (" & idset & ")"
end if

if action="lock" then    
    conn.execute "update myuser set LockUser=1 where id in (" & idset & ")"
end if

if action="unlock" then    
    conn.execute "update myuser set LockUser=0 where id in (" & idset & ")"
end if


' 组织SQL
where="where a.id>0"

if title<>"" then
    where = where & " and (a.loginid+a.email like '%" & title & "%')"
end if

if status<>"" then
    where = where & " and a.status= " & status
end if

if lockuser<>"" then
    where = where & " and a.lockuser= " & lockuser
end if

if userlevel<>"" then
    where = where & " and a.userlevel= " & userlevel
end if


%>
<body>
    <div class="bodydiv">
        <div id="newitem" class="jbox" style="width: 500px;">
        <div class="closejbox"><img src="images/close.gif" alt="" align="absmiddle" onclick="$.RemoveJBox($('#newitem'));" /></div>
        <fieldset>
            <legend>高级搜索</legend>
            <form method="get" action="?" name="webForm" class="aform">
            <table cellpadding="6" cellspacing="2" class="table1">
                <tr>
                    <td class="td01">关键字: </td>
                    <td class="td02">
                        <input type="text" name="title" class="text" value="<%=title %>" style="width: 255px;" />
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="td02" style="text-align: center;">
                        <button type="submit" class="btn">
                            搜 索</button>&nbsp;&nbsp;<button type="button" class="btn" onclick="$.RemoveJBox($('#newitem'));">
                            关 闭</button></td>
                </tr>
            </table>
            </form>
        </fieldset>
    </div>
    
        <div class="bodybox">
            <div class="titlenav">
                注册会员管理
            </div>
            <div class="contentdiv" style="text-align:center; height:550px;">
                <div class="ListDiv"  style="width:98%;">
                    <div class="SetDiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <select name="action" onchange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>                                        
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="pass" >&nbsp;&nbsp;标记为已审</option>
                                        <option value="unpass" >&nbsp;&nbsp;标记为待审</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="lock" >&nbsp;&nbsp;标记为锁定</option>
                                        <option value="unlock" >&nbsp;&nbsp;标记为正常</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select>
                                    &nbsp;
                                    <button class="btn" onclick="$.ShowJBox($('#newitem'));">搜索。。。</button></td>
                            </tr>
                        </table>
                    </div>
                    <div >
                        <%       
                        set rs=myuser.load("",where,"order by a.id desc")
MaxItemSize=50
rs.PageSize=MaxItemSize
TotalNumber=rs.RecordCount
TotalPage=rs.PageCount                 
                        if rs.EOF then
                            Response.Write ("<div style='color:red;font-size:14px;margin:50px;'>没有匹配的记录!!</div>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage                        
                        %>
                        <table cellspacing="0" cellpadding="6" border="1" class="gv" style="border-collapse: collapse;">
                            <tr class="gvrowhead">
                                <th class="th" style="width: 20px;">
                                    <input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />
                                </th>
                                <th class="th" style="width: 40px;">编号 </th>
                                <th class="th">用户名 </th>
                                <th class="th">Email </th>
                                <th class="th" style="width: 120px;">注册时间 </th>
                                <th class="th" style="width: 100px;">登陆IP </th>
                                <th class="th" style="width: 80px;">属性 </th>
                                <th class="th" style="width: 100px;">操作 </th>
                            </tr>
                            <% for i=1 to CurrentRecord(rs,CurrentPage) %>
                            <tr class="gvrow">
                                <td class="tic">
                                    <input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" />
                                </td>
                                <td class="tic">
                                    <%=rs.fields("id") %>
                                </td>
                                <td class="til">
                                    <%=rs.fields("loginid") %>
                                </td>
                                <td class="til">
                                    <%=rs.fields("email") %>
                                </td>
                                <td class="tic">
                                    <%=shortdate(rs.fields("RegDate")) %>
                                </td>
                                <td class="tic">
                                    <%=rs.fields("LoginIP") %>
                                </td>
                                <td class="tic">
                                    <% if rs.fields("LockUser")=1 then %><span style="color:red;">锁定</span><% else %><span style="color:Green;">正常</span><%end if %>&nbsp;
                                    <% if rs.fields("status")=0 then %><span style="color:red;">待审</span><% else %><span style="color:Green;">已审</span><%end if %>
                                </td>
                                <td class="tic">
                                    <a href="user_edit.asp?id=<%=rs.fields("id") %>" title="编辑用户" class="b">
                                        编辑</a>
                                </td>
                            </tr>
                            <% rs.movenext
                               next
                            %>
                        </table>
                        <% end if %>
                        <% rs.close:set rs=nothing %>
                    </div>
                                        
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

