﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,id,fso,feedback
action=getquerystring("action","")
id=getquerystring("id",0)

set feedback=new feedbackObj
feedback.feedbackObj(id)

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
        }
      -->
    </script>

</head>
<%
if action="save" then
    feedback.replytime=now()
    feedback.reply=encode(getform("reply",""))
    feedback.update()
    call alert("操作成功","feedback.asp")
end if

%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                <a href="feedback.asp">留言管理</a>
            </div>
            <div class="contentdiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            回复</td>
                        <td class="formTabMsg">
                            带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=feedback.id %>" name="webForm" class="aform" method="post" onSubmit="return checkForm(this)">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:100px;">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">                                
                                <tr>
                                    <td class="td02">
                                        <textarea class="textarea" style="width:100%" rows="14" name="reply"><%=unencode(feedback.reply) %></textarea>
                                    </td>
                                </tr>                                
                            </table>
                        </div>                        
                    </div>
                    <div class="submit">
                        <button type="submit" class="btn">保存修改</button>&nbsp;&nbsp;<button type="button" onClick="window.location='feedback.asp'" class="btn">返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
