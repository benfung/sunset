﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="../inc/md5.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim region,classArray
set region=new regionobj
classArray=region.GetArray()

dim action,id,news
action=getquerystring("action","")
id=getquerystring("id",0)


set myuser=new myuserObj
myuser.myuserObj(id)


%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,3,"Tab","formTabA","formTabB","click",1);
        });

        <%=region.JsArray("areaArray") %>

        function initSelectbyPid(argClass, SelectObj, pid) {
            SelectObj.innerHTML = "";
            var opt = document.createElement("option");
            opt.value = "0";
            opt.text = "--请选择分类--";
            SelectObj.add(opt);
            for (var i = 0; i < argClass.length; i++) {
                if (argClass[i] == undefined) continue;
                if (argClass[i][1] == pid) {
                    var opt = document.createElement("option");
                    opt.value = argClass[i][0];
                    opt.text = argClass[i][3];
                    SelectObj.add(opt);
                }
            }
        }
        
        function checkForm(form){
        }
      -->
    </script>

</head>
<%
if action="save" then
    if getform("Password","")<>"" then
        myuser.pwd=md5(getform("Password",""),32)
    end if
    myuser.Email=getform("Email","")
    myuser.sex=getform("sex","")
    myuser.country=getform("country","")
    myuser.state=getform("state","")
    myuser.city=getform("city","")
    myuser.Address=getform("Address","")
    myuser.Postcode=getform("Postcode","")
    myuser.firstname=getform("firstname","")
    myuser.lastname=getform("lastname","")
    myuser.mobile=getform("mobile","")
    myuser.Phone=getform("Phone","")
    myuser.Homepage=getform("Homepage","")
    myuser.UserLevel=getform("UserLevel",0)
    myuser.LockUser=getform("LockUser",0)
    myuser.status=getform("status",0)
    myuser.Update()
    call alert("操作成功!!","?id=" & myuser.id )
end if
if action="del" then
    deliveryid=getquerystring("deliveryid",0)
    conn.execute "delete from delivery where id =" & deliveryid
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">会员管理
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">基本属性</td>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab2">修改密码</td>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab3">收货人信息</td>
                        <td class="formTabMsg">带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=myuser.id %>" name="webForm" class="aform" method="post" onSubmit="return checkForm(this)">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">用户名：</td>
                                    <td class="td02">
                                        <input type="text" name="UserName" class="text" readonly="readonly" value="<%=myuser.loginid %>" style="width: 250px;" />
                                        
                                        <input type="radio" name="sex" value="男" />
                                        男
                                        <input type="radio" name="sex" value="女" />
                                        女

                                        <script type="text/javascript">
                                        $.setElementValue(webForm.sex,"<%=myuser.sex %>");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">邮 箱：</td>
                                    <td class="td02">
                                        <input type="text" name="Email" value="<%=myuser.Email %>" class="text" style="width: 250px;" />
                                    </td>
                                </tr>                              
                                <tr>
                                    <td class="td01">真实姓名：</td>
                                    <td class="td02">
                                        <input type="text" name="firstname" value="<%=myuser.Firstname %>" class="text" style="width: 250px;" />
                                    </td>
                                </tr>
                                <!--<tr>
                                    <td class="td01">LastName：</td>
                                    <td class="td02">
                                        <input type="text" name="lastname" value="<%=myuser.lastname %>" class="text" style="width: 250px;" />
                                    </td>
                                </tr>-->
                                <% if conOpenSelectRegion=1 then %>
                                <tr>
                                    <td class="td01">地区：</td>
                                    <td class="td02">
                                        <select name="country" onChange="initSelectbyPid(areaArray,webForm.state,this.value);"> 
                                                                                       
                                        </select><br />
                                        <select name="state" onChange="initSelectbyPid(areaArray,webForm.city,this.value);">                                            
                                        </select><br />
                                        <select name="city">                                            
                                        </select>
                                        <script type="text/javascript">
                                            initSelectbyPid(areaArray, webForm.country, 0);
                                            $.setElementValue(webForm.country, "<%=myuser.country %>", "44");
                                            $.setElementValue(webForm.state, "<%=myuser.state %>", "0");
                                            $.setElementValue(webForm.city, "<%=myuser.city %>", "0");
                                        </script>                                       
                                    </td>
                                </tr>
                                <% else %>
                                <tr>
                                    <td class="td01">地区：</td>
                                    <td class="td02">
                                        国:<select name="country"></select><br />
                                        
                                        省:<input type="text" name="state" value="<%=myuser.state %>" class="text" style="width: 200px;" />
                                        
                                        市:<input type="text" name="city" value="<%=myuser.city %>" class="text" style="width: 200px;" />
                                        
                                        <script type="text/javascript">
                                            initSelectbyPid(areaArray, webForm.country, 0);
                                            $.setElementValue(webForm.country, "<%=myuser.country %>", "44");
                                        </script> 
                                        
                                    </td>
                                </tr>
                                <% end if %>
                                <tr>
                                    <td class="td01">地 址：</td>
                                    <td class="td02">
                                        <input type="text" name="Address" value="<%=myuser.Address %>" class="text" style="width: 300px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">邮 编：</td>
                                    <td class="td02">
                                        <input type="text" name="Postcode" value="<%=myuser.Postcode %>" class="text" style="width: 100px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">联系电话：</td>
                                    <td class="td02">
                                        <input type="text" name="Phone" value="<%=myuser.Phone %>" class="text" style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">手 机：</td>
                                    <td class="td02">
                                        <input type="text" name="mobile" value="<%=myuser.mobile %>" class="text" style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">传 真：</td>
                                    <td class="td02">
                                        <input type="text" name="Fax" value="<%=myuser.Fax %>" class="text" style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">主 页：</td>
                                    <td class="td02">
                                        <input type="text" name="Homepage" value="<%=myuser.Homepage %>" class="text" style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">登陆IP：</td>
                                    <td class="td02b">
                                        <%=myuser.LoginIP %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">登陆次数：</td>
                                    <td class="td02b">
                                        <%=myuser.logins %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">最后登陆时间：</td>
                                    <td class="td02b">
                                        <%=myuser.LastLoginTime %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">注册时间：</td>
                                    <td class="td02b">
                                        <%=myuser.RegDate %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">用户级别：</td>
                                    <td class="td02">
                                        <input type="Radio" name="UserLevel" value="0" />
                                        普通
                                        <input type="Radio" name="UserLevel" value="1" />
                                        高级
                                        <input type="Radio" name="UserLevel" value="2" />
                                        VIP

                                        <script type="text/javascript">
                                        $.setElementValue(webForm.UserLevel,"<%=myuser.UserLevel %>");
                                        </script>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">属性：</td>
                                    <td class="td02">
                                        <input type="checkbox" name="LockUser" value="1" />
                                        锁定

                                        <script type="text/javascript">
                                        $.setElementValue(webForm.LockUser,"<%=myuser.LockUser %>");
                                        </script>

                                        <input type="checkbox" name="status" value="1" />
                                        审核

                                        <script type="text/javascript">
                                        $.setElementValue(webForm.status,"<%=myuser.status %>");
                                        </script>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="div1" id="Tab2_Box">
                        <div class="div2">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">                                
                                <tr>
                                    <td class="td01">密 码：</td>
                                    <td class="td02">
                                        <input type="text" name="Password" class="text" style="width: 250px;" />
                                        <span>不修改密码请留空</span> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="div1" id="Tab3_Box">
                        <% set delivery=new deliveryObj %>
                        <% set rs=delivery.load("","where a.userid=" & id,"order by a.id") %>
                        <div class="div2" style="height: 300px;">
                            <% for i=1 to rs.RecordCount %>
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc" style="margin-bottom:10px;">
                                <tr>
                                    <td class="td01">收货人：</td>
                                    <td class="td02b" style="color:Green; font-weight:bolder;">
                                        <%=rs.fields("firstname") %> <%=rs.fields("lastname") %>
                                        
                                        <a href="javascript:if(confirm('确定要删除吗?'))window.location='user_edit.asp?id=<%=id %>&deliveryid=<%=rs.fields("id") %>&action=del'" class="b">删除</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">地 区：</td>
                                    <td class="td02b">
                                    <% if conOpenSelectRegion=1 then %>
                                        <% region.regionObj(rs.fields("country")):response.Write region.name  %>&nbsp; 
                                    <% region.regionObj(rs.fields("state")):response.Write region.name  %>&nbsp;
                                    <% region.regionObj(rs.fields("city")):response.Write region.name  %>&nbsp;  
                                    <% else %>
                                        <% region.regionObj(rs.fields("country")):response.Write region.name  %> <%=rs.fields("state") %> <%=rs.fields("city") %>
                                     <% end if %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">地 址：</td>
                                    <td class="td02b">
                                        <%=rs.fields("address") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">邮 编：</td>
                                    <td class="td02b">
                                        <%=rs.fields("postcode") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">电 话：</td>
                                    <td class="td02b">
                                        <%=rs.fields("phone") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">手 机：</td>
                                    <td class="td02b">
                                        <%=rs.fields("mobile") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">电子邮箱：</td>
                                    <td class="td02b">
                                        <%=rs.fields("email") %>
                                    </td>
                                </tr>
                            </table>                            
                            <% rs.movenext:next %>
                            
                        </div>
                        <% rs.close:set rs=nothing %>
                    </div>
                    <div class="submit">
                        <button type="submit"  class="btn" >保存修改</button>&nbsp;&nbsp;<button class="btn" onclick="window.location='user.asp';" >返 回</button></div>
                </form>
                
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
