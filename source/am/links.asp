﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,idset,idArray,where,links,classid,isgood,flag
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

classid=getquerystring("classid","")
isgood=getquerystring("isgood","")
flag=getquerystring("flag","")
filePath="?classid=" & classid & "&isgood=" & isgood & "&flag=" & flag & "&"

CurrentPage=Cint(getquerystring("pageno",1))

set links=new linksObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>

</head>
<%
' 批量操作
if action="del" then    
    for i=0 to ubound(idArray)
	    conn.execute "delete from links where id =" & idArray(i)
    next       
end if

if action="pass" then    
    conn.execute "update links set flag=1 where id in (" & idset & ")"
end if

if action="unpass" then    
    conn.execute "update links set flag=0 where id in (" & idset & ")"
end if

if action="commend" then    
    conn.execute "update links set isgood=1 where id in (" & idset & ")"
end if

if action="nocommend" then    
    conn.execute "update links set isgood=0 where id in (" & idset & ")"
end if


' 组织SQL
where="where a.id>0"
if classid<>"" then   where = where & " and a.classid=" & classid
if isgood<>"" then       where = where & " and a.isgood=" & isgood
if flag<>"" then        where = where & " and a.flag=" & flag

%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                高级搜索
            </div>
            <div class="contentdiv" style="text-align:center; height:550px;">
                <div class="searchdiv">
                        <table cellpadding="0" cellspacing="2">
                            <tr>                               
                                <td>
                                </td>
                            </tr>
                        </table>
                </div>
                <div class="listdiv"  style="width:98%;">
                    <div class="setdiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <select name="action" onchange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>                                        
                                        <option value="pass" >&nbsp;&nbsp;标记为已审</option>
                                        <option value="unpass" >&nbsp;&nbsp;标记为待审</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="commend" >&nbsp;&nbsp;推荐</option>
                                        <option value="nocommend" >&nbsp;&nbsp;撤消推荐</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div> 
                    <div >
                        <%                        
                        set rs=links.load("",where,"order by a.id")
                        MaxItemSize=50
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<span style='color:red;font-size:14px;'>未添加任何数据!!</span>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        end if
                        %>
                        <table cellspacing="0" cellpadding="6"  border="1" class="gv"  style="border-collapse: collapse;">
                            <tr class="gvrowhead">
                                <th class="th"  style="width: 20px;"><input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" /></th><th class="th"  style="width: 40px;">编号</th><th class="th"  style="width: 50px;">类别</th><th class="th" >标识名</th><th class="th"  style="width: 120px;">LOGO</th><th class="th"  style="width: 100px;">相关属性</th><th class="th"  style="width: 50px;">操作</th>
                            </tr>
                            <% for i=1 to CurrentRecord(rs,CurrentPage) %>
<tr  class="gvrow"><td class="tic"><input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" /></td><td class="tic"><%=rs.fields("id") %></td><td class="tic"><%=rs.fields("classname") %></td><td class="til"><a href="<%=rs.fields("siteurl") %>" target="_blank" class="b"><%=rs.fields("name") %> <% if rs.fields("subname")<>"" then %>( <%=rs.fields("subname") %> )<%end if %></a></td><td class="tic"><% if rs.fields("logo")<>"" then %><img src="<%=rs.fields("logo") %>" width="70" height="45" style="margin:2px 0px;" align="absmiddle" alt="" /><% end if %></td><td class="tic"><% if rs.fields("flag")=1 then %><span style="color:red;">审</span><% else %>&nbsp;&nbsp;<%end if %>&nbsp;<% if rs.fields("isgood")=1 then %><span style="color:green;">推</span><% else %>&nbsp;&nbsp;<%end if %>&nbsp;</td><td class="tic"><a href="Links_edit.asp?id=<%=rs.fields("id") %>">编辑</a></td></tr>
<% rs.movenext
                              Next
                            %>
                        </table>
                        <% rs.close:set rs=nothing %>
                    </div>
                                       
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

