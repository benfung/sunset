﻿<%@  language="VBScript" codepage="65001" %>
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/conn.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<%
Response.expires = 0
Response.expiresabsolute = Now() - 1
Response.addHeader "pragma", "no-cache"
Response.addHeader "cache-control", "private"
Response.CacheControl = "no-cache"
%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站配置</title>    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script type="text/JavaScript">
<!--
function MM_displayStatusMsg(msgStr) { //v1.0
  status=msgStr;
  document.MM_returnValue = true;
}
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,4,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
//          
        }
      -->
    </script>

</head>
<%
dim httphost
httphost="http://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("URL")
httphost=mid(httphost,1,InStrRev(httphost,"/")-1)
httphost=mid(httphost,1,InStrRev(httphost,"/"))


if getquerystring("action","")="save" then
    if IsObjInstalled("Adodb.Stream") then
        dim str
        str = str & "<" & "%" & vbcrlf
        str = str & " '//// 网站信息配置 ////////////////////////////////" & vbcrlf
        str = str & "PageStartTime=timer()" & vbcrlf
        str = str & "const conSiteTitle=" & chr(34) & getform("SiteTitle","网站标题") & chr(34) & "        '网站标题" & vbcrlf
        str = str & "const conSiteName=" & chr(34) & getform("SiteName","你的公司名") & chr(34) & "        '网站名称或公司名" & vbcrlf
        str = str & "const conSiteDomain=" & chr(34) & getform("SiteDomain","你的网站地址") & chr(34) & "        '网站域名" & vbcrlf
        str = str & "const conSiteUrl=" & chr(34) & httphost & chr(34) & "        '网站访问地址" & vbcrlf        
        str = str & "const conLogoUrl=" & chr(34) & getform("LogoUrl","") & chr(34) & "        'LOGO地址" & vbcrlf
        str = str & "const conSeoTitle=" & chr(34) & getform("SeoTitle","") & chr(34) & "        'SEO标题附加字" & vbcrlf        
        str = str & "const conSiteKeyword=" & chr(34) & getform("SiteKeyword","") & chr(34) & "        '网站关键字" & vbcrlf
        str = str & "const conSiteDescription=" & chr(34) & getform("SiteDescription","") & chr(34) & "        '网站描述meta" & vbcrlf
        str = str & "const conSiteBeiAn=" & chr(34) & getform("SiteBeiAn","粤ICP00001") & chr(34) & "        '网站备案号" & vbcrlf
        str = str & "const conEnterpriseMail=" & chr(34) & getform("EnterpriseMail","") & chr(34) & "        '企业邮局入口" & vbcrlf
        str = str & "const conWebmasterName=" & chr(34) & getform("WebmasterName","网站联系人") & chr(34) & "        '网站联系人" & vbcrlf
        str = str & "const conWebmasterAddress=" & chr(34) & getform("WebmasterAddress","广州市") & chr(34) & "        '联系地址" & vbcrlf
        str = str & "const conWebmasterPostCode=" & chr(34) & getform("WebmasterPostCode","000000") & chr(34) & "        '邮政编码" & vbcrlf        
        str = str & "const conWebmasterEmail=" & chr(34) & getform("WebmasterEmail","kofbqfeng@gmail.com") & chr(34) & "        '联系邮箱" & vbcrlf
        str = str & "const conWebmasterTel=" & chr(34) & getform("WebmasterTel","") & chr(34) & "        '联系电话" & vbcrlf
        str = str & "const conWebmasterFax=" & chr(34) & getform("WebmasterFax","") & chr(34) & "        '联系传真" & vbcrlf
        str = str & "const conWebmasterMobile=" & chr(34) & getform("WebmasterMobile","") & chr(34) & "        '联系手机" & vbcrlf
               
        str = str & " '//// 邮件服务器 ////////////////////////////////" & vbcrlf
        str = str & "const conMailServer=" & chr(34) & getform("MailServer","") & chr(34) & "        'SMTP服务器" & vbcrlf
        str = str & "const conMailServerUserName=" & chr(34) & getform("MailServerUserName","") & chr(34) & "        'SMTP登录用户名" & vbcrlf
        str = str & "const conMailServerPassWord=" & chr(34) & getform("MailServerPassWord","") & chr(34) & "        'SMTP登录密码" & vbcrlf
        str = str & "const conMailDomain=" & chr(34) & getform("MailDomain","") & chr(34) & "        'SMTP域名" & vbcrlf
        
        str = str & " '//// 网站选项配置 ////////////////////////////////" & vbcrlf
        str = str & "const conMaxFileSize="  & getform("MaxFileSize",1024)  & "        '上传文件大小限制" & vbcrlf
        str = str & "const conAllowFileType=" & chr(34) & getform("AllowFileType","gif|jpg|bmp|png|flv|swf|rar|doc|pdf") & chr(34) & "        '允许的上传文件类型" & vbcrlf
        str = str & "const conUpFilePath=" & chr(34) & getform("UpFilePath","f/") & chr(34)  & "        '上传文件目录" & vbcrlf
		str = str & "const conUpFileNameMode=" & getform("UpFileNameMode",0) & "        '文件命名模式" & vbcrlf
        str = str & "const conLoginTimeOut=" & getform("LoginTimeOut",20) & "        '用户/管理员登陆时限" & vbcrlf        
        str = str & "const conCheckLoginLog=" & getform("CheckLoginLog",0) & "        '记录登陆历史" & vbcrlf        
        
        str = str & "const conOrderForUser=" & getform("OrderForUser",0) & "        ' 只有会员才能购物" & vbcrlf
        str = str & "const conOpenSelectRegion=" & getform("OpenSelectRegion",0) & "        '地区填写模式" & vbcrlf        
        
        str = str & "const conOpenAutoCode=" & getform("OpenAutoCode",0) & "        '是否启用自动生成产品编号" & vbcrlf        
        str = str & "const conOpenAsp2Html=" & getform("OpenAsp2Html",0) & "        '是否启用静态页生成" & vbcrlf
        str = str & "const conOpenCreateThumb=" & getform("OpenCreateThumb",0) & "        '是否生成缩略图" & vbcrlf   
        str = str & "const conThumbWidth=" & getform("ThumbWidth",130) & "        ' 缩略图宽度" & vbcrlf 
        str = str & "const conOpenQuickMode=" & getform("OpenQuickMode",0) & "        '是否启用快速添加模式" & vbcrlf           
        str = str & "const conCurrencyUnit=" & chr(34) & getform("CurrencyUnit","$") & chr(34) & "        '货币单位" & vbcrlf 
        'str = str & "const conUSD2EUR=" & getform("USD2EUR",7.6) & "        '美元(USD)兑欧元(EUR)" & vbcrlf
        str = str & "const conFilterString=" & chr(34) & request.Form("filterString") & chr(34) & "        '过滤字符" & vbcrlf 
        str = str & "const conCustomerPage=" & chr(34) & getform("CustomerPage","") & chr(34) & "        '自定义生成页" & vbcrlf
        str = str & "const conBackUpFiles=" & chr(34) & getform("BackUpFiles","") & chr(34) & "        '需要备份页面" & vbcrlf        
        str = str & "const conDefaultPageCharset=" & chr(34) & getform("DefaultPageCharset","gb2312") & chr(34) & "        '页面编码" & vbcrlf 
        
        str = str & "const conSysname=" & chr(34) & getform("conSysname","iSixTeam") & chr(34) & "        '系统名称" & vbcrlf 
        str = str & "const conSysurl=" & chr(34) & getform("conSysurl","http://www.isixteam.com/") & chr(34) & "        '网址" & vbcrlf 
        str = str & "const conAuthor=" & chr(34) & getform("conAuthor","Ben Feng") & chr(34) & "        '系统联系人" & vbcrlf 
        str = str & "const conAuthorurl=" & chr(34) & getform("conAuthorurl","http://kofbqfeng.blogspot.com/") & chr(34) & "        '联系人网址" & vbcrlf 
        str = str & "const conAuthorPhone=" & chr(34) & getform("conAuthorPhone","020-00000000") & chr(34) & "        '联系人电话" & vbcrlf 
        
        str = str & "%" & ">"
        WriteTextFile Server.MapPath("../inc/cf.asp"),str,"utf-8"
        if admin.Purview=1 then
            call ExecScript("window.top.document.carnoc.location=window.top.document.carnoc.location;")
        end if
        call ToPage("?")
    else
		call alertback("你的服务器不支持Adodb.Stream组件")  
    end if
end if
%>
<body>
    <div class="bodydiv ">
        <div class="bodybox">
            <div class="titlenav">
                网 站 配 置
            </div>
            <div class="contentdiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            网站信息</td>                        
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab2">
                            邮件服务器</td>                             
                        <% if admin.Purview=1 then %>    
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab3">
                            网站选项</td>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab4">
                            系统版本</td>
                        <% end if %>  
                        <td class="formTabMsg">
                            所有项必须填写</td>
                    </tr>
                </table>
                <form action="?action=save" name="webForm" method="post" class="aform" onsubmit="return checkForm(this)">
                <div class="div1" id="Tab1_Box">
                    <div class="div2">
                        <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    网站标题：
                                </td>
                                <td class="td02">
                                    <input type="text" name="SiteTitle" class="text" value="<%=conSiteTitle %>" style="width: 200px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    网站名称或公司名：
                                </td>
                                <td class="td02">
                                    <input type="text" name="SiteName" class="text" value="<%=conSiteName %>" style="width: 200px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    网站域名：
                                </td>
                                <td class="td02">
                                    <input type="text" name="SiteDomain" class="text" value="<%=conSiteDomain %>" style="width: 200px;" /><span>如:domain.com</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    访问地址：
                                </td>
                                <td class="td02">
                                    <input type="text" name="SiteUrl" class="text" value="<%=conSiteUrl %>" readonly="readonly"
                                        style="width: 200px;" /><span>如:http://www.domain.com/</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    LOGO地址：
                                </td>
                                <td class="td02">
                                    <input type="text" name="logourl" id="logourl" class="text" value="<%=conLogoUrl %>"
                                        style="width: 200px;" />
                                    <a href="javascript:void(0);" onclick="DelFile(logourl.value,logourl)" class="c">删除</a><div>
                                        <span id="spanButtonPlaceHolder"></span>
                                    </div>

                                    <script>
                                        UploadInit("spanButtonPlaceHolder", $.FileType["Image"], "图片文件", 0, 200, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "logourl", "false")</script>

                                    <span>如:uploadfiles/logo.gif</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    SEO标题附加字：
                                </td>
                                <td class="td02">
                                    <input type="text" name="SeoTitle" class="text" value="<%=conSeoTitle %>" style="width: 400px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    网站关键字：<br />
                                    <span style="color: #666;">请用逗号隔开</span>
                                </td>
                                <td class="td02">
                                    <textarea name="SiteKeyword" class="textarea" rows="2" cols="60"><%=conSiteKeyword %></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    网站描述meta：
                                </td>
                                <td class="td02">
                                    <textarea name="SiteDescription" class="textarea" rows="3" cols="60"><%=conSiteDescription %></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    网站备案号：
                                </td>
                                <td class="td02">
                                    <input type="text" name="SiteBeiAn" class="text" value="<%=conSiteBeiAn %>" style="width: 200px;" /><span>链接地址:http://www.miibeian.gov.cn/</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    企业邮局入口：
                                </td>
                                <td class="td02">
                                    <input type="text" name="EnterpriseMail" class="text" value="<%=conEnterpriseMail %>"
                                        style="width: 200px;" /><span>如:http://main.domain.com</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    网站联系人：
                                </td>
                                <td class="td02">
                                    <input type="text" name="WebmasterName" class="text" value="<%=conWebmasterName %>"
                                        style="width: 200px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    联系地址：
                                </td>
                                <td class="td02">
                                    <input type="text" name="WebmasterAddress" class="text" value="<%=conWebmasterAddress %>"
                                        style="width: 300px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    邮政编码：
                                </td>
                                <td class="td02">
                                    <input type="text" name="WebmasterPostCode" class="text" value="<%=conWebmasterPostCode %>"
                                        style="width: 100px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    联系邮箱：
                                </td>
                                <td class="td02">
                                    <input type="text" name="WebmasterEmail" class="text" value="<%=conWebmasterEmail %>"
                                        style="width: 200px;" /><span>如:webmaster@domain.com</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    联系电话：
                                </td>
                                <td class="td02">
                                    <input type="text" name="WebmasterTel" class="text" value="<%=conWebmasterTel %>" style="width: 200px;" /><span>如:020-88888888
                                        33333333</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    联系传真：
                                </td>
                                <td class="td02">
                                    <input type="text" name="WebmasterFax" class="text" value="<%=conWebmasterFax %>" style="width: 200px;" /><span>如:020-88888888
                                        33333333</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    联系手机：
                                </td>
                                <td class="td02">
                                    <input type="text" name="WebmasterMobile" class="text" value="<%=conWebmasterMobile %>"
                                        style="width: 200px;" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="div1" id="Tab2_Box">
                    <div class="div2">
                        <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    用来发送邮件的SMTP服务器：
                                </td>
                                <td class="td02">
                                    <input type="text" name="MailServer" class="text" value="<%=conMailServer %>" style="width: 150px;" /><span>如:mail.domain.com</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    SMTP登录用户名：
                                </td>
                                <td class="td02">
                                    <input type="text" name="MailServerUserName" class="text" value="<%=conMailServerUserName %>"
                                        style="width: 150px;" /><span>如:name@domain.com</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    SMTP登录密码：
                                </td>
                                <td class="td02">
                                    <input type="password" name="MailServerPassWord" class="text" value="<%=conMailServerPassWord %>"
                                        style="width: 150px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    SMTP域名：
                                </td>
                                <td class="td02">
                                    <input type="text" name="MailDomain" class="text" value="<%=conMailDomain %>" style="width: 150px;" /><span>如用户名为“name@domain.com”,则填写domain.com</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="div1" id="Tab3_Box">
                    <div class="div2">
                        <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    上传文件大小限制：
                                </td>
                                <td class="td02">
                                    <input type="text" name="MaxFileSize" class="text" value="<%=conMaxFileSize %>" style="width: 100px;" /><span>单位:KB,
                                        建议不要超过1024KB</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    允许的上传文件类型：
                                </td>
                                <td class="td02">
                                    <input type="text" name="AllowFileType" class="text" value="<%=conAllowFileType %>"
                                        style="width: 300px;" /><span>用“|”号分开</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    上传文件目录：
                                </td>
                                <td class="td02">
                                    <input type="text" name="UpFilePath" class="text" value="<%=conUpFilePath %>" style="width: 300px;" /><span>结尾加上“/”</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    文件命名模式：
                                </td>
                                <td class="td02">
                                    <input type="radio" name="UpFileNameMode" id="UpFileNameMode1" value="0" /><label
                                        for="UpFileNameMode1">f/<span>XXXXXXXXXX.jpg</span></label><br />
                                    <input type="radio" name="UpFileNameMode" id="UpFileNameMode2" value="1" /><label
                                        for="UpFileNameMode2">path<span>/09_12_01/</span>pic.jpg</label>
                                        <script type="text/javascript">
                                            $.setElementValue(webForm.UpFileNameMode, "<%=conUpFileNameMode %>");
                                        </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    用户/管理员登陆时限：
                                </td>
                                <td class="td02">
                                    <input type="text" name="LoginTimeOut" class="text" value="<%=conLoginTimeOut %>" style="width: 80px;" /><span>单位:分钟</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    记录登陆历史：
                                </td>
                                <td class="td02">
                                    <input type="checkbox" name="CheckLoginLog" value="1" />是
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.CheckLoginLog, "<%=conCheckLoginLog %>");
                                    </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    只限会员购物：
                                </td>
                                <td class="td02">
                                    <input type="checkbox" name="OrderForUser" value="1" />是
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.OrderForUser, "<%=conOrderForUser %>");
                                    </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    自动生成产品编号：
                                </td>
                                <td class="td02">
                                    <input type="checkbox" name="OpenAutoCode" value="1" />是
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.OpenAutoCode, "<%=conOpenAutoCode %>");
                                    </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    地区填写模式：
                                </td>
                                <td class="td02">
                                    <input type="radio" name="OpenSelectRegion" id="OpenSelectRegion1" value="0" /><label
                                        for="OpenSelectRegion1">填写</label>
                                    <input type="radio" name="OpenSelectRegion" id="OpenSelectRegion2" value="1" /><label
                                        for="OpenSelectRegion2">选择</label>
                                        <script type="text/javascript">
                                            $.setElementValue(webForm.OpenSelectRegion, "<%=conOpenSelectRegion %>");
                                        </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    是否启用静态页生成：
                                </td>
                                <td class="td02">
                                    <input type="checkbox" name="OpenAsp2Html" value="1" />是
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.OpenAsp2Html, "<%=conOpenAsp2Html %>");
                                    </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    是否生成缩略图：
                                </td>
                                <td class="td02">
                                    <input type="checkbox" name="OpenCreateThumb" value="1" />是
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.OpenCreateThumb, "<%=conOpenCreateThumb %>");</script><span>只对新闻与产品有效
                                    </span>
                                    宽：<input type="text" name="ThumbWidth" class="text" value="<%=conThumbWidth %>" style="width: 40px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    是否启用快速添加模式：
                                </td>
                                <td class="td02">
                                    <input type="checkbox" name="OpenQuickMode" value="1" />是
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.OpenQuickMode, "<%=conOpenQuickMode %>");
                                    </script><span>修改后请重新刷新左边目录列表</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    货币单位：
                                </td>
                                <td class="td02">
                                    <input type="text" name="CurrencyUnit" class="text" value="<%=conCurrencyUnit %>" style="width: 40px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    过滤字符：
                                </td>
                                <td class="td02">
                                    <textarea name="filterString" class="textarea" rows="3" cols="50"><%=conFilterString %></textarea><span>用“|”号分开</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    自定义生成页：
                                </td>
                                <td class="td02">
                                    <textarea name="CustomerPage" class="textarea" rows="3" cols="70"><%=conCustomerPage %></textarea><br />
                                    <span>如:index.asp</span> 用“|”号分开
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    需要备份文件：
                                </td>
                                <td class="td02">
                                    <textarea name="BackUpFiles" class="textarea" rows="3" cols="70"><%=conBackUpFiles %></textarea><br />
                                    <span>http://<%=conSiteDomain %>/it%20is%20backup.asp '进行恢复</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    页面编码：
                                </td>
                                <td class="td02">
                                    <input type="text" name="DefaultPageCharset" class="text" value="<%=conDefaultPageCharset %>"
                                        style="width: 80px;" /><span>如:gb2312/utf-8</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="div1" id="Tab4_Box">
                    <div class="div2">
                        <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    系统名称：</td>
                                <td class="td02">
                                    <input type="text" name="conSysname" class="text" value="<%=conSysname %>"
                                        style="width: 250px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    网址：</td>
                                <td class="td02">
                                    <input type="text" name="conSysurl" class="text" value="<%=conSysurl %>"
                                        style="width: 250px;" /> <span>如:http://www.domain.com/</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    联系人：</td>
                                <td class="td02">
                                    <input type="text" name="conAuthor" class="text" value="<%=conAuthor %>"
                                        style="width: 250px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    联系人网址：</td>
                                <td class="td02">
                                    <input type="text" name="conAuthorurl" class="text" value="<%=conAuthorurl %>"
                                        style="width: 250px;" /> <span>如:http://www.domain.com/</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01" style="width: 180px;">
                                    联系电话：</td>
                                <td class="td02">
                                    <input type="text" name="conAuthorPhone" class="text" value="<%=conAuthorPhone %>"
                                        style="width: 100px;" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="submit">
                    <button type="submit" class="btn">
                        提 交</button>&nbsp;&nbsp;
                    <button type="button" onclick="window.history.back();" class="btn">
                            返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
