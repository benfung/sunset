﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="../inc/md5.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim myclass,classArray
set myclass=new classobj
classArray=myclass.GetArrayByTypename("links")

dim action,id,links
action=getquerystring("action","")
id=getquerystring("id",0)

set links=new linksObj
links.linksObj(id)
' 设置默认值
if links.isnull then
    links.flag=1 ' 默认已审核
    links.classid=42 ' 默认已审核
end if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form) {
            if($.trim(form.siteurl.value)==""){
                alert("请填写链接!!");
                form.siteurl.focus();
                return false;
            }
        }

        function initRadioByPid(argClass, name, pid,defvalue) {
            for (var i = 0; i < argClass.length; i++) {
                if (argClass[i] == undefined) continue;
                if (argClass[i][1] == pid) {
                    if (defvalue == argClass[i][0]) {
                        document.writeln("<input type=\"radio\" id=\"" + name + "_" + i + "\" checked='checked' name=\"" + name + "\" value=\"" + argClass[i][0] + "\" /><label for=\"" + name + "_" + i + "\">" + argClass[i][3] + "</label>&nbsp;");
                    } else {
                        document.writeln("<input type=\"radio\" id=\"" + name + "_" + i + "\" name=\"" + name + "\" value=\"" + argClass[i][0] + "\" /><label for=\"" + name + "_" + i + "\">" + argClass[i][3] + "</label>&nbsp;");
                    }
                }
            }
        }
      -->
    </script>

</head>
<%

if action="save" then	
	links.classid=getForm("classid",0)
	links.name=TurnSql(getForm("name",""))
	links.subname=TurnSql(getForm("subname",""))
	links.siteurl=TurnSql(getForm("siteurl",""))
	links.siteintro=TurnSql(getForm("siteintro",""))
	links.logo=TurnSql(getForm("logo",""))
	links.siteadmin=TurnSql(getForm("siteadmin",""))
	links.email=TurnSql(getForm("email",""))
	links.sort=getForm("sort",0)
	links.isgood=getForm("isgood",0)
	links.flag=getForm("flag",0)

	if links.isnull() then
		links.add()
		call confirm("操作成功<br />是否继续添加","?","links.asp")
	else
		links.update()
	end if
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                <a href="links.asp">友情链接管理</a> <span class="titleNavSpot">&nbsp;</span>
                <% if links.isnull then %>
                添加
                <% else %>
                编辑
                <% end if %>
            </div>
            <div class="contentdiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            基本属性</td>
                        <td class="formTabMsg">
                            带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=links.id %>" name="webForm" class="aform" method="post" onSubmit="return checkForm(this)">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:100px;">
                            <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        <span>*</span>类别：</td>
                                    <td class="td02">
                                        <select name="classid">
                                        <option value="0">-- 请选择栏目 --</option>
                                        <%=myclass.ShowOption(classArray,0,0,3) %>
                                        </select>
                                        <script type="text/javascript">
                                            $.setElementValue(webForm.classid, "<%=Product.classid %>", "0");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>标识名：</td>
                                    <td class="td02">
                                        <input type="text" name="name" class="text" value="<%=links.name %>"  style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        简 称：</td>
                                    <td class="td02">
                                        <input type="text" name="subname" class="text" value="<%=links.subname %>"  style="width: 100px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>链 接：</td>
                                    <td class="td02">
                                        <input type="text"  name="siteurl" class="text" value="<%=links.siteurl %>" style="width: 300px;" /> <span>请输入完整路径(http://)</span>
                                    </td>
                                </tr> 
                                <tr>
                                    <td class="td01">
                                        Logo/图片：</td>
                                    <td class="td02">
                                        <input type="text" id="logo"  name="logo" class="text" value="<%=links.logo %>" style="width: 250px;" />
                                        <a href="javascript:void(0);" onClick="DelFile(logo.value,logo)" class="c">删除</a>
                                        <div><span id="spanButtonPlaceHolder"></span></div>
                                        <script>
                                            UploadInit("spanButtonPlaceHolder", $.FileType["Image"], "图片文件", 0, 200, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "logo", "false")
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        站 长：</td>
                                    <td class="td02">
                                        <input type="text" name="siteadmin" class="text" value="<%=links.siteadmin %>"  style="width: 100px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        邮 箱：</td>
                                    <td class="td02">
                                        <input type="text" name="email" class="text" value="<%=links.email %>"  style="width: 100px;" /><span>格式:a@b.com</span>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="td01">
                                        站点说明：</td>
                                    <td class="td02">
                                        <textarea name="siteintro" cols="60" rows="5"><%=links.siteintro %></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        排序号：</td>
                                    <td class="td02">
                                        <input type="text" name="sort" class="text" value="<%=links.sort %>"  style="width: 80px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        其他属性：</td>
                                    <td class="td02">
                                        <input type="checkbox" name="flag" value="1" />
                                        审核通过
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.flag,"<%=links.flag %>");
                                        </script>  
                                        
                                        <input type="checkbox" name="isgood" value="1" />
                                        推荐
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.isgood,"<%=links.isgood %>");
                                        </script>
                                    </td>
                                </tr>
                            </table>
                        </div>                        
                    </div>
                    <div class="submit">
                        <button type="submit" class="btn">保存修改</button>&nbsp;&nbsp;
                        <button type="button" onClick="window.location='links.asp';"  class="btn">返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
