﻿<%@  language="VBScript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>管理系统首页</title>
    
    <!--#include file="headmeta.asp"-->
    <style type="text/css">
        .back_020web { background-image: url('image/titlebg.gif'); color: #000; }
        .table_020web { background-color: #A4B6D7; }
        .td_020web { background-color: #F2F8FF; }
        .tr_020web { background-color: #ECF5FF; }
        .t1 { font: 12px '宋体'; color: #000; }
        .t2 { font: 12px '宋体'; color: #fff; }
        .t3 { font: 12px '宋体'; color: #ff0; }
        .t4 { font: 12px '宋体'; color: #800000; }
        .t5 { font: 12px '宋体'; color: #191970; }
        .weiqun:hover { font-family: '宋体'; color: #fff; text-decoration: underline; background-color: #ccc; }
        td { font-size: 12px; }
        a:link,a:visited { color: #000000; text-decoration: none; }
        a:hover { color: #000000; text-decoration: underline; }
        a.b:link,a.b:visited { color: blue; text-decoration: none; }
        a.b:hover { color: blue; text-decoration: underline; }
    </style>
</head>
<body>

    <%

Dim theInstalledObjects(17)
theInstalledObjects(0) = "MSWC.AdRotator"
theInstalledObjects(1) = "MSWC.BrowserType"
theInstalledObjects(2) = "MSWC.NextLink"
theInstalledObjects(3) = "MSWC.Tools"
theInstalledObjects(4) = "MSWC.Status"
theInstalledObjects(5) = "MSWC.Counters"
theInstalledObjects(6) = "IISSample.ContentRotator"
theInstalledObjects(7) = "IISSample.PageCounter"
theInstalledObjects(8) = "MSWC.PermissionChecker"
theInstalledObjects(9) = "Scripting.FileSystemObject"
theInstalledObjects(10) = "adodb.connection"    
theInstalledObjects(11) = "SoftArtisans.FileUp"
theInstalledObjects(12) = "SoftArtisans.FileManager"
theInstalledObjects(13) = "JMail.SMTPMail"
theInstalledObjects(14) = "CDONTS.NewMail"
theInstalledObjects(15) = "Persits.MailSender"
theInstalledObjects(16) = "LyfUpload.UploadFile"
theInstalledObjects(17) = "Persits.Upload.1"
    %>

    
    <table cellpadding="2" cellspacing="1" border="0" width="95%" align="center" class="table_020web">
        <tr>
            <td class="back_020web" colspan="2" height="25" align="center"><b>统计</b></td>
        </tr>
        <tr class="tr_020web">
            <td width="11%" height="23">注册用户</td>
            <td width="88%" height="23"><a href="user.asp" class="b">注册用户</a>
            </td>
        </tr>
        <tr class="tr_020web">
            <td width="11%" height="23">产品/商品</td>
            <td width="88%" height="23"><a href="product.asp" class="b">产品/商品</a>
            </td>
        </tr>
    </table>
    <br>
    <br>
    
    <table cellpadding="2" cellspacing="1" border="0" width="95%" align="center" class="table_020web">
        <tr>
            <td class="back_020web" colspan="2" height="25" align="center">
                <b>系统环境信息</b></td>
        </tr>
        <tr class="tr_020web">
            <td width="48%" height="23">
                用户名：<font class="t4">
                    <%=admin.loginid%>
                </font>
            </td>
            <td width="52%">
                IP：<font class="t4">
                    <%=Request.ServerVariables("REMOTE_ADDR")%>
                </font>
            </td>
        </tr>
        <tr class="tr_020web">
            <td width="48%" height="23">
                身份过期：<font class="t4"><%=conLoginTimeOut%>
                    分钟</font></td>
            <td width="52%">
                现在时间：<font class="t4">
                    <%=year(now())%>
                    年<%=month(now())%>月<%=day(now())%>日<%=hour(now())%>:<%=minute(now())%></font></td>
        </tr>
        <tr class="tr_020web">
            <td width="48%" height="23">
                上线次数： <font class="t4">
                    <%=admin.LoginTimes%>
                </font>
            </td>
            <td width="52%">
                上线时间：<font class="t4">
                    <%=admin.LastLoginTime%>
                </font>
            </td>
        </tr>
        <tr class="tr_020web">
            <td width="48%" height="23">
                服务器域名：<font class="t4">
                    <%=Request.ServerVariables("server_name")%>
                    /
                    <%=Request.ServerVariables("Http_HOST")%>
                </font>
            </td>
            <td width="52%">
                脚本解释引擎：<font class="t4">
                    <%=ScriptEngine & "/"& ScriptEngineMajorVersion &"."&ScriptEngineMinorVersion&"."& ScriptEngineBuildVersion %>
                </font>
            </td>
        </tr>
        <tr class="tr_020web">
            <td height="23">
                服务器软件的名称：<font class="t4">
                    <%=Request.ServerVariables("SERVER_SOFTWARE")%>
                </font>
            </td>
            <td>
                浏览器版本：<font class="t4">
                    <%=Request.ServerVariables("Http_User_Agent")%>
                </font>
            </td>
        </tr>
        <tr class="tr_020web">
            <td height="23">
                FSO文本读写：
                <%If Not IsObjInstalled(theInstalledObjects(9)) Then%>
                <font color="red"><b>×</b></font>
                <%else%>
                <b>√</b>
                <%end if%>
            </td>
            <td>
                数据库使用：
                <%If Not IsObjInstalled(theInstalledObjects(10)) Then%>
                <font color="red"><b>×</b></font>
                <%else%>
                <b>√</b>
                <%end if%>
            </td>
        </tr>
        <tr class="tr_020web">
            <td width="48%" height="23">
                Jmail组件支持：
                <%If Not IsObjInstalled(theInstalledObjects(13)) Then%>
                <font color="red"><b>×</b></font>
                <%else%>
                <b>√</b>
                <%end if%>
            </td>
            <td width="52%">
                Aspjpeg组件支持：
                <%If Not IsObjInstalled(theInstalledObjects(17)) Then%>
                <font color="red"><b>×</b></font>
                <%else%>
                <b>√</b>
                <%end if%>
            </td>
        </tr>
    </table>
    <a href="Help.asp"><font color="ffffff">help</font></a>
    <br>
    <%htmlend%>
</body>
</html>
<%call closeconn %>
<%
sub htmlend
%>
<p>
    <table cellspacing="0" cellpadding="0" width="95%" align="center">
        <tr>
            <td align="middle">
                Script Execution Time:<%=formatnumber((timer()-PageStartTime)*1000,3)%> 毫秒</td>
        </tr>
    </table>
    <%
end sub
    %>
    <%
''''''''''''''''''''''''''''''
Function IsObjInstalled(strClassString)
On Error Resume Next
IsObjInstalled = False
Err = 0
Dim xTestObj
Set xTestObj = Server.CreateObject(strClassString)
If 0 = Err Then IsObjInstalled = True
Set xTestObj = Nothing
Err = 0
End Function
''''''''''''''''''''''''''''''
Function getver(Classstr)
On Error Resume Next
getver=""
Err = 0
Dim xTestObj
Set xTestObj = Server.CreateObject(Classstr)
If 0 = Err Then getver=xtestobj.version
Set xTestObj = Nothing
Err = 0
End Function

    %>
    <%sub discreteness%>
    <table border="0" width="95%" cellspacing="1" cellpadding="3" class="table_020web"
        align="center">
        <tr class="back_020web">
            <td width="57%" height="25">
                &nbsp;组件名称</td>
            <td width="41%" height="25">
                支持及版本</td>
        </tr>
        <%
Dim theInstalledObjects(17)
theInstalledObjects(0) = "MSWC.AdRotator"
theInstalledObjects(1) = "MSWC.BrowserType"
theInstalledObjects(2) = "MSWC.NextLink"
theInstalledObjects(3) = "MSWC.Tools"
theInstalledObjects(4) = "MSWC.Status"
theInstalledObjects(5) = "MSWC.Counters"
theInstalledObjects(6) = "MSWC.PermissionChecker"
theInstalledObjects(7) = "ADODB.Stream"
theInstalledObjects(8) = "adodb.connection"
theInstalledObjects(9) = "Scripting.FileSystemObject"
theInstalledObjects(10) = "SoftArtisans.FileUp"
theInstalledObjects(11) = "SoftArtisans.FileManager"
theInstalledObjects(12) = "JMail.Message"
theInstalledObjects(13) = "CDONTS.NewMail"
theInstalledObjects(14) = "Persits.MailSender"
theInstalledObjects(15) = "LyfUpload.UploadFile"
theInstalledObjects(16) = "Persits.Upload.1"
theInstalledObjects(17) = "w3.upload"
For i=0 to 17
Response.Write "<TR class=td_020web><TD>&nbsp;" & theInstalledObjects(i) & "<font color=888888>&nbsp;"
select case i
case 8
Response.Write "(ACCESS 数据库)"
case 9
Response.Write "(FSO 文本文件读写)"
case 10
Response.Write "(SA-FileUp 文件上传)"
case 11
Response.Write "(SA-FM 文件管理)"
case 12
Response.Write "(JMail 邮件发送)"
case 13
Response.Write "(WIN虚拟SMTP 发信)"
case 14
Response.Write "(ASPEmail 邮件发送)"
case 15
Response.Write "(LyfUpload 文件上传)"
case 16
Response.Write "(ASPUpload 文件上传)"
case 17
Response.Write "(w3 upload 文件上传)"
end select
Response.Write "</font></td><td height=25>"
If Not IsObjInstalled(theInstalledObjects(i)) Then
Response.Write "<font color=red><b>×</b></font>"
Else
Response.Write "<b>√</b> " & getver(theInstalledObjects(i)) & ""
End If
Response.Write "</td></TR>" & vbCrLf
Next
        %>
    </table>
    <%end sub%>
    