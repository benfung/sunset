﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<%call closeconn %>
<% 
dim action,name,path
action=getquerystring("action","")
name=getquerystring("name","")
If Instr(strDB,":")>0 Then : strDB = strDB : Else : strDB = Server.MapPath(strDB) : End If
filepath=mid(strDB,1,InStrRev(strDB,"\"))

dim errormsg
errormsg=""
Set fso = CreateObject("Scripting.FileSystemObject")

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
      -->
    </script>
</head>
<body>
    <%
    
    if action="backup" then
        if fso.FileExists(strdb) then
            strNewDB=filepath & "@@" & DateToNum(now()) & "@@.mdb"
            fso.CopyFile strdb,strNewDB   
            call alert("备份成功!!" ,"?")
        else
			call alertback("备份失败。。。文件不存在")
        end if    
    end if
    if action="roll" then
        strNewDB=filepath & name
        fso.CopyFile strNewDB,strDB,true
        call alert("还原成功!!" ,"?")
    end if
    if action="del" then
        strNewDB=filepath & name
        fso.DeleteFile(strNewDB)
        call alert("删除成功!!" ,"?")
    end if
     %>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                备份/还原
            </div>
            <div class="contentdiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabB" style="width: 80px;" id="Tab1">
                            备份/还原</td>
                        <td class="formTabMsg">&nbsp;
                            </td>
                    </tr>
                </table>
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:40px;">                        
                            <% if errormsg<>"" then Response.Write("<span style='color:red;'>" & errormsg & "</span>") %>
                             <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                            <tr>
                                    <td class="td02">
                                        <button type="button" onClick="window.location='?action=backup'" class="btn">立即备份</button>
                                        <span>点击备份将生成新的文件，名称如:#20080808121401.mdb</span></td>
                                </tr>
                            
                            </table>                            
                        </div>  
                             <table cellspacing="0" cellpadding="6" border="1" class="table1" style="border-collapse: collapse;">
                            <tr class="gvrowhead">
                                <th class="th" >
                                        文件
                                </th>
                                <th class="th" style="width: 120px;">
                                        备份日期
                                </th>
                                <th class="th"  style="width: 80px;">
                                        操作
                                </th>
                            </tr>
                            <% set files=fso.GetFolder(filepath) %>
                            <% for each file in files.Files %>
                            <% if Lcase(file.Path)<>Lcase(strDB)  then %>
                            <tr class="gvrow">
                                <td class="til">
                                    <%=file.name %>
                                </td>
                                <td class="tic">
                                    <%=file.DateCreated     %>
                                </td>
                                <td class="tic">
                                    <a href="javascript:if(confirm('确定要还原吗?'))window.parent.main.location='?action=roll&name=<%=file.name %>'" class="b">
                                        还原</a>
                                    <% if admin.Purview=1 then %>
                                    <a href="javascript:if(confirm('确定要删除吗?'))window.parent.main.location='?action=del&name=<%=file.name %>'" class="b">
                                        删除</a>
                                    <% end if %>
                                </td>
                            </tr>
                            <% end if %>
                            <% next %>
                        </table>                 
                    </div>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%set FSO = nothing %>
<%
'***************************************************
'函数名称:ShortDate
'作用:日期格式转换,如1982-4-2 12:30:1 转成  1982-04-02
'参数:日期
'***************************************************
function DateToNum(idate)
    if IsDate(idate) then
        dim iyear,imonth,iday,ihour,iminute,isecond
        iyear=year(idate)
        imonth=month(idate)
        iday=day(idate)
        ihour=hour(idate)
        iminute=minute(idate)
        isecond=second(idate)
        if len(imonth) <> 2 then imonth="0" & imonth
        if len(iday) <> 2 then iday="0" & iday
        if len(ihour) <> 2 then ihour="0" & ihour
        if len(iminute) <> 2 then iminute="0" & iminute
        if len(isecond) <> 2 then isecond="0" & isecond
        DateToNum=iyear & imonth & iday & ihour & iminute & isecond
    else
        DateToNum=idate
    end if
end function
%>