﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<%
dim action,idset,idArray
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

filePath="?"

CurrentPage=Cint(getquerystring("pageno",1))

set payplatform=new payplatformObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--
        // 在document就绪或加载完成时执行
        $(document).ready(function() {
        });

        function SetSet(action, msg) {
            var sets = $.getChecked('idset').join(',');
            if (sets == '') {
                alert("请选定要操作的记录!!");
            } else if (action == null || action == '') {
                alert("不明操作！！\r\n请检查参数:action");
            } else {
                if (msg != null) {
                    if (!confirm(msg)) return false;
                }
                window.location = "<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>

</head>
<%
' 批量操作
if action="del" then    
    for i=0 to ubound(idArray)
	    conn.execute "delete from payplatform where id =" & idArray(i)
    next       
end if

' 设置isdisabled
if action="pass" then  
    conn.execute "update payplatform set isdisabled=0"
    conn.execute "update payplatform set isdisabled=1 where id in (" & idset & ")"
end if

' 设置isdisabled
if action="unpass" then  
    conn.execute "update payplatform set isdisabled=0 where id in (" & idset & ")"
end if

' 设置isdisabled
if action="poundage" then  
    conn.execute "update payplatform set pluspoundage=1 where id in (" & idset & ")"
end if

' 设置isdisabled
if action="unpoundage" then  
    conn.execute "update payplatform set pluspoundage=0 where id in (" & idset & ")"
end if


' 组织SQL
where=""

%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                支付平台
            </div>
            <div class="contentdiv" style="text-align: center; height: 550px;">
                <!--<div class="ListDiv"  style="width:98%;">
                    <div class="SetDiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <select name="action" onchange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="poundage" >&nbsp;&nbsp;标记为要手续费</option>
                                        <option value="unpoundage" >&nbsp;&nbsp;标记不要手续费</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div> -->
                <div>
                    <%
                        
                        set rs=payplatform.load("",where,"order by a.id")
                        MaxItemSize=18
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<span style='color:red;font-size:14px;'>没有数据!!</span>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        end if
                    %>
                    <table cellspacing="0" cellpadding="6" border="1" class="gv" style="border-collapse: collapse;">
                        <tr class="gvrowhead">
                            <th class="th" style="width: 20px;">
                                <input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />
                            </th>
                            <th class="th" style="width: 120px;">
                                支付平台
                            </th>
                            <th class="th" style="width: 70px;">
                                商户号
                            </th>
                            <th class="th" style="width: 130px;">
                                MD5Key
                            </th>
                            <th class="th" style="width: 70px;">
                                手续费(%)
                            </th>
                            <th class="th" style="width: 50px;">
                                属性
                            </th>
                            <th class="th">
                                描述
                            </th>
                        </tr>
                        <% for i=1 to rs.RecordCount %>
                        <tr class="gvrow">
                            <td class="tic">
                                <input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" />
                            </td>
                            <td class="til">
                                <input type="text" class="text" value="<%=rs.fields("platformname") %>" onfocus="this.select()"
                                    id="payplatform_platformname_string_<%=rs.fields("id") %>" onblur='bgUpdate(this)' style="width: 114px;" />
                            </td>
                            <td class="tic">
                                <input type="text" class="text" value="<%=rs.fields("accountsid") %>" onfocus="this.select()"
                                    id="payplatform_accountsid_string_<%=rs.fields("id") %>" onblur='bgUpdate(this)' style="width: 64px;" />
                            </td>
                            <td class="til">
                                <input type="text" class="text" value="<%=rs.fields("md5key") %>" onfocus="this.select()"
                                    id="payplatform_md5key_string_<%=rs.fields("id") %>" onblur='bgUpdate(this)' style="width: 124px;" />
                            </td>
                            <td class="tic">
                                <input type="text" class="text" value="<%=rs.fields("rate") %>" onfocus="this.select()"
                                    id="payplatform_rate_string_<%=rs.fields("id") %>" onblur='bgUpdate(this)' style="width: 20px;" />
                                [<% if rs.fields("pluspoundage")=1 then %><span style="color: Green;">√</span><% else %><span
                                    style="color: Red;">×</span><% end if %>]
                            </td>
                            <td class="tic">
                                <% if rs.fields("isdisabled")=1 then %>
                                <span style="color: Green;">启用</span><% else %><a href="?action=pass&idset=<%=rs.fields("id") %>"
                                    class="b"><span style="color: Red;">禁用</span></a><% end if %>
                            </td>
                            <td class="til">
                                <input type="text" class="text" value="<%=rs.fields("description") %>" onfocus="this.select()"
                                    id="payplatform_description_string_<%=rs.fields("id") %>" onblur='bgUpdate(this)' style="width: 250px;" />
                            </td>
                        </tr>
                        <% rs.movenext
                               next
                        %>
                    </table>
                    <% rs.close:set rs=nothing %>
                </div>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
