﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/conn.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim myclass,classArray
const classDepth=2  ' 限制深度
const navtitle="位置管理"
const typename="gg"
set myclass=new ClassObj
myclass.typename=typename 
classArray=myclass.GetArray()

dim action,id,pid,idset,idArray
action=getquerystring("action","list")
idset=getquerystring("idset","0")
idArray=split(idset,",")

id=getquerystring("id",0)
pid=getquerystring("pid",0)

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<%
if action="del" then
    for i=0 to ubound(idArray)
        myclass.ClassObj(idArray(i))
        myclass.ExeDelete()
    next
    call ToPage("?")
end if


if action="save" then   
   
    myclass.ClassObj(id)
    myclass.pid=getform("pid",0)
    myclass.name=TurnSql(getform("name",""))
    myclass.subname=TurnSql(getform("subname",""))
    myclass.pic=getform("pic","")
    myclass.width=getform("width",0)
    myclass.height=getform("height",0)
    'myclass.sort=getform("sort",0)
    myclass.status=getform("status",0)
    
    if myclass.isnull() then
        myclass.add()
        call confirm("操作成功<br />是否继续添加子栏目","?action=edit&pid=" & myclass.pid,"?")
    else
        myclass.update()
        call topage("?")
    end if
end if

%>
<body>
    <% 
    ' //////////////////////////////////////////
if action="list" then
    call classlist()
elseif action="edit" then
    call classEdit()
end if
    ' //////////////////////////////////////////
    %>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
<% 
' // 栏目列表 /////////////////////////////////////////////////////
sub classlist %>
<script type="text/javascript">
    function SetSet(action, msg) {
        var sets = $.getChecked('idset').join(',');
        if (sets == '') {
            alert("请选定要操作的记录!!");
        } else if (action == null || action == '') {
            alert("不明操作！！\r\n请检查参数:action");
        } else {
            if (msg != null) {
                if (!confirm(msg)) return false;
            }
            window.location = "?action=" + action + "&idset=" + sets
        }
    }
</script>
<div class="bodydiv">
    <div class="bodybox">
        <div class="titlenav">
            <%=navtitle %>
        </div>
        <div class="contentdiv" style="text-align: center; height: 480px;">
            <div class="searchdiv" style="width: 70%;">
                <a href="?action=edit&pid=0" title="添加栏目" class="b">添加一级栏目</a>
            </div>
            <div class="ListDiv" style="width: 70%;">
                <div>
                    <table cellspacing="0" cellpadding="6" border="1" class="gv" style="border-collapse: collapse;">                        
                        <tr class="gvrowhead">
                            <th class="th" style="width: 20px;"><input type="checkbox" name="checkbox" onClick="$.SelectAllCB(this,'idset');" value="0" /></th>
                            <th class="th" style="width: 40px;">ID</th>
                            <th class="th">栏目名称</th>
                            <th class="th" style="width: 50px;">序号</th>
                            <th class="th" style="width: 220px;">操作</th>
                        </tr>
                        <% call ClassArrayListByPid(classArray,0) %>
                    </table>
                                        <div class="SetDiv"><table cellpadding="0" cellspacing="0"><tr><td><button type="button" class="btn" onClick="SetSet('del','册除后不能恢复,确定要册除吗?');">删除选定项</button></td></tr></table></div>  
  

                </div>
            </div>
        </div>
    </div>
</div>
<%end sub%>
<% 
' // 栏目编辑 ///////////////////////////////////////////////////////////////////
sub classEdit %>
<%
myclass.ClassObj(id)
if myclass.isnull() then
    myclass.pid=pid
end if
%>
<script type="text/javascript">
    $(document).ready(function() {
        $.slipTab(1, 1, "Tab", "formTabA", "formTabB", "click", 1);
    });


    function checkForm(form) {
        if ($.trim(form.name.value) == "") {
            alert("请填写名称栏目!!");
            form.name.focus();
            return false;
        }
    }  
</script>
<div class="bodydiv">
    <div class="bodybox">
        <div class="titlenav">
                <%=navtitle %>
            </div>
        <div class="contentdiv">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="formTabSpace">&nbsp;
                        </td>
                    <td class="formTabA" style="width: 80px;" id="Tab1">
                        基本属性</td>
                    <td class="formTabMsg">
                        带*号为必填项</td>
                </tr>
            </table>
            <form action="?action=save&id=<%=myclass.id %>" name="webForm" method="post" class="aform" onSubmit="return checkForm(this)">
                <input type="hidden" name="pid" value="<%=myclass.pid %>" />
                <div class="div1" id="Tab1_Box">
                    <div class="div2" style="height:150px;">
                        <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                            <tr>
                                <td class="td01">
                                    父栏目：</td>
                                <td class="td02">
                                    <select name="pidview"  disabled="disabled">
                                        <option value="0">无</option>
                                        <%=myclass.ShowOption(classArray,0,0,3) %>
                                    </select>
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.pidview, "<%=myclass.pid %>", "0");
                                    </script>
                                </td>
                            </tr>
                            <% if myclass.pid<>"0" then %>
                            <tr>
                                <td class="td01">类型：</td>
                                <td class="td02">
                                    <input type="radio" name="subname" value="点击广告" />点击广告<br />
                                    <input type="radio" name="subname" value="图片轮换" />图片轮换<br />
                                    <input type="radio" name="subname" value="资讯模块" />资讯模块
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.subname, "<%=myclass.subname %>", "点击广告");
                                    </script>
                                 </td>
                            </tr>
                            <% end if %>
                            <tr><td class="td01">名称：</td><td class="td02"><input type="text" name="name" class="text" value="<%=myclass.name %>" style="width: 150px;" /><span>如:通栏</span></td></tr>
                            <% if myclass.pid<>"0" then %>
                            <tr><td class="td01">相关图片：</td><td class="td02"><input type="text" id="pic" name="pic" class="text" value="<%=myclass.pic %>" style="width: 250px;" /> <a href="javascript:void(0);" onClick="DelFile(pic.value,pic)" class="c">删除</a>
                            <div>
                                            <span id="spanButtonPlaceHolder"></span>
                                        </div>
                                        <script>
                                            UploadInit("spanButtonPlaceHolder", $.FileType["Image"], "图片文件", 0, 200, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "pic", "false")
                                        </script>
                            </td></tr>
                            <tr><td class="td01">尺寸：</td><td class="td02">宽:<input type="text"  name="width" value="<%=myclass.width %>" class="text" style="width: 40px;" />
                                        高:<input type="text"  name="height" value="<%=myclass.height %>" class="text" style="width: 40px;" /></td></tr>
                                <% end if %>
                            <!--<tr>
                                <td class="td01">
                                    排序号：</td>
                                <td class="td02">
                                    <input type="text" name="sort" class="text" value="<%=myclass.sort %>" style="width: 50px;" />
                                </td>
                            </tr>-->
                            <!--<tr>
                                <td class="td01">
                                    状态：</td>
                                <td class="td02">
                                    <input type="checkbox" name="status" value="1" />
                                    是
                                    <script type="text/javascript">
                                        $.setElementValue(webForm.status,"<%=myclass.status %>","0");
                                    </script>
                                </td>
                            </tr>-->
                        </table>
                    </div>
                </div>
                <div class="submit">
                    <button type="submit"  class="btn" >保存修改</button>&nbsp;&nbsp;
                        <button class="btn" onclick="window.location='?';" >返 回</button></div>
            </form>
        </div>
    </div>
</div>
<% end sub %>


<% sub ClassArrayListByPid(argArray,arg0) %>
    <% for i=1 to ubound(argArray)  %>
        <% if argArray(i-1,1)=arg0 then %>
            <tr class="gvrow">
                <td class="tic"><input type="checkbox" class="idset" name="idset" value="<%=argArray(i-1,0) %>" /></td>
                <td class="tic"><%=argArray(i-1,0) %></td>
                <td class="til" style="padding-left:<%= argArray(i-1,5)*14%>px;">&nbsp;<% if argArray(i-1,1)>0 then %><span style="color:#ccc;">└ </span><% end if %><%=argArray(i-1,3) %></td>
                <td class="til"><input type="text" onfocus="this.select()" id="myclass_sort_int_<%=argArray(i-1,0) %>"  onblur="bgUpdate(this)" class="text" value="<%=argArray(i-1,6) %>" style="width: 40px;" \/> </td>
                <td class="tic">
                <% if argArray(i-1,5)<classDepth-1 then %>
                <a href="?action=edit&pid=<%=argArray(i-1,0) %>" title="添加子栏目" class="b">添加子栏目</a>&nbsp;&nbsp;
                <% else %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <% end if %>
                <a href="?action=edit&id=<%=argArray(i-1,0) %>" title="编辑" class="b">编辑</a>&nbsp;&nbsp;<a href="javascript:if(confirm('册除后不能恢复,确定要册除吗?')){window.location='?action=del&idset=<%=argArray(i-1,0) %>'}" class="b">删除</a></td>
            </tr>
        <% if argArray(i-1,2)>0 then call ClassArrayListByPid(argArray,argArray(i-1,0)) %>
        <% end if %>
    <%  next %>
<%  end sub   %>