﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,idset,idArray,feedback,title,flag,reply,where,selected
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

selected=getquerystring("selected",0)
flag=getquerystring("flag","")
title=getquerystring("title","")
reply=getquerystring("reply","")
filePath="?selected=" & selected & "&flag=" & flag & "&title=" & title & "&reply=" & reply & "&"

CurrentPage=Cint(getquerystring("pageno",1))

set feedback=new feedbackObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>
</head>
<%
' 批量操作
if action="del" then    
    for i=0 to ubound(idArray)
	    conn.execute "delete from feedback where id =" & idArray(i)
    next       
end if

if action="pass" then    
    conn.execute "update feedback set flag=1 where id in (" & idset & ")"
end if

if action="unpass" then    
    conn.execute "update feedback set flag=0 where id in (" & idset & ")"
end if



' 组织SQL
where="where a.types=0"
if title<>"" then
    where = where & " and (a.title+a.content like '%" & title & "%')"
end if
if flag<>"" then
    where = where & " and a.flag=" & flag
end if

if reply="1" then
    where = where & " and (a.reply='' or a.reply is null)"
end if
%>
<body>
    <div class="bodydiv">
        <div id="newitem" class="jbox" style="width: 500px;">
        <div class="closejbox"><img src="images/close.gif" alt="" align="absmiddle" onclick="$.RemoveJBox($('#newitem'));" /></div>
        <fieldset>
            <legend>高级搜索</legend>
            <form method="get" action="?" name="webForm" class="aform">
            <table cellpadding="6" cellspacing="2" class="table1">
                <tr>
                    <td class="td01">关键字: </td>
                    <td class="td02">
                        <input type="text" name="title" class="text" value="<%=title %>" style="width: 255px;" />
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="td02" style="text-align: center;">
                        <button type="submit" class="btn">
                            搜 索</button>&nbsp;&nbsp;<button type="button" class="btn" onclick="$.RemoveJBox($('#newitem'));">
                            关 闭</button></td>
                </tr>
            </table>
            </form>
        </fieldset>
    </div>
        <div class="bodybox">
            <div class="titlenav">
                高级搜索
            </div>
            <div class="contentdiv" style="text-align:center; height:550px;">
                <div class="listdiv"  style="width:98%;">
                    <div class="setdiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td><input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />全选&nbsp;
                                    <select name="action" onchange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>
                                        <option value="" style="color:#666;">-----------</option>                                        
                                        <option value="pass" >&nbsp;&nbsp;标记为已审</option>
                                        <option value="unpass" >&nbsp;&nbsp;标记为待审</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select>&nbsp;
                                    <button class="btn" onclick="$.ShowJBox($('#newitem'));">搜索。。。</button></td>
                            </tr>
                        </table>
                    </div>
                    <ul class="ultab">
                        <li class="space">&nbsp;</li>
                        <li class="<%=checkTab(selected,0) %>"><a href="?selected=0" class="b">全部</a></li>
                        <li class="space">&nbsp;</li>
                        <li class="<%=checkTab(selected,1) %>"><a href="?selected=1&flag=0" class="b">待审</a></li>
                        <li class="space">&nbsp;</li>
                        <li class="<%=checkTab(selected,2) %>"><a href="?selected=2&reply=1" class="b">未回复</a></li>
                        <li class="tabmsg">&nbsp;</li>              
                    </ul>
                    <div >
                        <%
                        
                        set rs=feedback.load("",where,"order by a.id desc")
                        MaxItemSize=10
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<div style='color:red;font-size:14px;margin:50px;'>没有匹配的记录!!</div>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        
                        %>
                        <% for i=1 to CurrentRecord(rs,CurrentPage) %>
                        <table cellpadding="3" cellspacing="1" class="table1" border="0" style="margin-bottom: 10px;
                            border: solid 1px lightblue;">
                            <tr >
                                <td style="width:180px;" class="td02b" style="background:aliceblue;line-height:150%;">
                                    <div style="border-bottom:solid 1px lightblue; "><table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="25"><input type="checkbox" class="idset" title="<%=rs.fields("id") %>" name="idset" value="<%=rs.fields("id") %>" align="absmiddle" /></td>
                                        <td style="padding-top:4px;"><span style="color: Green; font-weight:bolder;"><%=rs.fields("username") %></span> <%=rs.fields("ip") %><% if rs.fields("flag")=0 then %>&nbsp;<span style="color:Red;">[待审]</span><% end if %></td>
                                    </tr>
                                    </table></div>
                                    <% if rs.fields("email")<>"" then %><%=rs.fields("email") %><br /><% end if %>
                                    <% if rs.fields("tel")<>"" then %><b>Tel:</b><%=rs.fields("tel") %> <br /><% end if %>
                                    <% if rs.fields("fax")<>"" then %><b>Fax:</b><%=rs.fields("fax") %><br /><% end if %>
                                    </td>
                                <td class="td02b" style="line-height:150%;">
                                <div style="border-bottom:solid 1px lightblue;padding-bottom:1px; margin-bottom:4px;">发表于:<%=FormatDateTime(rs.fields("addtime"),2) %>&nbsp;
                                    <%=FormatDateTime(rs.fields("addtime"),4) %></div>
                                    <% if rs.fields("title")<>"" then %><span style="font-weight: bolder; color: Black;"><%=rs.fields("title") %></span><br /> <% end if %>
                                <%=rs.fields("content") %><br />
                                
                                <% if rs.fields("reply")<>"" then %>
                                    <div style="margin: 10px; border: dashed 1px green; padding: 6px;">
                                        <span style="color: Green;">回复于
                                            <%=rs.fields("replytime") %>
                                        </span> <a href="feedback_reply.asp?id=<%=rs.fields("id") %>" class="b">修改</a>
                                        <br />
                                        <%=rs.fields("reply") %>
                                    </div>
                                <% else %>
                                    <a href="feedback_reply.asp?id=<%=rs.fields("id") %>" class="b">回复</a>
                                <% end if %>
                                </td>
                            </tr>
                        </table>
                        <% rs.movenext
                               
                               next
                            %>
                            <%end if %>
                        <% rs.close:set rs=nothing %>
                    </div>
                                      
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

