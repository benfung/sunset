﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--
        // 在document就绪或加载完成时执行
        $(document).ready(function() {
            $.slipTab(1, 1, "Tab", "formTabA", "formTabB", "click", 1);
        });        
        
      -->
    </script>
</head>
<% 
dim idate,df
idate=getquerystring("idate",now())
df=getquerystring("df","json1.asp")
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titleNav">
                订单量统计信息
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">天</td>
                        <td class="formTabMsg">&nbsp;</td>
                    </tr>
                </table>
                <form  name="webForm" class="aform" >
                <div class="div1" id="Tab1_Box">
                    <div style="margin-bottom:10px;">
                        <input type="text" id="idate1" onClick="setday(this)"  class="text"
                            value="<%=shortdate(idate) %>" />
                        <input type="button" name="button" class="btn" value=" 显示 " onClick="window.location='?df=<%=df %>&idate=' + idate1.value;" /></div>
                    <div id="my_day" class="div2" >
                    </div>
                    <script>
                        swfobject.embedSWF("../content/plugIn/open-flash-chart.swf", "my_day", "100%", "400", "9.0.0", "../content/plugIn/expressInstall.swf", { "data-file": "<%=df %>?idate=<%=idate %>" });
                    </script>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>