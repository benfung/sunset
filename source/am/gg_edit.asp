﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/conn.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="../inc/md5.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
set myclass=new classobj
myclass.typename="gg"

dim action,id,ads
action=getquerystring("action","")
id=getquerystring("id",0)
set ads=new adsObj
ads.adsObj(id)

classid=getquerystring("classid",ads.classid)
myclass.classobj(classid)

' 设置默认值
if ads.isnull then
    ads.status=1 ' 默认已审核
end if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
            if($.trim(form.title.value)==""){
                alert("请填写标识名!!");
                form.title.focus();
                return false;
            }
        }
      -->
    </script>

</head>
<%

if action="save" then
    ads.userid=admin.id
	ads.classid=myclass.id
	ads.title=TurnSql(getForm("title",""))
	ads.adtext=TurnSql(getForm("adtext",""))
	ads.ext=TurnSql(getForm("ext","pic"))
	
	ads.linkurl=TurnSql(getForm("linkurl",""))
	ads.linkalt=TurnSql(getForm("linkalt",""))
	ads.filename=TurnSql(getForm("filename",""))
	ads.width=getForm("width",0)
	ads.height=getForm("height",0)
	ads.other=TurnSql(getForm("other",0))
	ads.sort=TurnSql(getForm("sort",0))
	ads.dtime=getForm("dtime",now())
	ads.overtime=getForm("overtime",now())
	ads.status=getForm("status",0)
		
	if lcase(right(ads.filename,3))="swf" then ads.ext="flash"    

	if ads.isnull() then	    
		ads.add()
	else
		ads.update()		    
	end if
	
	if myclass.subname="点击广告" then
        call genggfile(ads.id,ads.ext,ads.linkurl,ads.filename,ads.width,ads.height,ads.linkalt)
    elseif myclass.subname="图片轮换" then
        call genpvfile(myclass.id)
    end if
    
	call toPage("gg.asp?classid=" & myclass.pid)
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                固定模块/广告
            </div>
            <div class="contentdiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            基本属性</td>
                        <td class="formTabMsg">
                            带*号为必填项</td>
                    </tr>
                </table>
                
                <form action="?action=save&id=<%=ads.id %>&classid=<%=myclass.id %>" name="webForm" class="aform" method="post" onSubmit="return checkForm(this)">
                    <% if myclass.subname="点击广告" then %>
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:120px;">
                            <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        栏 目：</td>
                                    <td class="td02b">
                                        <%=myclass.name %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>类 型：</td>
                                    <td class="td02">
                                        <input type="radio" name="ext" value="pic" /> 图片<br />
                                        <input type="radio" name="ext" value="flash" /> Flash
                                        <script type="text/javascript">                                       
                                        $.setElementValue(webForm.ext,"<%=ads.ext %>","pic");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>标识名：</td>
                                    <td class="td02">
                                        <input type="text" name="title" class="text" value="<%=ads.title %>"  style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>文 件：</td>
                                    <td class="td02">
                                        <input type="text"  name="filename" id="filename" class="text" value="<%=ads.filename %>" style="width: 250px;" />
                                        <a href="javascript:void(0);" onClick="DelFile(filename.value,filename)" class="c">删除</a>
                                        &nbsp;&nbsp;宽:<input type="text"  name="width" value="<%=ads.width %>" class="text" style="width: 40px;" />
                                        高:<input type="text"  name="height" value="<%=ads.height %>" class="text" style="width: 40px;" />
                                        
                                        <div>
                                            <span id="spanButtonPlaceHolder"></span>
                                        </div>
                                        <div class="fsUploadProgress" id="fsUploadProgress"><div></div>
                                        <img src="images/loadingAnimation.gif" align="absmiddle" />
                                        </div>
                                        <script>
                                            UploadInit("spanButtonPlaceHolder", "*.jpg;*.gif;*.png;*.swf", "图片;SWF", 0, 2000, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "filename", "false","fsUploadProgress")
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        链 接：</td>
                                    <td class="td02">
                                        <input type="text"  name="linkurl" class="text" value="<%=ads.linkurl %>" style="width: 250px;" /><span>(http://),此链接在Flash中不生效</span>
                                    </td>
                                </tr>  
                                <tr>
                                    <td class="td01">
                                        链接提示：</td>
                                    <td class="td02">
                                        <input type="text" name="linkalt" class="text" value="<%=ads.linkalt %>"  style="width: 250px;" />
                                    </td>
                                </tr>
                                <% if admin.Purview=1 then %>
                                <tr>
                                    <td class="td01">
                                        上次修改：</td>
                                    <td class="td02b">
                                        <% 
                                        set user=new adminObj
                                        user.adminObj(ads.userid)
                                        Response.Write user.loginid %>
                                    </td>
                                </tr>
                                <% end if %>
                                <!--<tr>
                                    <td class="td01">
                                        其他属性：</td>
                                    <td class="td02">
                                        <input type="checkbox" name="status" value="1" />
                                        启用
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.status,"<%=ads.status %>");
                                        </script>                                        
                                    </td>
                                </tr>   -->                           
                            </table>
                        </div>                        
                    </div>
                    <% elseif myclass.subname="图片轮换" then  %>
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:120px;">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        栏 目：</td>
                                    <td class="td02b">
                                        <%=myclass.name %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>标题：</td>
                                    <td class="td02">
                                        <input type="text" name="title" class="text" value="<%=ads.title %>"  style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>文 件：</td>
                                    <td class="td02">
                                        <input type="text"  name="filename" id="filename" class="text" value="<%=ads.filename %>" style="width: 250px;" />
                                        <a href="javascript:void(0);" onClick="DelFile(filename.value,filename)" class="c">删除</a>
                                            <span>宽:<%=myclass.width %> 高:<%=myclass.height %></span>
                                            <div>
                                            <span id="spanButtonPlaceHolder"></span>
                                        </div>
                                        <div class="fsUploadProgress" id="fsUploadProgress"><div></div>
                                        <img src="images/loadingAnimation.gif" align="absmiddle" />
                                        </div>
                                        <script>
                                            UploadInit("spanButtonPlaceHolder", "*.jpg;*.gif;*.png;*.swf", "图片;SWF", 0, 200, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "filename", "false", "fsUploadProgress")
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        链 接：</td>
                                    <td class="td02">
                                        <input type="text"  name="linkurl" class="text" value="<%=ads.linkurl %>" style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        其他属性：</td>
                                    <td class="td02">
                                        <input type="checkbox" name="status" value="1" />
                                        显示
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.status,"<%=ads.status %>");
                                        </script>                                        
                                    </td>
                                </tr>  
                                <% if admin.Purview=1 then %>
                                <tr>
                                    <td class="td01">
                                        上次修改：</td>
                                    <td class="td02b">
                                        <% 
                                        set user=new adminObj
                                        user.adminObj(ads.userid)
                                        Response.Write user.loginid %>
                                    </td>
                                </tr>
                                <% end if %>                         
                            </table>
                        </div>                        
                    </div>
                    <% elseif myclass.subname="资讯模块" then  %>
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:120px;">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        <span>*</span>栏 目：</td>
                                    <td class="td02b">
                                        <%=myclass.name %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>显示标题：</td>
                                    <td class="td02">
                                        <input type="text" name="title" class="text" value="<%=ads.title %>"  style="width: 250px;" />
                                        <a href="javascript:void(0);" onClick="webForm.title.value='<b>'+webForm.title.value+'</b>';">加粗</a> <a href="javascript:void(0);" onClick="webForm.title.value='<font style=\'color:red;\'>'+webForm.title.value+'</font>';">标红</a>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="td01">
                                        链 接：</td>
                                    <td class="td02">
                                        <input type="text"  name="linkurl" class="text" value="<%=ads.linkurl %>" style="width: 250px;" />
                                    </td>
                                </tr>  
                                <tr>
                                    <td class="td01">
                                        链接提示：</td>
                                    <td class="td02">
                                        <input type="text" name="linkalt" class="text" value="<%=ads.linkalt %>"  style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        图片：</td>
                                    <td class="td02">
                                        <input type="text"  name="filename" id="filename" class="text" value="<%=ads.filename %>" style="width: 250px;" />
                                        <a href="javascript:void(0);" onClick="DelFile(filename.value,filename)" class="c">删除</a>
                                        &nbsp;&nbsp;宽:<input type="text"  name="width" value="<%=ads.width %>" class="text" style="width: 40px;" />
                                        高:<input type="text"  name="height" value="<%=ads.height %>" class="text" style="width: 40px;" />
                                        <div>
                                            <span id="spanButtonPlaceHolder"></span>
                                        </div>
                                        <div class="fsUploadProgress" id="fsUploadProgress"><div></div>
                                        <img src="images/loadingAnimation.gif" align="absmiddle" />
                                        </div>
                                        <script>
                                            UploadInit("spanButtonPlaceHolder", $.FileType["Image"], "图片文件", 0, 200, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "filename", "false", "fsUploadProgress")
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        文字：</td>
                                    <td class="td02">
                                        <textarea name="adtext" class="textarea" rows="3" cols="60"><%=ads.adtext %></textarea>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        日期时间：</td>
                                    <td class="td02">
                                        <input type="text" name="dtime" class="text" value="<%=ads.dtime %>"  style="width: 250px;" />
                                    </td>
                                </tr>
                                <% if admin.Purview=1 then %>
                                <tr>
                                    <td class="td01">
                                        上次修改：</td>
                                    <td class="td02b">
                                        <% 
                                        set user=new adminObj
                                        user.adminObj(ads.userid)
                                        Response.Write user.loginid %>
                                    </td>
                                </tr>
                                <% end if %>
                                <!--<tr>
                                    <td class="td01">
                                        其他属性：</td>
                                    <td class="td02">
                                        <input type="checkbox" name="status" value="1" />
                                        启用
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.status,"<%=ads.status %>");
                                        </script>                                        
                                    </td>
                                </tr>-->                           
                            </table>
                        </div>                        
                    </div>
                    <% end if %>
                    <div class="submit">
                        <button type="submit" class="btn">保存修改</button>&nbsp;&nbsp;<button type="button" onClick="window.location='gg.asp?classid=<%=myclass.pid %>';"  class="btn">返 回</button></div>
                </form>
                
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>


<%
' 生成单击广告脚本
sub genggfile(tempid,fileExt,linkurl,filename,width,height,linkalt)
    dim Result,h
    Result=""
    h=""
    if filename="" then
        Result="document.write('');"
    else
        if fileExt="pic"  then
            if linkurl<>"" then h="href='" & linkurl & "'"
            Result="document.write(" & chr(34) & "<a " & h & " target='_blank'><img src='" & filename & "' border='0' width='" & width & "' height='" & height & "' alt='" & linkalt & "' align='absmiddle' /></a>" & chr(34) & ");"
        else
            Result="document.write(" & chr(34) & "<embed pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' src='" & filename & "' width='" & width & "' height='" & height & "' type='application/x-shockwave-flash' wmode='opaque' align='absmiddle'></embed>" & chr(34) & ");"
        end if
    end if
    WriteTextFile Server.MapPath("../gg_" & tempid & ".js"),Result,conDefaultPageCharset
end sub

' 生成轮播广告脚本
sub genpvfile(cid)    
    dim Result,apics,alinks,atexts,width,height
    Result="pixview" & cid & "=function(){" & vbLf
    
    set rs=ads.load("","where a.status=1 and a.filename<>'' and a.classid=" & cid,"order by a.sort,a.id")
    for i=1 to rs.recordcount
        width=myclass.width
        height=myclass.height
        apics = apics & rs.fields("filename")
        alinks = alinks & rs.fields("linkurl")
        atexts = atexts & rs.fields("title")
        rs.movenext
        if not rs.eof then
            apics = apics & "|"
            alinks = alinks & "|"
            atexts = atexts & "|"
        end if
    next
    rs.close:set rs=nothing
    
    Result=Result & "var focus_width=" & width & ";" & vbLf
    Result=Result & "var focus_height=" & height & ";" & vbLf
    Result=Result & "var text_height=0;" & vbLf
    Result=Result & "var swf_height = focus_height+text_height;" & vbLf
    Result=Result & "var pics = '" & apics & "';" & vbLf
    Result=Result & "var links = '" & alinks & "';" & vbLf
    Result=Result & "var texts = '" & atexts & "';" & vbLf
    
    Result=Result & "document.write('<object classid=" & chr(34) & "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" & chr(34) & " codebase=" & chr(34) & "http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" & chr(34) & " width=" & chr(34) & "'+ focus_width +'" & chr(34) & " height=" & chr(34) & "'+ swf_height +'" & chr(34) & ">');" & vbLf
    Result=Result & "document.write('<param name=" & chr(34) & "allowScriptAccess" & chr(34) & " value=" & chr(34) & "sameDomain" & chr(34) & "><param name=" & chr(34) & "movie" & chr(34) & " value=" & chr(34) & "plugIn/pixviewer.swf" & chr(34) & "><param name=" & chr(34) & "quality" & chr(34) & " value=" & chr(34) & "high" & chr(34) & "><param name=" & chr(34) & "bgcolor" & chr(34) & " value=" & chr(34) & "#F0F0F0" & chr(34) & ">');" & vbLf
    Result=Result & "document.write('<param name=" & chr(34) & "menu" & chr(34) & " value=" & chr(34) & "false" & chr(34) & "><param name=" & chr(34) & "wmode" & chr(34) & " value=" & chr(34) & "opaque" & chr(34) & ">');" & vbLf
    Result=Result & "document.write('<param name=" & chr(34) & "FlashVars" & chr(34) & " value=" & chr(34) & "pics='+pics+'&links='+links+'&texts='+texts+'&borderwidth='+focus_width+'&borderheight='+focus_height+'&textheight='+text_height+'" & chr(34) & ">');" & vbLf
    Result=Result & "document.write('<embed src=" & chr(34) & "plugIn/pixviewer.swf" & chr(34) & " wmode=" & chr(34) & "opaque" & chr(34) & " FlashVars=" & chr(34) & "pics='+pics+'&links='+links+'&texts='+texts+'&borderwidth='+focus_width+'&borderheight='+focus_height+'&textheight='+text_height+'" & chr(34) & " menu=" & chr(34) & "false" & chr(34) & " bgcolor=" & chr(34) & "#F0F0F0" & chr(34) & " quality=" & chr(34) & "high" & chr(34) & " width=" & chr(34) & "'+ focus_width +'" & chr(34) & " height=" & chr(34) & "'+ focus_height +'" & chr(34) & " allowScriptAccess=" & chr(34) & "sameDomain" & chr(34) & " type=" & chr(34) & "application/x-shockwave-flash" & chr(34) & " pluginspage=" & chr(34) & "http://www.macromedia.com/go/getflashplayer" & chr(34) & " />');" & vbLf
    Result=Result & "document.write('</object>');" & vbLf
	
	Result=Result & "document.write('<object classid=" & chr(34) & "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" & chr(34) & " codebase=" & chr(34) & "http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" & chr(34) & " width=" & chr(34) & "'+ focus_width +'" & chr(34) & " height=" & chr(34) & "'+ focus_height +'" & chr(34) & ">');" & vbLf
    Result=Result & "document.write('<param name=" & chr(34) & "movie" & chr(34) & " value=" & chr(34) & "plugIn/bcastr.swf" & chr(34) & "><param name=" & chr(34) & "quality" & chr(34) & " value=" & chr(34) & "high" & chr(34) & ">');" & vbLf
    Result=Result & "document.write('<param name=" & chr(34) & "menu" & chr(34) & " value=" & chr(34) & "false" & chr(34) & "><param name=" & chr(34) & "wmode" & chr(34) & " value=" & chr(34) & "opaque" & chr(34) & ">');" & vbLf
    Result=Result & "document.write('<param name=" & chr(34) & "FlashVars" & chr(34) & " value=" & chr(34) & "bcastr_file=' + pics + '&bcastr_link=' + links + '&bcastr_title=' + texts + '&IsShowBtn=0" & chr(34) & ">');" & vbLf
    Result=Result & "document.write('<embed src=" & chr(34) & "plugIn/bcastr.swf" & chr(34) & " wmode=" & chr(34) & "opaque" & chr(34) & " FlashVars=" & chr(34) & "bcastr_file=' + pics + '&bcastr_link=' + links + '&bcastr_title=' + texts + '&IsShowBtn=0" & chr(34) & " menu=" & chr(34) & "false" & chr(34) & " quality=" & chr(34) & "high" & chr(34) & " width=" & chr(34) & "'+ focus_width +'" & chr(34) & " height=" & chr(34) & "'+ focus_height +'" & chr(34) & " type=" & chr(34) & "application/x-shockwave-flash" & chr(34) & " pluginspage=" & chr(34) & "http://www.macromedia.com/go/getflashplayer" & chr(34) & " />');" & vbLf
    Result=Result & "document.write('</object>');" & vbLf

    
    Result=Result & "}" & vbLf
    Result=Result & "pixview" & cid & "();"
    WriteTextFile Server.MapPath("../picviewer_" & cid & ".js"),Result,conDefaultPageCharset
end sub
%>