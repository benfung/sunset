﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% dim myclass:set myclass=new classObj %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title>栏目管理</title>    
    <!--#include file="headmeta.asp"-->
    <style type="text/css">
        BODY { background: #4096ee; margin: 0px auto; text-align: center; font-family: "宋体" , Verdana, Arial, Helvetica, sans-serif; }
        table { width: 100%; border: 0px; border-collapse: collapse; }
        td, div { font-family: Tahoma,Simsun,Helvetica,sans-serif; font-size: 12px; text-align: left; vertical-align: top; color: #000; }
        img { border: 0px; }
        a:link, a:visited { color: #000; text-decoration: none; }
        a:hover { color: #f00; text-decoration: none; }
        .md { margin: 0px auto; text-align: center; }
        .mt { padding: 4px 8px 0px; color: #215dc6; background: url('images/title_bg_quit.gif') no-repeat; height: 25px; width: 170px; font-weight: bolder; }
        .mt a:link, .mt a:visited { color: #215dc6; }
        .mt a:hover { color: #215dc6; text-decoration: underline; }
        .sm { background: #fff; overflow: hidden; border: solid 1px white; border-top: 0px; }
        .sm div { width: 100%; background: #d6dff7; padding: 6px 10px; line-height: 150%; }
        .menu { background: url('images/menudown.gif') no-repeat; cursor: pointer; font-weight: bolder; padding: 4px 8px 0px; color: #215dc6; }
        .subMenu { width: 170px; background: #d6dff7; overflow: hidden; border: solid 1px white; border-top: 0px; line-height: 150%; margin-bottom: 12px; padding: 6px 10px; }
        .subMenu a:link, .subMenu a:visited { color: #000; text-decoration: none; }
        .subMenu a:hover { color: #f00; text-decoration: none; }
        .sp { color: Green; }
    </style>

<script type="text/javascript">
function expand(obj)
{
    var subName=obj.id+"_sub";
    var subObj=$("#"+subName)[0];
    
    
    $(".subMenu").css("display","none");
    $(".Menu").css("background ","url('image/menudown.gif')");
    
    if(subObj.style.display=="none")
    {
        subObj.style.display="";
        obj.style.BACKGROUND="url('image/menuup.gif')";
    }
}


</script>

<script language="javascript">
    var menuvar=0;
    function addtitle(name){
        menuvar+=1;
        document.writeln("<div class=\"md\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"margin-bottom:1px;width:170px;\"><tr><td id=\"s" + menuvar + "\" class=\"Menu\" onmouseover=\"this.style.color='#428eff';\" onmouseout=\"this.style.color='#215dc6';\" onclick=\"expand(this)\" height=\"25\">" + name + "</td></tr></table></div>");
        document.writeln("<div class=\"md\"><div class=\"subMenu\" id=\"s" + menuvar + "_sub\" style=\"display:none;\" ></div></div>");
        return document.getElementById("s"+menuvar+"_sub");
    }
    function additem(obj,name,link,target,link2){
        var Result;
        if(target==null)target="main";
        if(link2==null)link2="";
        Result="<a target=\"" + target + "\" href=\"" + link + "\">" + name + "</a>" + link2 + "<br />"
        obj.innerHTML+=Result;        
    }
</script>
</head>
<body>
    <div class="md" style="margin-bottom:2px;"><img src="images/title.gif" alt="" align="absmiddle"></div>
    <div class="md" style="margin-bottom:1px;">
        <div class="mt">
                <a href="sysindex.asp" target="main">管理首页</a> | <a target="_top" href="signin.asp?action=logout&rurl=signin.asp">退出</a></div>
    </div>
    <div class="md" style="margin-bottom:12px;">
        <div class="sm" style="width: 170px;">
            <div>
            当前管理员：<%=admin.loginid %>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var t;
//        /// 系统管理 ///////////////////////////////////////
<% if admin.hasPower(1) or admin.Purview=1 then %>
        t=addtitle("系统管理");
        additem(t,"系统管理员设置","admin.asp");
        additem(t,"网站公告","callboard.asp");
        <% if admin.Purview=1 then %>
        additem(t,"频道管理","channelClass.asp");
        additem(t,"标签管理","tagClass.asp");
        additem(t,"系统登陆记录","log.asp?usertype=admin");
        additem(t,"支付/送货","payclass.asp");
        additem(t,"支付平台","payplatform.asp");  
        additem(t,"国家/城市管理","areaclass.asp");        
        additem(t,"备份文件","../it%20is%20backup.asp?a=21232F297A57A5A743894A0E4A801FC3");
        <% end if %>        
        <% end if %>
//        additem(t,"上传文件管理","FileManage.asp");
//        /// 固定模块 //////////////////////////////////////
<% if admin.hasPower(512) or admin.Purview=1 then %>
        t=addtitle("固定模块/广告");     
        <% if admin.Purview=1 then %>   
        additem(t,"位置设置","ggClass.asp");    
        <% end if %>
        <% set rs=myclass.load("","where a.typename='gg' and a.pid=0","order by a.sort,a.id") %>
        <% for i=1 to rs.recordcount %>
        additem(t,"<%=rs.fields("name") %>","gg.asp?classid=<%=rs.fields("id") %>");
        <% rs.movenext:next %>    
        <% rs.close:set rs=nothing %>
<% end if %> 
<%if conOpenAsp2Html=1 then %>
        t=addtitle("生成静态页");  
        additem(t,"生成自定静态页","javascript:if(confirm('确定要重新生成?'))window.parent.main.location='genhtm.asp'"); 
        additem(t,"生成商品页","javascript:if(confirm('确定要重新生成?'))window.parent.main.location='genprohtm.asp'");
        additem(t,"生成新闻页","javascript:if(confirm('确定要重新生成?'))window.parent.main.location='gennewshtm.asp'");
<% end if %>    
//        /// 企业信息 /////////////////////////////////////////
<% if admin.hasPower(2) or admin.Purview=1 then %>
        t=addtitle("企业信息");        
        <% if admin.Purview=1 then %>
        additem(t,"添 加","main_edit.asp");
        <% end if %>
        additem(t,"查 询","main.asp");
        <% set main=new mainObj %>
        <% set rs=main.load("","","order by a.id ") %>
        <% for i=1 to rs.RecordCount %>
        additem(t," <span class='sp'> └ <%=rs.fields("title") %></span>","main_edit.asp?id=<%=rs.fields("id") %>");
        <% rs.movenext:next %>
        <% rs.close:set rs=nothing %>
<% end if %>
//        /// 会员管理 /////////////////////////////////////////////
<% if admin.hasPower(4) or admin.Purview=1 then %>
        t=addtitle("会员管理");        
        additem(t,"查 询","user.asp");
        additem(t," <span class='sp'>└ 普通会员</span>","user.asp?userlevel=0");
        additem(t," <span class='sp'>└ 高级会员</span>","user.asp?userlevel=1");
        additem(t," <span class='sp'>└ VIP会员</span>","user.asp?userlevel=2");
        additem(t,"----------------","javascript:void(0);");
        additem(t," <span class='sp'>└ 待审会员</span>","user.asp?status=0");
        additem(t," <span class='sp'>└ 被锁定的会员</span>","user.asp?lockuser=1");
        <% if admin.Purview=1 then %>
        additem(t,"会员登陆记录","log.asp?usertype=myuser");
        <% end if %>
        additem(t,"邮件列表管理","maillist.asp");
<% end if %>
//        /// 产品管理 ///////////////////////////////////////////
<% if admin.hasPower(8) or admin.Purview=1 then %>
        t=addtitle("产品管理");    
        <% if conOpenQuickMode=1 then %>
        additem(t,"栏目管理","productClass.asp");
        additem(t,"----------------","javascript:void(0);");
        <% call QuickMode("product","product.asp","product_edit.asp",2) %>
        <% else %>
        additem(t,"栏目管理","productClass.asp");
        additem(t,"----------------","javascript:void(0);");
        additem(t,"添 加","product_edit.asp");
        additem(t,"查 询","product.asp"); 
        <% call QuickMode("product","product.asp","product_edit.asp",2) %>     
        <% end if %>  
<% end if %>
//        /// 订单管理 /////////////////////////////////////////
<% if admin.hasPower(16) or admin.Purview=1 then %>
        t=addtitle("订单管理");        
        additem(t,"查 询","orderform.asp");
        additem(t," <span class='sp'>└ 今天的订单</span>","orderform.asp?idate=<%=shortdate(date()) %>");
        additem(t," <span class='sp'>└ 昨天的订单</span>","orderform.asp?idate=<%=shortdate(DateAdd("d",-1,date())) %>");
        additem(t,"----------------","javascript:void(0);");
        additem(t," <span class='sp'>└ 已发货</span>","orderform.asp?status=4");
        additem(t," <span class='sp'>└ 有效正在配货</span>","orderform.asp?status=3");
        additem(t," <span class='sp'>└ 等待支付</span>","orderform.asp?status=2");
        additem(t," <span style='color:red;'>└ 等待处理</span>","orderform.asp?status=1");
        additem(t,"----------------","javascript:void(0);");
        additem(t," <span  class='sp'>└ 完成</span>","orderform.asp?status=5");         
        additem(t," <span style='color:dimgray;'>└ 已失效</span>","orderform.asp?status=0"); 
        additem(t," <span style='color:indigo;'>└ 退货</span>","orderform.asp?status=6");   
        <% if admin.Purview=1 then %>
            additem(t,"购物车记录","shoppingcart.asp");
            additem(t,"在线支付记录","payrecord.asp");
            additem(t,"----------------","javascript:void(0);");
            additem(t," <span class='sp'>└ 成功的支付</span>","payrecord.asp?status=2");
        <% end if %> 
<% end if %>
//        /// 统计管理 /////////////////////////////////////////
<% if admin.Purview=1 then %>
    t=addtitle("订单统计");
    additem(t,"订单量","orderscount.asp?df=json1.asp");
	additem(t,"销量排行","orderscount.asp?df=json2.asp");  
    additem(t,"订购额","orderscount.asp?df=json3.asp"); 
<% end if %>
//        /// 新闻管理 //////////////////////////////////////////
<% if admin.hasPower(32) or admin.Purview=1 then %>
        t=addtitle("新闻/资讯管理");        
        <% if conOpenQuickMode=1 then %>
        additem(t,"栏目管理","newsClass.asp");
        additem(t,"----------------","javascript:void(0);");
        <% call QuickMode("news","news.asp","news_edit.asp",2) %>
        <% else %>
        <% if admin.Purview=1 then %> 
            additem(t,"栏目管理","newsClass.asp");
            additem(t,"----------------","javascript:void(0);");
        <% end if %>        
        additem(t,"添 加","news_edit.asp");
        additem(t,"查 询","news.asp");
        <% call QuickMode("news","news.asp","news_edit.asp",2) %>
        <% end if %>
        <% if OpenNewsComment = "1" then%>
        //additem(t,"管理新闻评论","Admin_Comment.asp");
        <% end if %>
<% end if %>
//        /// 留言管理 //////////////////////////////////////////
<% if admin.hasPower(64) or admin.Purview=1 then %>
        t=addtitle("留言管理");        
        additem(t,"查 询","feedback.asp");
<% end if %>
<% if admin.hasPower(128) or admin.Purview=1 then %>
        /// 调查管理 ////////////////////////////////////////////
        t=addtitle("调查管理"); 
        additem(t,"添 加","vote_edit.asp");       
        additem(t,"查 询","vote.asp");        
<% end if %>
//        /// 友情链接 //////////////////////////////////////
<% if admin.hasPower(1024) or admin.Purview=1 then %>
        t=addtitle("友情链接");  
        additem(t,"添 加","Links_edit.asp");      
        additem(t,"查 询","Links.asp");
        <% if admin.Purview=1 then %>
        additem(t,"类别管理","linksclass.asp");
        <% end if %>
<% end if %>  
//        /// "数据库管理 /////////////////////////////////////////  
<% if admin.Purview=1 then %>
        t=addtitle("数据库管理");
        additem(t,"备份/还原","db_backup.asp");
        additem(t,"压缩Access数据库","db_compact.asp");
<% end if %>
    </script>
<div class="md" style="margin-bottom:1px;">
        <div class="mt" >
                系统信息</div>
    </div>
    <div class="md" style="margin-bottom: 12px;">
        <div class="sm" style="width: 170px;">
            <div>
                技术支持：<a href="<%=conAuthorurl %>" target="_blank"><%=conAuthor %></a><br />
                联系电话：<%=conAuthorPhone %>
            </div>
        </div>
    </div>
</body>
</html>
<%call closeconn %>


<% 
' 快速通道模式
' typename:对应类表的typename字段
' file1:列表文件名(如:news.asp)
' file2:编辑文件名(如:news_eidt.asp)
' rank:开放级别(一般地,新闻为2,产品为3)
sub QuickMode(typename,file1,file2,rank)
    dim myclass,rs,rs1,rs2,classid1,classid2,i
    set myclass=new classobj:myclass.typename=typename
    ' 一级 //////////////////////////////////
    set rs=myclass.load("","where a.pid=0 and a.typename='" & typename & "'","order by a.sort,id")    
    for i=1 to rs.RecordCount
        if rank>1 then
            Response.Write "additem(t,'" & rs.fields("name") & "','" & file1 & "?classid=" & rs.fields("id") & "','main');"
        else
            Response.Write "additem(t,'" & rs.fields("name") & "','" & file1 & "?classid=" & rs.fields("id") & "','main');"
        end if
        ' 二级 //////////////////////////////
        if rs.fields("child")>0 and rank>1 then
            classid1=rs.fields("id")
            set rs1=myclass.load("","where a.pid=" & classid1 & " and a.typename='" & typename & "'","order by a.sort,id")
            do while not rs1.eof
                if rank>2 then
                    Response.Write "additem(t,'&nbsp;└ " & rs1.fields("name") & "','" & file1 & "?classid=" & rs1.fields("id") & "');"
                else
                    Response.Write "additem(t,'&nbsp;└ " & rs1.fields("name") & "','" & file1 & "?classid=" & rs1.fields("id") & "');"
                end if
                ' 三级 //////////////////////////////
                if rs1.fields("child")>0 and rank>2 then
                    classid2=rs1.fields("id")
                    set rs2=myclass.load("","where a.pid=" & classid2 & " and a.typename='" & typename & "'","order by a.sort,id")
                    do while not rs2.eof
                        if rank>3 then
                            Response.Write "additem(t,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└ " & rs2.fields("name") & "','" & file1 & "?classid=" & rs2.fields("id") & "');"
                        else
                            Response.Write "additem(t,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└ " & rs2.fields("name") & "','" & file1 & "?classid=" & rs2.fields("id") & "');"
                        end if
                        rs2.movenext
                    loop
                    rs2.close:set rs2=nothing
                end if
                ' 三级 /////////////////////////// END
                rs1.movenext
            loop
            rs1.close:set rs1=nothing
        end if
        ' 二级 /////////////////////////// END
        rs.movenext        
    next
    ' 一级 /////////////////////////// END
    rs.close:set rs=nothing
end sub
%>