﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/conn.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,id,fso,feedback
action=getquerystring("action","")
id=getquerystring("id",0)

set feedback=new feedbackObj
feedback.feedbackObj(id)

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
            if($.trim(form.title.value)==""){
                alert("请填写标题!!");
                form.title.focus();
                return false;
            }
        }
      -->
    </script>

</head>
<%
if action="save" then
    feedback.types=1
    feedback.username=conSiteName
    feedback.title=getform("title","")
    feedback.content=encode(getform("content",feedback.title))
    feedback.flag=1
    
    if feedback.isnull() then
        feedback.add()		
		call confirm("操作成功<br />是否继续添加","?","callboard.asp")       
    else
        feedback.update()
        call topage("callboard.asp")       
    end if
end if

%>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                <a href="callboard.asp">网站公告</a>
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabMsg" style="padding:0px 0px; font-size:0px;">&nbsp;
                            </td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=id %>" name="webForm" class="aform" method="post" onSubmit="return checkForm(this)">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:100px;">
                            <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">                                
                                <tr>
                                    <td class="td01">
                                        <span>*</span>标题：</td>
                                    <td class="td02">
                                        <input type="text" name="title" class="text" value="<%=feedback.title %>" style="width: 150px;" />
                                        <span>如:五一休假</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>内容：</td>
                                    <td class="td02">
                                        <textarea class="textarea" style="width:100%" rows="14" name="content"><%=unencode(feedback.content) %></textarea>
                                    </td>
                                </tr>                                
                            </table>
                        </div>                        
                    </div>
                    <div class="submit">
                        <button type="submit" class="btn">提 交</button>&nbsp;&nbsp;<button type="button" onClick="window.location='callboard.asp';"  class="btn">返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
