﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<%
set admin=new adminObj
admin.AdminObjByStatus()
if admin.isnull() then
    response.Write  "刷新吧"
	response.End()
end if
%>
<% 
dim orders
set orders=new ordersobj

dim product
set product=new productobj
    
dim idate,sd,ed,days
idate=cdate(getquerystring("idate",now()))
sd=Formatdatetime(DateAdd("d",-7,idate),2)
ed=Formatdatetime(idate,2)
days=DateDiff("d",sd,ed)
sql="SELECT top 10 sum(amount) as n,productid from (orderitem a inner join orders b on a.orderid=b.id) inner join product c on a.productid=c.id where b.inputtime between #" & sd & "# and #" & ed & "# group by a.productid order by sum(amount) desc"
dim rs
set rs=Server.CreateObject("Adodb.Recordset")
rs.open sql,conn,1,1
max=5
%>
{
  "title":{
    "text": "一周销量排行  <%=shortdate(sd) %> -- <%=shortdate(ed) %>",
			"style": "{font-size: 18px; color:#000; text-align: right;margin-bottom:10px;margin-right:30px;}"
  },
"grid_colour": "#eeeeee",
"bg_colour":"#ffffff",
  "elements":[
    {
      "tip" : "#val#",
      "type":      "bar_glass",
      "alpha":     0.5,
      "colour":    "#0000fa",
      "font-size": 12,
      "values" :   [
      <% for i=1 to 10 %>
      <% if not rs.eof then %>
      <% product.productobj(rs.fields("productid")) %>
      {"top":<%=rs.fields("n")%>,"on-click":  "http://127.0.0.1/productdetail.asp?id=<%=rs.fields("productid")%>", "tip": "<%=product.title%><br>#val#"}
      <% rs.movenext %>
      <% else %>
      0
      <% end if %>
      
      <% if i<10 then response.write(",") %>
      <% next %>
      ]
    }
  ],

  "x_axis":{
    "stroke":       1,
    "tick_height":  10,
    "colour":      "#aaaaaa",
    "grid_colour": "#eeeeee",
    "labels": {
      "labels": [
      "1","2","3","4","5","6","7","8","9","10"
      ]
    }
   },

  "y_axis":{
    "stroke":      1,
    "tick_length": 3,
    "colour":      "#aaaaaa",
    "grid_colour": "#eeeeee",
    "offset": false,
	"steps": 5,
    "max":         50
  },
  "y_axis_right":{
    "stroke":      1,
    "tick_length": 3,
    "colour":      "#aaaaaa",
    "grid_colour": "#eeeeee",
    "offset": false,
	"steps": 5,
    "max":         50
  }

}

<% rs.close:Set rs=nothing %>
<%call closeconn %>

<%
function CWeekDay(d)
    dim result    
    select case Weekday(d)
        case 1 result="星期日"
        case 2 result="星期一"
        case 3 result="星期二"
        case 4 result="星期三"
        case 5 result="星期四"
        case 6 result="星期五"
        case 7 result="星期六"
        case else result="星期日"
    end select
    CWeekDay=result
end function
    

%>