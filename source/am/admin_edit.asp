﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="../inc/md5.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,id,aadmin
action=getquerystring("action","")
id=getquerystring("id",0)

set aadmin=new AdminObj
aadmin.AdminObj(id)

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站配置</title>
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
            if($.trim(form.username.value).length<4){
                alert("用户名长度不能少于4!!");
                form.username.focus();
                return false;
            }
            if($.trim(form.password.value).length<4){
                alert("密码长度不能少于4!!");
                form.password.focus();
                return false;
            }
        }
      -->
    </script>

</head>
<%
if action="save" then
    if len(getform("username",""))< 4 then
        call alertback("用户名长度不少于4！")
    end if
    if len(getform("password",""))< 4 then
        call alertback("密码长度不少于4！")
    end if
    aadmin.loginid=TurnSql(getform("username",""))
    aadmin.pwd=GenPasswrod(getform("password",""),aadmin.loginid)
    
    if aadmin.isnull() then
        if aadmin.HasName() then
            call alertback("该用户名已存在！")
        end if
        aadmin.power=admin.power
        aadmin.add()
        call Alert("添加成功","admin.asp")        
    else
        aadmin.update()
    end if
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                <a href="admin.asp">系统管理员</a> <span class="titleNavSpot">&nbsp;</span>
                <% if aadmin.isnull then %>
                添加
                <% else %>
                编辑
                <% end if %>
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">
                            &nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            编辑</td>
                        <td class="formTabMsg">
                            带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=aadmin.id %>" name="webForm" class="aform" method="post" onsubmit="return checkForm(this)">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:100px;">
                            <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        <span>*</span>用户名：</td>
                                    <td class="td02">
                                        <input type="text" name="username" class="text" value="<%=aadmin.loginid %>" <% if not aadmin.isnull then %> readonly="readonly" <% end if %> style="width: 150px;" />
                                        <span>用户名长度不能少于4,如:Joey</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>密&nbsp;&nbsp;码：</td>
                                    <td class="td02">
                                        <input type="password"  name="password" class="text" style="width: 150px;" />
                                        <span>密码长度不能少于4</span>
                                    </td>
                                </tr>
                            </table>
                        </div>                        
                    </div>                    
                    <div class="submit">
                        <button type="submit" class="btn">保存修改</button>&nbsp;&nbsp;<button type="button" onClick="window.location='admin.asp';"  class="btn">返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
