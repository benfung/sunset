﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/conn.asp"-->

<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
set myclass=new classobj
myclass.typename="gg"

dim action,idset,idArray,classid,where

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

classid=getquerystring("classid",0)
filePath="?"
if classid="0" then 
    call topage("ggclass.asp")
end if

myclass.classobj(classid)


%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>classid=<%=classid %>&action=" + action + "&idset=" + sets
            }
        }        
        
        function getCode(value){
            ViewCode.value="<scr"+"ipt type='text/javascript' src='gg_" + value + ".js'></s"+"cript>";
        }
        function getCode2(value){
            ViewCode.value="<scr"+"ipt type='text/javascript' src='picviewer_" + value + ".js'></s"+"cript>";
        }
      -->
    </script>

</head>

<%
if action="del" then
    conn.execute "delete from ads where id in (" & idset & ")"
    call Alert("操作成功执行!","?classid=" & classid)
end if

if myclass.child=0 then
    call alert("请先添加子栏目","ggclass.asp")
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                位置:<%=myclass.name %>
            </div>
            <div class="contentdiv" style="text-align:center; height:480px;">
                <div class="searchdiv">
                    <% if admin.Purview=1 then %>
                    <div style="margin-bottom:6px; vertical-align:top;">代码:<textarea style="width:100%;" rows="2" class="textarea" name="ViewCode" id="ViewCode"></textarea></div>
                    <% end if %>
                </div>                
                <div class="ListDiv"  style="width:98%;">
                    <% set rs=myclass.load("","where a.pid=" & classid,"order by a.sort,a.id") %>
                    <% for i=1 to rs.RecordCount %>
                    <div style="width:400; margin:0px 14px 20px 0px;">                    
                    <table cellspacing="0" cellpadding="6" border="1" class="dv" style="border-collapse: collapse;">
                        <tr class="gvrowhead">
                            <th class="th" style="width: 20px;">
                                    <!--<input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />-->
                            </th>
                            <th class="th" >
                                    <%=rs.Fields("name") %>
                            </th>
                            <th class="th"  style="width: 100px;">
                                        操作
                                </th>
                        </tr>
                        <%
                        set ads=new adsObj
                        set rs2=ads.load("","where a.classid=" & rs.fields("id"),"order by a.sort,a.id")
                        if rs2.EOF then Response.Write ("<span style='color:red;font-size:14px;'><span>")
                        %>
                        <% for j=1 to rs2.RecordCount  %>
                        <tr>
                            <td class="tic" >
                                    <input type="checkbox" class="idset" name="idset" value="<%=rs2.fields("id") %>" />
                            </td>
                            <td class="til">
                                    <%=rs2.Fields("title") %>
                            </td>
                            <td class="tic">
                                    <a href="gg_edit.asp?id=<%=rs2.fields("id") %>" title="编辑" class="b">
                                        编辑</a>
                                        <% if admin.Purview=1 and rs.fields("subname")="点击广告" then %>
                                        &nbsp;<a href="#" onclick="getCode('<%=rs2.fields("id") %>')" class="b">脚本代码</a>
                                        <% end if %>
                                </td>
                        </tr>
                        <% rs2.MoveNext:next %>
                        
                        <% rs2.close:set rs2=nothing %>
                        <% if admin.Purview=1 then %>
                        <tr>
                            <td class="tir" colspan="3" >
                                    <a href="gg_edit.asp?classid=<%=rs.fields("id") %>" title="添加" class="b">
                                        <u>添加</u></a>
                                    &nbsp;
                                    <% if rs.fields("subname")="图片轮换" then %>
                                    <a href="#" onclick="getCode2(<%=rs.fields("id") %>)" title="脚本" class="b">
                                        <u>脚本代码</u></a>
                                    <% end if %>
                                </td>
                        </tr>
                        <% end if %>
                    </table>
                    </div>
                    <% rs.MoveNext:next %>
                    <% rs.Close:set rs=nothing %>
                    <% if admin.Purview=1 then %>
                    <div class="SetDiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <button  class="btn" onclick="SetSet('del','册除后不能恢复,确定要册除吗?');">删除选定项</button></td>
                            </tr>
                        </table>
                    </div> 
                    <% end if %>                   
                </div>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

