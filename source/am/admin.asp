﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,idset,idArray
action=getquerystring("action","")
idset=getquerystring("idset","0")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站配置</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="?action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>

</head>
<%
if action="del" then 
    conn.execute "delete from admin where id in (" & idset & ")"
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                系统管理员
            </div>
            <div class="contentdiv" style="text-align:center; height:550px;">
                <div class="listdiv"  style="width:80%;">
                    <div>
                        <div style="margin-bottom:8px;"><a href="admin_edit.asp" class="b">添加管理员</a></div>
                        <table cellspacing="0" cellpadding="6" border="1" class="gv" style="border-collapse: collapse;">
                            <tr class="gvrowhead">
                                <th class="th"  style="width: 20px;"><input type="checkbox" name="checkbox" onClick="$.SelectAllCB(this,'idset');" value="0" /></th><th class="th"  style="width: 40px;">编号</th><th class="th" >登陆名</th><th class="th"  style="width: 100px;">最后登陆IP</th><th class="th"  style="width: 60px;">登陆次数</th> <th class="th"  style="width: 120px;">最后时间</th><th class="th"  style="width: 100px;">操作</th>
                            </tr>
                            <% set rs=admin.load("","where a.Purview=0","order by a.id") %>
                            <% for i=1 to rs.RecordCount %>
                            <tr class="gvrow">
                                <td class="tic"><input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" /></td><td class="tic"><%=rs.fields("id") %></td><td class="til"><%=rs.fields("loginid") %></td><td class="tic"><%=rs.fields("LastLoginIP") %></td><td class="tic"><%=rs.fields("LoginTimes") %></td> <td class="tic"><%=rs.fields("LastLoginTime") %></td>                               
                                <td class="tic">
                                    <% if admin.id<>rs.fields("id") then %>
                                    <a href="admin_edit.asp?id=<%=rs.fields("id") %>" class="b">
                                        编辑</a>&nbsp;&nbsp;
                                        
                                    <% if admin.Purview=1 then %>
                                    <a href="admin_power.asp?id=<%=rs.fields("id") %>" class="b">
                                        授权</a>
                                    <% end if %>
                                    <% end if %>
                                </td>
                            </tr>
                            <% rs.movenext:next %>
                        </table>
                        <%rs.close:set rs=nothing %>
                    </div>
                    <div class="setdiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <button  class="btn" onclick="SetSet('del','册除后不能恢复,确定要册除吗?');">批量删除</button></td>
                            </tr>
                        </table>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

