﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,idset,idArray,where,cartid,status
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

formnumber=getquerystring("formnumber","")
status=getquerystring("status","")
filePath="?formnumber=" & formnumber & "&status=" & status & "&"

CurrentPage=Cint(getquerystring("pageno",1))

set payrecord=new payrecordObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }        
        
      -->
    </script>
</head>
<%
' 批量操作
if action="del" then    
    for i=0 to ubound(idArray)
	    conn.execute "delete from payrecord where id='" & idArray(i) & "'"
    next       
end if


' 组织SQL
where="where a.id>0"
if formnumber<>"" then
    where = where & " and b.formnumber = '" & formnumber & "'"
end if
if status<>"" then
    where = where & " and a.status = " & status
end if

%>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                付款记录
            </div>
            <div class="contentDiv" style="text-align:center; height:550px;">
                <div class="searchDiv">
                    <form method="get" action="?" name="webForm">
                        <table cellpadding="0" cellspacing="2">
                            <tr>
                                <td style="width: 100px; padding-top: 3px;">
                                    <b>订单编号:</b>
                                </td>
                                <td style="width: 110px;">
                                    <input type="text" name="formnumber" class="text" value="<%=formnumber %>" style="width: 100px;" />
                                </td>                                
                                <td>                                        
                                    <input type="submit" value=" 搜 索 " class="inputBtn" />
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="ListDiv"  style="width:98%;">
                    <!--<div class="SetDiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <select name="action" onchange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div> -->
                    <div >
                        <%                     
                        set rs=payrecord.load("",where,"order by a.id desc")
                        MaxItemSize=18
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<span style='color:red;font-size:14px;'>未添加任何数据!!</span>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        end if
                        %>
                        <table cellspacing="0" cellpadding="6" border="1" class="gv" style="border-collapse: collapse;">
                            <tr class="gvrowhead">
                                <th class="th" style="width: 20px;">
                                    <input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />
                                </th>
                                <th class="th" style="width: 120px;">付款时间 </th>
                                <th class="th" style="width: 110px;">用户 </th>
                                <th class="th" style="width: 110px;">订单号 </th>
                                <th class="th" style="width: 80px;">支付平台 </th>
                                <th class="th">实际支付(<%=conCurrencyUnit %>) </th>
                                <th class="th">银行信息 </th>
                                <th class="th">状态 </th>
                            </tr>
                            <% for i=1 to CurrentRecord(rs,CurrentPage) %>
                            <tr  class="gvrow">
                                <td class="tic">
                                    <input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" />
                                </td>
                                <td class="tic">
                                    <%=rs.fields("paytime") %>
                                </td>
                                <td class="tic">
                                    <%=rs.fields("username") %>                                    
                                </td>
                                <td class="tic">
                                    <a href="orderform_edit.asp?id=<%=rs.fields("orderformid") %>" class="b" target="_blank"><%=rs.fields("ordernum") %></a>
                                </td>
                                <td class="tic">
                                    <%=rs.fields("platformname") %> 
                                </td>
                                <td class="tic">
                                     
                                    <%=rs.fields("moneytrue") %> 
                                </td>
                                <td class="tic">
                                    <%=rs.fields("ebankinfo") %>
                                </td>
                                 <td class="tic">
                                    <% if rs.fields("status")=0 then %><span style="color:red;">付款已提交 <u><%=DateDiff("h",rs.fields("paytime"),now()) %></u> 小时</span>
                                    <% elseif rs.fields("status")=2 then  %><span style="color:Green;">付款成功</span><%end if %>
                                </td>
                            </tr>
                            <% rs.movenext
                               next
                            %>

                        </table>
                        <% rs.close:set rs=nothing %>
                    </div>                                       
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

