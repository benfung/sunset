﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/conn.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim myclass,classArray
set myclass=new classobj

classArray=myclass.GetArrayByTypename("news")

dim action,idset,idArray,classid,channelid,status,where,title,ishot,iscommend,ispic,selected
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

selected=getquerystring("selected",0)
channelid=getquerystring("channelid",0)
classid=getquerystring("classid",0)
status=getquerystring("status","")
title=getquerystring("title","")
ishot=getquerystring("ishot","")
iscommend=getquerystring("iscommend","")
ispic=getquerystring("ispic","")
filePath="?selected=" & selected & "&classid=" & classid & "&channelid=" & channelid & "&status=" & status & "&title=" & title & "&ishot=" & ishot & "&iscommend=" & iscommend & "&ispic=" & ispic & "&"

CurrentPage=Cint(getquerystring("pageno",1))

set news=new newsObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>

</head>
<%
' 该变量(httphost)与生成HTMl相关
dim httphost
httphost="http://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("URL")
httphost=mid(httphost,1,InStrRev(httphost,"/")-1)
httphost=mid(httphost,1,InStrRev(httphost,"/"))

' 批量操作
if action="del" then    
    for i=0 to ubound(idArray)
	    conn.execute "delete from news where id =" & idArray(i)
	    MyAsp2Html idArray(i),action '删除文件
    next       
end if

' 批量操作
if action="create" and conOpenAsp2Html=1 then    
    for i=0 to ubound(idArray)
        MyAsp2Html idArray(i),action '创建静态文件
    next 
end if


if action="pass" then    
    conn.execute "update news set status=1 where id in (" & idset & ")"
end if

if action="unpass" then    
    conn.execute "update news set status=0 where id in (" & idset & ")"
end if

if action="commend" then    
    conn.execute "update news set iscommend=1 where id in (" & idset & ")"
end if

if action="nocommend" then    
    conn.execute "update news set iscommend=0 where id in (" & idset & ")"
end if

if action="hot" then    
    conn.execute "update news set ishot=1 where id in (" & idset & ")"
end if

if action="nohot" then    
    conn.execute "update news set ishot=0 where id in (" & idset & ")"
end if


' 组织SQL
where="where a.id>0"
if title<>"" then
    where = where & " and (a.title like '%" & title & "%' or a.content like '%" & title & "%')"
end if
if classid<>"0" then
    where = where & " and (a.classid in ("&myclass.GetSubs(classid)&"))"
end if
if channelid<>"0" then
    where = where & " and a.channelid=" & channelid
end if
if status<>"" then
    where = where & " and a.status=" & status
end if
if ishot<>"" then
    where = where & " and a.ishot=" & ishot
end if
if iscommend<>"" then
    where = where & " and a.iscommend=" & iscommend
end if
if ispic<>"" then
    where = where & " and a.ispic=" & ispic
end if

%>
<body>
    <div class="bodydiv">
        <div id="newitem" class="jbox" style="width: 500px;">
        <div class="closejbox"><img src="images/close.gif" alt="" align="absmiddle" onclick="$.RemoveJBox($('#newitem'));" /></div>
        <fieldset>
            <legend>高级搜索</legend>
            <form method="get" action="?" name="webForm" class="aform">
            <table cellpadding="6" cellspacing="2" class="table1">
                <tr>
                    <td class="td01">关键字: </td>
                    <td class="td02">
                        <input type="text" name="title" class="text" value="<%=title %>" style="width: 255px;" />
                        
                    </td>
                </tr>
                <tr>
                    <td class="td01">类别: </td>
                    <td class="td02">
                        <select name="classid">
                                        <option value="">----全部----</option>
                                        <%=myclass.ShowOption(classArray,0,0,3) %>
                                    </select>
                                        <script type="text/javascript">
                                            $.setElementValue(webForm.classid, "<%=classid %>", "");
                                        </script> 
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="td02" style="text-align: center;">
                        <button type="submit" class="btn">
                            搜 索</button>&nbsp;&nbsp;<button type="button" class="btn" onclick="$.RemoveJBox($('#newitem'));">
                            关 闭</button></td>
                </tr>
            </table>
            </form>
        </fieldset>
    </div>
    
        <div class="bodybox">
            <div class="titlenav">
                高级搜索
            </div>
            <div class="contentdiv" style="text-align:center; height:550px;">
                <div class="listdiv"  style="width:98%;">
                    <div class="setdiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <select name="action" onChange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>                                        
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="pass" >&nbsp;&nbsp;标记为已审</option>
                                        <option value="unpass" >&nbsp;&nbsp;标记为待审</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="commend" >&nbsp;&nbsp;设为首页显示</option>
                                        <option value="nocommend" >&nbsp;&nbsp;撤消首页显示</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="hot" >&nbsp;&nbsp;设为热点</option>
                                        <option value="nohot" >&nbsp;&nbsp;撤消热点</option>
                                        <% if conOpenAsp2Html=1 then %>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="create" >&nbsp;&nbsp;生成HTML</option>
                                        <% end if %>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select>
                                    &nbsp;
                                    <button class="btn" onclick="$.ShowJBox($('#newitem'));">搜索。。。</button>
                                    </td>
                            </tr>
                        </table>
                    </div>  
                    <ul class="ultab">
                        <li class="space">&nbsp;</li>
                        <li class="<%=checkTab(selected,0) %>"><a href="?selected=0" class="b">全部</a></li>
                        <li class="space">&nbsp;</li>
                        <li class="<%=checkTab(selected,1) %>"><a href="?selected=1&status=0" class="b">待审</a></li>
                        <li class="space">&nbsp;</li>
                        <li class="<%=checkTab(selected,2) %>"><a href="?selected=2&iscommend=1" class="b">首页显示</a></li>
                        <li class="space">&nbsp;</li>
                        <li class="<%=checkTab(selected,3) %>"><a href="?selected=3&ishot=1" class="b">热点</a></li>
                        <li class="space">&nbsp;</li>
                        <li class="<%=checkTab(selected,4) %>"><a href="?selected=4&ispic=1" class="b">图片新闻</a></li>
                        <li class="tabmsg">&nbsp;</li>                   
                    </ul>
                    <div >
                        <%                  
                        set rs=news.load("",where,"order by a.sort,a.id desc")
                        MaxItemSize=50
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<div style='color:red;font-size:14px;margin:50px;'>没有匹配的记录!!</div>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        
                        %>
                        <table cellspacing="0" cellpadding="6" border="1" class="gv" style="border-collapse: collapse;">
                            <tr class="gvrowhead">
                                <th class="th" style="width: 20px;">
                                    <input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />
                                </th>
                                <th class="th">标题 </th>
                                <th class="th" style="width: 120px;">所属栏目 </th>
                                <!--<th class="th" style="width: 80px;">所属频道 </th>-->
                                <th class="th" style="width: 100px;">相关属性 </th>
                                <th class="th" style="width: 40px;">序号 </th>
                                <th class="th" style="width: 50px;">操作 </th>
                            </tr>
                            <% for i=1 to CurrentRecord(rs,CurrentPage) %>
                            <tr  class="gvrow">
                                <td class="tic"><input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" /></td>
                                <td class="til"><a href="../newsdetail.asp?id=<%=rs.fields("id") %>&gen=y" class="b" target="_blank"><%=rs.fields("title") %></a></td>
                                <td class="tic"><%=rs.fields("classname") %></td>
                                <!--<td class="tic"><% myclass.classobj(rs.fields("channelid"))%><%=myclass.name  %></td>-->
                                <td class="tic"><% if rs.fields("status")=1 then %><span style="color:red;">审</span><% else %>&nbsp;&nbsp;<%end if %>&nbsp;<% if rs.fields("iscommend")=1 then %><span style="color:green;">首</span><% else %>&nbsp;&nbsp;<%end if %>&nbsp;<% if rs.fields("ishot")=1 then %><span style="color:blue;">热</span><% else %>&nbsp;&nbsp;<%end if %>&nbsp;<% if rs.fields("ispic")=1 then %><span style="color:Indigo;">图</span><% else %>&nbsp;&nbsp;<%end if %></td>
                                <td class="tic"><input type="text" class="text" value="<%=rs.fields("sort") %>" onFocus="this.select()" id="news_sort_int_<%=rs.fields("id") %>"  onblur="bgUpdate(this)" style="width:100%;" /></td>
                                <td class="tic"><a href="news_edit.asp?id=<%=rs.fields("id") %>" class="b">编辑</a></td></tr>
                            <% rs.movenext
                               next
                            %>
                        </table>
                        <% end if %>
                        <% rs.close:set rs=nothing %>
                    </div>
                                      
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>
            </div>
        </div>
        <%=formatnumber((timer()-PageStartTime)*1000,3)%> ms
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

<%
function MyAsp2Html(id,act)
    dim fso,htmlpage
    set fso=CreateObject("Scripting.FileSystemObject")
    htmlpage=Server.MapPath("../newsdetail_" & id & ".htm")
    if act="del" then        
        if fso.FileExists(htmlpage) then
            fso.DeleteFile(htmlpage)
        end if
    else   
        dim tempurl,Text
        tempurl=httphost & "newsdetail.asp" & "?gen=n&id=" & id
        Text=getHTTPPage(tempurl,conDefaultPageCharset)
        WriteTextFile htmlpage,Text,conDefaultPageCharset
    end if
    set fso=nothing
end function
%>