﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<%
set admin=new adminObj
admin.AdminObjByStatus()
if admin.isnull() then
    response.Write  "刷新吧"
	response.End()
end if
%>
<% 
dim orders
set orders=new ordersobj
    
dim idate,sd,ed,days
idate=cdate(getquerystring("idate",now()))
sd=Formatdatetime(DateAdd("m",-1,idate),2)
ed=Formatdatetime(idate,2)
days=DateDiff("d",sd,ed)
sql="SELECT count(id) as n,formatdatetime(inputtime,1) as day  from orders where inputtime between #" & sd & "# and #" & ed & "# group by formatdatetime(inputtime,1) order by count(id) desc"
max=5
%>
{
  "title":{
    "text": "订单量统计  <%=shortdate(sd) %> -- <%=shortdate(ed) %>",
			"style": "{font-size: 18px; color:#000; text-align: right;margin-bottom:10px;margin-right:30px;}"
  },

  "x_axis":{
        "colour": "#aaaaaa",
		"offset": true,
		"grid-colour": "#eeeeee",
		
		"labels":{
		    
			"steps":7,
			"labels":[
			    <% for i=0 to days %>
			    "<%=FormatDateTime(DateAdd("d",i,sd),1) %>"			    
			    <%
			        if i<days then Response.Write(",")
			    next
			    %>]
				}
		},
   
	"bg_colour":"#ffffff",
	"elements":[
		{
			"type":			"area",
			"colour":		"#0077cc",
			"dot-style":	{"type":"solid-dot","background-colour":"#a44a80"},
			"values":[
				<% for i=0 to days %>
				<% cday=DateAdd("d",i,sd) %>
				{"value" :"<%=orders.CountByDay(Shortdate(cday)) %>","tip":"<%=FormatDateTime(cday,1) %> <%=CWeekDay(cday) %>\n订单数:<%=orders.CountByDay(Shortdate(cday)) %>"}		    
			    <%
			        if orders.CountByDay(Shortdate(cday))>max then max=orders.CountByDay(Shortdate(cday))
			        if i<days then Response.Write(",")
			    next
			    %>],
			"fill-alpha":0.1,
			"width":4
		}
	],
	 "y_axis": 
		{
		    "colour": "#aaaaaa",
			"grid-colour": "#eeeeee",
			"offset": false,
			"steps": 5,
			"min": 0,
			"max": <%=(Cint(max/10)+1) *10 %>
		},
		"y_axis_right":{    
    "colour": "#aaaaaa",
			"grid-colour": "#eeeeee",
			"offset": false,
			"steps": 5,
			"min": 0,
			"max": <%=(Cint(max/10)+1) *10 %>
  }
}
<%call closeconn %>

<%
function CWeekDay(d)
    dim result    
    select case Weekday(d)
        case 1 result="星期日"
        case 2 result="星期一"
        case 3 result="星期二"
        case 4 result="星期三"
        case 5 result="星期四"
        case 6 result="星期五"
        case 7 result="星期六"
        case else result="星期日"
    end select
    CWeekDay=result
end function
    

%>