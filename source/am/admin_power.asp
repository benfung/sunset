﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,id,aadmin
action=getquerystring("action","")
id=getquerystring("id",0)

set aadmin=new AdminObj
aadmin.AdminObj(id)

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站配置</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
        }
      -->
    </script>

</head>
<%
if action="save" then    
    dim i,power
    powerArray=split(getForm("power","0"),",")
    power=0
    for i=0 to ubound(powerArray)
        power = power + Clng(trim(powerArray(i)))
    next
    aadmin.power=power
    aadmin.update
	
	call Alert("操作成功","?id=" & aadmin.id)    
end if

%>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                <a href="admin.asp">系统管理员</a> <span class="titleNavSpot">&nbsp;</span>
                授权
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            授权</td>
                        <td class="formTabMsg">
                            带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=aadmin.id %>" name="webForm" class="aform" method="post" onSubmit="return checkForm(this)">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:100px;">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        系统：</td>
                                    <td class="td02">
                                        <% p=0:po=getMi(p) %>
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        <label for>系统管理</label> <%=po %>
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        企业信息 <%=po %>
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        会员管理 <%=po %>
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        产品管理 <%=po %>
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        订单管理 <%=po %>
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        新闻/资讯管理 <%=po %> <br />
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        留言管理 <%=po %> 
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        调查管理 <%=po %> 
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        !!!! <%=po %> 
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        广告管理 <%=po %>
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        友情链接 <%=po %>
                                        
                                        <% p=p+1:po=getMi(p) %>                              
                                        <input type="checkbox" name="power" <% if aadmin.hasPower(po) then %> checked="checked" <% end if %> value="<%=po %>" />
                                        访问统计 <%=po %>
                                    </td>
                                </tr>                               
                            </table>
                        </div>                        
                    </div>
                    <div class="submit">
                        <button type="submit" class="btn">提 交</button>&nbsp;&nbsp;<button type="button" onClick="window.location='admin.asp';"  class="btn">返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>


<%
'***************************************************
'函数名称:getMi
'作用:计算2的n次方
'参数:arg0 - 次方
'***************************************************
function getMi(arg0)
    dim Result,i
    Result=1
    for i=1 to arg0
        Result=Result*2
    next
    getMi=Result
end function
%>