﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,idset,idArray,where,cartid
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

cartid=getquerystring("cartid","")
filePath="?cartid=" & cartid & "&"

CurrentPage=Cint(getquerystring("pageno",1))

set shoppingcarts=new shoppingcartsObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }        
        
      -->
    </script>
</head>
<%
' 批量操作
if action="del" then    
    for i=0 to ubound(idArray)
	    conn.execute "delete from shoppingcarts where cartid='" & idArray(i) & "'"
    next       
end if


' 组织SQL
where=""
if cartid<>"" then
    where = where & " where a.cartid like '%" & cartid & "%'"
end if
%>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                高级搜索
            </div>
            <div class="contentDiv" style="text-align:center; height:550px;">
                <div class="searchDiv">
                    <form method="get" action="?" name="webForm">
                        <table cellpadding="0" cellspacing="2">
                            <tr>
                                <td style="width: 100px; padding-top: 3px;">
                                    <b>购物车编号:</b>
                                </td>
                                <td style="width: 110px;">
                                    <input type="text" name="cartid" class="text" value="<%=cartid %>" style="width: 100px;" />
                                </td>                                
                                <td>                                        
                                    <input type="submit" value=" 搜 索 " class="inputBtn" />
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="ListDiv"  style="width:98%;">
                    <div class="SetDiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <select name="action" onchange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div> 
                    <div >
                        
                        <table cellspacing="0" cellpadding="6" border="1" class="gv" style="border-collapse: collapse;">
                            <tr class="gvrowhead">
                                <th class="th" style="width: 20px;">
                                    <input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />
                                </th>
                                <th class="th">购物车编号</th>
                                <th class="th" style="width: 150px;">商品 </th>
                                <th class="th" style="width: 70px;">购买数量 </th>
                                <th class="th" style="width: 120px;">加入时间 </th>
                                <th class="th">备注 </th>
                            </tr>
                            <%
                            sql="SELECT cartid,max(updatetime) as dtime from  shoppingcarts " & where & " group by cartid order by max(updatetime) desc"
                        set ors=Server.CreateObject("Adodb.RecordSet")
                        ors.Open sql,conn,1,1
                        MaxItemSize=18
                        ors.PageSize=MaxItemSize
                        TotalNumber=ors.RecordCount
                        TotalPage=ors.PageCount
                        if ors.EOF then
                            Response.Write ("<span style='color:red;font-size:14px;'>没有数据!!<span>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            ors.AbsolutePage=CurrentPage
                        end if
                        j=0
                        do while not ors.eof
                        set rs=shoppingcarts.load("","where a.cartid='" & ors.fields("cartid") & "'","order by a.updatetime")                        
                        i=0
                        %>
                            <% do while not rs.eof %><tr class="gvrow"><td class="tic"><%if i=0 then %><input type="checkbox" class="idset" name="idset" value="<%=rs.fields("cartid") %>" /><%end if %></td><td class="tic"><%if i=0 then %><%=rs.fields("cartid") %><% end if %></td><td class="til"><a href="productdetail.asp?id=<%=rs.fields("productid") %>" target="_blank" class="B"><%=rs.fields("productname") %></a></td><td class="tic"><%=rs.fields("quantity") %></td><td class="tic"><%=rs.fields("updatetime") %></td><td class="tic"><%=rs.fields("other") %></td></tr>
                            <% rs.movenext:i=i+1
                               loop
                            %>
                            <% rs.close:set rs=nothing
                            ors.movenext
                          
                               j=j+1
                               if j>=MaxItemSize then
                                    exit do
                               end if
                        loop %>
                        <% ors.close:set ors=nothing %>
                        </table>
                        
                    </div>                                       
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

