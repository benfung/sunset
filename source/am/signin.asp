﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/function.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/md5.asp"-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>系统登陆</title>    
    <!--#include file="headmeta.asp"-->    
    <style type="text/css">
        <!
        -- body { margin: 0px auto; background: #fff url('images/sys_bg.jpg') repeat-x; text-align: center; }
        table { width: 100%; border: 0px; border-collapse: collapse; }
        td, div { font-family: Tahoma,Simsun,Helvetica,sans-serif; font-size: 12px; text-align: left; vertical-align: top; color: #000; }
        form { margin: 0px; padding: 0px; }
        img { border: 0px; }
        input { vertical-align: middle; }
        .text { font-size: 12px; border: 1px solid #4e74a5; height: 20px; }
        .button { font-family: tahoma,'宋体'; color: #15428b; font-size: 9pt; border: solid 1px #3376bc; background: #fff url('images/btn_bg.png') repeat-x bottom; }
        .textarea { font-size: 12px; border: 1px solid #4e74a5; }
        a, a:link, a:visited { color: #08D; text-decoration: none; }
        a:hover { color: #08D; text-decoration: underline; }
        a.b:link, a.b:visited { color: #666; text-decoration: none; }
        a.b:hover { color: #08D; text-decoration: underline; }
        .left { padding: 40px; width: 42%; border-right: solid 1px #ddd; text-align: right; vertical-align: middle; }
        .left div { text-align: right; }
        .right { padding: 40px; vertical-align: middle; }
        div.note { color: #25A; font-size: 14px; }
        .td01 { padding-top: 8px; text-align: right; width: 90px; }
        .td02 { padding-bottom: 10px; }
        .td02 span { color: #666; }
        .bottom { height: 26px; border-top: solid 1px #cadffb; padding: 5px 20px; }
        -- ></style>
    <script type="text/javascript">
    // 在document就绪或加载完成时执行
    $(document).ready(function(){
        checkBrowser();  // 检测浏览器
        //setFocus();  // 设置默认焦点
    });
    ////////////////////////
    if (window.top != window){
        window.top.location.href = document.location.href;
    }
    /////////////////////
    function setFocus(){
        if(document.webForm!=null){
            if (document.webForm.username.value=="")
                document.webForm.username.focus();
            else
                document.webForm.username.select();
        }
    }
    ///////////////////////
    function checkBrowser(){
        var app=navigator.appName;
        var verStr=navigator.appVersion;
        if (!$.browser.msie) {
            alert("友情提示：\n    你使用的是不是IE浏览器，可能会导致无法使用后台的部分功能。建议您使用 IE6.0 或以上版本。");
        } 
        else{
            if (verStr.indexOf("MSIE 3.0")!=-1 || verStr.indexOf("MSIE 4.0") != -1 || verStr.indexOf("MSIE 5.0") != -1 || verStr.indexOf("MSIE 5.1") != -1)
                alert("友情提示：\n    您的浏览器版本太低，可能会导致无法使用后台的部分功能。建议您使用 IE6.0 或以上版本。");
        }
    } 
    </script>
</head>

<body>
<% 
dim rurl,action,username,password,validatecode,errormsg,savecookies
rurl=getquerystring("rurl","default.asp")
action=getquerystring("action","")
username=getform("username","")
password=getform("password",turnsql(Request.Cookies("sysadmin")("password")))
validatecode=getform("validatecode","")
savecookies=getform("savecookies","")
errormsg=""

set admin=new adminObj
if action="login" then ' 登陆
    if username="" then
        errormsg="请填写用户名!!"
    end if
    if password="" then
        errormsg="请填写密码!!"
    end if
    if validatecode<>Session("ValidateCode") then     
        errormsg="对不起,验证码不正确!!"  
    end if
    Session("ValidateCode")="" 
       
    if errormsg="" then
        
        if getForm("s","")="" then password= GenPasswrod(password,username)
        
        admin.AdminObjByNP username,password
        if admin.isnull() then
            ' 记录登陆历史 ////////////////////////   
            if conCheckLoginLog="1" then 
                set loginlog=new loginlogObj
                loginlog.usertype="admin"
                loginlog.username="<font color=red>Failing!!</font>"
                loginlog.ip=getip()
                loginlog.logcontent="<font color=red>登录失败</font>"
                loginlog.scriptname=Trim(Request.ServerVariables("HTTP_REFERER"))
                loginlog.poststring=Replace(Request.Form,"'","''")
                loginlog.add()
            end if
            ' //////////////////////////////////////////
            errormsg="用户名或密码错误!!"
            
            admin.ClearStatus()
            Response.Cookies("sysadmin")("username") = ""
            Response.Cookies("sysadmin")("password") = ""
            Response.Cookies("sysadmin").Expires=Date()-1
        else
            ' 更新登陆信息
            admin.LastLoginIP=getip()
            admin.LastLoginTime=now()
            admin.LoginTimes=admin.LoginTimes+1
            admin.Update()
            
            ' 记录登陆历史 ////////////////////////  
            if conCheckLoginLog="1" and getip()<>"127.0.0.1" then 
                set loginlog=new loginlogObj     
                loginlog.usertype="admin"
                loginlog.username=admin.loginid
                loginlog.ip=getip()
                loginlog.logcontent="正常登录"
                loginlog.scriptname=Trim(Request.ServerVariables("HTTP_REFERER"))
                loginlog.add()
            end if
            
            if savecookies="save" then
                Response.Cookies("sysadmin")("username") = username
                Response.Cookies("sysadmin")("password") = Password
                Response.Cookies("sysadmin").Expires=DateAdd("m",3,now())
            else
                Response.Cookies("sysadmin")("username") = ""
                Response.Cookies("sysadmin")("password") = ""
                Response.Cookies("sysadmin").Expires=Date()-1
            end if
            ' //////////////////////////////////////////
            
            admin.SaveStatus(conLoginTimeOut)
            ' // 开启 上传功能////////////////////////////////////////
            Session("upload")="open"
            call topage(rurl)
        end if  
    end if
elseif action="logout" then
    admin.ClearStatus()
    Response.Cookies("sysadmin")("username") = ""
    Response.Cookies("sysadmin")("password") = ""
    Response.Cookies("sysadmin").Expires=Date()-1
    
    call topage(rurl)
end if
%>
    <table cellpadding="0" cellspacing="0" height="100%">
        <tr>
            <td style="padding: 170px 0px 50px;">
                <table cellpadding="0" cellspacing="0" height="100%">
                    <tr>
                        <td class="left">
                            <div><img src="images/syslogo.gif" align="absmiddle" alt="" /></div>                              
                             <div class="note" style="line-height:150%;">
                                <!--修改制作：<a href="<%=sysurl %>" target="_blank"><%=sysname %></a><br />-->
技术支持：<a href="<%=conSysurl %>" target="_blank"><%=conSysname %></div>
                        </td>
                        <td class="right">
                            <% if Request.Cookies("sysadmin")("username")<>"" and Request.Cookies("sysadmin")("password")<>"" then %>
                            <div class="note" style="color: Black; margin-bottom: 20px;">
                                单击某个 用户名 以登录</div>
                             <form name="webForm" method="post" action="?action=login&url=<%=rurl %>">
                             <input type="hidden" name="username" value="<%=Request.Cookies("sysadmin")("username") %>" />
                             <input type="hidden" name="savecookies" value="save" />
                             <input type="hidden" name="s" value="k" />
                            <table cellpadding="6" cellspacing="0" style="background: #d6dff7; width: 60%;margin-bottom: 20px;">
                                <tr>
                                    <td style="vertical-align: middle; width: 50px;">
                                        <img src="images/UT.png" alt="" align="absmiddle" /></td>
                                    <td style="vertical-align: middle;">
                                        <b><%=Request.Cookies("sysadmin")("username") %></b>
                                    </td>
                                    <td style="vertical-align: middle; text-align: right;">
                                        <input type="submit" class="button" id="Submit1" value=" 立即登录 " /></td>
                                </tr>
                            </table>
                            </form>
                            <div><a href="signin.asp?action=logout">使用其他帐号</a></div>
                            <% else %>
                            <div class="note" style="margin-bottom: 20px;">
                                如果您未经管理人员授权登录,请点击 <a href="../">这里</a></div>
                            <div style="color:Red; margin-bottom:8px; background:url('images/error.gif') no-repeat; padding-left:20px;"><%=errormsg %></div>
                            <div style="font-size: 14px; margin-bottom: 10px;">
                                <b>管理系统 - 登录</b></div>
                            <form name="webForm" method="post" action="?action=login&url=<%=url %>" onSubmit="return $.ValidateForm(this,1);">
                                <table cellpadding="4" cellspacing="0">
                                    <tr>
                                        <td class="td01">
                                            授权用户名：</td>
                                        <td class="td02">
                                            <input name="username" type="text" onFocus="this.select()" datatype="username" tip="此处只能由数字、字母和下划线组成"
                                                style="width: 250px;" /><br />
                                            <span>此处只能由数字、字母和下划线组成</span></td>
                                    </tr>
                                    <tr>
                                        <td class="td01">
                                            密 码：</td>
                                        <td class="td02">
                                            <input name="password" type="password" onFocus="this.select()" style="width: 250px;" /><br />
                                            <span>忘记密码请联系相关管理人员</span></td>
                                    </tr>
                                    <tr>
                                        <td class="td01">
                                            验证码：</td>
                                        <td class="td02">
                                            <input name="validatecode" maxlength="4" type="text" onFocus="this.select()" datatype="require"
                                                tip="请输入验证码" style="width: 80px;" />
                                            &nbsp;&nbsp;<img src="../inc/validateimg.asp" align="absmiddle" onClick="this.src=this.src;"
                                                style="cursor: pointer;" alt="看不清楚，换个图片" /><br />
                                            <span>请输入你看到的4位数字 | 看不清楚，可点击图片更换</span></td>
                                    </tr>
                                    <tr>
                                        <td class="td01">&nbsp;
                                            </td>
                                        <td class="td02">
                                            <input type="checkbox" name="savecookies" checked="checked" value="save" />&nbsp;&nbsp;在此计算机上保存我的信息。</td>
                                    </tr>
                                    <tr>
                                        <td class="td01">&nbsp;
                                            </td>
                                        <td class="td02">
                                            <input type="submit" class="button" id="btnLogin" value=" 立即登录 " />&nbsp;&nbsp;&nbsp;&nbsp;<input
                                                type="button" class="button" id="btnLogout" onClick="window.top.location=('../');"
                                                value=" 退 出 " /></td>
                                    </tr>
                                    <tr>
                                        <td class="td01">&nbsp;
                                            </td>
                                        <td class="td02">
                                            按 <b>Tab</b> 或 <b>Shift + Tab</b> 键可快速切换输入框</td>
                                    </tr>
                                </table>
                            </form>
                            <% end if %>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="bottom">
                <table cellpadding="0" cellspacing="0" height="100%">
                    <tr>
                        <td>
                            &copy;&nbsp;2008 <%=conSysname %></td>
                        <td style="text-align: right;">
                            <a href="<%=conSysurl %>aboutus.htm" class="b">关于我们</a>&nbsp;&nbsp;<a href="<%=conAuthorurl %>" class="b">HELP</a>&nbsp;&nbsp;<a
                                href="<%=conSysurl %>feedback.asp" class="b">反馈</a></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
<%call closeconn %>
