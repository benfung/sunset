﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim myclass,classArray
set myclass=new classobj
classArray=myclass.GetArrayByTypename("product")

dim action,id,product
action=getquerystring("action","")
id=getquerystring("id",0)

set product=new productObj
product.productObj(id)

' 设置默认值
if product.isnull then
    ' 产生随机编号
    Randomize
    ranNum=int(9*rnd)+10
    iddata=month(now)&day(now)&hour(now)&minute(now)&second(now)&ranNum
    
    if conOpenAutoCode=1 then
        product.productnum=iddata
    end if
    product.status=1 ' 默认已审核
    
    Product.classid=getquerystring("classid",0)
end if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    <script language="javascript" type="text/javascript">
      <!--  
        var picsetObj,foucspicObj
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            picsetObj=webForm.picset;
            foucspicObj=webForm.focuspic;

            $.slipTab(1, 1, "Tab", "formTabA", "formTabB", "click", 1);
            displayPic(picsetObj.value.split("|"));
        });
        function checkForm(form){
            if($.trim(form.classid.value)=="0"){
                alert("请选择一级栏目!!");                
                form.classid.focus();
                return false;
            }
            if($.trim(form.title.value)==""){
                alert("请填写标题!!");
                form.title.select();
                return false;
            }
            if(foucspicObj.value==""){
                alert("请选择焦点图!!");                
                return false;
            }
        }

        /*function initSelectbyPid(argClass,SelectObj, pid)
        {
            SelectObj.innerHTML="";
            var opt=document.createElement("option");
            opt.value="0";
            opt.text="--请选择分类--";
            SelectObj.add(opt);
            for (var i = 0; i < argClass.length; i++)
            {
                if (argClass[i] == undefined) continue;
                if (argClass[i][1] == pid)
                {
                    var opt=document.createElement("option");
                    opt.value = argClass[i][0];
                    opt.text = argClass[i][3];
                    SelectObj.add(opt);
                }
            }
        }*/
        
        // 添加到图片集
        function updatePicSet(ifilename){            
            if($.trim(ifilename)!=""){
                if(picsetObj.value==""){
                    picsetObj.value=ifilename;
                }
                else{
                    picsetObj.value += "|"+ifilename;
                }
            }
            displayPic(picsetObj.value.split("|")); // 显示图片
        }
        
        
        // 从图片集中删除
        function trimPic(ifilename) {
            //if (DelFile(ifilename, null)) {
                var picsetArray = picsetObj.value.split("|");
                var index = picsetArray.indexof(ifilename);
                picsetArray.splice(index, 1);

                displayPic(picsetArray); // 显示图片
                picsetObj.value = picsetArray.join("|");
            //}
        }
        
        // 列表方式显示图片
        function displayPic(tempArray){
            var str="";
            for(var i=0;i<tempArray.length;i++){
                if(tempArray[i]!=""){
                    str += "<div class=\"dspic\" onmouseover=\"this.style.background='#f6f9fd'\" onmouseout=\"this.style.background=''\">";
                    str += "<a href=\"../" + tempArray[i] + "\" target=\"_blank\"><img src=\"../s_" + tempArray[i] + "\" width=\"100\"  alt=\"\" style=\"border:solid 1px #ccc;\" align=\"absmiddle\" /></a>";
                    str += "<div style=\"line-height: 150%; width: 98%; text-align:center;\"><input type=\"radio\" name=\"focuspics\" value=\"" + tempArray[i] + "\" onclick=\"foucspicObj.value=this.value\" />焦点图<br /><a href=\"javascript:trimPic('" + tempArray[i] + "')\" class='c'>删除</a></div>";
                    str += "</div>";
                }
            }
            picDiv.innerHTML=str;
            
            $.setElementValue(webForm.focuspics,webForm.focuspic.value);  // 还原焦点图            
        }
      -->
    </script>

</head>
<%
if action="save" then   

    Product.productnum=TurnSql(getForm("productnum",""))
	Product.classid=getForm("classid",0)
	Product.title=TurnSql(getForm("title",""))
	Product.keywords=TurnSql(getForm("keywords",""))
	Product.thetext=TurnSql(getForm("thetext",Product.title))
	Product.saleprice=getForm("saleprice",0)
	Product.memberprice=getForm("memberprice",Product.saleprice)
	Product.price1=getForm("price1",0)
	Product.price2=getForm("price2",0)
	Product.other1=TurnSql(getForm("other1",""))
	Product.other2=TurnSql(getForm("other2",""))
	Product.other3=TurnSql(getForm("other3",""))
	Product.stocks=getForm("stocks",0)
	Product.focuspic=TurnSql(getForm("focuspic",""))
	Product.picset=TurnSql(getForm("picset",""))
	Product.hits=getForm("hits",0)
	Product.ishot=getForm("ishot",0)
	Product.isnew=getForm("isnew",0)
	Product.iscommend=getForm("iscommend",0)
	Product.sort=getForm("sort",0)
	Product.updatedate=getForm("updatedate",now())
	Product.creationdate=getForm("creationdate",now())
	Product.status=getForm("status",0)

	if Product.isnull() then
		Product.add()
		call confirm("操作成功<br />是否继续添加","?","product.asp?action=create&idset=" & Product.id) 
	else
		Product.Update()
		call toPage("product.asp?action=create&idset=" & Product.id)
	end if

end if


%>
<body>
    
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                企业产品管理 <span class="titleNavSpot">&nbsp;</span>
                <% if Product.isnull then %>
                添加
                <% else %>
                编辑
                <% end if %>
            </div>
            <div class="contentdiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            基本属性</td>
                        <td class="formTabMsg">
                            带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=Product.id %>" name="webForm" method="post" class="aform" onSubmit="return checkForm(this)">
                    <input name="picset" type="hidden" value="<%=Product.picset %>" />
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:50px;">
                            <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">                                
                                <tr>
                                    <td class="td01">
                                        上传：</td>
                                    <td class="td02">
                                        <input type="hidden" name="focuspic"  value="<%=Product.focuspic %>"   />                                        
                                        <input type="hidden" name="tempfocuspic" readonly="readonly" id="tempfocuspic" onChange="updatePicSet(this.value)" class="text"  />
                                        <div><span id="spanButtonPlaceHolder"></span></div>
                                        <div class="fsUploadProgress" id="fsUploadProgress"><div></div>
                                        <img src="images/loadingAnimation.gif" align="absmiddle" />
                                        </div>
                                        <script>
                                            UploadInit("spanButtonPlaceHolder", $.FileType["Image"], "图片文件", 0, 200, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "tempfocuspic", "true", "fsUploadProgress")
                                        </script>
                                    </td>
                                </tr>   
                            </table>
                            <div id="picDiv">
                            </div>
                        </div> 
                        <div class="div2">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        <span>*</span>栏目：</td>
                                    <td class="td02">
                                        <select name="classid">
                                        <option value="0">-- 请选择栏目 --</option>
                                        <%=myclass.ShowOption(classArray,0,0,3) %>
                                        </select>
                                        <script type="text/javascript">                                       
                                        $.setElementValue(webForm.classid,"<%=Product.classid %>","0");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>标题：</td>
                                    <td class="td02">
                                        <input type="text" name="title" class="text" value="<%=Product.title %>" style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        产品编号：</td>
                                    <td class="td02">
                                        <input type="text" name="productnum" class="text" value="<%=Product.productnum %>" style="width: 80px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        价格：</td>
                                    <td class="td02">
                                        市场价:<input type="text" name="saleprice" class="text" value="<%=Product.saleprice %>" style="width: 50px;" />&nbsp;&nbsp;
                                        会员价:<input type="text" name="memberprice" class="text" value="<%=Product.memberprice %>" style="width: 50px;" />&nbsp;&nbsp;
                                        参考价1:<input type="text" name="price1" class="text" value="<%=Product.price1 %>" style="width: 50px;" />&nbsp;&nbsp
                                        参考价1:<input type="text" name="price2" class="text" value="<%=Product.price2 %>" style="width: 50px;" />&nbsp;&nbsp;
                                        <span>单位:元</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        关键字：</td>
                                    <td class="td02">
                                        <input type="text" name="keywords" class="text" value="<%=Product.keywords %>" style="width: 300px;" /><span>请用逗号隔开,如:手机,配件</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        其他参数1：</td>
                                    <td class="td02">
                                        <input type="text" name="other1" class="text" value="<%=Product.other1 %>" style="width: 300px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        其他参数2：</td>
                                    <td class="td02">
                                        <input type="text" name="other2" class="text" value="<%=Product.other2 %>" style="width: 300px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        其他参数3：</td>
                                    <td class="td02">
                                        <input type="text" name="other3" class="text" value="<%=Product.other3 %>" style="width: 300px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        库存数量：</td>
                                    <td class="td02">
                                        <input type="text" name="stocks" class="text" value="<%=Product.stocks %>" style="width: 50px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>相关说明：</td>
                                    <td class="td02">                                    
                                        <textarea name="thetext" style="display: none;"><%=Product.thetext %></textarea>
                                        <iframe id="eEditorLite" src="../content/eEditorLite/ewebeditor.htm?id=thetext&style=gray" frameborder="0"  scrolling="no" width="100%" height="380"></iframe>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="td01">
                                        点击数：</td>
                                    <td class="td02">
                                        <input type="text" name="hits" class="text" value="<%=Product.hits %>" style="width: 100px;" />
                                    </td>
                                </tr>                             
                                <tr>
                                    <td class="td01">
                                        其他属性：</td>
                                    <td class="td02">
                                    
                                        <input type="checkbox" name="status" value="1" />
                                        上架/销售
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.status,"<%=Product.status %>");
                                        </script>
                                        
                                        <input type="checkbox" name="ishot" value="1" />
                                        热点
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.ishot,"<%=Product.ishot %>");
                                        </script>
                                        
                                        <input type="checkbox" name="iscommend" value="1" />
                                        推荐
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.iscommend,"<%=Product.iscommend %>");
                                        </script>
                                        
                                        <input type="checkbox" name="isnew" value="1" />
                                        新产品
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.isnew,"<%=Product.isnew %>");
                                        </script>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="td01">
                                        时间：</td>
                                    <td class="td02">
                                        <input type="text" name="creationdate" class="text" readonly="readonly" value="<%=Product.creationdate %>" style="width: 150px;" /> <span>如:2008-4-11</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        排序号：</td>
                                    <td class="td02">
                                        <input type="text" name="sort" class="text" value="<%=Product.sort %>" style="width: 50px;" />
                                    </td>
                                </tr>
                            </table>
                        </div>                        
                    </div>
                    <div class="submit">
                        <button type="submit" class="btn" >保存修改</button>&nbsp;&nbsp;<button class="btn" onclick="window.location='product.asp';" >返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
