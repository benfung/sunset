﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/object.asp"-->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<%
' 不使用输出缓冲区，直接将运行结果显示在客户端
Response.Buffer = true
dim path,asppages,jsfile


' 该变量(httphost)与生成HTMl相关
dim httphost
httphost="http://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("URL")
httphost=mid(httphost,1,InStrRev(httphost,"/")-1)
httphost=mid(httphost,1,InStrRev(httphost,"/"))
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                更新脚本
            </div>
            <div class="contentDiv" style="text-align: center; height: 550px;">
                <div class="ListDiv" style="width: 98%;">
                    <div>
                        正在处理,请稍侯...
                        <div <% if admin.Purview<>1 then %> style="display:none;"<%end if %>>
                        <% set myclass=new classobj %>
                        <% set regin=new regionObj %>
                        <div class="ListDivSubDiv" onmouseover="this.style.background='#f6f9fd'" onmouseout="this.style.background=''">
                            <%
                                jsfile="../js/class.js"
                                ' product分类
                                myclass.typename="product"
                                jsStr= jsStr & myclass.JsArray(myclass.typename & "Array") & vbLf
                                
                                ' news分类
                                myclass.typename="news"
                                jsStr= jsStr & myclass.JsArray(myclass.typename & "Array") & vbLf
                                
                                ' channel分类
                                myclass.typename="channel"
                                jsStr= jsStr & myclass.JsArray(myclass.typename & "Array") & vbLf
                                
                                
                                ' links分类
                                myclass.typename="links"
                                jsStr= jsStr & myclass.JsArray(myclass.typename & "Array") & vbLf
                                
                                ' 地区分类
                                jsStr= jsStr & regin.JsArray("areaArray") & vbLf
                                
                                WriteTextFile Server.MapPath(jsfile),jsStr,conDefaultPageCharset
                                Response.Write jsfile & " ---------------------- 完成<br /> "
                            %>
                        </div>                      
                        <span style="color:Green;">完成</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<%call closeconn %>