﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<%
set admin=new adminObj
admin.AdminObjByStatus()
if admin.isnull() then
    response.redirect "signin.asp"
	response.End()
end if
%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />    
    <title>管理系统</title>
    <!--#include file="headmeta.asp"-->
    <style type="text/css">
        body { margin: 0px auto; background: #dfe8f6; text-align: center; }
        table { width: 100%; border: 0px; border-collapse: collapse; }
        td, div { font-family: Tahoma,Simsun,Helvetica,sans-serif; font-size: 12px; text-align: left; vertical-align: top; color: #000; }
        form { margin: 0px; padding: 0px; }
        img { border: 0px; }
        input { vertical-align: middle; }
        .text { font-size: 12px; border: 1px solid #4e74a5; height: 20px; }
        .button { font-family: tahoma,宋体; color: #15428b; font-size: 9pt; border: solid 1px #3376bc; background: #fff url('images/btn_bg.png') repeat-x bottom; }
        .textarea { font-size: 12px; border: 1px solid #4e74a5; }
        a, a:link, a:visited { color: #08D; text-decoration: none; }
        a:hover { color: #08D; text-decoration: underline; }
        a.b:link, a.b:visited { color: #666; text-decoration: none; }
        a.b:hover { color: #08D; text-decoration: underline; }
        .left { padding: 40px; width: 42%; border-right: solid 1px #ddd; text-align: right; vertical-align: middle; }
        .left div { text-align: right; }
        .right { padding: 40px; vertical-align: middle; }
        div.note { color: #25A; font-size: 14px; }
        .td01 { padding-top: 8px; text-align: right; width: 90px; }
        .td02 { padding-bottom: 10px; }
        .td02 span { color: #666; }
        .bottom { height: 26px; border-top: solid 1px #cadffb; padding: 5px 20px; }
    </style>
</head>
<body>
    <div style="height:25px; padding:3px 20px; background:url('images/topbg.gif') repeat-x;"><input type="button" class="button" id="btnLogin" onClick="window.parent.main.location='admin_pwd.asp';" value=" 修改密码 " />&nbsp;&nbsp;<input type="button" class="button" onClick="window.parent.main.location='siteconfig.asp';" value=" 网站配置 " /><% if admin.purview=1 then %>&nbsp;&nbsp;<input type="button" class="button" onClick="window.parent.main.location='aspcheck.asp?T=B';" value=" 网站探针 " />&nbsp;&nbsp;<input type="button" class="button" onClick="window.parent.main.location='spacesize.asp';" value=" 空间占用情况 " /><% end if %>&nbsp;&nbsp;<span style="FILTER: Glow(Color=#0F42A6, Strength=2) dropshadow(Color=#0F42A6, OffX=2, OffY=1,); padding-left:30px; font-weight:bolder; font-size:14px;">&copy;&nbsp;2009 <%=conSysname %> 后台管理系统 4.0 ACCESS</span></div>
</body>
</html>
<%call closeconn %>
