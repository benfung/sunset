﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,idset,idArray,where,friendlinks,classid,isgood,flag
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

filePath="?"

CurrentPage=Cint(getquerystring("pageno",1))

set vote=new voteObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>

</head>
<%
' 批量操作
if action="del" then    
    for i=0 to ubound(idArray)
	    conn.execute "delete from vote where id =" & idArray(i)
    next       
end if

if action="select" then    
    conn.execute "update vote set isselected=1 where id in (" & idset & ")"
end if

if action="unselect" then    
    conn.execute "update vote set isselected=0 where id in (" & idset & ")"
end if

if action="radio" then    
    conn.execute "update vote set votetype='radio' where id in (" & idset & ")"
end if

if action="checkbox" then    
    conn.execute "update vote set votetype='checkbox' where id in (" & idset & ")"
end if

' 组织SQL
where=""
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                高级搜索
            </div>
            <div class="contentdiv" style="text-align:center; height:550px;">
                <div class="listdiv"  style="width:98%;">
                    <div class="SetDiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <select name="action" onchange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>                                        
                                        <option value="select" >&nbsp;&nbsp;首页显示</option>
                                        <option value="unselect" >&nbsp;&nbsp;撤消首页显示</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="radio" >&nbsp;&nbsp;设为单选</option>
                                        <option value="checkbox" >&nbsp;&nbsp;设为多选</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div> 
                    <div >
                        <%                        
                        set rs=vote.load("",where,"order by a.id")
                        MaxItemSize=50
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<span style='color:red;font-size:14px;'>未添加任何数据!!</span>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        end if
                        %>
                        <table cellspacing="0" cellpadding="6"  border="1"  class="gv" style="border-collapse: collapse;">
                            <tr class="gvrowhead">
                                <th class="th" style="width: 20px;">
                                    <input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />
                                </th>
                                <th class="th" style="width: 40px;">编号 </th>
                                <th class="th" style="width: 80px;">类型 </th>
                                <th class="th">标题(总票数) </th>
                                <th class="th" style="width: 120px;">最后投票时间 </th>
                                <th class="th" style="width: 60px;">属性 </th>
                                <th class="th" style="width: 50px;">操作 </th>
                            </tr>
                            <% for i=1 to CurrentRecord(rs,CurrentPage) %>
                            <% vote.voteObj(rs.fields("id")) %>
<tr class="gvrow"><td class="tic"><input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" /></td><td class="tic"><%=rs.fields("id") %></td><td class="tic"><% if vote.votetype="radio" then %><span style="color:Red;">单选</span><%else %><span style="color:Green;">多选</span><%end if %></td><td class="til"><a href="../votedetail.asp?id=<%=rs.fields("id") %>" target="_blank" class="b"><%=vote.title %> </a>(<%=vote.total %>)</td><td class="tic"><%=vote.votetime  %></td><td class="tic"><% if rs.fields("isselected")=1 then %><span style="color:red;">首</span><% else %>&nbsp;&nbsp;<%end if %>&nbsp;</td><td class="tic"><a href="vote_edit.asp?id=<%=rs.fields("id") %>">编辑</a></td></tr>
<% rs.movenext
                              Next
                            %>
                        </table>
                        <% rs.close:set rs=nothing %>
                    </div>
                                       
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

