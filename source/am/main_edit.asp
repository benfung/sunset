﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,id,fso,main
action=getquerystring("action","")
id=getquerystring("id",0)

set fso = CreateObject("Scripting.FileSystemObject")
set main=new MainObj
main.MainObj(id)

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,2,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
            $.slipTab(1,2,"Tab","formTabA","formTabB","click",1);
            if($.trim(form.title.value)==""){
                alert("请填写标题!!");
                form.title.focus();
                return false;
            }            
        }
      -->
    </script>

</head>
<%
if action="save" then 
    if admin.Purview=1 then main.tempfile=getform("tempfile","help.asp")
    main.title=TurnSql(getform("title",""))
    main.content=TurnSql(getform("content",main.title))
    main.pic=getform("pic","")
    main.webTitle=TurnSql(getform("webTitle",webtitle))
    main.webKeyword=TurnSql(getform("webKeyword",main.title))
    main.webDescription=getform("webDescription",main.title)
    main.link=getform("link","")
    main.islink=getform("islink",0)
    if admin.Purview=1 then  main.filename=getform("filename","")
    main.dtime=getform("dtime",now())
    main.sort=getform("sort",0)
    main.status=getform("status",0)
    
    if main.isnull() then
        if main.filename<>"" and fso.FileExists(Server.MapPath("../" & main.filename)) then
            AlertBack("文件名已存在!!")
        else
            main.add()   
			call confirm("操作成功<br />是否继续添加","?","main.asp?action=create&idset=" & main.id)         
        end if        
    else
         main.update()
         call alert("提示:修改成功!!","main.asp?action=create&idset=" & main.id)
    end if
end if

if main.isnull() then
    main.status=1
    main.link=""
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                单页面管理 <span class="titleNavSpot">&nbsp;</span>
                <% if main.isnull then %>
                添加
                <% else %>
                编辑
                <% end if %>
            </div>
            <div class="contentdiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">基本属性</td>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab2">其他属性</td>
                        <td class="formTabMsg">
                            带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=main.id %>" name="webForm" method="post" class="aform" onSubmit="return checkForm(this)">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        <span>*</span>模板：</td>
                                    <td class="td02">
                                        <select name="tempfile">
                                            <option value="help.asp">模板一</option>
                                        </select>

                                        <script type="text/javascript">$.setElementValue(webForm.tempfile,"<%=main.tempfile %>");</script>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>标题：</td>
                                    <td class="td02">
                                        <input type="text" name="title" class="text" value="<%=main.title %>" style="width: 150px;" /><span>如:关于我们</span></td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        生成文件：</td>
                                    <td class="td02">
                                        <input type="text" name="filename" class="text" value="<%=main.filename %>"
                                            style="width: 150px;" /><span>如:aboutus.htm(不生成文件请留空)</span>
                                            </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        外部链接：</td>
                                    <td class="td02">
                                        <input type="text" name="link" class="text" value="<%=main.link %>"
                                            style="width: 250px;" />&nbsp;
                                            <input type="checkbox" name="islink" value="1" />
                                        启用外部链接
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.islink,"<%=main.islink %>");
                                        </script></td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>主要内容：</td>
                                    <td class="td02">
                                        <textarea name="content" style="display: none;"><%=main.content %></textarea>
                                        <iframe id="eEditorLite" src="../content/eEditorLite/ewebeditor.htm?id=content&style=gray" frameborder="0"  scrolling="no" width="100%" height="320"></iframe>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        相关图片：</td>
                                    <td class="td02">
                                        <input type="text" id="pic" name="pic" class="text" value="<%=main.pic %>" style="width: 250px;" />&nbsp;
                                        <a href="javascript:void(0);" onClick="DelFile(pic.value,pic)" class="c">删除</a>
                                        <div>
                                            <span id="spanButtonPlaceHolder"></span>
                                        </div>
                                        <div class="fsUploadProgress" id="fsUploadProgress"><div></div>
                                        <img src="images/loadingAnimation.gif" align="absmiddle" />
                                        </div>
                                        <script>
                                            UploadInit("spanButtonPlaceHolder", $.FileType["Image"], "图片文件", 0, 200, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "pic","false","fsUploadProgress")
                                        </script>
                                        </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        排序号：</td>
                                    <td class="td02">
                                        <input type="text" name="sort" class="text" value="<%=main.sort %>" style="width: 50px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        状态：</td>
                                    <td class="td02">
                                        <input type="checkbox" name="status" value="1" />
                                        正常
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.status,"<%=main.status %>");
                                        </script>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="div1" id="Tab2_Box">
                        <div class="div2">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <!--<tr>
                                    <td class="td01">
                                        页面标题：</td>
                                    <td class="td02">
                                        <input type="text" name="webTitle" class="text" value="<%=main.webTitle %>"
                                            style="width: 150px;" /></td>
                                </tr>-->
                                <tr>
                                    <td class="td01">
                                        关键字：</td>
                                    <td class="td02">
                                        <textarea name="webKeyword" class="textarea" rows="2" cols="60"><%=main.webKeyword %></textarea></td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        描述meta：</td>
                                    <td class="td02">
                                        <textarea name="webDescription" class="textarea" rows="3" cols="60"><%=main.webDescription %></textarea></td>
                                </tr>
                                <!--<tr>
                                    <td class="td01">
                                        时间：</td>
                                    <td class="td02">
                                        <input type="text" name="dtime" class="text" readonly="readonly" value="<%=main.dtime %>"
                                            style="width: 150px;" /><span>如:2008-4-11</span></td>
                                </tr>-->
                            </table>
                        </div>
                    </div>
                    <div class="submit">
                        <button type="submit"  class="btn" >保存修改</button>&nbsp;&nbsp;<button class="btn" onclick="window.history.back();" >返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%set fso=nothing:call closeconn %>