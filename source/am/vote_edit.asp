﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="../inc/md5.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,id,vote
action=getquerystring("action","")
id=getquerystring("id",0)

set vote=new voteobj
vote.voteobj(id)
' 设置默认值
if vote.isnull then
    vote.isselected=1 
    vote.votetype="radio"
end if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form) {
            if($.trim(form.title.value)==""){
                alert("请填写链接!!");
                form.title.focus();
                return false;
            }
        }
      -->
    </script>

</head>
<%

if action="save" then
    vote.title=(getForm("title",""))
	vote.select1=(getForm("select1",""))
	vote.answer1=getForm("answer1",0)
	vote.select2=(getForm("select2",""))
	vote.answer2=getForm("answer2",0)
	vote.select3=(getForm("select3",""))
	vote.answer3=getForm("answer3",0)
	vote.select4=(getForm("select4",""))
	vote.answer4=getForm("answer4",0)
	vote.select5=(getForm("select5",""))
	vote.answer5=getForm("answer5",0)
	vote.select6=(getForm("select6",""))
	vote.answer6=getForm("answer6",0)
	vote.select7=(getForm("select7",""))
	vote.answer7=getForm("answer7",0)
	vote.select8=(getForm("select8",""))
	vote.answer8=getForm("answer8",0)
	vote.votetime=getForm("votetime",now())
	vote.votetype=(getForm("votetype","radio"))
	vote.startdate=getForm("startdate",now())
	vote.enddate=getForm("enddate",now())
	vote.isselected=getForm("isselected",0)
	vote.status=getForm("status",1)

	if vote.isnull() then
		vote.add()
		call confirm("操作成功<br />是否继续添加","?","vote.asp") 
	else
		vote.update()
		call alert("操作成功","vote.asp")
	end if	
end if
%>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                投票管理 <span class="titleNavSpot">&nbsp;</span>
                <% if vote.isnull then %>
                添加
                <% else %>
                编辑
                <% end if %>
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            基本属性</td>
                        <td class="formTabMsg">
                            带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=vote.id %>" name="webForm" method="post" class="aform"  onSubmit="return checkForm(this)">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2" style="height:100px;">
                            <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        <span>*</span>标题：</td>
                                    <td class="td02">
                                        <input type="text" name="title" class="text" value="<%=vote.title %>"  style="width: 255px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        类型：</td>
                                    <td class="td02">
                                        <input type="radio" name="votetype" value="radio" /> 单选
                                        <input type="radio" name="votetype" value="checkbox" /> 多选
                                        <script type="text/javascript">
                                            $.setElementValue(webForm.votetype, "<%=vote.votetype %>");
                                        </script>  
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        选项1：</td>
                                    <td class="td02">
                                        <input type="text" name="select1" class="text" value="<%=vote.select1 %>"  style="width: 255px;" />&nbsp;
                                        <input type="text" name="answer1" class="text" value="<%=vote.answer1 %>"  style="width: 60px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        选项2：</td>
                                    <td class="td02">
                                        <input type="text" name="select2" class="text" value="<%=vote.select2 %>"  style="width: 255px;" />&nbsp;
                                        <input type="text" name="answer2" class="text" value="<%=vote.answer2 %>"  style="width: 60px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        选项3：</td>
                                    <td class="td02">
                                        <input type="text" name="select3" class="text" value="<%=vote.select3 %>"  style="width: 255px;" />&nbsp;
                                        <input type="text" name="answer3" class="text" value="<%=vote.answer3 %>"  style="width: 60px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        选项4：</td>
                                    <td class="td02">
                                        <input type="text" name="select4" class="text" value="<%=vote.select4 %>"  style="width: 255px;" />&nbsp;
                                        <input type="text" name="answer4" class="text" value="<%=vote.answer4 %>"  style="width: 60px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        选项5：</td>
                                    <td class="td02">
                                        <input type="text" name="select5" class="text" value="<%=vote.select5 %>"  style="width: 255px;" />&nbsp;
                                        <input type="text" name="answer5" class="text" value="<%=vote.answer5 %>"  style="width: 60px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        选项6：</td>
                                    <td class="td02">
                                        <input type="text" name="select6" class="text" value="<%=vote.select6 %>"  style="width: 255px;" />&nbsp;
                                        <input type="text" name="answer6" class="text" value="<%=vote.answer6 %>"  style="width: 60px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        选项7：</td>
                                    <td class="td02">
                                        <input type="text" name="select7" class="text" value="<%=vote.select7 %>"  style="width: 255px;" />&nbsp;
                                        <input type="text" name="answer7" class="text" value="<%=vote.answer7 %>"  style="width: 60px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        选项8：</td>
                                    <td class="td02">
                                        <input type="text" name="select8" class="text" value="<%=vote.select8 %>"  style="width: 255px;" />&nbsp;
                                        <input type="text" name="answer8" class="text" value="<%=vote.answer8 %>"  style="width: 60px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        时间：</td>
                                    <td class="td02">
                                        <input type="text" name="startdate" class="text" value="<%=shortdate(vote.startdate) %>" onFocus="setday(this)"  style="width: 80px;" /> 至
                                        <input type="text" name="enddate" class="text" value="<%=shortdate(vote.enddate) %>" onFocus="setday(this)"  style="width: 80px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        其他属性：</td>
                                    <td class="td02">
                                        <input type="checkbox" name="isselected" value="1" />
                                        首页显示
                                        <script type="text/javascript">
                                            $.setElementValue(webForm.isselected, "<%=vote.isselected %>");
                                        </script>                                      
                                    </td>
                                </tr>  
                                <tr>
                                    <td class="td01">
                                        总票数：</td>
                                    <td class="td02b">
                                        <%=vote.total %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        最后投票时间：</td>
                                    <td class="td02b">
                                        <%=vote.votetime %>
                                    </td>
                                </tr>                          
                            </table>
                        </div>                        
                    </div>
                    <div class="submit">
                        <button type="submit"  class="btn" >保存修改</button>&nbsp;&nbsp;<button class="btn" onclick="window.location='vote.asp';" >返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
