﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/conn.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim myclass,classArray,classArray2
set myclass=new classobj
classArray=myclass.GetArrayByTypename("news")
classArray2=myclass.GetArrayByTypename("channel")

dim action,id,news
action=getquerystring("action","")
id=getquerystring("id",0)

set news=new NewsObj
news.NewsObj(id)

' 设置默认值
if news.isnull then
    news.publisher=admin.loginid
    news.status=1 ' 默认已审核
    
    news.classid=getquerystring("classid",0)
end if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
        
            if($.trim(form.classid.value)=="0"){
                alert("请选择一级栏目!!");                
                form.classid.focus();
                return false;
            }
            if($.trim(form.title.value)==""){
                alert("请填写标题!!");
                form.title.focus();
                return false;
            }
        }        		
        
        
        // 更新图片集字段
        function updatePicSet(ifilename,obj){
            if($.trim(ifilename)!=""){
                if(obj.value==""){
                    obj.value=ifilename;
                }
                else{
                    obj.value += "|"+ifilename;
                }
            }            
        }
      -->
    </script>

</head>
<%
if action="save" then   
    news.classid=getform("classid",0)
    news.channelid=getform("channelid",0)
    news.title=TurnSql(getform("title",""))
    news.keywords=TurnSql(getform("keywords",news.title))
    news.thetext=TurnSql(getform("thetext",news.title))    
    news.mediafile=getform("mediafile","")
    news.focuspic=getform("focuspic","")
    news.picset=getform("picset","")    
    news.publisher=getform("publisher","")    
    news.newsfrom=getform("newsfrom","")
    news.hits=getform("hits",0)  
    news.ispic=getform("ispic",0)  
    news.ishot=getform("ishot",0)
    news.iscommend=getform("iscommend",0)
    news.creationdate=getform("creationdate",now())
    news.sort=getform("sort",0)
    news.status=getform("status",0)
    
    if news.isnull() then
        news.add()
		call confirm("操作成功<br />是否继续添加","?","news.asp?action=create&idset=" & news.id)    
    else
        news.update()
        call toPage("news.asp?action=create&idset=" & news.id)
    end if
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
            新闻资讯管理 <span class="titleNavSpot">&nbsp;</span>
                <% if news.isnull then %>
                添加
                <% else %>
                编辑
                <% end if %>
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            基本属性</td>
                        <td class="formTabMsg">
                            带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=save&id=<%=news.id %>" name="webForm" method="post" class="aform" onSubmit="return checkForm(this)">
                    <input name="picset" type="hidden" value="<%=news.picset %>" />
                    <div class="div1" id="Tab1_Box">
                        <div class="div2">
                            <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01">
                                        <span>*</span>栏目：</td>
                                    <td class="td02">
                                        <select name="classid">
                                        <option value="0">-- 请选择栏目 --</option>
                                        <%=myclass.ShowOption(classArray,0,0,3) %>
                                        </select>
                                        <script type="text/javascript">                                        
                                        $.setElementValue(webForm.classid,"<%=news.classid %>","0");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        频道：</td>
                                    <td class="td02">
                                        <select name="channelid">
                                        <option value="0">-- 请选择频道 --</option>
                                        <%=myclass.ShowOption(classArray2,0,0,3) %>
                                        </select>
                                        <script type="text/javascript">
                                            $.setElementValue(webForm.channelid, "<%=news.channelid %>", "0");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>标题：</td>
                                    <td class="td02">
                                        <input type="text" name="title" class="text" value="<%=news.title %>" style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        关键字：</td>
                                    <td class="td02">
                                        <input type="text" name="keywords" class="text" value="<%=news.keywords %>" style="width: 300px;" /><span>请用逗号隔开,如:手机,配件</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        <span>*</span>主要内容：</td>
                                    <td class="td02">                                    
                                        <textarea name="thetext" style="display: none;"><%=news.thetext %></textarea>
                                        <iframe id="eEditorLite" src="../content/eEditorLite/ewebeditor.htm?id=thetext&style=gray" frameborder="0"  scrolling="no" width="100%" height="380"></iframe>
                                    </td>
                                </tr> 
                                <tr>
                                    <td class="td01">
                                        视频文件：</td>
                                    <td class="td02">
                                        <input type="text" id="mediafile" name="mediafile"  class="text" value="<%=news.mediafile %>" style="width: 250px;" />
                                        <a href="javascript:void(0);" onClick="DelFile(mediafile.value,mediafile)" class="c">删除</a>                                        
                                        <span>只支持FLV</span>
                                        <div><span id="spanButtonPlaceHolder"></span></div>
                                        <div class="fsUploadProgress" id="fsUploadProgress"><div></div>
                                        <img src="images/loadingAnimation.gif" align="absmiddle" />
                                        </div>
                                        <script>
                                            UploadInit("spanButtonPlaceHolder", $.FileType["Media"], "媒体", 0, 20000, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "mediafile", "false", "fsUploadProgress")
                                        </script>
                                    </td>
                                </tr>  
                                <tr>
                                    <td class="td01">
                                        相关焦点图片：</td>
                                    <td class="td02">
                                        <input type="text" id="focuspic" name="focuspic"  class="text" value="<%=news.focuspic %>" onChange="updatePicSet(this.value,this.form.picset)" style="width: 250px;" />
                                        <a href="javascript:void(0);" onClick="DelFile(focuspic.value,focuspic)" class="c">删除</a>
                                        
                                        <input type="checkbox" name="ispic" value="1" />
                                        包含焦点图
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.ispic,"<%=news.ispic %>");
                                        </script>
                                        <div><span id="spanButtonPlaceHolder1"></span></div>
                                        <div class="fsUploadProgress" id="fsUploadProgress1"><div></div>
                                        <img src="images/loadingAnimation.gif" align="absmiddle" />
                                        </div>
                                        <script>
                                            UploadInit("spanButtonPlaceHolder1", $.FileType["Image"], "图片文件", 0, 200, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete, "focuspic", "true","fsUploadProgress1")
                                        </script>
                                    </td>
                                </tr>   
                                
                                <tr>
                                    <td class="td01">
                                        发布者：</td>
                                    <td class="td02">
                                        <input type="text" name="publisher" class="text" value="<%=news.publisher %>" style="width: 100px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        来源：</td>
                                    <td class="td02">
                                        <input type="text" name="newsfrom" class="text" value="<%=news.newsfrom %>" style="width: 100px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        点击数：</td>
                                    <td class="td02">
                                        <input type="text" name="hits" class="text" value="<%=news.hits %>" style="width: 100px;" />
                                    </td>
                                </tr>
                                                          
                                <tr>
                                    <td class="td01">
                                        其他属性：</td>
                                    <td class="td02">
                                    
                                        <input type="checkbox" name="status" value="1" />
                                        审核通过
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.status,"<%=news.status %>");
                                        </script>
                                        
                                        <input type="checkbox" name="ishot" value="1" />
                                        热点
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.ishot,"<%=news.ishot %>");
                                        </script>
                                        
                                        <input type="checkbox" name="iscommend" value="1" />
                                        首页显示
                                        <script type="text/javascript">
                                        $.setElementValue(webForm.iscommend,"<%=news.iscommend %>");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        时间：</td>
                                    <td class="td02">
                                        <input type="text" name="creationdate" class="text" readonly="readonly" value="<%=Shortdate(news.creationdate) %>"  onClick="setday(this)" style="width: 150px;" /> <span>如:2008-4-11</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01">
                                        排序号：</td>
                                    <td class="td02">
                                        <input type="text" name="sort" class="text" value="<%=news.sort %>" style="width: 50px;" />
                                    </td>
                                </tr>
                            </table>
                        </div>                        
                    </div>
                    <div class="submit">
                        <button type="submit"  class="btn" >保存修改</button>&nbsp;&nbsp;<button class="btn" onclick="window.location='news.asp';" >返 回</button></div>
                </form>
            </div>
        </div>
        <%=formatnumber((timer()-PageStartTime)*1000,3)%> ms
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
