﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/md5.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站配置</title>
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
            if($.trim(form.password1.value).length<4){
                alert("请输入旧密码且长度必须大于4！！");
                form.password1.focus();
                return false;
            }
            if($.trim(form.password2.value).length<4){
                alert("请输入新密码且长度必须大于4！！");
                form.password2.focus();
                return false;
            }
            if($.trim(form.password2.value)!=$.trim(form.password3.value)){
                alert("确认密码错误！！请重新输入");
                form.password2.value='';
                form.password3.value='';
                form.password2.focus();
                return false;
            }
        }
      -->
    </script>

</head>
<% 
password1=getform("password1","")
password2=getform("password2","")
password3=getform("password3","")

if getQueryString("action","")="save" then
    if password1="" then
        call alertback("请输入旧密码!!")
    end if
    if password2="" then
        call alertback("请输入新密码!!")
    end if
    if password3<>password2 then
        call alertback("确认密码错误!!")
    end if
    
    if admin.pwd<> GenPasswrod(password1,admin.loginid) then
        call alertback("旧密码输入错误!!")
    end if
    
    admin.pwd=GenPasswrod(password2,admin.loginid)
    admin.update()
    call Alert("密码修改成功!!重新登陆!!","SignIn.asp?action=Logout&url=default.asp")
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                管理员安全信息
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">
                            &nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">
                            修改密码</td>
                        <td class="formTabMsg">
                            所有项必须填写</td>
                    </tr>
                </table>
                <form action="?action=save" name="webForm" method="post" class="aform" onsubmit="return checkForm(this)"><div class="div1" id="Tab1_Box"><div class="div2" style="height:100px;"><table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc"><tr><td class="td01" style="width:100px;">当前用户：</td><td class="td02"><%=admin.loginid %></td></tr><tr><td class="td01" style="width:100px;">旧密码：</td><td class="td02"><input type="password" name="password1" class="text" style="width: 200px;" /> </td></tr><tr><td class="td01" style="width:100px;">新密码：</td><td class="td02"><input type="password" name="password2" class="text"style="width: 200px;" /></td></tr><tr><td class="td01" style="width:100px;">确认密码：</td><td class="td02"><input type="password" name="password3" class="text"style="width: 200px;" /></td></tr></table></div></div><div class="submit"><input type="submit" value=" 提 交 " class="btn" />&nbsp;&nbsp;<input type="button"value=" 返 回 " class="btn" onclick="window.history.back();" /></div></form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
