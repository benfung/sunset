﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="../Inc/MailObject.asp" -->
<!--#include file="isAdmin.asp"-->
<% 
dim action
action=getquerystring("action","")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){  
            if($.trim(form.inceptemail.value)==""){
                alert("请填写收件人Email!!");
                form.inceptemail.focus();
                return false;
            } 
            if($.trim(form.title.value)==""){
                alert("请填写邮件主题!!");
                form.title.focus();
                return false;
            }  
            if($.trim(form.sendername.value)==""){
                alert("请填写发件人!!");
                form.sendername.focus();
                return false;
            }
            if($.trim(form.senderemail.value)==""){
                alert("请填写发件人Email!!");
                form.senderemail.focus();
                return false;
            } 
        }        
      -->
    </script>

</head>
<%
if action="send" then   
    set Mail=new MailObject        
    Mail.MailServer=conMailServer
    Mail.MailServerUserName=conMailServerUserName
    Mail.MailServerPassWord=conMailServerPassWord
    Mail.MailDomain=conMailDomain
        
    title=getform("title","测试邮件")
    body=getform("content","测试邮件")
    inceptemails=split(getform("inceptemail",""),",")
    sendername=getform("sendername",conSiteName)
    senderemail=getform("senderemail",conWebmasterEmail)
    
    For Each inceptemail in inceptemails
        mail.send inceptemail,inceptemail, title, body, sendername, senderemail, 3
    next
    set mail=nothing    
    call alert("操作成功<br />邮箱发送完成","maillist.asp")    
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                邮件列表管理
            </div>
            <div class="contentdiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">发送邮件</td>
                        <td class="formTabMsg">带*号为必填项</td>
                    </tr>
                </table>
                <form action="?action=send" name="webForm" method="post" class="aform" onSubmit="return checkForm(this)">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2">
                            <table cellpadding="3" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                                <tr>
                                    <td class="td01"><span>*</span>收件人Email：</td>
                                    <td class="td02">
                                        <textarea rows="2" cols="50" class="textarea" id="inceptemail" name="inceptemail"><%=getquerystring("mail","") %></textarea><br /><br /><span class="inputBtn" style="padding:3px 10px;"><a href='javascript:function(){return false;} ' onclick='$.ajax({ type: "GET", url: "getuser.asp", success: function(msg){ inceptemail.value=msg; }}); '>所有会员邮箱地址...</a></span>&nbsp;&nbsp;<span>多个Email间请用<font style="color: #f00;">英文的逗号</font>分隔</span> <br />&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="td01"><span>*</span>邮件主题：</td>
                                    <td class="td02">
                                        <input type="text" name="title" class="text" value="<%=getquerystring("title","") %>" style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01"><span>*</span>邮件内容：</td>
                                    <td class="td02">
                                        <textarea name="content" style="display: none;"></textarea>
                                        <iframe id="eEditorLite" src="../content/eEditorLite/ewebeditor.htm?id=content&style=mini" frameborder="0"  scrolling="no" width="100%" height="340"></iframe>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01"><span>*</span>发件人：</td>
                                    <td class="td02">
                                        <input type="text" name="sendername" class="text" value="<%=conSiteName %>" style="width: 250px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td01"><span>*</span>发件人Email：</td>
                                    <td class="td02">
                                        <input type="text" name="senderemail" class="text" value="<%=conWebmasterEmail %>" style="width: 250px;" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="submit">
                        <button type="submit"  class="btn" >保存修改</button>&nbsp;&nbsp;<button class="btn" onclick="window.history.back();" >返 回</button></div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
