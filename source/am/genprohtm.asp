﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/object.asp"-->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<%
Server.ScriptTimeout=50000000
' 不使用输出缓冲区，直接将运行结果显示在客户端
Response.Buffer = true
dim path,asppages,htmlpage



' 该变量(httphost)与生成HTMl相关
dim httphost
httphost="http://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("URL")
httphost=mid(httphost,1,InStrRev(httphost,"/")-1)
httphost=mid(httphost,1,InStrRev(httphost,"/"))
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                生成静态页面
            </div>
            <div class="contentDiv" style="text-align: center; height: 550px;">
                <div class="ListDiv" style="width: 98%;">
                    <div>
                        正在处理,请稍侯...
                        <div <% if admin.Purview<>1 then %> style="display:none;"<%end if %>>
                        <!--////////////////////////////////////////////////-->
                        <!--产品详细页-->
                        <%
                            set product=new productObj
                            set rs=product.load("","","order by a.id desc")
                            for i=1 to rs.recordcount
                        %>
                        <div class="ListDivSubDiv" onmouseover="this.style.background='#f6f9fd'" onmouseout="this.style.background=''">
                            <%
                                    page="productdetail.asp?id=" & rs.fields("id") 
                                    htmlpage="../productdetail_" & rs.fields("id") & ".htm"
                                    MyAsp2Html page,htmlpage
                            %>
                        </div>
                        <%
                            rs.movenext:next
                            rs.close:set rs=nothing
                        %>
                        <!--////////////////////////////////////////////////-->
                        <!--产品类别-->
                        <%
                             set myclass=new classObj
                            set rs=myclass.load("","where a.typename='product'","order by a.id")
                            for j=1 to rs.recordcount
                        %>                        
                        <div class="ListDivSubDiv"  onmouseover="this.style.background='#f6f9fd'" onmouseout="this.style.background=''">
                            <%
                                    page="product.asp?classid=" & rs.fields("id")
                                    htmlpage="../product_" & rs.fields("id") & ".htm"
                                    call MyAsp2Html(page,htmlpage)
                                    
                                    set product=new productObj
                                    set pprs=product.load("","where a.status=1 and a.classid=" & rs.fields("id"),"")
                                    pprs.PageSize=24  ' 每页项数
                                    TotalPage=pprs.PageCount   ' 计算出总页数
                                    pprs.close:set pprs=nothing
                                    
                                    for p=1 to TotalPage
                                        page="product.asp?classid=" & rs.fields("id")&"&pageno="&p
                                        htmlpage="../product_" & rs.fields("id") & "_" & p & ".htm"
                                        call MyAsp2Html(page,htmlpage)
                                    next
                            %>
                        </div>
                        <%  rs.movenext:next
                            rs.close:set rs=nothing
                        %>
                        <span style="color:Green;">完成</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<%set fso=nothing:call closeconn %>

<%
function MyAsp2Html(aspfilename,htmlfilename)
    Response.Write ("<a href='" & htmlfilename & "' target='_blank'>" & htmlfilename & "</a>")
    dim fso,htmlpage
    set fso=CreateObject("Scripting.FileSystemObject")
    htmlpage=Server.MapPath(htmlfilename)
    if act="del" then        
        if fso.FileExists(htmlpage) then
            fso.DeleteFile(htmlpage)
        end if
    else        
        dim tempurl,Text
        if InStrRev(aspfilename,"?")>0 then
            tempurl=httphost & aspfilename & "&gen=n"
        else
            tempurl=httphost & aspfilename & "?gen=n"
        end if
        Text=getHTTPPage(tempurl,conDefaultPageCharset)
        WriteTextFile htmlpage,Text,conDefaultPageCharset
    end if
    set fso=nothing
    Response.Write (" ---------------------- 完成<br />")
    Response.Flush()
end function
%>