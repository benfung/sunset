﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,idset,idArray,feedback,where
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")


filePath="?"

CurrentPage=Cint(getquerystring("pageno",1))

set feedback=new feedbackObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>
</head>
<%
' 批量操作
if action="del" then    
    for i=0 to ubound(idArray)
	    conn.execute "delete from feedback where id =" & idArray(i)
    next       
end if
' 组织SQL
where="where a.types=1"
%>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                网站公告
            </div>
            <div class="contentDiv" style="text-align:center; height:550px;">
                <div class="searchDiv" style="padding:4px;"><span class="inputBtn" style="padding:3px 10px;"><a href="callboard_edit.asp?KeepThis=true&TB_iframe=true&height=280&width=650" title="添加" >添加公告</a></span></div>
                <div class="ListDiv"  style="width:98%;">
                    <div class="SetDiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td><input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />全选&nbsp;
                                    <select name="action" onchange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div> 
                    <div >
                        <%                        
                        set rs=feedback.load("",where,"order by a.id desc")
                        MaxItemSize=10
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<span style='color:red;font-size:14px;'>未添加任何数据!!<span>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        end if
                        %>
                        <% for i=1 to CurrentRecord(rs,CurrentPage) %>
                        <table cellpadding="6" cellspacing="0" class="table1" border="1"  style="margin-bottom:10px; border:solid 1px #eee;">
                            <tr style="background:#dbeafc">
                                <td class="td02b">
                                    <input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" />&nbsp;<span style="font-weight:bolder; color:Black;"><%=rs.fields("title") %></span>
                                    &nbsp;<a href="callboard_edit.asp?id=<%=rs.fields("id") %>&KeepThis=true&TB_iframe=true&height=330&width=450" title="编辑网站公告" class="" style="color:Blue; text-decoration:underline;">编辑</a>
                                </td>
                            </tr>  
                            <tr>
                                <td class="td02b" style="line-height:150%;">
                                    <span style="color:Green;">发布于 <%=shortdate(rs.fields("addtime")) %>&nbsp;&nbsp;&nbsp;</span> <br />
                                    <%=rs.fields("content") %>                                   
                                </td>
                            </tr>                           
                        </table>
                            <% rs.movenext
                               next
                            %>
                        <% rs.close:set rs=nothing %>
                    </div>
                                       
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

