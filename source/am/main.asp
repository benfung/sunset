﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim action,idset,fso,idArray
action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")
set main=new mainObj
set fso = CreateObject("Scripting.FileSystemObject")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="?action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>

</head>
<%
' 该变量(httphost)与生成HTMl相关
dim httphost
httphost="http://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("URL")
httphost=mid(httphost,1,InStrRev(httphost,"/")-1)
httphost=mid(httphost,1,InStrRev(httphost,"/"))

if action="del" then    
    for i=0 to ubound(idArray)
        main.mainObj(idArray(i))
        filename=Server.MapPath("../" & main.filename)
        If (fso.FileExists(filename)) Then
		    fso.DeleteFile filename
	    end if
	    conn.execute "delete from main where id =" & idArray(i)
    next
    set fso=nothing    
    call Alert("操作成功执行","?")
end if

if action="create" then
    Response.Write "页面生成中....请稍侯!!"
    for i=0 to ubound(idArray)
        MyAsp2Html idArray(i),fso  '生成html
    next
	call alert("操作完成,页面已生成","main_edit.asp?id=" & idArray(i-1))
end if
%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                单页面管理
            </div>
            <div class="contentdiv" style="text-align:center; height:480px;">
                <div class="listdiv"  style="width:80%;">
                    <% if admin.Purview=1 then %>
                    <div class="SetDiv"><table cellpadding="0" cellspacing="0"><tr><td><input type="button" value=" 批量删除 " class="btn" onClick="SetSet('del','册除后不能恢复,确定要册除吗?');" />&nbsp;<input type="button" value=" 批量生成 " class="btn" onClick="SetSet('create','生成将会覆盖原来文件,确定要生成吗?');" /></td></tr></table></div>  
                    <% end if %> 
                    <div >
                        <table cellspacing="0" cellpadding="6"  border="1" class="gv"  style="border-collapse: collapse;">
                            <tr class="gvrowhead"><th class="th"  style="width: 20px;"><input type="checkbox" name="checkbox" onClick="$.SelectAllCB(this,'idset');" value="0" /></th><th class="th"  style="width: 40px;">编号</th><th class="th" >标题</th><th class="th"  style="width: 200px;">文件名</th><th class="th"  style="width: 120px;">操作</th></tr>
                            <% set rs=main.load("","","order by a.id ") %>
                            <% for i=1 to rs.RecordCount %><tr class="gvrow"><td class="tic"><input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" /></td><td class="tic"><%=rs.fields("id") %></td><td class="til"><%=rs.fields("title") %></td><td class="til"><% if rs.fields("filename")<>"" then %><a href="../<%=rs.fields("filename") %>" target="_blank"><%=rs.fields("filename") %></a> <% If fso.FileExists(Server.MapPath("../" & rs.fields("filename"))) Then%><span style="color:green;">√</span><% else %><span style="color:Red;">×</span><% end if %><% else %><a href="../help.asp?id=<%=rs.fields("id") %>" target="_blank">help.asp?id=<%=rs.fields("id") %></a><% end if %></td><td class="tic"><a href="main_edit.asp?id=<%=rs.fields("id") %>">编辑</a></td></tr>
                            <% rs.movenext:next %>
                            <% rs.close:set rs=nothing %>
                        </table>
                    </div>
                                     
                </div>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%set fso=nothing:call closeconn %>

<%
function MyAsp2Html(id,fso)
    dim tempobj
    Set tempobj=new MainObj
    tempobj.MainObj(id)
    if tempobj.islink=0 and tempobj.filename<>"" and fso.FileExists(Server.MapPath("../" & tempobj.tempfile)) then    
        dim tempurl,Text
        tempurl=httphost & tempobj.tempfile & "?gen=n&id=" & tempobj.id
        Text=getHTTPPage(tempurl,conDefaultPageCharset)
        WriteTextFile Server.MapPath("../" & tempobj.filename),Text,conDefaultPageCharset
    end if
end function
%>