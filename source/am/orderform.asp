﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim myclass
set myclass=new classobj
myclass.typename="pay"
dim action,idset,idArray,where,title,status
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

title=getquerystring("title","")
status=getquerystring("status","")
idate=getquerystring("idate","")
filePath="?title=" & title & "&status=" & status & "&idate=" & idate & "&"

CurrentPage=Cint(getquerystring("pageno",1))

set orderform=new ordersObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>
    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>
</head>
<%
' 批量操作
if action="del" then    
    for i=0 to ubound(idArray)
        orderform.ordersObj(idArray(i))
        orderform.delete()
    next       
end if

' 设置status=6
if action="status6" then  
    conn.execute "update orders set status=6,BeginDate='" & now() & "' where id in (" & idset & ")"
end if

' 设置status=5
if action="status5" then  
    conn.execute "update orders set status=5,BeginDate='" & now() & "' where id in (" & idset & ")"
end if

' 设置status=4
if action="status4" then  
    conn.execute "update orders set status=4,BeginDate='" & now() & "' where id in (" & idset & ")"
end if

' 设置status=3
if action="status3" then  
    conn.execute "update orders set status=3,BeginDate='" & now() & "' where id in (" & idset & ")"
end if

' 设置status=2
if action="status2" then  
    conn.execute "update orders set status=2,BeginDate='" & now() & "' where id in (" & idset & ")"
end if

' 设置status=1
if action="status1" then  
    conn.execute "update orders set status=1,BeginDate='" & now() & "' where id in (" & idset & ")"
end if

' 设置status=0
if action="status0" then    
    conn.execute "update orders set status=0 where id in (" & idset & ")"
end if


' 组织SQL
where="where a.id>0"
if title<>"" then
    where = where & " and (a.ordernum like '%" & title & "%' or a.firstname like '%" & title & "%' or a.email like '%" & title & "%')"
end if
if status<>"" then
    where = where & " and a.status=" & status
end if
if idate<>"" then
    where = where & " and (inputtime between #" & idate & "# and #" & DateAdd("d",1,idate) & "#)"
end if

%>
<body>
    <div class="bodydiv">
        <div id="newitem" class="jbox" style="width: 500px;">
        <div class="closejbox"><img src="images/close.gif" alt="" align="absmiddle" onclick="$.RemoveJBox($('#newitem'));" /></div>
        <fieldset>
            <legend>高级搜索</legend>
            <form method="get" action="?" name="webForm" class="aform">
            <table cellpadding="6" cellspacing="2" class="table1">
                <tr>
                    <td class="td01">关键字: </td>
                    <td class="td02">
                        <input type="text" name="title" class="text" value="<%=title %>" style="width: 255px;" />
                        
                    </td>
                </tr>
                <tr>
                    <td class="td01">日期: </td>
                    <td class="td02">
                        <input type="text" name="idate" class="text" onclick="setday(this);" value="<%=shortdate(idate) %>" style="width: 100px;" />                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="td02" style="text-align: center;">
                        <button type="submit" class="btn">
                            搜 索</button>&nbsp;&nbsp;<button type="button" class="btn" onclick="$.RemoveJBox($('#newitem'));">
                            关 闭</button></td>
                </tr>
            </table>
            </form>
        </fieldset>
    </div>
    
        <div class="bodybox">
            <div class="titlenav">
                高级搜索
            </div>
            <div class="contentDiv" style="text-align:center; height:550px;">
                <div class="ListDiv"  style="width:98%;">
                    <div class="SetDiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <select name="action" onChange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>                                        
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="status1" >&nbsp;&nbsp;标记为等待客服处理</option>
                                        <option value="status2" >&nbsp;&nbsp;标记为有效等待支付</option>
                                        <option value="status3" >&nbsp;&nbsp;标记为有效正在配货</option>
                                        <option value="status4" >&nbsp;&nbsp;标记为已发货</option>
                                        <option value="status5" >&nbsp;&nbsp;标记为完成</option>
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="status6" >&nbsp;&nbsp;标记为客户退货</option>
                                        <option value="status0" >&nbsp;&nbsp;标记为失效</option>
                                        
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select>
                                    标记为<span style="color:Red;">失效</span>的订单才能<span style="color:Red;">删除</span>
                                    &nbsp;
                                    <button class="btn" onclick="$.ShowJBox($('#newitem'));">搜索。。。</button>
                                    </td>
                            </tr>
                        </table>
                    </div> 
                    <div >
                        <%
                        
                        set rs=orderform.load("",where,"order by a.id desc")
                        MaxItemSize=50
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<div style='color:red;font-size:14px;margin:50px;'>没有匹配的记录!!</div>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        
                        %>
                        <table cellspacing="0" cellpadding="6"  border="1" class="gv" style="border-collapse: collapse; ">
                            <tr class="gvrowhead">
                                <th class="th" style="width: 20px;">
                                        <input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />
                                </th>
                                <th class="th" style="width: 130px;">
                                        订单编号
                                </th>
                                <th class="th">
                                        收货人
                                </th>
                                <th class="th" style="width: 200px;">
                                        Email
                                </th>
                                <th class="th" style="width: 100px;">
                                        金额(<%=conCurrencyUnit %>)
                                </th>
                                <th class="th" style="width: 100px;">
                                        订单状态
                                </th>
                                <th class="th" style="width: 80px;">
                                        操作
                                </th>
                            </tr>
                           <% for i=1 to CurrentRecord(rs,CurrentPage) %>
<tr class="gvrow"><td class="tic"><input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" /></td><td class="tic"><%=rs.fields("ordernum") %></td><td class="tic"><%=rs.fields("firstname") %></td><td class="tic"><%=rs.fields("email") %></td><td class="tic"><%=rs.fields("moneytotal")%></td><td class="tic"><%=GetOrderStatus(rs.fields("status")) %></td><td class="tic"><a href="orderform_edit.asp?id=<%=rs.fields("id") %>" title="订单详情" class="b" >订单详情</a></td></tr>
                            <% rs.movenext
                               next
                            %>
                        </table>
                        <% end if %>
                        <% rs.close:set rs=nothing %>
                    </div>                                       
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>                
            </div>
            
        </div>
        <div style="line-height:150%;">
                状态说明：<br />                
                <span style="color:red;">等待客服处理</span>(1) -- 客户下单,管理员未处理<br />
                <span style="color:darkmagenta;">有效等待支付</span>(2)  -- 管理员判断为有效订单，等待客户付款<br />
                <span style="color:cadetblue;">有效正在配货</span>(3) -- 正在配送货物<br />
                <span style="color:darkorange;">已发货</span>(4) -- 管理员已发送货物<br />
                <span style="color:green;">完成</span>(5) -- 整个订单流程执行完成<br />
                <span style="color:indigo;">退货</span>(6) -- 客户退货<br />
                <span style="color:dimgray;">失效/取消</span>(0) ---- 客户取消 或 管理员判断为无效订单<br />
                </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

<%
function date2num(val)
    date2num=join(split(val,"-"),"")
end function

function num2date(val)
    dim dateArray(2)
    dateArray(0)=left(val,4)
    dateArray(1)=Mid(val,5,2)
    dateArray(2)=right(val,2)
    num2date=Cdate(Join(dateArray,"-"))
end function
%>