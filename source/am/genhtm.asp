﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../inc/object.asp"-->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<%
' 不使用输出缓冲区，直接将运行结果显示在客户端
Response.Buffer = true
dim path,asppages,htmlpage


' 该变量(httphost)与生成HTMl相关
dim httphost
httphost="http://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("URL")
httphost=mid(httphost,1,InStrRev(httphost,"/")-1)
httphost=mid(httphost,1,InStrRev(httphost,"/"))
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                生成静态页面
            </div>
            <div class="contentDiv" style="text-align: center; height: 550px;">
                <div class="ListDiv" style="width: 98%;">
                    <div>
                        正在处理,请稍侯...
                        <div <% if admin.Purview<>1 then %> style="display:none;"<%end if %>>
                        <%for each page in split(conCustomerPage,"|")  %>
                        <div class="ListDivSubDiv" onmouseover="this.style.background='#f6f9fd'" onmouseout="this.style.background=''">
                            <%
                                    htmlpage=mid(page,1,InStr(page,".")) & "htm"
                                    htmlpage="../" & htmlpage
                                    MyAsp2Html page,htmlpage
                            %>
                        </div>
                        <%next %>                        
                        <span style="color:Green;">完成</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<%set fso=nothing:call closeconn %>

<%
function MyAsp2Html(aspfilename,htmlfilename)
    Response.Write ("<a href='" & htmlfilename & "' target='_blank'>" & htmlfilename & "</a>")
    dim fso,htmlpage
    set fso=CreateObject("Scripting.FileSystemObject")
    htmlpage=Server.MapPath(htmlfilename)
    if act="del" then        
        if fso.FileExists(htmlpage) then
            fso.DeleteFile(htmlpage)
        end if
    else        
        dim tempurl,Text
        if InStrRev(aspfilename,"?")>0 then
            tempurl=httphost & aspfilename & "&gen=n"
        else
            tempurl=httphost & aspfilename & "?gen=n"
        end if
        Text=getHTTPPage(tempurl,conDefaultPageCharset)
        WriteTextFile htmlpage,Text,conDefaultPageCharset
    end if
    set fso=nothing
    Response.Write (" ---------------------- 完成<br />")
    Response.Flush()
end function
%>