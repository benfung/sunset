﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isAdmin.asp"-->
<%call closeconn %>
<% 
dim oldsize,newsize
oldsize=0
newsize=0
If Instr(strDB,":")>0 Then : strDB = strDB : Else : strDB = Server.MapPath(strDB) : End If
filepath=mid(strDB,1,InStrRev(strDB,"\"))

dim errormsg
errormsg=""
Set fso = CreateObject("Scripting.FileSystemObject")
if fso.FileExists(strdb) then
    oldsize=fso.GetFile(strdb).Size  ' 读取旧文件大小

    strNewDB=filepath & "cTemp.mdb"

    if IsObjInstalled("JRO.JetEngine") then
        ' 执行压缩
        dim engine,strpvd
        Set Engine = Server.CreateObject("JRO.JetEngine")
        strPvd = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="
        Engine.CompactDatabase strPvd & strDB , strPvd & strNewDB
        set Engine = nothing

        newsize=fso.GetFile(strNewDB).Size ' 读取新文件大小

        fso.DeleteFile strDB  '删除旧的数据库文件
        fso.MoveFile strNewDB, strDB  ' 将压缩好的数据库文件拷贝回来
    else
		call alertback("压缩失败。。。系统不支持 JRO.JetEngine")
    end if
else
	call alertback("压缩失败。。。文件不存在")
end if
set FSO = nothing

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
      -->
    </script>
</head>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                数据库压缩
            </div>
            <div class="contentdiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;
                            </td>
                        <td class="formTabB" style="width: 80px;" id="Tab1">
                            压缩完成</td>
                        <td class="formTabMsg">&nbsp;
                            </td>
                    </tr>
                </table>
                    <form name="webForm" class="aform">
                    <div class="div1" id="Tab1_Box">
                        <div class="div2">
                            <% if errormsg<>"" then Response.Write("<span style='color:red;'>" & errormsg & "</span>") %>
                             <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                             <tr>
                                <td class="td01">数据库：</td>
                                <td class="td02b">
                                    <%=replace(strDB,filepath,"") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">压缩前：</td>
                                <td class="td02b">
                                    <%=oldsize/1024 %> KB
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">压缩后：</td>
                                <td class="td02b">
                                    <%=newsize/1024 %> KB
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">压缩率：</td>
                                <td class="td02b">
                                    <%=FormatPercent(newsize/oldsize) %>
                                </td>
                            </tr>
                            </table>                            
                        </div>                        
                    </div>
                    </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
