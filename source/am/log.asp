﻿<%@language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="isadmin.asp"-->
<% 

dim action,idset,idArray,usertype,where
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage

action=getquerystring("action","")
idset=getquerystring("idset","0")
idArray=split(idset,",")

usertype=getquerystring("usertype","admin")
filePath="?usertype=" & usertype & "&"

CurrentPage=Cint(getquerystring("pageno",1))

set loginlog=new loginlogObj
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>网站管理后台_2.01</title>    
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    
    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
        });
        
        function SetSet(action,msg){
            var sets=$.getChecked('idset').join(',');
            if(sets==''){
                alert("请选定要操作的记录!!");
            }else if(action==null||action==''){
                alert("不明操作！！\r\n请检查参数:action");
            }else{
                if(msg!=null){
                    if(!confirm(msg)) return false;
                }
                window.location="<%=filePath %>pageno=<%=CurrentPage %>&action=" + action + "&idset=" + sets
            }
        }
      -->
    </script>

</head>
<%
' 批量操作
if action="del" then     
    conn.execute "delete from loginlog where id in (" & idset & ")"
end if



' 组织SQL
where="where a.usertype='" & usertype & "'"
if title<>"" then
    where = where & " and a.username like '%" & title & "%'"
end if

%>
<body>
    <div class="bodydiv">
        <div class="bodybox">
            <div class="titlenav">
                登陆记录
            </div>
            <div class="contentdiv" style="text-align:center; height:550px;">
                <div class="listdiv"  style="width:98%;">
                    <div class="setdiv">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <select name="action" onchange="SetSet(this.value,'确定要执行该操作吗?');this.value=''">
                                        <option value="" style="color:#666;">其他操作...</option>    
                                        <option value="" style="color:#666;">-----------</option>
                                        <option value="del" >&nbsp;&nbsp;×&nbsp;删除</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div>  
                    <div >
                        <%
                        
                        set rs=loginlog.load("",where,"order by a.id desc")
                        MaxItemSize=18
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<div style='color:red;font-size:14px;margin:50px;'>没有匹配的记录!!</div>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        
                        %>
                        <table cellspacing="0" cellpadding="6" rules="all" border="1" class="gv"  style="border-collapse: collapse;">
                            <tr class="gvrowhead">
                                <th class="th" style="width: 20px;">
                                        <input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />
                                </th>
                                <!--<th class="th"  style="width: 40px;">
                                        编号
                                </th>-->
                                <th class="th" style="width: 100px;">操作</th>
                                <th class="th" >用户名</th>
                                <th class="th"  style="width: 120px;">IP</th>
                                <th class="th" style="width: 150px;">登陆时间</th>
                            </tr>
                            <% for i=1 to CurrentRecord(rs,CurrentPage) %>
                            <tr class="gvrow">
                                <td class="tic">
                                    <input type="checkbox" class="idset" name="idset" value="<%=rs.fields("id") %>" />
                                </td>
                                <!--<td class="tic">
                                    <%=rs.fields("id") %>
                                </td>-->
                                <td class="tic">
                                    <%=rs.fields("logcontent") %>
                                </td>
                                <td class="til">
                                    <%=rs.fields("username") %>
                                </td>
                                <td class="tic">
                                    <%=rs.fields("ip") %>
                                </td>
                                <td class="tic">
                                    <%=rs.fields("LogTime") %>
                                </td>
                            </tr>
                            <% rs.movenext
                               next
                            %>
                        </table>
                        <% end if %>
                        <% rs.close:set rs=nothing %>
                    </div>
                                      
                </div>
                <%call AbineShowPage2(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>

