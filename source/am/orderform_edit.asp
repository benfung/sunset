﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/cf.asp"-->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../inc/function.asp"-->
<!--#include file="../inc/md5.asp"-->
<!--#include file="isAdmin.asp"-->
<% 
dim region
set region=new regionobj

dim myclass
set myclass=new classobj
myclass.typename="pay"

dim action,id,orderform
action=getquerystring("action","")
id=getquerystring("id",0)


set orderform=new ordersObj
orderform.ordersObj(id)


%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>网站管理后台_2.01</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <script language="javascript" type="text/javascript">
      <!--  
        // 在document就绪或加载完成时执行
        $(document).ready(function(){
            $.slipTab(1,1,"Tab","formTabA","formTabB","click",1);
        });
        function checkForm(form){
        }
      -->
    </script>

</head>
<%
if action="save" then
    orderform.remark=getForm("remark","")
    orderform.update()
    call alert("操作成功!!","?id=" & id)
end if
%>
<body>
    <div class="bodyDiv ">
        <div class="bodyBox">
            <div class="titleNav">
                订单信息
            </div>
            <div class="contentDiv">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabA" style="width: 80px;" id="Tab1">基本属性</td>
                        <td class="formTabMsg">&nbsp;</td>
                    </tr>
                </table>
                <form name="webForm2" action="?action=save&id=<%=id %>" class="aform" method="post">
                <div class="div1" id="Tab1_Box">
                    <div class="div2" style="height: 300px;">
                        <!--<input type="button" value=" 打 印 " class="inputBtn" onClick="window.print();" />-->
                        <table cellpadding="6" cellspacing="0" class="table1" border="1" bordercolor="#dbeafc">
                            <tr>
                                <td class="td01">订单状态：</td>
                                <td class="td02b">
                                    <%=GetOrderStatus(orderform.status) %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">订单号：</td>
                                <td class="td02b">
                                    <%=orderform.ordernum %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">收货人：</td>
                                <td class="td02b">
                                    <%=orderform.firstname %> <%=orderform.lastname %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">国家/城市：</td>
                                <td class="td02b">
                                    <% if conOpenSelectRegion=1 then %>
                                    <% region.regionObj(orderform.country):response.Write region.name  %>&nbsp; 
                                    <% region.regionObj(orderform.state):response.Write region.name  %>&nbsp;
                                    <% region.regionObj(orderform.city):response.Write region.name  %>&nbsp;   
                                    <% else %>
                                        <% region.regionObj(orderform.country):response.Write region.name  %> <%=orderform.state %> <%=orderform.city %>
                                    <% end if %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">收货地址：</td>
                                <td class="td02b">
                                    <%=orderform.address %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">邮编：</td>
                                <td class="td02b">
                                    <%=orderform.postcode %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">手机：</td>
                                <td class="td02b">
                                    <%=orderform.mobile %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">联系电话：</td>
                                <td class="td02b">
                                    <%=orderform.phone %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">电子邮件：</td>
                                <td class="td02b">
                                    <%=orderform.email %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">付款方式：</td>
                                <td class="td02b">
                                    <%=orderform.paymenttype %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">送货方式：</td>
                                <td class="td02b">
                                    <%=orderform.delivertype %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">客户留言：</td>
                                <td class="td02b">
                                    <%=orderform.content %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">录入时间：</td>
                                <td class="td02b">
                                    <%=shortdate(orderform.InputTime ) %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">处理日期：</td>
                                <td class="td02b">
                                    <%=shortdate(orderform.BeginDate) %>
                                </td>
                            </tr>
                            <tr>
                                <td class="td01">备注/EMS快递号：</td>
                                <td class="td02">
                                    
                                    <input type="text" class="text" name="remark" value="<%=orderform.remark %>" style="width:300px;" />
                                    &nbsp;<input type="submit" class="btn" value=" 更新 " />
                                    &nbsp;&nbsp;<input type="button"
                            value=" 返 回 " class="btn" onClick="window.location='orderform.asp';" />
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>&nbsp;</div>
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="formTabSpace">&nbsp;</td>
                        <td class="formTabB" style="width: 80px;" id="Tab2">订购产品</td>
                        <td class="formTabMsg">&nbsp;</td>
                    </tr>
                </table>
                <div class="div1" id="Tab2_Box">
                    <div >
                        <%
                        set orderformitem=new orderitemObj
                        orderformitem.orderid=id
                        set rs=orderformitem.LoadItems()
                        if rs.EOF then
                            Response.Write ("<span style='color:red;font-size:14px;'>没有数据!!<span>")
                        end if
                        %>
                        <table cellspacing="0" cellpadding="6" class="gv"  border="1" style="border-collapse: collapse; ">
                            <tr class="gvrowhead">
                                <th class="th" style="width: 60px;">图</th><th class="th"  >产品名称</th><th class="th" style="width: 90px;">原价</th><th class="th"  style="width: 90px;">售价</th><th class="th"  style="width: 50px;">数量</th><th class="th"  style="width: 90px;">合计</th>
                            </tr>
                            <% for i=1 to rs.RecordCount %>
                            <tr class="gvrow">
                                <td class="tic2"><a href="../product/detail.asp?id=<%=rs.fields("productid") %>" target="_blank" ><img src="../s_<%=rs.fields("focuspic") %>" width="54" /></a></td><td class="til"><%=rs.fields("productname") %><br /><%=rs.fields("productnumber") %></td><td class="tic"><s><%=FormatNumber(rs.fields("price_original"))%></s></td><td class="tic"><%=FormatNumber(rs.fields("trueprice"))%></td><td class="tic"><%=rs.fields("amount") %></td><td class="tir"><%=FormatNumber(rs.fields("subtotal"))%></td>                               
                            </tr>
                            <% rs.movenext
                               next
                            %>
                            <tr>
                                <td colspan="5" class="tic"><b>其他费用(运费):</b></td>
                                <td  class="tir"><%=FormatNumber(orderform.fee)  %></td>
                            </tr>
                            <tr style="background:#f6f9fd;">
                                <td colspan="5" class="tic"><b>合计:</b></td>
                                <td  class="tir"><%=conCurrencyUnit %> <%=FormatNumber(orderform.moneytotal)  %></td>
                            </tr>
                        </table>
                        <% rs.close:set rs=nothing %>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
