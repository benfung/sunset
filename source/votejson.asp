﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="inc/conn.asp"-->
<!--#include file="inc/cf.asp"-->
<!--#include file="Inc/object.asp" -->
<!--#include file="inc/function.asp"-->
<% 
dim vote
set vote=new voteobj
    
dim id
id=getquerystring("id",0)
vote.voteobj(id)
%>
{
  "elements" : [
    {
      "tip" : "#val# / #total# <br>#percent#",
      "colours" : [
        "0x336699", "0x88AACC", "0x999933", "0x666699",
		"0xCC9933", "0x006666", "0x3399FF", "0x993300",
		"0xAAAA77", "0x666666", "0xFFCC66", "0x6699CC",
		"0x663366", "0x9999CC", "0xAAAAAA", "0x669999",
		"0xBBBB55", "0xCC6600", "0x9999FF", "0x0066CC",
		"0x99CCCC", "0x999999", "0xFFCC00", "0x009999",
		"0x99CC33", "0xFF9900", "0x999966", "0x66CCCC",
		"0x339966", "0xCCCC33"
      ],
      
      "alpha" : "0.5",
      "ani--mate" : true,
      "font-size":12,
      "values" : [
      <% for i=0 to vote.totalitem %>
      <% if vote.ItemSet(i,1)<>"" then%>    
        {"value" : <%=vote.ItemSet(i,2) %>,"label" : "<%=vote.ItemSet(i,1) %>"}       
	 <% if i<(vote.totalitem-1) then Response.Write(",") %>
	 <% end if %>
	 <% next %>
      ],
      "type" : "pie",
      "border" : "2"
    }
  ],
  "bg_colour" : "#ffffff"
  
}
<%call closeconn %>