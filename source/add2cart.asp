﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/md5.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>
        <%=conSiteTitle %>
    </title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--#include file="headmeta.asp"-->
</head>
<body>
<%
' 动作
' 添加:add2cart.asp?productid=X
' 修改:
' 删除:add2cart.asp?action=del&productid=X
' 清空购物车:add2cart.asp?action=clear

' --- 初始化cartid ----------------------------------------
dim myshopcart
set myshopcart=new shoppingcartsObj
myshopcart.setCartID(MD5(Now() & Timer(),16))
' ---------------------------------------------------------
action=getquerystring("action","add")
productid = getquerystring("productid",0)


Select Case Action
    Case "add"  ' 加入购物车------------------
        if productid<>"0" then
            quantity=cint(getForm("quantity",1))
            myshopcart.productid=productid
            
            If myshopcart.HasSame Then  ' 判断购物车是否存在相同产品,是则为对象变量赋值
                '更新购物车产品数量
                myshopcart.quantity=myshopcart.quantity+quantity
                myshopcart.update()
            Else
                '添加新产品到购物车
                myshopcart.quantity=quantity
                myshopcart.add()
            End If
        else
            call topage("index.asp")
        end if
    Case "del"  ' 从购物车中删除---------------------
        myshopcart.productid=productid
        myshopcart.Delete()
    Case "mod"  ' 批量修改数量----------------------
        dim productidset,otherset,tempid,tempother
        productidset=split(getForm("productidset",""),",")
        
        for i=0 to ubound(productidset)            
            tempid=trim(productidset(i))            
            myshopcart.productid = tempid
            
            Quantity = getform("quantity" & tempid,1)         
            If myshopcart.HasSame Then
                myshopcart.quantity = Quantity                   
                myshopcart.update()
            End If
              
        next     
    Case "clear" ' 清空购物车--------------------------
        myshopcart.clear()
        call toPage("index.asp")
End Select

call toPage("step1.asp")
%>
</body>
</html>
<%call CloseConn %>