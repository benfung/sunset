﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/MailObject.asp" -->
<!--#include file="Inc/Md5.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>取回密码 -
        <%=conSiteTitle %>
    </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
dim myuser
set myuser=new myuserObj

dim loginid,password,action,rurl,validatecode,errormsg
action=getquerystring("action",getForm("action",""))
rurl="index.asp"
email=getForm("email","")
validatecode=getform("validatecode","")


errormsg=""
if action="send" then 
    myuser.email=email
    if not myuser.HasEmail() then
        errormsg="没有用户注册此邮箱地址!!"
    end if
    if validatecode<>Cstr(Session("ValidateCode")) then    
        errormsg="对不起,验证码不正确!!"    
    end if
    
    if errormsg="" then
        myuser.myuserObjByEmail()
        
        ' 生成新密码
        password=GetRndPassword(6)
        
        ' 发送邮件
        set Mail=new MailObject        
        Mail.MailServer=conMailServer
        Mail.MailServerUserName=conMailServerUserName
        Mail.MailServerPassWord=conMailServerPassWord
        Mail.MailDomain=conMailDomain
        subject=conSiteName &" - 取回密码"
        body= "你好,以下是网站" & conSiteTitle & "发送给你的新密码:<br /><br /><b>" & password & "</b> <br /><br /><a href='" & conSiteUrl & "signin.asp'>马上登陆</a>"        
        mail.send myuser.email,myuser.email, subject, body, conSiteTitle, conWebmasterEmail, 3
        
        ' 更新密码
        myuser.pwd=MD5(password,32)
        myuser.update() 
        
        call alert("发送成功","?")       
        
    end if
    
end if
%>
<body style="background:#cddcec;">
    <div class="body" style="text-align:center;">
    <script language="javascript">
        function CheckForm(form) {
            if (!$.Validator["email"].test(form.email.value)) {
                alert("请输入邮箱地址！");
                form.email.select();
                return false;
            }
            if (!$.Validator["require"].test(form.validatecode.value)) {
                alert("请输入您的验证码！");
                form.validatecode.select();
                return false;
            }
        }
        
    </script>
    <img id="top" src="images/top.png" alt="" align="absmiddle" />
	<div style="background:white; width:770px;">
	
		<div style="background:#4b75b3; color:White; width:100%; padding:10px;"><h1 >取回密码</h1></div>
        <form name="webForm" id="webForm" class="appnitro" method="post" action="?" onsubmit="return CheckForm(this)">
            <input type="hidden" name="action" value="send" />
            <ul>
            <li class="section_break">
                    <h3>
                        请输入你注册时的邮箱地址</h3>
                </li>
            <% if ErrorMsg<>"" then  %><li class="imsg">
                    <%=ErrorMsg %>
                </li>
                <% end if %>
                <li>
                    <label class="description" for="email">
                        Email
                    </label>
                    <div>
                        <input id="email" name="email" class="element text medium" type="text" value="" maxlength="100"/>
                    </div>
                </li>
                <li>
                    <label class="description" for="validatecode">
                        验证码
                    </label>
                    <div>
                        <input id="validatecode" name="validatecode" class="element text small" type="text" maxlength="5" />
                        &nbsp;<img src="inc/validateimg.asp" onclick="this.src=this.src;" style="cursor: pointer;" align="absmiddle" />
                    </div>
                    <p class="guidelines">
                        请在左边输入你看到的数字或字母</p>
                </li>
                <li class="buttons">
                    <input id="saveForm" class="button" type="submit" name="submit" value="发 送" />&nbsp;&nbsp;&nbsp;
                    <input  class="button" type="button"  value="关闭页面(Ctrl + W)" onclick="javascript:window.close();" />
                </li>
            </ul>
        </form>
    </div>
	<img id="bottom" src="images/bottom.png" alt="" align="absmiddle" />
    </div>
</body>
</html>
<%call closeconn %>
