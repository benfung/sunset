﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<% GeneratedHtml conOpenAsp2Html,conDefaultPageCharset ' 生成静态页 %>
<%
set news=new newsObj
%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>
        信息列表:<%=conSeoTitle %>
    </title>    
    <!--#include file="headmeta.asp"-->
    <meta name="keywords" content="<%=conSiteKeyword %>" />
    <meta name="description" content="<%=conSiteDescription %>" />
</head>
<body>
    <!--#include file="head.asp"-->
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <b>分类</b><br />
                    <% 
                    set newsclass=new ClassObj
                    newsclass.typename="news" 
                    set rs=newsclass.load("","where a.pid=0 and a.status=0 and typename='news'","order by a.sort,a.pid")
                     %>
                    <% do while not rs.eof %>
                    <a href="newslist.asp?classid1=<%=rs.fields("id") %>" class="linkB" target="_blank">
                        <%=rs.fields("name") %>
                    </a>
                    <br />
                    <% rs.movenext:loop %>
                    <% rs.close:set rs=nothing %>
            </td>
            <td>
                <div>
                    <b>推荐新闻</b><br />
                    <% set rs=news.load("top 10","where a.iscommend=1 and a.status=1","order by a.id desc") %>
                    <% do while not rs.eof %>
                    <a href="newsdetail_<%=rs.fields("id") %>.htm" class="linkB" target="_blank">
                        <%=rs.fields("title") %>
                    </a>
                    <br />
                    <% rs.movenext:loop %>
                    <% rs.close:set rs=nothing %>
                </div>
            </td>
        </tr>
    </table>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
