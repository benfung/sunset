﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>订单查询 -
        <%=conSiteTitle %>
    </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
if conOrderForUser=1 then
	' 验证用户
	call CheckUserLogin("")
end if

dim region
set region=new regionobj

dim orders
set orders=new ordersObj

dim ordernum,email,action,rurl
ordernum=turnsql(getquerystring("ordernum",""))
email=getquerystring("email","")
rurl="index.asp"

errormsg=""
if not Validator("email",email) or ordernum="" then
	errormsg="查询错误"
else
	orders.orderformObjByNE ordernum,email
	if orders.isnull then
		errormsg="订单号或邮件输入错误"
	end if
end if
%>
<body style="background:#cddcec;">
    <div class="body" style="text-align:center;">
    <img id="top" src="<%=conSiteUrl %>images/top.png" alt="" align="absmiddle" />
	<div style="background:white; width:770px;">
		<div style="background:#4b75b3; color:White; width:100%; padding:10px;"><h1 >订单</h1></div>
        <form name="webForm" id="webForm" class="appnitro" method="post" action="<%=conSiteUrl %>pay/default.asp">
            <ul>
            <% if ErrorMsg<>"" then  %>
            <li class="imsg">
                    <%=ErrorMsg %>
                </li>
                <% end if %>
                <li class="section_break">
                    <h3>
                        No:<b style="color:red;"><%=orders.ordernum %></b>
                        <br />
                        <b style="color:red;"><%=conCurrencyUnit %>  <%=orders.moneytotal %></b></h3>
                     <p><%=GetOrderStatus(orders.status) %></p><br />
                     <% if orders.status<4 and orders.status>0 then%>
                     <input type="hidden" name="orderid" value="<%=orders.id %>"  />
                     <input class="button" type="submit" value="马上支付" />
                     <%end if %>
                </li>
                <li>
                    	 <fieldset>
                         <legend>送货信息</legend>
                         <div ><%=orders.firstname %> <%=orders.lastname %><br />
                        <% if conOpenSelectRegion=1 then %>
						<% region.regionObj(orders.country):response.Write region.name  %>&nbsp; 
                        <% region.regionObj(orders.state):response.Write region.name  %>&nbsp;
                        <% region.regionObj(orders.city):response.Write region.name  %>&nbsp;   
                        <% else %>
                            <% region.regionObj(orders.country):response.Write region.name  %> <%=orders.state %> <%=orders.city %>
                        <% end if %><br />
                        <%=orders.address %><br />
                        <%=orders.postcode %><br />
                        <%=orders.phone %><br />
                        <%=orders.email %><br /></div>
                         </fieldset>
                </li>
                <li>
                    	 <fieldset>
                         <legend>其他信息</legend>
                         <div >                       
                        <%=orders.paymenttype %><br />
                        <%=orders.delivertype %><br /></div>
                         </fieldset>
                </li>
                <% if orders.remark<>"" then %>
                <li>
                    	 <fieldset>
                         <legend>快递追踪/客服回复</legend>
                         <div style="color:green;">
                        <%=orders.remark %><br /></div>
                         </fieldset>
                </li>
                <% end if %>
                <li style="width:80%;">
                    	 <fieldset>
                         <legend>订购商品</legend>
                         <div>
                         <table cellpadding="4" cellspacing="0" class="table2">	
                         	<% dim orderitem
							set orderitem=new orderitemobj
							orderitem.orderid=orders.id
							%>
                            <% set rs=orderitem.LoadItems() %>
                            <% for i=1 to rs.RecordCount %>
                            <tr>
                                <td class="tic2" width="60">
                                    <a href="<%=conSiteUrl %>productdetail.asp?id=<%=rs.fields("productid") %>" target="_blank"><img src="<%=conSiteUrl %>s_<%=rs.fields("focuspic")%>" align="absmiddle" alt width="54" /></a></td>
                                <td class="til" >
                                        <%=rs.fields("productname") %><br /><%=rs.fields("productnumber") %>
                                </td>
                                <td class="til" width="80">
                                     <%=FormatNumber(rs.fields("trueprice")) %>                                </td>
                                <td class="til" width="30">
                                	<%=rs.fields("amount") %>
                                </td>
                                <td class="tir" width="80">
                                    <%=conCurrencyUnit %>
                                    <%=FormatNumber(rs.fields("subtotal")) %>
                                </td>
                            </tr>
                            <%  rs.movenext %>
                            <% next %>
                            <% rs.close:set rs=nothing %>
                        </table>
                        <% if orders.fee<>0 then %>其他费用:<%=conCurrencyUnit %> <%=FormatNumber(orders.fee) %><% end if %>
                         </div>
                         </fieldset>
                        
                </li>
            </ul>
        </form>
    </div>
	<img id="bottom" src="<%=conSiteUrl %>images/bottom.png" alt="" align="absmiddle" />
    </div>
</body>
</html>
<%call closeconn %>
