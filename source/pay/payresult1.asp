﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../Inc/cf.asp" -->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../Inc/conn.asp" -->
<!--#include file="../Inc/function.asp" -->
<!--#include file="../Inc/MailObject.asp" -->
<!--#include file="../Inc/Md5.asp" -->
<!--#include file="../Inc/getuser.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>支付结果 -
        <%=conSiteTitle %>
    </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
dim payplatform
Const platformid = 1  '网银在线
set payplatform=new payplatformObj
payplatform.payplatformObj(platformid)

dim resultmsg
Dim  v_oid,v_pmode, v_pstatus, v_pstring, v_amount, v_md5str, v_date, v_moneytype,remark1,remark2
Dim md5string

v_oid=trim(request("v_oid"))                               ' 商户发送的v_oid定单编号
v_pmode=trim(request("v_pmode"))                           ' 支付方式（字符串） 
v_pstatus=trim(request("v_pstatus"))                       ' 支付状态 20（支付成功）;30（支付失败）
v_pstring=trim(request("v_pstring"))                       ' 支付结果信息 支付完成（当v_pstatus=20时）；失败原因（当v_pstatus=30时）；
v_amount=trim(request("v_amount"))                         ' 订单实际支付金额
v_moneytype=trim(request("v_moneytype"))                   ' 订单实际支付币种
remark1=trim(request("remark1"))                           ' 备注字段1(付款记录ID)
remark2=trim(request("remark2"))                           ' 备注字段2
v_md5str=trim(request("v_md5str"))                         ' 网银在线拼凑的Md5校验串

if v_md5str="" then
    resultmsg="支付失败<br />原因:v_md5str为空!!"
else
    md5string = Ucase(trim(MD5(v_oid & v_pstatus & v_amount & v_moneytype & payplatform.md5key, 32)))

    if md5string<>v_md5str then
        resultmsg="支付失败<br />原因:校验失败,数据可疑!!"
    elseif v_pstatus=30 then
        resultmsg=v_pstring
    elseif v_pstatus=20 then
        resultmsg="恭喜您，在线支付成功了！<br />相关人员马上为您配送商品。"
        
        '更新付款记录 -------------------------
        dim payrecord,orderform
        set payrecord=new payrecordObj
        payrecord.payrecordObj(remark1)
        payrecord.status=2 '付款成功
        payrecord.ebankinfo=v_pmode '付款成功
        payrecord.remark=v_pstring
        
        payrecord.Update()
        
        ' 更新订单状态------------------------
        'set orderform=new ordersObj
        'orderform.ordersObj(payrecord.orderformid)
        'orderform.status=1 ''设为已收款,发货中
        'orderform.update()
        
        '-----------------------------------
        
        
        '邮件发送代码 -------------------------
        set Mail=new MailObject        
        Mail.MailServer=conMailServer
        Mail.MailServerUserName=conMailServerUserName
        Mail.MailServerPassWord=conMailServerPassWord
        Mail.MailDomain=conMailDomain

        subject=conSiteName & " - 客户在线支付成功"
        body = "用户名:" & payrecord.username  & "<br />"
        body = body & "订单编号:" & v_oid  & "<br />"
        body = body & "实际付款金额:$" & v_amount & "<br />"
        body=body & "<br />请尽快配送商品!!<br />"
        body= body & "<br />相关链接<br /><a href='" & conSiteUrl & "checkorder.asp?formnumber=" & v_oid & "&email=" & orderform.email & "'>" & conSiteUrl & "/shopping/checkorder.asp?formnumber=" & v_oid & "&email=" & orderform.email & "</a><br />"
        mail.send conWebmasterEmail,conWebmasterEmail, subject, body, conMailServerUserName, conMailServerUserName, 3
        set Mail=nothing
    end if
end if






%>
<body style="background:#cddcec;">
    <div class="body" style="text-align:center;">
    <img id="top" src="../images/top.png" alt="" align="absmiddle" />
	<div style="background:white; width:770px;">
	
		<div style="background:#4b75b3; color:White; width:100%; padding:10px;"><h1 >支付结果</h1></div>
        <form name="webForm" id="webForm" class="appnitro" method="post" action="?" onsubmit="return CheckForm(this)">
            <input type="hidden" name="action" value="send" />
            <ul>
            <% if resultmsg<>"" then  %><li class="imsg">
                    <%=resultmsg %>
                </li>
                <% end if %>
                <li class="buttons">
                    <input  class="button" type="button"  value="关闭页面(Ctrl + W)" onclick="javascript:window.close();" />
                </li>
            </ul>
        </form>
    </div>
	<img id="bottom" src="../images/bottom.png" alt="" align="absmiddle" />
    </div>
</body>
</html>
<%call closeconn %>
