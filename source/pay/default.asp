﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="../Inc/cf.asp" -->
<!--#include file="../Inc/conn.asp" -->
<!--#include file="../Inc/object.asp" -->
<!--#include file="../Inc/function.asp" -->
<!--#include file="../Inc/md5.asp" -->
<!--#include file="../Inc/getuser.asp" -->
<!--在线付款-->
<%
dim v_amount,posturl,strHiddenField,v_md5info,v_moneytype,v_rurl
dim orderform,orderformid,payplatform,payrecord
orderid=getform("orderid",0)
set payplatform=new payplatformObj
payplatform.payplatformObj(getquerystring("payplatformid",1))

set orderform=new ordersObj
orderform.ordersObj(orderid)
if orderform.isnull then
    call topage("../")
end if

if payplatform.pluspoundage>0 then
    v_amount = Round(orderform.moneytotal + orderform.moneytotal * (payplatform.rate / 100),2) '加入手续费
else
    v_amount=orderform.moneytotal
end if

' 登记付款记录
set payrecord=new payrecordObj
payrecord.username=myuser.email
payrecord.orderformid=orderform.id
payrecord.paymentnum=orderform.ordernum
payrecord.platformid=payplatform.id
payrecord.moneypay=orderform.moneytotal
payrecord.moneytrue=v_amount
payrecord.paytime=Now()
payrecord.status=0
payrecord.add()

v_rurl = "http://" & Trim(Request.ServerVariables("HTTP_HOST")) & Trim(Request.ServerVariables("SCRIPT_NAME")) ' 商户自定义返回接收支付结果的页面 Receive.asp 为接收页面
v_rurl = Left(v_rurl, InStrRev(v_rurl, "/")) & "PayResult" & payplatform.id & ".asp"

Select Case payplatform.id
Case "1" '网银在线
    posturl = "https://pay3.chinabank.com.cn/PayGate?encoding=utf-8" '提交地址
    v_moneytype="CNY"    
    v_md5info = UCase(Trim(MD5(v_amount & v_moneytype & orderform.ordernum & payplatform.accountsid & v_rurl & payplatform.md5key,32)))
    
    strHiddenField="<input type='hidden' name='v_oid' value='" & orderform.ordernum &"' />"
    strHiddenField = strHiddenField & "<input type='hidden' name='v_md5info' value='" & v_md5info &"' />"
    strHiddenField = strHiddenField & "<input type='hidden' name='v_mid' value='" & payplatform.accountsid &"' />"
    strHiddenField = strHiddenField & "<input type='hidden' name='v_amount' value='" & v_amount &"' />"
    strHiddenField = strHiddenField & "<input type='hidden' name='v_moneytype' value='" & v_moneytype &"' />"
    strHiddenField = strHiddenField & "<input type='hidden' name='v_url' value='" & v_rurl &"' />"
    strHiddenField = strHiddenField & "<input type='hidden' name='remark1' value='" & payrecord.id &"' />" '付款记录ID放入备注1
    ' 以下几项只是用来记录客户信息，可以不用，不影响支付 /////////////////////////////
    strHiddenField = strHiddenField & "<input type='hidden' name='v_rcvname' value='" & orderform.firstname &"' />"
    strHiddenField = strHiddenField & "<input type='hidden' name='v_rcvaddr' value='" & orderform.address &"' />"
    strHiddenField = strHiddenField & "<input type='hidden' name='v_rcvemail' value='" & orderform.email &"' />"
End Select

%>
<html>
<head>
    <title>正在付款 | <%=conSiteTitle %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body onLoad="document.webForm.submit()">
            <form action="<%=posturl %>" method="post" name="webForm">
            <%=strHiddenField %>
            <div style="text-align: center; margin-bottom: 20px;">正在转向支付平台,请稍等...</div>
            </form>
</body>
</html>
<%call closeconn %>
