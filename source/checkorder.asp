﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/MailObject.asp" -->
<!--#include file="Inc/Md5.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>订单查询 -
        <%=conSiteTitle %>
    </title>
    <!--#include file="headmeta.asp"-->
</head>
<body style="background:#cddcec;">
    <div class="body" style="text-align:center;">
    <script language="javascript">
        function CheckForm(form) {
            if (!$.Validator["email"].test(form.email.value)) {
                alert("请输入邮箱地址！");
                form.email.select();
                return false;
            }
            if (!$.Validator["require"].test(form.ordernum.value)) {
                alert("请输入您的订单号！");
                form.ordernum.select();
                return false;
            }
        }
        
    </script>
    <img id="top" src="images/top.png" alt="" align="absmiddle" />
	<div style="background:white; width:770px;">
	
		<div style="background:#4b75b3; color:White; width:100%; padding:10px;"><h1 >订单查询</h1></div>
        <form name="webForm" id="webForm" class="appnitro" method="get" action="orderdetail.asp" onsubmit="return CheckForm(this)">
            <ul>
            <li class="section_break">
                    <h3>
                        输入订单号及您的邮箱，按ENTER</h3>
                </li>
                <li>
                    <label class="description" for="ordernum">
                        订单号
                    </label>
                    <div>
                        <input id="ordernum" name="ordernum" class="element text medium" type="text" onfocus="this.select()" maxlength="100" />
                    </div>
                </li>
                <li>
                    <label class="description" for="email">
                        Email
                    </label>
                    <div>
                        <input id="email" name="email" class="element text medium" type="text" value="@" onfocus="this.select()" maxlength="100"/>
                    </div>
                </li>
                
                <li class="buttons">
                    <input id="saveForm" class="button" type="submit" name="submit" value="提交" />&nbsp;&nbsp;&nbsp;
                    <input  class="button" type="button"  value="关闭页面(Ctrl + W)" onclick="javascript:window.close();" />
                </li>
            </ul>
        </form>
    </div>
	<img id="bottom" src="images/bottom.png" alt="" align="absmiddle" />
    </div>
</body>
</html>
<%call closeconn %>
