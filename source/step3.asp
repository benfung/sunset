﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>结算中心 </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
if conOrderForUser=1 then
	' 验证用户
	call CheckUserLogin("")
end if

dim myshopcart
set myshopcart=new shoppingcartsObj
myshopcart.checkcartid "",""

set delivery=new deliveryObj
delivery.getInfoByCookies("Cart")

dim id,action,rurl
id=getquerystring("id",0)
action=getform("action","")
rurl=getquerystring("rurl","step4.asp")

%>
<%
ErrorMsg=""

dim myclass
set myclass=new classObj

dim delivertype,paytype
delivertype=getform("delivertype",Request.Cookies("Cart")("delivertype"))
Response.Cookies("Cart")("delivertype")=delivertype

paytype=getform("paytype",Request.Cookies("Cart")("paytype"))
Response.Cookies("Cart")("paytype")=paytype

if action="save" then
    if delivertype="" or paytype="" then
        call alert("请选择送货方式","?")
    else
        call topage(rurl)
    end if
    
end if
%>

<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <div style="font-size: 14px; font-weight: bolder; margin-bottom: 20px;width:100%;">
            订购流程:<span>1.我的购物车</span> => <span style="color: Red;">2.填写订购信息</span> => <span>3.确认订购信息</span>
            => <span>4.完成订购</span>
        </div>
        <!--<div style="padding: 6px 10px 6px; width: 100%; text-align:left;">
            <input type="button" class="button" value="<< 上一步" onclick="window.location='step2.asp';" /></div>-->
            <form name="webForm" id="webForm" class="appnitro" action="?" method="post">
            <input type="hidden" name="action" value="save" />
        <div style=" margin-bottom:6px; background:#7f0019; padding:6px; color:White;">送货方式&支付方式</div>
        
            <ul>
                <% if ErrorMsg<>"" then  %><li class="imsg">
                    <%=ErrorMsg %>
                </li>
                <% end if %>
                <li class="highlighted">
                    <label class="description" for="delivertypename" style="font-size:110%;  margin-bottom:8px;">
                        选择送货方式
                    </label>
                    <div >
                        <%  %>
                        <% set rs=myclass.load("","where a.typename='pay' and a.pid=21","order by a.id") %>
                        <% for i=1 to rs.RecordCount %>
                        <div style="margin-bottom:10px;"><input id="delivertypename<%=i %>"  name="delivertype" class="element radio"  type="radio" value="<%=rs.fields("id") %>" />
                        <label class="choice" for="delivertypename<%=i %>" >
                            <%=rs.fields("name") %> ( + <%=rs.fields("subname") %> 元 ) </label>
                            </div>
                        <%  rs.movenext %>
                        <% next %>
                        <% rs.close:Set rs=nothing %>
                        <script type="text/javascript">
                            $.setElementValue(webForm.delivertype, '<%=delivertype %>');
                                        </script>
                    </div>
                </li>  
                <li></li>              
                <li class="highlighted">
                    <label class="description" for="delivertypename"  style="font-size:110%;  margin-bottom:8px;">
                        选择支付方式
                    </label>
                    <div >
                        <%  %>
                        <% set rs=myclass.load("","where a.typename='pay' and a.pid=20","order by a.id") %>
                        <% for i=1 to rs.RecordCount %>
                        <div style="margin-bottom:10px;"><input id="paytype<%=i %>"  name="paytype" class="element radio"  type="radio" value="<%=rs.fields("id") %>" />
                        <label class="choice" for="paytype<%=i %>" >
                            <%=rs.fields("name") %> </label>
                            </div>
                        <%  rs.movenext %>
                        <% next %>
                        <% rs.close:Set rs=nothing %>
                        <script type="text/javascript">
                            $.setElementValue(webForm.paytype, '<%=paytype %>');
                                        </script>
                    </div>
                </li> 
            <li class="buttons">
                    <input class="button" type="submit" value="确认此方式" />
                </li>
            </ul>
        </form>
        
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
