﻿/// <reference path="jquery.js" />
//**************************************************************
// My jQuery extend 2.0
// i'm looking for a job,pick me up!!!
// mail: gzbiing@gmail.com
//**************************************************************
jQuery.extend({
    // 获取 HTTP 查询字符串变量
    // 例:$.getQueryString("id","1")
    /////////////////////////////////////////////////////////////////////
    GetQueryString: function(key, def) {
        var ResultArray = window.location.search.match(new RegExp("[\?\&]" + key + "=([^\&]*)", "i"));
        return (ResultArray ? ResultArray[1] : def).toLowerCase();
    },

    // 获取 某对象的 所有属性及方法 
    // 例:$.GetObjectProperty(div)
    /////////////////////////////////////////////////////////////////////
    GetObjectProperty: function(obj) {
        var props;
        for (var p in obj) {
            props += p + " = " + this[p] + "<br />";
        };
        document.write(props.sub());
    },

    // 显示无模式对话框,文件上传用
    // 例:<input class="btn" type="button" value=" 上 传 " onclick="javascript:$.ShowDialog('../uploadcontrol.asp?id=mediafile&path=uploadfiles/',400,76);" />
    ///////////////////////////////////////////////////////////
    ShowDialog: function(url, width, height, scroll) { if ($.browser.msie) { if (scroll == null) scroll = "no"; return showModalDialog(url, window, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;help:no;scroll:" + scroll + ";status:no"); } else if ($.browser.mozilla) { return window.open(url, "mcePopup", "top=200,left=200,scrollbars=no,dialog=yes,modal=yes,width=" + width + ",height=" + height + ",resizable=no"); } },

    // 框架跳转
    // 例:$.FrameGoUrl("left","menu.asp")
    /////////////////////////////////////////////////////////////////////
    FrameGoUrl: function(frame, url) { eval("window.parent." + frame + ".document.location='" + url + "'"); },

    // 标签页切换
    // 应用:$.slipTab(1,2,"Tab","formTabA","formTabB","click",1);
    //////////////////////////////////////////////
    slipTab: function(from, to, prefname, styleA, styleB, event, def) {
        var objTab, objTabBox;
        for (var i = from; i <= to; i++) {
            objTab = $("#" + prefname + i);
            objTabBox = $("#" + prefname + i + "_Box");
            objTab.attr("class", styleA);
            objTab.unbind();
            objTab.bind(event, function() { $.slipTab(from, to, prefname, styleA, styleB, event, this.id.substr(prefname.length, 2)) });
            objTabBox.hide();
            if (i == def) {
                objTab = $("#" + prefname + i);
                objTabBox = $("#" + prefname + i + "_Box");
                objTab.attr("class", styleB);
                objTabBox.show();
            }
        }

    },

    // 显示与隐藏子标签
    // 参数:cn:对象的class    subtag:子标签的class
    // 应用:$.DisplayHideSubTag("navli","subtag");
    //////////////////////////////////////////////
    DisplayHideSubTag: function(cn, subtag) {
        $("." + cn).hover(
            function() {
                $(this).find("." + subtag).show("fast");
            },
            function() {
                $(this).find("." + subtag).hide("fast");
            }
        )
    },

    // 无限无逢循环滚动  例子:$(document).ready(function() { $.cycRoll("demo", "LEFT", 10); });
    // 参数:prefname:ID前缀名,direction:方向,s:速度
    // 规则:如prefname为"demo",则ID分别为:demo,demo1,demo2
    //      direction:有向上:"UP",向下:"DOWN",向左:"LEFT",向右:"RIGHT"
    /////////////////////////////////////////////////////////////
    cycRoll: function(prefname, direction, s) {
        prefname = (prefname == null ? "demo" : prefname) //  默认:demo
        direction = (direction == null ? "RIGHT" : direction) //默认:向右
        s = (s == null ? 10 : s) //默认值:10

        var tab = $("#" + prefname);
        var tab1 = $("#" + prefname + "1");
        var tab2 = $("#" + prefname + "2");

        if (tab.length == 0) { alert("\"" + prefname + "\" lost!!"); return false; }
        if (tab1.length == 0) { alert("\"" + prefname + "1\" lost!!"); return false; }
        if (tab2.length == 0) { alert("\"" + prefname + "2\" lost!!"); return false; }



        if (tab1[0].offsetWidth >= tab[0].offsetWidth) {

            tab2.html(tab1.html());  //克隆HTML
            var MyMar = setInterval(function() { $.Marquee[direction.toUpperCase()](tab[0], tab1[0], tab2[0]) }, s);

            // 绑定事件
            tab.bind("mouseover", function() { clearInterval(MyMar) }); //鼠标移上时清除定时器达到滚动停止的目的 
            tab.bind("mouseout", function() { MyMar = setInterval(function() { $.Marquee[direction.toUpperCase()](tab[0], tab1[0], tab2[0]) }, s) }); //鼠标移开时重设定时器 
        }
    },


    // 滚动函数
    ///////////////////////////////////////////////////////////
    Marquee: Object({
        UP: function(obj, obj1, obj2) { if (obj2.offsetTop - obj.scrollTop <= 0) { obj.scrollTop -= obj1.offsetHeight; } else { obj.scrollTop++; }; },
        DOWN: function(obj, obj1, obj2) { if (obj1.offsetTop - obj.scrollTop >= 0) { obj.scrollTop += obj2.offsetHeight; } else { obj.scrollTop--; }; },
        LEFT: function(obj, obj1, obj2) { if (obj2.offsetWidth - obj.scrollLeft <= 0) { obj.scrollLeft -= obj1.offsetWidth; } else { obj.scrollLeft++; }; },
        RIGHT: function(obj, obj1, obj2) { if (obj.scrollLeft <= 0) { obj.scrollLeft += obj2.offsetWidth; } else { obj.scrollLeft--; }; }
    }),

    // 表单验证
    // 例:
    ////////////////////////////////////////////////////
    ValidateForm: function(form, mode) {
        var objForm = form || event.srcElement;
        var count = objForm.elements.length;
        var ErrorMessage = new Array("以下原因导致提交失败:\t\t\t\t");
        var ErrorItem = new Array();
        ErrorItem[0] = document.forms[0];
        for (var i = 0; i < count; i++) {
            with (objForm.elements[i]) {
                var _dataType = getAttribute("dataType");
                if (typeof (_dataType) == "object" || typeof (_dataType) == "undefined") continue;
                if (getAttribute("require") == "false" && value == "") continue;
                switch (_dataType.toLowerCase()) {
                    case "idcard":
                        break;
                    default:
                        if (!$.Validator[_dataType.toLowerCase()].test(value)) {
                            ErrorItem[ErrorItem.length] = objForm.elements[i];
                            ErrorMessage[ErrorMessage.length] = ErrorMessage.length + ":" + getAttribute("tip");
                        }
                        break;
                }
            }
        }
        if (ErrorMessage.length > 1) {
            switch (mode) {
                case 1:
                    alert(ErrorMessage.join("\n"));
                    ErrorItem[1].focus();
                    break;
                default:
                    break;
            }
            return false
        }
        else {
            return true;
        }
    },


    // 文件类型对象
    // 例:$.FileType["Image"]
    ///////////////////////////////////////////////
    FileType: Object({
        Image: "*.jpg;*.gif;*.png",
        Media: "*.rm;*.mp3;*.wma;*.wmv;*.mpg;*.avi;*.asf;*.flv",
        Flash: "*.swf",
        Other: "*.rar;*.zip;*.doc;*.xls;*.pdf;*.xml",
        AllFiles: ".jpg;*.gif;*.png;*.rm;*.mp3;*.wma;*.wmv;*.mpg;*.avi;*.asf;*.flv;*.swf;*.rar;*.zip;*.doc;*.xls;*.pdf;*.xml"
    }),

    // 验证对象,一般用在表单验证
    // 例:if(!$.Validator["require"].test(form.title.value))
    ///////////////////////////////////////////////
    Validator: Object({
        require: /\S+/,   // 非空
        email: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
        phone: /^\+?\d+(\-\d+)?$/,
        mobile: /^((\(\d{2,3}\))|(\d{3}\-))?13\d{9}$/,
        Url: /^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/,
        currency: /^\d+(\.\d+)?$/,
        number: /^\d+$/,
        zip: /^[1-9]\d{5}$/,
        qq: /^[1-9]\d{4,11}$/,
        integer: /^[-\+]?\d+$/,
        double: /^[-\+]?\d+(\.\d+)?$/,
        english: /^[A-Za-z]+$/,
        chinese: /^[\u0391-\uFFE5]+$/,
        username: /^[a-z]\w{3,14}$/i,
        password: /^\S{5,20}$/i
    }),

    // 初始化input or select对象
    // formElement=对象,value=赋值,defvalue=默认值
    // 例:$.setElementValue(webForm.classid1,"2","0");
    ///////////////////////////////////////////////////////////////
    setElementValue: function(formElement, value, defvalue) {
        var Result = defvalue;
        if (formElement != null) {
            var eleType = formElement.type;
            if (value == "") {
                value = defvalue;
            }
            if (eleType == undefined) eleType = formElement[0].type;

            switch (eleType) {
                case "select-one":
                case "select-multiple":
                    for (var i = 0; i < formElement.length; i++) {
                        if (formElement.options[i].value == value) {
                            formElement.selectedIndex = i;
                            Result = value;
                        }
                    }
                    formElement.fireEvent("onchange");
                    break;
                case "radio":
                    if (formElement.length != undefined) {
                        for (var i = 0; i < formElement.length; i++) {
                            if (formElement[i].value == value) {
                                formElement[i].checked = 'on';
                                formElement[i].fireEvent("onclick");
                                Result = value;
                            }
                        }
                    }
                    else {
                        if (formElement.value == value) {
                            formElement.checked = 'on';
                            formElement.fireEvent("onclick");
                            Result = value;
                        }
                    }
                    break;
                case "checkbox":
                    if (formElement.length != undefined) {
                        var valAry = value.split(",");
                        for (var i = 0; i < formElement.length; i++) {
                            if (valAry.indexof(formElement[i].value.trim()) >= 0) {
                                formElement[i].checked = 'on';
                                formElement[i].fireEvent("onchange");
                            }
                        }
                    }
                    else {
                        if (formElement.value == value) {
                            formElement.checked = 'on';
                            formElement.fireEvent("onchange");
                            Result = value;
                        }
                    }
                    break;
                default:
                    formElement.value = value;
                    break;
            }
        }
        return Result;

    },



    // checkbox批量选择,classname:类名(class)
    // 例:<input type="checkbox" name="checkbox" onclick="$.SelectAllCB(this,'idset');" value="0" />
    //////////////////////////////
    SelectAllCB: function(cobj, classname) {
        $("." + classname).each(function(i) { this.checked = cobj.checked; })
    },

    // 返回选中checkbox的值集合
    // 例:$.getChecked('idset')
    //////////////////////////////////
    getChecked: function(classname) {
        var Result = new Array(); $("." + classname).each(function(i) { if (this.checked) Result.push(this.value); }); return Result;
    },

    // 按比例缩小图片
    // imgobj=图片对象,iwidth=最大宽度,iheight=最大高度
    // 例:<img src="" alt="" align="absbottom" onload="$.DrawImage(this,150,200);" >
    ////////////////////////////////////////////////////
    DrawImage: function(imgobj, iwidth, iheight) {
        var image = new Image(); image.src = imgobj.src; if (image.width > 0 && image.height > 0) {
            flag = true; if (image.width / image.height >= iwidth / iheight) {
                if (image.width > iwidth) { imgobj.width = iwidth; imgobj.height = (image.height * iwidth) / image.width; } else { imgobj.width = image.width; imgobj.height = image.height; } imgobj.alt = image.width + "×" + image.height;
            } else { if (image.height > iheight) { imgobj.height = iheight; imgobj.width = (image.width * iheight) / image.height; } else { imgobj.width = image.width; imgobj.height = image.height; } imgobj.alt = image.width + "×" + image.height; }
        }
    },


    // 显示JBox
    // obj=对象
    // 例:$.ShowJBox($("#newitem"))
    ////////////////////////////////////////////////////
    ShowJBox: function(obj) {
        if (obj.length == 0) { return; }
        $.InitJBox(obj);

        //$(window).bind("resize", function() { $.InitJBox(obj); });
        //$(window).bind("scroll", function() { $.InitJBox(obj); });
    },

    // 显示JBox
    // obj=对象
    // 例:$.RemoveJBox($("#newitem"))
    ////////////////////////////////////////////////////
    RemoveJBox: function(obj) {
        if (obj.length == 0) { return; }
        $(".jboxoverlay").hide();
        obj.hide();
        //$(window).unbind("resize", function() { $.InitJBox(obj); });
        //$(window).unbind("scroll", function() { $.InitJBox(obj); });
    },

    InitJBox: function(obj) {
        if (obj.length == 0) { return; }
        $("body").prepend("<div class=\"jboxoverlay\"></div>");
        var scrollLeft = $(document).scrollLeft();
        var scrollTop = $(document).scrollTop();
        var clientWidth = $(document).width();
        var clientHeight = $(document).height();
        var bo = $(".jboxoverlay");
        bo.css({ left: scrollLeft + 'px' });
        bo.css({ top: scrollTop + 'px' });
        bo.css({ width: clientWidth - 20 + 'px' });
        bo.css({ height: clientHeight + 'px' });

        obj.css({ position: "absolute" });
        obj.css({ zIndex: 101 });
        obj.css({ left: "40%" });
        obj.css({ top: "25%" });
        obj.css({ left: (($(document).width() - obj.width()) / 2) + "px" });
        obj.show();

        //var top = (parseInt((clientHeight - obj.height() - 43) / 2));
        var top = obj.height();
        if (top < 0) top = 0;
        top += $(document).scrollTop();
        obj.css({ top: top + "px" });
    },

    // 预读图片
    // 例:$(document).ready(function() { $.PreloadImages('image/a.jpg','image/b.jpg'); });
    ////////////////////////////////////////////////////
    PreloadImages: function() {
        var d = window.document;
        if (d.images) {
            if (!d.MM_p) d.MM_p = new Array();
            var i, j = d.MM_p.length, a = $.PreloadImages.arguments;
            for (i = 0; i < a.length; i++)
                if (a[i].indexOf("#") != 0) {
                d.MM_p[j] = new Image; d.MM_p[j++].src = a[i];
            }
        }
    },



    // 获得即时时间
    // 例setInterval("$.GetDateTime('objid')",1000);
    ////////////////////////////////////////////////////
    GetDateTime: function(objid) {
        var isnMonth = new Array("1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"); //定义月份描述
        var isnDay = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"); //定义星期描述
        var today = new Date(); //创建日期时间对象
        var Year = today.getYear(); //获得日期年份部分
        var Month = today.getMonth(); //获得日期月份部分
        var Date1 = today.getDate(); //获得日期日部分
        var Day = today.getDay(); //获得日期星期部分
        var Hours = today.getHours(); //获得小时值
        var Minutes = today.getMinutes(); //获得分钟值
        var Seconds = today.getSeconds(); //获得秒钟值
        if (Hours < 10) Hours = "0" + Hours; //格式化小时，小于10前面+0字符
        if (Minutes < 10) Minutes = "0" + Minutes; //格式化分钟，小于10前面+0字符
        if (Seconds < 10) Seconds = "0" + Seconds; //格式化秒，小于10前面+0字符
        var titlestr = "今天是：";
        titlestr = titlestr + Year;
        titlestr = titlestr + "年";
        titlestr = titlestr + isnMonth[Month];
        titlestr = titlestr + Date1;
        titlestr = titlestr + "日 ";
        titlestr = titlestr + isnDay[Day] + " ";
        titlestr = titlestr + Hours + ":";
        titlestr = titlestr + Minutes + ":";
        titlestr = titlestr + Seconds;
        $("#" + objid).html(titlestr);
    },

    // 添加到收藏夹
    // 例$.AddFavorite("标题","http://test.com");
    ////////////////////////////////////////////////////
    AddFavorite: function(title, url) {
        window.external.addFavorite(url, title);
    },

    // 设为主页
    // 例$.SetHomePage(this,"http://test.com");
    ////////////////////////////////////////////////////
    SetHomePage: function(obj, url) {
        obj.style.behavior = 'url(#default#homepage)';
        obj.setHomePage(url);
    },

    // 禁止右键
    // 例$.NoRightClick(); // 禁止右键
    ////////////////////////////////////////////////////
    NoRightClick: function(e) {
        $(window.document).mousedown(function() {    // 页面发生mousedown事件时触发
            if (window.Event)
                document.captureEvents(Event.MOUSEUP);

            document.oncontextmenu = function() {
                event.cancelBubble = true
                event.returnValue = false;

                return false;
            }
            document.onmousedown = function(e) {
                if (window.Event) {
                    if (e.which == 2 || e.which == 3)
                        return false;
                }
                else
                    if (event.button == 2 || event.button == 3) {
                    event.cancelBubble = true
                    event.returnValue = false;
                    return false;
                }
            }
            document.body.onselectstart = function() {
                return false;
            }
        });
    },

    // 添加订单代码
    // 例$.GoogleAddTrans("订单号","联系人/方式","金额","小费","运费","城市","省/州","国家");
    ////////////////////////////////////////////////////
    GoogleAddTrans: function(o_id, af, to, ta, sh, ci, st, co) {
        pageTracker._addTrans(o_id, af, to, ta, sh, ci, st, co);
    },

    // 设置浮动广告层
    // 例$.SetFloatAds("ads1");
    ////////////////////////////////////////////////////
    SetFloatAds: function(objid, top, txt) {
        if (top == null) top = 100;
        if ($("#" + objid).length > 0) {
            $(window).scroll(function() {    // 页面发生scroll事件时触发
                var bodyTop = 0;
                if (typeof window.pageYOffset != 'undefined') {
                    bodyTop = window.pageYOffset;
                }
                else if (typeof document.compatMode != 'undefined' && document.compatMode != 'BackCompat') {
                    bodyTop = document.documentElement.scrollTop;
                }
                else if (typeof document.body != 'undefined') {
                    bodyTop = document.body.scrollTop;
                }
                $("#" + objid).css("top", top + bodyTop)   // 设置层的CSS样式中的top属性, 注意要是小写，要符合“标准”
                if (txt != null) $("#" + objid).text(txt);   // 设置层的内容，这里只是显示当前的scrollTop
            });
        }
    }
});


// 去掉前后空格
// 例:" aa ".trim()
String.prototype.trim = function() { return $.trim(this); }

// 搜索数组，返回位置
// 例:temparray.indexof("aa")
Array.prototype.indexof = function(val) { var Result = -1; for (var i = 0; i < this.length; i++) { if (this[i].toString() == val.toString()) Result = i; } return Result; }


function InitArea(argClass, SelectObj, pid) {
    SelectObj.innerHTML = "";
    var opt = document.createElement("option");
    for (var i = 0; i < argClass.length; i++) {
        if (argClass[i] == undefined) continue;
        if (argClass[i][1] == pid) {
            var opt = document.createElement("option");
            opt.value = argClass[i][0];
            opt.text = argClass[i][3];
            SelectObj.add(opt);
        }
    }
}