var URLParams = new Object() ;
var aParams = document.location.search.substr(1).split('&') ;
for (i=0 ; i < aParams.length ; i++) {
	var aParam = aParams[i].split('=') ;
	URLParams[aParam[0]] = aParam[1] ;
}

var config;
try{config = dialogArguments.config;}catch(e){try{config = opener.config;}catch(e){}}

function BaseTrim(str){
	  lIdx=0;rIdx=str.length;
	  if (BaseTrim.arguments.length==2)
	    act=BaseTrim.arguments[1].toLowerCase()
	  else
	    act="all"
      for(var i=0;i<str.length;i++){
	  	thelStr=str.substring(lIdx,lIdx+1)
		therStr=str.substring(rIdx,rIdx-1)
        if ((act=="all" || act=="left") && thelStr==" "){
			lIdx++
        }
        if ((act=="all" || act=="right") && therStr==" "){
			rIdx--
        }
      }
	  str=str.slice(lIdx,rIdx)
      return str
}

function BaseAlert(theText,notice){
	alert(notice);
	theText.focus();
	theText.select();
	return false;
}

function IsColor(color){
	var temp=color;
	if (temp=="") return true;
	if (temp.length!=7) return false;
	return (temp.search(/\#[a-fA-F0-9]{6}/) != -1);
}

function IsDigit(){
  return ((event.keyCode >= 48) && (event.keyCode <= 57));
}

function SelectColor(what){
	var dEL = document.all("d_"+what);
	var sEL = document.all("s_"+what);
	var url = "selcolor.htm?color="+encodeURIComponent(dEL.value);
	var arr = showModalDialog(url,window,"dialogWidth:0px;dialogHeight:0px;help:no;scroll:no;status:no");
	if (arr) {
		dEL.value=arr;
		sEL.style.backgroundColor=arr;
	}
}

function SelectImage(){
	showModalDialog("backimage.htm?action=other",window,"dialogWidth:0px;dialogHeight:0px;help:no;scroll:no;status:no");
}

function SelectBrowse(type, what){
	var el = document.all("d_"+what);
	var arr = showModalDialog('browse.htm?type='+type, window, "dialogWidth:0px;dialogHeight:0px;help:no;scroll:no;status:no");
	if (arr){
		el.value = arr;
	}
}

function SearchSelectValue(o_Select, s_Value){
	for (var i=0;i<o_Select.length;i++){
		if (o_Select.options[i].value == s_Value){
			o_Select.selectedIndex = i;
			return true;
		}
	}
	return false;
}

function ToInt(str){
	str=BaseTrim(str);
	if (str!=""){
		var sTemp=parseFloat(str);
		if (isNaN(sTemp)){
			str="";
		}else{
			str=sTemp;
		}
	}
	return str;
}

function IsURL(url){
	var sTemp;
	var b=true;
	sTemp=url.substring(0,7);
	sTemp=sTemp.toUpperCase();
	if ((sTemp!="HTTP://")||(url.length<10)){
		b=false;
	}
	return b;
}

function IsExt(url, opt){
	var sTemp;
	var b=false;
	var s=opt.toUpperCase().split("|");
	for (var i=0;i<s.length ;i++ ){
		sTemp=url.substr(url.length-s[i].length-1);
		sTemp=sTemp.toUpperCase();
		s[i]="."+s[i];
		if (s[i]==sTemp){
			b=true;
			break;
		}
	}
	return b;
}

function relativePath2rootPath(url){
	if(url.substring(0,1)=="/") {return url;}
	if(url.indexOf("://")>=0) {return url;}

	var sWebEditorPath = getWebEditorRootPath();
	while(url.substr(0,3)=="../"){
		url = url.substr(3);
		sWebEditorPath = sWebEditorPath.substring(0,sWebEditorPath.lastIndexOf("/"));
	}
	return sWebEditorPath + "/" + url;
}

function relativePath2setPath(url){
	return relativePath2rootPath(url);
}

function getWebEditorRootPath(){
	var url = "/" + document.location.pathname;
	return url.substring(0,url.lastIndexOf("/dialog/"));
}

function getWebEditorPerPath(){
	var url=document.location.href.toLowerCase();
	return url.substring(0,url.lastIndexOf("eeditorlite"));
}

function adjustDialog(){
	var w = tabDialogSize.offsetWidth + 6;
	var h = tabDialogSize.offsetHeight + 25;
	if(config.IsSP2){
		h += 20;
	}
	window.dialogWidth = w + "px";
	window.dialogHeight = h + "px";
	window.dialogLeft = (screen.availWidth - w) / 2;
	window.dialogTop = (screen.availHeight - h) / 2;
}

function imgButtonOver(el){
	if(!el["imageinitliazed"]){
		el["oncontextmenu"]= new Function("event.returnValue=false") ;
		el["onmouseout"]= new Function("imgButtonOut(this)") ;
		el["onmousedown"]= new Function("imgButtonDown(this)") ;
		el["unselectable"]="on" ;
		el["imageinitliazed"]=true ;
	} ;
	el.className = "imgButtonOver";
}  ;

function imgButtonOut(el){
	el.className = "imgButtonOut";
}  ;

function imgButtonDown(el){
	el.className = "imgButtonDown";
}  ;

/*
*######################################
About upload
*######################################
*/
//var swfu;
    // 初始上传控件
    // 例:UploadInit("spanButtonPlaceHolder", $.FileType["Image"], "图片文件", 0,500, fileQueued, fileQueueError, fileDialogComplete, uploadStart, uploadProgress, uploadError, uploadSuccess, uploadComplete)
    // 参数:
    // bid: 安钮所在标签ID
    // t: 文件类型,多个用分号(;)隔开,如(*.jpg)
    // td: 说明,如(图片,All Files)
    // l: 限制数量,0为不限
    // si:文件大小,单位(KB),0表示文件大小无限制,可用的单位有B,KB,MB,GB
    // h_1: 添加队列后处理函数,如(fileQueued)
    // h_2: 队列错误
    // h_3: 对话框关闭时
    // h_4: 上传开始时
    // h_5: 上传时，一般用于进度条
    // h_6: 上传错误时
    // h_7: 上传成功时
    // h_8: 上传完成时
    // isthumb: 是否生成缩略图(true or false)
    /////////////////////////////////////////////////////////////////////
    function UploadInit(bid, t, td, l, si, h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, ipID, isthumb) {
        var settings = {
            flash_url: getWebEditorPerPath() + "plugIn/swfupload.swf",
            upload_url: getWebEditorPerPath() + "../up.asp?u=u&isthumb=" + isthumb,
            file_post_name:"file",
            post_params: { "u": "u" },
            file_size_limit: si,
            file_types: t,
            file_types_description: td,
            file_upload_limit: l,
            file_queue_limit: l,
            custom_settings: {
                InputObject: $("#" + ipID)[0],
                progressTarget: "fsUploadProgress",
                cancelButtonId: "btnCancel"
            },
            debug: true,

            // Button settings
            button_image_url: getWebEditorPerPath() + "../am/images/BtnUpload.png",
            button_width: 61,
            button_height: 22,
            button_placeholder_id: bid,

            // The event handler functions are defined in handlers.js
            file_queued_handler: h_1,
            file_queue_error_handler: h_2,
            file_dialog_complete_handler: h_3,
            upload_start_handler: h_4,
            upload_progress_handler: h_5,
            upload_error_handler: h_6,
            upload_success_handler: h_7,
            upload_complete_handler: h_8
        };
        return new SWFUpload(settings);
    }

    function fileQueued(file) {
        //this.startUpload();
    }
    function fileQueueError(file, errorCode, message) {
        alert(message);
    }
    function fileDialogComplete() {
        if (this.getStats().files_queued > 0) {
            this.startUpload();
        }
        //alert("fileDialogComplete");

    }
    function uploadStart() {

    }
    function uploadProgress() {
        //alert("uploadProgress");
    }
    function uploadError(file, errorCode, message) {
        alert(message);
    }
    function uploadSuccess(file, server) {
        var statuscode = server.trim().split("&")[0];
        var msg = server.trim().split("&")[1];
        if (statuscode == 0) {
            this.customSettings.InputObject.value = "/" + msg;
            this.customSettings.InputObject.fireEvent("onchange");
        }
        else {
            alert(file.name + "\r\n\r\n" + msg);
        }
    }
    function uploadComplete(file) {
        if (this.getStats().files_queued > 0) {
            this.startUpload();
        }
    }
