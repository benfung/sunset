﻿<%
on error resume next
dim connstr,strDB

strDB="D:\Biing\Bs\Asp\databases\#020#.mdb"
'strDB="D:\wwwroot\asp\databases\#020#.mdb"
connstr=CreatConnStr("ACCESS", strDB, "", "", "")  'Access
'connstr=CreatConnStr("MSSQL", "020web", "(local)", "sa", "")  'MSSQL

dim conn
set conn=Server.Createobject("ADODB.CONNECTION")
conn.open connstr

if err.number<>0 then
    err.clear
    Response.Write("conn error!!<br />")
    Response.Write "<span style='color:white;'>" & Server.MapPath("db") & "</span>"
    conn.Close()
    Response.End()
end if

'**************************************************
'函数名：CloseConn
'作  用：关闭连接
'**************************************************
sub CloseConn()
	conn.close()
	set conn=nothing
end sub

'**************************************************
'函数名：CloseRs
'作  用：关闭RecorsSet
'**************************************************
sub CloseRs(arg1)
	arg1.close()
	set arg1=nothing
end sub

'**************************************************
'函数名：CreatConnStr
'作  用：创建连接这符串
'参  数：dbType   ----数据类型,0 -MSSQL,1-ACCESS,2-ACCESS,3-MYSQL,4-ORACLE
'        strDB    ----数据库
'        strServer -  服务器地址
'        strUid -用户名 
'        strPwd -密码
'例子 connstr = CreatConnStr("1", "\%TestDB%.mdb", "", "", "")
'     connstr = CreatConnStr("0", "master", "localhost", "sa", "")	
'**************************************************
Function CreatConnStr(ByVal dbType, ByVal strDB, ByVal strServer, ByVal strUid, ByVal strPwd)
	Dim TempStr,tDb
	Select Case dbType
		Case "0","MSSQL"
			TempStr = "driver={sql server};server=" & strServer & ";uid=" & strUid & ";pwd=" & strPwd & ";database=" & strDB
        Case "1","MSSQL2"
			TempStr = "PROVIDER=SQLOLEDB;Data Source=" & strServer & ";Initial Catalog=" & strDB & ";Persist Security Info=True;User ID=" & strUid & ";Password=" & strPwd & ";Connect Timeout=30" 
		Case "2","ACCESS"
			If Instr(strDB,":")>0 Then : tDb = strDB : Else : tDb = Server.MapPath(strDB) : End If
			TempStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="&tDb&";Jet OLEDB:Database Password="&strPwd&";"
	    Case "3","ACCESS2"
			If Instr(strDB,":")>0 Then : tDb = strDB : Else : tDb = Server.MapPath(strDB) : End If
			TempStr = "DBQ=" & tDb & ";DefaultDir=;DRIVER={Microsoft Access Driver (*.mdb)};"
		Case "4","MYSQL"
			TempStr = "Driver={mySQL};Server=" & strServer & ";Port=3306;Option=131072;Stmt=; Database="&strDB&";Uid="&strUid&";Pwd="&strPwd&";"
		Case "5","ORACLE"
			TempStr = "Driver={Microsoft ODBC for Oracle};Server="&strServer&";Uid="&strUid&";Pwd="&strPwd&";"
	End Select
	CreatConnStr = TempStr
End Function

%>

