﻿<%
 '//// 网站信息配置 ////////////////////////////////
PageStartTime=timer()
const conSiteTitle="网站标题"        '网站标题
const conSiteName="网站名称"        '网站名称或公司名
const conSiteDomain="domain.com"        '网站域名
const conSiteUrl="http://127.0.0.1:8042/www/"        '网站访问地址
const conLogoUrl=""        'LOGO地址
const conSeoTitle="SEO标题附加字"        'SEO标题附加字
const conSiteKeyword="网站关键字"        '网站关键字
const conSiteDescription="网站描述meta"        '网站描述meta
const conSiteBeiAn="ICP90002"        '网站备案号
const conEnterpriseMail="http://main.domain.com"        '企业邮局入口
const conWebmasterName="小冯"        '网站联系人
const conWebmasterAddress="联系地址"        '联系地址
const conWebmasterPostCode="510000"        '邮政编码
const conWebmasterEmail="kofbqfeng@gmail.com"        '联系邮箱
const conWebmasterTel="020-88888888"        '联系电话
const conWebmasterFax="020-88888888"        '联系传真
const conWebmasterMobile=""        '联系手机
 '//// 邮件服务器 ////////////////////////////////
const conMailServer="mail.luxunion.com"        'SMTP服务器
const conMailServerUserName="luxunion@luxunion.com"        'SMTP登录用户名
const conMailServerPassWord="168_luxunion"        'SMTP登录密码
const conMailDomain="luxunion.com"        'SMTP域名
 '//// 网站选项配置 ////////////////////////////////
const conMaxFileSize=2048        '上传文件大小限制
const conAllowFileType="jpg|gif|png|bmp|rm|mp3|wma|wmv|avi|asf|flv|swf|rar|zip|doc|xls|pdf|xml"        '允许的上传文件类型
const conUpFilePath="f/"        '上传文件目录
const conUpFileNameMode=0        '文件命名模式
const conLoginTimeOut=20        '用户/管理员登陆时限
const conCheckLoginLog=0        '记录登陆历史
const conOrderForUser=1        ' 只有会员才能购物
const conOpenSelectRegion=1        '地区填写模式
const conOpenAutoCode=0        '是否启用自动生成产品编号
const conOpenAsp2Html=0        '是否启用静态页生成
const conOpenCreateThumb=1        '是否生成缩略图
const conThumbWidth=160        ' 缩略图宽度
const conOpenQuickMode=0        '是否启用快速添加模式
const conCurrencyUnit="￥"        '货币单位
const conFilterString=""        '过滤字符
const conCustomerPage="index.asp"        '自定义生成页
const conBackUpFiles="index.asp|inc/conn.asp|foot.asp|head.asp|am/admin.asp|am/signin.asp|am/default.asp|am/user.asp"        '需要备份页面
const conDefaultPageCharset="utf-8"        '页面编码
const conSysname="iSixTeam"        '系统名称
const conSysurl="http://www.isixteam.com/"        '网址
const conAuthor="Ben Feng"        '系统联系人
const conAuthorurl="http://kofbqfeng.blogspot.com/"        '联系人网址
const conAuthorPhone="020-00000000"        '联系人电话
%>