﻿<%
' 关于类说明
' 每个对象类共有的方法(Load,Add,Update)

'****************************************************
' 地区对象类
'****************************************************
class regionObj
	public id,pid,name,subname,pic,depth,child,sort,status,typename

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[region]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		pid=0
		name=""
		subname=""
		pic=""
		depth=0
		child=0
		sort=0
		status=0
		
		typename="region"
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.* from " & tableName & " a {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub regionObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	' 无限级分类表转JS数组
	' arg0:类别
    ' ArryName:数组名
    ' 对应数据库  [id,pid,child,name,subname,depth,sort,status]
    public function JsArray(ArryName)
        dim i,Result,tempRs,tempArray
        Result="var " & ArryName & "=new Array();" & vbLf
        tempArray=GetArray()               
        for i=1 to ubound(tempArray)
            Result= Result & ArryName & "[" & (i-1) & "] = [" & tempArray(i-1,0) & "," & tempArray(i-1,1) & "," & tempArray(i-1,2) & "," & chr(34) & tempArray(i-1,3) & chr(34) & "," & chr(34) & tempArray(i-1,4) & chr(34) & "," & tempArray(i-1,5) & "," & tempArray(i-1,6) & "," & tempArray(i-1,7) & "," & chr(34) & tempArray(i-1,8) & chr(34) & "];" & vbLf
        next
        JsArray=Result
    end function
    
    '// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		if pid>0 then
            depth=connObj.execute("select top 1 depth from " & tableName & " where id=" & pid)(0)+1
        else
            Depth=0
        end if  
        
		cmdText="insert into " & tableName & "(pid,name,subname,pic,depth,child,sort,status)"&_
		" values(" & pid & ",'" & name & "','" & subname & "','" & pic & "'," & depth & "," & child & "," & sort & "," & status & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
		
		cmdText="update " & tableName & " set child=child+1 where id=" & pid
        connObj.execute cmdText
        CleanCache()

        id=connobj.execute("select MAX(id) from " & tableName)(0)      
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set pid=" & pid & ",name='" & name & "',subname='" & subname & "',pic='" & pic & "',depth=" & depth & ",child=" & child & ",sort=" & sort & ",status=" & status & " where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
		CleanCache()
	end function


	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("pid")<>"" then pid=tempRs.Fields("pid")
		if tempRs("name")<>"" then name=tempRs.Fields("name")
		if tempRs("subname")<>"" then subname=tempRs.Fields("subname")
		if tempRs("pic")<>"" then pic=tempRs.Fields("pic")
		if tempRs("depth")<>"" then depth=tempRs.Fields("depth")
		if tempRs("child")<>"" then child=tempRs.Fields("child")
		if tempRs("sort")<>"" then sort=tempRs.Fields("sort")
		if tempRs("status")<>"" then status=tempRs.Fields("status")
	end function
	
	' // 取子类ID集合(包括自身) ///////////////////////////////////
	public function GetSubs(arg0)
		dim Result,temprs,k
		Result=arg0
		set temprs=load("","where a.pid=" & arg0,"order by a.sort,a.pid,a.id")
		for k=1 to temprs.recordcount
			if temprs.fields("child")=0 then
				Result= Result & "," & temprs.fields("id")
			else
				Result= Result & "," & GetSubs(temprs.fields("id"))
			end if
			temprs.movenext
		next
		temprs.close:set temprs=nothing
		GetSubs=Result
	end function
	
	' // 回塑父类ID集合(包括自身和0) ///////////////////////////////////
	' 如 0,10,23
	public function Trackback(arg0)
		dim Result,temprs
		set temprs=load("","where a.id=" & arg0,"")
		if not temprs.eof then
			if temprs.fields("pid")=0 then
				Result= "0," & temprs.fields("id")
			else
				Result= Trackback(temprs.fields("pid")) & "," & temprs.fields("id")
			end if
		else
			Result= "0"
		end if
		temprs.close:set temprs=nothing
		Trackback=Result
	end function
	
	' // 删除文件 ///////////////////////////////////
    Public function ExeDelete
        connObj.execute "update " & tableName & " set child=child-1 where id=" & Pid        
        connObj.execute "delete from " & tableName & " where id= " & id
        CleanCache()
    end function
	
	' // 删除缓存 //////////////////////////////
    public function CleanCache
        CleanCacheByTypename(typename)     
    end function
    
    ' // 删除缓存 //////////////////////////////
    ' arg0 : typename(如:news)
    public function CleanCacheByTypename(arg0)
        dim tempCache,cacheName
        cacheName="cl"  & arg0
        set tempCache=new Cache
        tempCache.DelCache(cacheName)        
    end function
    
    ' // 获取某类二维数组  ///////////////////////////////
    public function GetArray
        GetArray=GetArrayByTypename(typename)
    end function
    
    ' // 获取某类二维数组  ///////////////////////////////
    ' // arg0:类型名(如:news)
    ' 对应数据库  [id,pid,child,name,subname,depth,sort,status]
    public function GetArrayByTypename(arg0)
        dim Result,tempRs,ca,cacheName
        cacheName="cl"  & arg0        
        set ca=new Cache
        if ca.CacheIsEmpty(cacheName) then
            set tempRs=Load("","","order by a.sort,a.pid,a.id")
            ReDim Result(tempRs.RecordCount,9)
            for i=1 to tempRs.RecordCount
                Result(i-1,0)=tempRs.fields("id")         ' section(x,0)  : id
                Result(i-1,1)=tempRs.fields("pid")        ' section(x,1)  : pid
                Result(i-1,2)=tempRs.fields("child")      ' section(x,2)  : child
                Result(i-1,3)=tempRs.fields("name")       ' section(x,3)  : name
                Result(i-1,4)=tempRs.fields("subname")    ' section(x,4)  : subname
                Result(i-1,5)=tempRs.fields("depth")      ' section(x,5)  : depth
                Result(i-1,6)=tempRs.fields("sort")       ' section(x,6)  : sort
                Result(i-1,7)=tempRs.fields("status")     ' section(x,7)  : status
                Result(i-1,8)=tempRs.fields("pic")        ' section(x,8)  : pic    
                tempRs.MoveNext
            next
            tempRs.close:set tempRs=nothing
            ca.SetValue cacheName,Result
        else
            Result=ca.GetValue(cacheName)
        end if            
        set ca=nothing
        GetArrayByTypename=Result
    end function
    
    ' // 获取指定数量的字符串 //////////////
    ' arg0 : 数量 
    ' arg1 : 要重复的字符串
    private function RepeatStr(arg0,arg1)
        dim Result
        for i=0 to arg0
            Result=Result & arg1
        next
        RepeatStr=Result
    end function 
    
    
    public function ShowOption(argArray,arg1,arg2,arg3)
        dim Result,tempStr
        for i=1 to ubound(argArray)
            tempStr=""
            if argArray(i-1,1)=arg1 then
                if argArray(i-1,1)>0 then
                    tempStr=" └ "
                end if
                if argArray(i-1,5)>1 then
                    tempStr= RepeatStr(argArray(i-1,5)*2,"&nbsp;") & tempStr
                end if
                tempStr=tempStr + argArray(i-1,arg3)
                
                Result=Result & "<option value='" & argArray(i-1,arg2) & "'>" & tempStr & "</option>"
                
                if argArray(i-1,2)>0 then 
                    Result=Result & ShowOption(argArray,argArray(i-1,0),arg2,arg3)
                end if
            end if            
        next
        ShowOption=Result
    end function

end class

'****************************************************
' 栏目对象类
'****************************************************
class ClassObj
	public id,typename,pid,name,subname,pic,width,height,depth,child,sort,status

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[myclass]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		typename="news"
		pid=0
		name=""
		subname=""
		pic=""
		width=0
		height=0
		depth=0
		child=0
		sort=0
		status=0
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function
	
	' 无限级分类表转JS数组
	' arg0:类别
    ' ArryName:数组名
    ' 对应数据库  [id,pid,child,name,subname,depth,sort,status]
    public function JsArray(ArryName)
        dim i,Result,tempRs,tempArray
        Result="var " & ArryName & "=new Array();" & vbLf
        tempArray=GetArray()               
        for i=1 to ubound(tempArray)
            Result= Result & ArryName & "[" & (i-1) & "] = [" & tempArray(i-1,0) & "," & tempArray(i-1,1) & "," & tempArray(i-1,2) & "," & chr(34) & tempArray(i-1,3) & chr(34) & "," & chr(34) & tempArray(i-1,4) & chr(34) & "," & tempArray(i-1,5) & "," & tempArray(i-1,6) & "," & tempArray(i-1,7) & "," & chr(34) & tempArray(i-1,8) & chr(34) & "];" & vbLf
        next
        JsArray=Result
    end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.* from " & tableName & " a {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public Default function ClassObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end function
	
	'// 调用时 ////////////////////////////////////////////////////////
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		if pid>0 then
            depth=connObj.execute("select top 1 depth from " & tableName & " where id=" & pid)(0)+1
        else
            Depth=0
        end if  
        
		cmdText="insert into " & tableName & "(typename,pid,name,subname,pic,width,height,depth,child,sort,status)"&_
		" values('" & typename & "'," & pid & ",'" & name & "','" & subname & "','" & pic & "'," & width & "," & height & "," & depth & "," & child & "," & sort & "," & status & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
		
		cmdText="update " & tableName & " set child=child+1 where id=" & pid
        connObj.execute cmdText
        
        CleanCache()
        id=connobj.execute("select MAX(id) from " & tableName)(0)      
	end function
	
	

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set typename='" & typename & "',pid=" & pid & ",name='" & name & "',subname='" & subname & "',pic='" & pic & "',width=" & width & ",height=" & height & ",depth=" & depth & ",child=" & child & ",sort=" & sort & ",status=" & status & " where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		
		connObj.execute(cmdText)
		CleanCache()
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("typename")<>"" then typename=tempRs.Fields("typename")
		if tempRs("pid")<>"" then pid=tempRs.Fields("pid")
		if tempRs("name")<>"" then name=tempRs.Fields("name")
		if tempRs("subname")<>"" then subname=tempRs.Fields("subname")
		if tempRs("pic")<>"" then pic=tempRs.Fields("pic")
		if tempRs("width")<>"" then width=tempRs.Fields("width")
		if tempRs("height")<>"" then height=tempRs.Fields("height")
		if tempRs("depth")<>"" then depth=tempRs.Fields("depth")
		if tempRs("child")<>"" then child=tempRs.Fields("child")
		if tempRs("sort")<>"" then sort=tempRs.Fields("sort")
		if tempRs("status")<>"" then status=tempRs.Fields("status")
	end function
	
	' // 取子类ID集合(包括自身) ///////////////////////////////////
	public function GetSubs(arg0)
		dim Result,temprs,k
		Result=arg0
		set temprs=load("","where a.pid=" & arg0,"order by a.sort,a.pid,a.id")
		for k=1 to temprs.recordcount
			if temprs.fields("child")=0 then
				Result= Result & "," & temprs.fields("id")
			else
				Result= Result & "," & GetSubs(temprs.fields("id"))
			end if
			temprs.movenext
		next
		temprs.close:set temprs=nothing
		GetSubs=Result
	end function
	
	' // 回塑父类ID集合(包括自身和0) ///////////////////////////////////
	' 如 0,10,23
	public function Trackback(arg0)
		dim Result,temprs
		set temprs=load("","where a.id=" & arg0,"")
		if not temprs.eof then
			if temprs.fields("pid")=0 then
				Result= "0," & temprs.fields("id")
			else
				Result= Trackback(temprs.fields("pid")) & "," & temprs.fields("id")
			end if
		else
			Result= "0"
		end if
		temprs.close:set temprs=nothing
		Trackback=Result
	end function
	
	' // 删除文件 ///////////////////////////////////
    Public function ExeDelete
        connObj.execute "update " & tableName & " set child=child-1 where id=" & Pid        
        connObj.execute "delete from " & tableName & " where id= " & id
        CleanCache()
    end function
    
    ' // 删除缓存 //////////////////////////////
    public function CleanCache
        CleanCacheByTypename(typename)     
    end function
    
    ' // 删除缓存 //////////////////////////////
    ' arg0 : typename(如:news)
    public function CleanCacheByTypename(arg0)
        dim tempCache,cacheName
        cacheName="cl"  & arg0
        set tempCache=new Cache
        tempCache.DelCache(cacheName)        
    end function
    
    ' // 获取某类二维数组  ///////////////////////////////
    public function GetArray
        GetArray=GetArrayByTypename(typename)
    end function
    
    ' // 获取某类二维数组  ///////////////////////////////
    ' // arg0:类型名(如:news)
    ' 对应数据库  [id,pid,child,name,subname,depth,sort,status]
    public function GetArrayByTypename(arg0)
        dim Result,tempRs,ca,cacheName
        cacheName="cl"  & arg0        
        set ca=new Cache
        if ca.CacheIsEmpty(cacheName) then
            set tempRs=Load("","where a.typename ='" & arg0 & "'","order by a.sort,a.pid,a.id")
            ReDim Result(tempRs.RecordCount,8)
            for i=1 to tempRs.RecordCount
                Result(i-1,0)=tempRs.fields("id")         ' section(x,0)  : id
                Result(i-1,1)=tempRs.fields("pid")        ' section(x,1)  : pid
                Result(i-1,2)=tempRs.fields("child")      ' section(x,2)  : child
                Result(i-1,3)=tempRs.fields("name")       ' section(x,3)  : name
                Result(i-1,4)=tempRs.fields("subname")    ' section(x,4)  : subname
                Result(i-1,5)=tempRs.fields("depth")      ' section(x,5)  : depth
                Result(i-1,6)=tempRs.fields("sort")       ' section(x,6)  : sort
                Result(i-1,7)=tempRs.fields("status")     ' section(x,7)  : status
                Result(i-1,8)=tempRs.fields("pic")        ' section(x,8)  : pic       
                tempRs.MoveNext
            next
            tempRs.close:set tempRs=nothing
            ca.SetValue cacheName,Result
        else
            Result=ca.GetValue(cacheName)
        end if            
        set ca=nothing
        GetArrayByTypename=Result
    end function
    
    ' // 获取指定数量的字符串 //////////////
    ' arg0 : 数量 
    ' arg1 : 要重复的字符串
    private function RepeatStr(arg0,arg1)
        dim Result
        for i=0 to arg0
            Result=Result & arg1
        next
        RepeatStr=Result
    end function 
    
    
    public function ShowOption(argArray,arg1,arg2,arg3)
        dim Result,tempStr
        for i=1 to ubound(argArray)
            tempStr=""
            if argArray(i-1,1)=arg1 then
                if argArray(i-1,1)>0 then
                    tempStr=" └"
                end if
                if argArray(i-1,5)>1 then
                    tempStr= RepeatStr(argArray(i-1,5)*2,"&nbsp;") & tempStr
                end if
                tempStr=tempStr + argArray(i-1,arg3)
                
                Result=Result & "<option value='" & argArray(i-1,arg2) & "'>" & tempStr & "</option>"
                
                if argArray(i-1,2)>0 then 
                    Result=Result & ShowOption(argArray,argArray(i-1,0),arg2,arg3)
                end if
            end if            
        next
        ShowOption=Result
    end function

end class


'****************************************************
' 管理员对象
'****************************************************
class AdminObj
    public id,loginid,pwd,Purview,LastLoginIP,LastLoginTime,LoginTimes,Power
    private connObj,tableName,test
    

    '// 设置 Initialize 事件。 /////////////////////////////////////////
    private sub Class_Initialize   
        call init()
        tableName="[admin]"
        test=false
        set connObj=conn   ' 默认为conn
    end sub
    
    '// 设置 Terminate 事件。 ////////////////////////////////////////////
    private Sub Class_Terminate   
        call init()
    End Sub
    
    '// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
        loginid=""
        pwd=""
        Purview=0
        LastLoginIP=Request.ServerVariables("REMOTE_ADDR")
        LastLoginTime=now()
        LoginTimes=0
        power="0"
	end sub
    
    '// 判断是否为空 ////////////////////////////////////////////////
    Public function isNull()
        isNull=(id=0)
    end function
    
    '// 判断是否有权限 ////////////////////////////////////////////////
    public function hasPower(powervalue)
        hasPower=((CLng(power) and powervalue) =powervalue)
    end function
    
    ' 
    '// 组织SQL语句(select) ///////////////////////////////////////////
    private function getSelect(arg0,arg1,arg2)
        dim cmdText
        cmdText="select {0} a.* from " & tableName & " a {1} {2}"
        cmdText=Replace(cmdText,"{0}",arg0)
        cmdText=Replace(cmdText,"{1}",arg1)
        cmdText=Replace(cmdText,"{2}",arg2)
        if test then
			Response.Write cmdText
			Response.End()
		end if
        getSelect=cmdText
    end function
    
    '//////////////////////////////////////////////////
    public sub AdminObj(tempid)
        dim tempSql,tempRs
        tempSql=getSelect("top 1"," where a.id=" & tempid,"")
        if tempid<>"" then
            set tempRs=Server.CreateObject("Adodb.RecordSet")
            tempRs.Open tempSql,connObj,1,1
            if not tempRs.EOF then
                EvaValue(tempRs)
            end if
            tempRs.close
            set tempRs=nothing
        end if
    end sub
    
    ' arg0: username arg1:passwor
    ' // 验证用户根据用户名、密码 ///////////////////////////////////////////////
    public function AdminObjByNP(arg0,arg1)        
        dim tempSql,tempRs,Result
        Result=false
        tempSql=getSelect("top 1","where a.loginid='" & arg0 & "' and a.pwd='" & arg1 & "'","")
        if arg0<>"" and arg1<>"" then
            set tempRs=Server.CreateObject("Adodb.RecordSet")
            tempRs.Open tempSql,connObj,1,1
            if not tempRs.EOF then
                Result=true
                EvaValue(tempRs)
            end if
            tempRs.close
            set tempRs=nothing
        end if
        AdminObjByNP=Result
    end function
    
    ' // 检验用户是否存在 ///////////////////////////////////////////////
    public function HasName()
        dim tempSql,tempRs
        tempSql=getSelect("top 1","where a.loginid='" & loginid & "'","")
        set tempRs=Server.CreateObject("Adodb.RecordSet")
        tempRs.Open tempSql,connObj,1,1
        if not tempRs.EOF then
            HasName=true
        else
            HasName=false
        end if
        tempRs.close
        set tempRs=nothing
    end function
    
    ' 调用时 
    'set rs=__.Load("top 8","where a.jie=1","order by q.id desc")
    '// 读取数据 ////////////////////////////////////////////////
    public function Load(arg0,arg1,arg2)
        dim Result
        set Result=Server.CreateObject("Adodb.RecordSet")
        Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
        set Load=Result
    end function
    
    
    
    '// 添加  ////////////////////////////////////////////////
    public function Add()
        dim tempSql
        tempSql="insert into " & tableName & "(loginid,pwd,Purview,LastLoginIP,LastLoginTime,LoginTimes,Power) "&_
        "values('" & loginid & "','" & pwd & "'," & Purview & ",'" & LastLoginIP & "','" & LastLoginTime & "'," & LoginTimes & ",'" & Power & "')"
        if test then
			Response.Write cmdText
			Response.End()
		end if
        connObj.execute(tempSql)
        
        id=connobj.execute("select MAX(id) from " & tableName)(0)
    end function
    
    '// 更新  ////////////////////////////////////////////////
    public function Update()        
        dim tempSql
        tempSql="update " & tableName & " set loginid='" & loginid & "',pwd='" & pwd & "',Purview=" & Purview & ",LastLoginIP='" & LastLoginIP & "',LastLoginTime='" & LastLoginTime & "',LoginTimes=" & LoginTimes & ",Power='" & Power & "' where id=" & id
        if test then
			Response.Write cmdText
			Response.End()
		end if
        connObj.execute(tempSql)
    end function    
    
    '// 给变量赋值 ////////////////////////////////////////////////
    private function EvaValue(tempRs) 
            if tempRs("id")<>"" then id=tempRs.Fields("id")
            if tempRs("loginid")<>"" then loginid=tempRs.Fields("loginid")
            if tempRs("pwd")<>"" then pwd=tempRs("pwd")
            if tempRs("Purview")<>"" then Purview=tempRs("Purview")            
            if tempRs("LastLoginIP")<>"" then LastLoginIP=tempRs("LastLoginIP")
            if tempRs("LastLoginTime")<>"" then LastLoginTime=tempRs("LastLoginTime")
            if tempRs("LoginTimes")<>"" then LoginTimes=tempRs("LoginTimes")
            if tempRs("Power")<>"" then Power=tempRs("Power")
    end function
    
    '// 以Cookies记录状态 ///////////////////////////////////////////////////
	'// arg0 到期时间
	public function SaveStatus(arg0)
        Session.Timeout=arg0
        Session("loginid")=loginid
        Session("pwd")=pwd
	end function
	
	'// 清除Cookies ///////////////////////////////////////////////////
	public function ClearStatus()        
        Session("loginid")=""
        Session("pwd")=""
        Session.Abandon()
	end function
	
	'// 根据状态读取对象 ///////////////////////////////////////////////////
	public function AdminObjByStatus()
        if trim(Session("loginid"))<>"" and trim(Session("pwd"))<>"" then
            AdminObjByNP Session("loginid"),Session("pwd")
        end if
	end function    
end Class



'****************************************************
' 会员对象类
'****************************************************
class myuserObj
	public id,loginid,pwd,Question,Answer,Email,Sex,city,state,country,address,postcode,firstname,lastname,mobile,Phone,Fax,Homepage,LoginIP,logins,LastLoginTime,RegDate,UserLevel,LockUser,status

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[myuser]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		loginid=Request.ServerVariables("REMOTE_ADDR")
		pwd=""
		Question=""
		Answer=""
		Email=""
		Sex=""
		city=""
		state=""
		country=""
		Address=""
		Postcode=""
		firstname=""
		lastname=""
		mobile=""
		Phone=""
		Fax=""
		Homepage=""
		LoginIP=Request.ServerVariables("REMOTE_ADDR")
		logins=0
		LastLoginTime=now()
		RegDate=now()
		UserLevel=0
		LockUser=0
		status=0
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.* from " & tableName & " a {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub myuserObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub
	
	public sub myuserObjByN()
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.loginid='" & loginid & "'","")
		if UserName<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub
	
	public sub myuserObjByEmail()
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.email='" & email & "'","")
		if email<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub
	
	' arg0: username arg1:passwor
    ' // 验证用户根据用户名、密码 ///////////////////////////////////////////////
    public function myuserObjByNP(arg0,arg1)        
        dim tempSql,tempRs,Result
        Result=false
        tempSql=getSelect("top 1","where a.loginid='" & arg0 & "' and a.pwd='" & arg1 & "'","")
        if arg0<>"" and arg1<>"" then
            set tempRs=Server.CreateObject("Adodb.RecordSet")
            tempRs.Open tempSql,connObj,1,1
            if not tempRs.EOF then
                Result=true
                EvaValue(tempRs)
            end if
            tempRs.close
            set tempRs=nothing
        end if
        AdminObjByNP=Result
    end function
    
    ' // 检验用户是否存在 ///////////////////////////////////////////////
    public function HasName()        
        HasName=(connObj.execute("select count(id) from " & tableName & " where loginid='" & loginid & "'")(0))>=1
    end function
    
    ' // 检验邮箱是否存在 ///////////////////////////////////////////////
    public function HasEmail()
        HasEmail=(connObj.execute("select count(id) from " & tableName & " where email='" & email & "'")(0))>=1
    end function
    

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into" & tableName & "(loginid,pwd,Question,Answer,Email,Sex,city,state,country,Address,Postcode,firstname,lastname,mobile,Phone,Fax,Homepage,LoginIP,logins,LastLoginTime,RegDate,UserLevel,LockUser,status)"&_
		"values('" & loginid & "','" & pwd & "','" & Question & "','" & Answer & "','" & Email & "','" & Sex & "','" & city & "','" & state & "','" & country & "','" & Address & "','" & Postcode & "','" & firstname & "','" & lastname & "','" & mobile & "','" & Phone & "','" & Fax & "','" & Homepage & "','" & LoginIP & "'," & logins & ",'" & LastLoginTime & "','" & RegDate & "'," & UserLevel & "," & LockUser & "," & status & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
		
		id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set loginid='" & loginid & "',pwd='" & pwd & "',Question='" & Question & "',Answer='" & Answer & "',Email='" & Email & "',Sex='" & Sex & "',city='" & city & "',state='" & state & "',country='" & country & "',Address='" & Address & "',Postcode='" & Postcode & "',firstname='" & firstname & "',lastname='" & lastname & "',mobile='" & mobile & "',Phone='" & Phone & "',Fax='" & Fax & "',Homepage='" & Homepage & "',LoginIP='" & LoginIP & "',logins=" & logins & ",LastLoginTime='" & LastLoginTime & "',RegDate='" & RegDate & "',UserLevel=" & UserLevel & ",LockUser=" & LockUser & ",status=" & status & " where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if		
		connObj.execute cmdText		
	end function	
	
	'// 以Cookies记录状态 ///////////////////////////////////////////////////
	'// arg0 到期时间
	public function SaveStatus(arg0)
            Session.Timeout=arg0
            Session("username")=loginid
            Session("password")=pwd
	end function
	
	'// 清除Cookies ///////////////////////////////////////////////////
	public function ClearStatus()   
            Session("username")=""
            Session("password")=""
            Session.Abandon()
	end function
	
	'// 根据状态读取对象 ///////////////////////////////////////////////////
	public function myuserObjByStatus()
	        if trim(Session("username"))<>"" and trim(Session("password"))<>"" then
	            myuserObjByNP Session("username"),Session("password")
	        end if
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("loginid")<>"" then loginid=tempRs.Fields("loginid")
		if tempRs("pwd")<>"" then pwd=tempRs.Fields("pwd")
		if tempRs("Question")<>"" then Question=tempRs.Fields("Question")
		if tempRs("Answer")<>"" then Answer=tempRs.Fields("Answer")
		if tempRs("Email")<>"" then Email=tempRs.Fields("Email")
		if tempRs("Sex")<>"" then Sex=tempRs.Fields("Sex")
		if tempRs("city")<>"" then city=tempRs.Fields("city")
		if tempRs("state")<>"" then state=tempRs.Fields("state")
		if tempRs("country")<>"" then country=tempRs.Fields("country")	
		if tempRs("Address")<>"" then Address=tempRs.Fields("Address")
		if tempRs("Postcode")<>"" then Postcode=tempRs.Fields("Postcode")
		if tempRs("firstname")<>"" then firstname=tempRs.Fields("firstname")
		if tempRs("lastname")<>"" then lastname=tempRs.Fields("lastname")			
		if tempRs("mobile")<>"" then mobile=tempRs.Fields("mobile")
		if tempRs("Phone")<>"" then Phone=tempRs.Fields("Phone")
		if tempRs("Fax")<>"" then Fax=tempRs.Fields("Fax")
		if tempRs("Homepage")<>"" then Homepage=tempRs.Fields("Homepage")
		if tempRs("LoginIP")<>"" then LoginIP=tempRs.Fields("LoginIP")
		if tempRs("logins")<>"" then logins=tempRs.Fields("logins")
		if tempRs("LastLoginTime")<>"" then LastLoginTime=tempRs.Fields("LastLoginTime")
		if tempRs("RegDate")<>"" then RegDate=tempRs.Fields("RegDate")
		if tempRs("UserLevel")<>"" then UserLevel=tempRs.Fields("UserLevel")
		if tempRs("LockUser")<>"" then LockUser=tempRs.Fields("LockUser")
		if tempRs("status")<>"" then status=tempRs.Fields("status")
	end function

end class

'****************************************************
' 收货人信息
'****************************************************
class deliveryObj
	public id,userid,firstname,lastname,city,state,country,address,postcode,phone,mobile,email,dtime

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[delivery]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		userid=0
		firstname=""
		lastname=""
		city=""
		state=""
		country=""
		address=""
		postcode=""
		phone=""
		mobile=""
		email=""
		dtime=now()
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.*,b.loginid from " & tableName & " a inner join myuser b on a.userid=b.id  {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub deliveryObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into " & tableName & "(userid,firstname,lastname,city,state,country,address,postcode,phone,mobile,email,dtime)"&_
		" values(" & userid & ",'" & firstname & "','" & lastname & "','" & city & "','" & state & "','" & country & "','" & address & "','" & postcode & "','" & phone & "','" & mobile & "','" & email & "','" & dtime & "')"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)

        id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set userid=" & userid & ",firstname='" & firstname & "',lastname='" & lastname & "',city='" & city & "',state='" & state & "',country='" & country & "',address='" & address & "',postcode='" & postcode & "',phone='" & phone & "',mobile='" & mobile & "',email='" & email & "',dtime='" & dtime & "' where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("userid")<>"" then userid=tempRs.Fields("userid")
		if tempRs("firstname")<>"" then firstname=tempRs.Fields("firstname")
		if tempRs("lastname")<>"" then lastname=tempRs.Fields("lastname")
		if tempRs("city")<>"" then city=tempRs.Fields("city")
		if tempRs("state")<>"" then state=tempRs.Fields("state")
		if tempRs("country")<>"" then country=tempRs.Fields("country")	
		if tempRs("address")<>"" then address=tempRs.Fields("address")
		if tempRs("postcode")<>"" then postcode=tempRs.Fields("postcode")
		if tempRs("phone")<>"" then phone=tempRs.Fields("phone")
		if tempRs("mobile")<>"" then mobile=tempRs.Fields("mobile")
		if tempRs("email")<>"" then email=tempRs.Fields("email")
		if tempRs("dtime")<>"" then dtime=tempRs.Fields("dtime")
	end function
	
	'// 将信息保存到Cookies /////////////////////////////////////////////////////
	public function Save2Cookies(key)
	    Response.Cookies(key)("deliveryid")=id
	    Response.Cookies(key)("firstname")=firstname
        Response.Cookies(key)("lastname")=lastname
        Response.Cookies(key)("city")=city
        Response.Cookies(key)("state")=state
        Response.Cookies(key)("country")=country
        Response.Cookies(key)("address")=address
        Response.Cookies(key)("postcode")=postcode
        Response.Cookies(key)("phone")=phone
        Response.Cookies(key)("mobile")=mobile
        Response.Cookies(key)("email")=email
	end function
	
	public function getInfoByCookies(key)
	    if Request.Cookies(key)("deliveryid")<>"" then id=Request.Cookies(key)("deliveryid")
	    if Request.Cookies(key)("firstname")<>"" then firstname=Request.Cookies(key)("firstname")
	    if Request.Cookies(key)("lastname")<>"" then lastname=Request.Cookies(key)("lastname")
	    if Request.Cookies(key)("city")<>"" then city=Request.Cookies(key)("city")
	    if Request.Cookies(key)("state")<>"" then state=Request.Cookies(key)("state")
	    if Request.Cookies(key)("country")<>"" then country=Request.Cookies(key)("country")	    
	    if Request.Cookies(key)("address")<>"" then address=Request.Cookies(key)("address")
	    if Request.Cookies(key)("postcode")<>"" then postcode=Request.Cookies(key)("postcode")
	    if Request.Cookies(key)("phone")<>"" then phone=Request.Cookies(key)("phone")
	    if Request.Cookies(key)("mobile")<>"" then mobile=Request.Cookies(key)("mobile")
	    if Request.Cookies(key)("email")<>"" then email=Request.Cookies(key)("email")
	end function

end class

'****************************************************
' 单页面对象
'****************************************************
class MainObj
    public id,tempfile,title,content,pic,webTitle,webKeyword,webDescription,link,islink,filename,dtime,sort,status
    private connObj,tableName,test
    

    '// 设置 Initialize 事件。 /////////////////////////////////////////
    private sub Class_Initialize   
        call init()
        tableName="[main]"
        test=false
        set connObj=conn   ' 默认为conn
    end sub
    
    '// 设置 Terminate 事件。 ////////////////////////////////////////////
    private Sub Class_Terminate   
        call init()
    End Sub
    
    '// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
        tempfile="maintemp_1.asp"
        title=""
        content=""
        pic=""
        webTitle=""
        webKeyword=""
        webDescription=""
        link=""
        islink=0
        filename=""
        dtime=now()
        sort=0
        status=0
	end sub
    
    '// 判断是否为空 ////////////////////////////////////////////////
    Public function isNull()
        isNull=(id=0)
    end function
    
    ' 
    '// 组织SQL语句(select) ///////////////////////////////////////////
    private function getSelect(arg0,arg1,arg2)
        dim cmdText
        cmdText="select {0} a.* from " & tableName & " a {1} {2}"
        cmdText=Replace(cmdText,"{0}",arg0)
        cmdText=Replace(cmdText,"{1}",arg1)
        cmdText=Replace(cmdText,"{2}",arg2)
        if test then
			Response.Write cmdText
			Response.End()
		end if	
        getSelect=cmdText
    end function
    
    '//////////////////////////////////////////////////
    public sub MainObj(tempid)
        dim tempSql,tempRs
        tempSql=getSelect("top 1"," where a.id=" & tempid,"")
        if tempid<>"" then
            set tempRs=Server.CreateObject("Adodb.RecordSet")
            tempRs.Open tempSql,connObj,1,1
            if not tempRs.EOF then
                EvaValue(tempRs)
            end if
            tempRs.close
            set tempRs=nothing
        end if
    end sub
    
    ' 调用时 
    'set rs=__.Load("top 8","where a.jie=1","order by q.id desc")
    '// 读取数据 ////////////////////////////////////////////////
    public function Load(arg0,arg1,arg2)
        dim Result
        set Result=Server.CreateObject("Adodb.RecordSet")
        Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
        set Load=Result
    end function
    
    '// 添加  ////////////////////////////////////////////////
    public function Add()
        dim tempSql
        tempSql="insert into " & tableName & "(tempfile,title,content,pic,webTitle,webKeyword,webDescription,link,islink,filename,dtime,sort,status) "&_
        "values('" & tempfile & "','" & title & "','" & content & "','" & pic & "','" & webTitle & "','" & webKeyword & "','" & webDescription & "','" & link & "'," & islink & ",'" & filename & "','" & dtime & "'," & sort & "," & status & ")"
        if test then
			Response.Write cmdText
			Response.End()
		end if	
        connObj.execute(tempSql)
        
        id=connobj.execute("select MAX(id) from " & tableName)(0)
    end function
    
    '// 更新  ////////////////////////////////////////////////
    public function Update()        
        dim tempSql
        tempSql="update " & tableName & " set tempfile='" & tempfile & "',title='" & title & "',content='" & content & "',pic='" & pic & "',webTitle='" & webTitle & "',webKeyword='" & webKeyword & "',webDescription='" & webDescription & "',link='" & link & "',islink=" & islink & ",filename='" & filename & "',sort=" & sort & ",status=" & status & " where id=" & id
        if test then
			Response.Write cmdText
			Response.End()
		end if	
        connObj.execute(tempSql)
    end function    
    
    '// 给变量赋值 ////////////////////////////////////////////////
    private function EvaValue(tempRs) 
        if tempRs("id")<>"" then id=tempRs.Fields("id")
        if tempRs("tempfile")<>"" then tempfile=tempRs.Fields("tempfile")
        if tempRs("title")<>"" then title=tempRs("title")
        if tempRs("content")<>"" then content=tempRs("content")
        if tempRs("pic")<>"" then pic=tempRs("pic")           
        if tempRs("webTitle")<>"" then webTitle=tempRs("webTitle")
        if tempRs("webKeyword")<>"" then webKeyword=tempRs("webKeyword")
        if tempRs("webDescription")<>"" then webDescription=tempRs("webDescription")
        if tempRs("link")<>"" then link=tempRs("link")
        if tempRs("islink")<>"" then islink=tempRs("islink")
        if tempRs("filename")<>"" then filename=tempRs("filename")
        if tempRs("dtime")<>"" then dtime=tempRs("dtime")
        if tempRs("sort")<>"" then sort=tempRs("sort")
        if tempRs("status")<>"" then status=tempRs("status")  
    end function
    
end Class


'****************************************************
' 新闻对象类
'****************************************************
class NewsObj
    public id,classid,classname,depth,channelid,title,thetext,keywords,mediafile,focuspic,picset,publisher,newsfrom,hits,ispic,ishot,iscommend,sort,creationdate,status
    public connObj,tableName,test
    

    '// 设置 Initialize 事件。 /////////////////////////////////////////
    private sub Class_Initialize   
        call init()
        tableName="[news]"
        test=false
        set connObj=conn   ' 默认为conn
    end sub
    
    '// 设置 Terminate 事件。 ////////////////////////////////////////////
    private Sub Class_Terminate   
        call init()
    End Sub
    
    '// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
        classid=0
        classname=""
		depth=0
        channelid=0
        title=""
        thetext=""
        keywords=""
        mediafile=""
        focuspic="images/nopic.gif"
        picset=""
        publisher="" 
        newsfrom=""       
        hits=0
        ispic=0
        ishot=0
        iscommend=0
        sort=0
        creationdate=now()
        status=0
	end sub
    
    '// 判断是否为空 ////////////////////////////////////////////////
    Public function isNull()
        isNull=(id=0)
    end function
    
    ' 
    '// 组织SQL语句(select) ///////////////////////////////////////////
    private function getSelect(arg0,arg1,arg2)
        dim cmdText
        cmdText="select {0} a.*,b.name as classname,b.depth from " & tableName & " a inner join [myclass] b on a.classid=b.id {1} {2}"
        cmdText=Replace(cmdText,"{0}",arg0)
        cmdText=Replace(cmdText,"{1}",arg1)
        cmdText=Replace(cmdText,"{2}",arg2)
        if test then
			Response.Write cmdText
			Response.End
		end if
        getSelect=cmdText
    end function
    
    '//////////////////////////////////////////////////
    public sub NewsObj(tempid)
        dim tempSql,tempRs
        tempSql=getSelect("top 1"," where a.id=" & tempid,"")
        if tempid<>"" then
            set tempRs=Server.CreateObject("Adodb.RecordSet")
            tempRs.Open tempSql,connObj,1,1
            if not tempRs.EOF then
                EvaValue(tempRs)
            end if
            tempRs.close
            set tempRs=nothing
        end if
    end sub
    
    ' 调用时 
    'set rs=__.Load("top 8","where q.jie=1","order by q.id desc")
    '// 读取数据 ////////////////////////////////////////////////
    public function Load(arg0,arg1,arg2)
        dim Result
        set Result=Server.CreateObject("Adodb.RecordSet")
        Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
        set Load=Result
    end function
    
    '// 添加 ////////////////////////////////////////////////
    public function Add()
        dim cmdText
        cmdText="insert into " & tableName & "(classid,channelid,title,thetext,keywords,mediafile,focuspic,picset,publisher,newsfrom,hits,ispic,ishot,iscommend,sort,creationdate,status) "&_
        "values(" & classid & "," & channelid & ",'" & title & "','" & thetext & "','" & keywords & "','" & mediafile & "','" & focuspic & "','" & picset & "','" & publisher & "','" & newsfrom & "'," & hits & "," & ispic & "," & ishot & "," & iscommend & "," & sort & ",'" & creationdate & "'," & status & ")"
        if test then
			Response.Write cmdText
			Response.End()
		end if
        connObj.execute(cmdText)
        
        id=connobj.execute("select MAX(id) from " & tableName)(0)
    end function
    
    '// 更新 ////////////////////////////////////////////////
    public function Update()        
        dim cmdText
        cmdText="update " & tableName & " set classid=" & classid & ",channelid=" & channelid & ",title='" & title & "',thetext='" & thetext & "',keywords='" & keywords & "',mediafile='" & mediafile & "',focuspic='" & focuspic & "',picset='" & picset & "',publisher='" & publisher & "',newsfrom='" & newsfrom & "',hits=" & hits & ",ispic=" & ispic & ",ishot=" & ishot & ",iscommend=" & iscommend & ",sort=" & sort & ",creationdate='" & creationdate & "',status=" & status & " where id=" & id
        if test then
			Response.Write cmdText
			Response.End()
		end if
        connObj.execute(cmdText)
    end function
    
    
    public function GetRepTagContent()
        dim tagClass,tempAry,Result
        Result=thetext
        set tagClass=new ClassObj
        tagClass.typename="tag"
        tempAry=tagClass.GetArray()
        for i=1 to ubound(tempAry)
            Result=replace(Result,tempAry(i-1,3),"<a href="""&tempAry(i-1,4)&""" target=""_blank""><font color='blue'>" & tempAry(i-1,3) & "</font></a>")
        next
        GetRepTagContent=Result
    end function
    
    '// 更新点击 ////////////////////////////////////////////////
    public function UpdateClick()
        connObj.execute("update " & tableName & " set hits=hits+1 where id=" & id)
    end function
    
    '// 给变量赋值 ////////////////////////////////////////////////
    private function EvaValue(tempRs) 
        if tempRs("id")<>"" then id=tempRs.Fields("id")
        if tempRs("classid")<>"" then classid=tempRs("classid")
        if tempRs("classname")<>"" then classname=tempRs("classname")
        if tempRs("depth")<>"" then depth=tempRs("depth")
        if tempRs("channelid")<>"" then channelid=tempRs("channelid")            
        if tempRs("title")<>"" then title=tempRs("title")
        if tempRs("thetext")<>"" then thetext=tempRs("thetext")
        if tempRs("keywords")<>"" then keywords=tempRs("keywords")            
        if tempRs("mediafile")<>"" then mediafile=tempRs("mediafile")
        if tempRs("focuspic")<>"" then focuspic=tempRs("focuspic")
        if tempRs("picset")<>"" then picset=tempRs("picset")
        if tempRs("publisher")<>"" then publisher=tempRs("publisher")
        if tempRs("newsfrom")<>"" then newsfrom=tempRs("newsfrom")
        if tempRs("hits")<>"" then hits=tempRs("hits")
        if tempRs("ispic")<>"" then ispic=tempRs("ispic")
        if tempRs("ishot")<>"" then ishot=tempRs("ishot")
        if tempRs("iscommend")<>"" then iscommend=tempRs("iscommend")
        if tempRs("sort")<>"" then sort=tempRs("sort")
        if tempRs("creationdate")<>"" then creationdate=tempRs("creationdate")
        if tempRs("status")<>"" then status=tempRs("status")
    end function    
end Class

'****************************************************
' 产品对象类
'****************************************************
class ProductObj
	public ID,productnum,classid,classname,depth,title,keywords,thetext,saleprice,memberprice,Price1,Price2,other1,other2,other3,focuspic,picset,stocks,hits,isHot,isNew,isCommend,sort,updatedate,creationdate,status

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[Product]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		ID=0
		productnum=""
		classid=0
		classname=""
		depth=0
		title=""
		keywords=""
		thetext=""
		saleprice=0
		memberprice=0
		Price1=0
		Price2=0
		other1=""
		other2=""
		other3=""
		focuspic=""
		picset=""
		stocks=0
		hits=0
		isHot=0
		isNew=0
		isCommend=0
		sort=0
		updatedate=now()
		creationdate=now()
		status=0
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(ID=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.*,b.name as classname,b.depth from " & tableName & " a inner join myclass b on a.classid=b.id {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub ProductObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into" & tableName & "(productnum,classid,title,keywords,thetext,saleprice,memberprice,Price1,Price2,other1,other2,other3,focuspic,picset,stocks,hits,isHot,isNew,isCommend,sort,updatedate,creationdate,status)"&_
		"values('" & productnum & "'," & classid & ",'" & title & "','" & keywords & "','" & thetext & "'," & saleprice & "," & memberprice & "," & Price1 & "," & Price2 & ",'" & other1 & "','" & other2 & "','" & other3 & "','" & focuspic & "','" & picset & "'," & stocks & "," & hits & "," & isHot & "," & isNew & "," & isCommend & "," & sort & ",'" & updatedate & "','" & creationdate & "'," & status & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
		
		id=connobj.execute("select MAX(id) from " & tableName)(0)
		
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set productnum='" & productnum & "',classid=" & classid & ",title='" & title & "',keywords='" & keywords & "',thetext='" & thetext & "',saleprice=" & saleprice & ",memberprice=" & memberprice & ",Price1=" & Price1 & ",Price2=" & Price2 & ",other1='" & other1 & "',other2='" & other2 & "',other3='" & other3 & "',focuspic='" & focuspic & "',picset='" & picset & "',stocks=" & stocks & ",hits=" & hits & ",isHot=" & isHot & ",isNew=" & isNew & ",isCommend=" & isCommend & ",sort=" & sort & ",updatedate='" & updatedate & "',creationdate='" & creationdate & "',status=" & status & " where ID=" & ID
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)    
		if tempRs("ID")<>"" then id=tempRs.Fields("ID")			
		if tempRs("productnum")<>"" then productnum=tempRs.Fields("productnum")			
		if tempRs("classid")<>"" then classid=tempRs.Fields("classid")
		if tempRs("classname")<>"" then classname=tempRs.Fields("classname")			
		if tempRs("depth")<>"" then depth=tempRs.Fields("depth")		
		if tempRs("title")<>"" then title=tempRs.Fields("title")
		if tempRs("keywords")<>"" then keywords=tempRs.Fields("keywords")
		if tempRs("thetext")<>"" then thetext=tempRs.Fields("thetext")
		if tempRs("saleprice")<>"" then saleprice=tempRs.Fields("saleprice")
		if tempRs("memberprice")<>"" then memberprice=tempRs.Fields("memberprice")
		if tempRs("Price1")<>"" then Price1=tempRs.Fields("Price1")
		if tempRs("Price2")<>"" then Price2=tempRs.Fields("Price2")
		if tempRs("other1")<>"" then other1=tempRs.Fields("other1")
		if tempRs("other2")<>"" then other2=tempRs.Fields("other2")
		if tempRs("other3")<>"" then other3=tempRs.Fields("other3")
		if tempRs("focuspic")<>"" then focuspic=tempRs.Fields("focuspic")
		if tempRs("picset")<>"" then picset=tempRs.Fields("picset")
		if tempRs("stocks")<>"" then stocks=tempRs.Fields("stocks")
		if tempRs("hits")<>"" then hits=tempRs.Fields("hits")
		if tempRs("isHot")<>"" then isHot=tempRs.Fields("isHot")
		if tempRs("isNew")<>"" then isNew=tempRs.Fields("isNew")
		if tempRs("isCommend")<>"" then isCommend=tempRs.Fields("isCommend")
		if tempRs("sort")<>"" then sort=tempRs.Fields("sort")
		if tempRs("updatedate")<>"" then updatedate=tempRs.Fields("updatedate")
		if tempRs("creationdate")<>"" then creationdate=tempRs.Fields("creationdate")
		if tempRs("status")<>"" then status=tempRs.Fields("status")		
	end function

end class


'****************************************************
' 广告对象
'****************************************************
class adsObj
	public id,userid,classid,title,adtext,ext,linkurl,linkalt,filename,width,height,other,sort,dtime,overtime,status

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[ads]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		userid=0
		classid=0
		title=""
		adtext=""
		ext=""
		linkurl=""
		linkalt=""
		filename=""
		width=0
		height=0
		other=0
		sort=0
		dtime=now()
		overtime=now()
		status=0
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.* from " & tableName & " a {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub adsObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into " & tableName & "(userid,classid,title,adtext,ext,linkurl,linkalt,filename,width,height,other,sort,dtime,overtime,status)"&_
		" values(" & userid & "," & classid & ",'" & title & "','" & adtext & "','" & ext & "','" & linkurl & "','" & linkalt & "','" & filename & "'," & width & "," & height & "," & other & "," & sort & ",'" & dtime & "','" & overtime & "'," & status & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)

		id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set userid=" & userid & ",classid=" & classid & ",title='" & title & "',adtext='" & adtext & "',ext='" & ext & "',linkurl='" & linkurl & "',linkalt='" & linkalt & "',filename='" & filename & "',width=" & width & ",height=" & height & ",other=" & other & ",sort=" & sort & ",dtime='" & dtime & "',overtime='" & overtime & "',status=" & status & " where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("userid")<>"" then userid=tempRs.Fields("userid")
		if tempRs("classid")<>"" then classid=tempRs.Fields("classid")
		if tempRs("title")<>"" then title=tempRs.Fields("title")
		if tempRs("adtext")<>"" then adtext=tempRs.Fields("adtext")
		if tempRs("ext")<>"" then ext=tempRs.Fields("ext")
		if tempRs("linkurl")<>"" then linkurl=tempRs.Fields("linkurl")
		if tempRs("linkalt")<>"" then linkalt=tempRs.Fields("linkalt")
		if tempRs("filename")<>"" then filename=tempRs.Fields("filename")
		if tempRs("width")<>"" then width=tempRs.Fields("width")
		if tempRs("height")<>"" then height=tempRs.Fields("height")
		if tempRs("other")<>"" then other=tempRs.Fields("other")
		if tempRs("sort")<>"" then sort=tempRs.Fields("sort")
		if tempRs("dtime")<>"" then dtime=tempRs.Fields("dtime")
		if tempRs("overtime")<>"" then overtime=tempRs.Fields("overtime")
		if tempRs("status")<>"" then status=tempRs.Fields("status")
	end function

end class

'****************************************************
' 友情连接对象类
'****************************************************
class linksObj
	public id,classid,classname,name,subname,siteurl,siteintro,logo,siteadmin,email,sort,isgood,flag

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[links]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		classid=0
		classname=""
		name=""
		subname=""
		siteurl=""
		siteintro=""
		logo=""
		siteadmin=""
		email=""
		sort=0
		isgood=0
		flag=0
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.*,b.name as classname from " & tableName & " a inner join [myclass] b on a.classid=b.id {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub linksObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into " & tableName & "(classid,name,subname,siteurl,siteintro,logo,siteadmin,email,sort,isgood,flag)"&_
		" values(" & classid & ",'" & name & "','" & subname & "','" & siteurl & "','" & siteintro & "','" & logo & "','" & siteadmin & "','" & email & "'," & sort & "," & isgood & "," & flag & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)

		id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set classid=" & classid & ",name='" & name & "',subname='" & subname & "',siteurl='" & siteurl & "',siteintro='" & siteintro & "',logo='" & logo & "',siteadmin='" & siteadmin & "',email='" & email & "',sort=" & sort & ",isgood=" & isgood & ",flag=" & flag & " where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("classid")<>"" then classid=tempRs.Fields("classid")
		if tempRs("classname")<>"" then classname=tempRs.Fields("classname")
		if tempRs("name")<>"" then name=tempRs.Fields("name")
		if tempRs("subname")<>"" then subname=tempRs.Fields("subname")
		if tempRs("siteurl")<>"" then siteurl=tempRs.Fields("siteurl")
		if tempRs("siteintro")<>"" then siteintro=tempRs.Fields("siteintro")
		if tempRs("logo")<>"" then logo=tempRs.Fields("logo")
		if tempRs("siteadmin")<>"" then siteadmin=tempRs.Fields("siteadmin")
		if tempRs("email")<>"" then email=tempRs.Fields("email")
		if tempRs("sort")<>"" then sort=tempRs.Fields("sort")
		if tempRs("isgood")<>"" then isgood=tempRs.Fields("isgood")
		if tempRs("flag")<>"" then flag=tempRs.Fields("flag")
	end function

end class

'****************************************************
' 留言本对象类
'****************************************************
class feedbackObj
	public id,types,username,tel,fax,email,title,content,ip,addtime,reply,replytime,flag

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[feedback]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		types=0
		username=""
		tel=""
		fax=""
		email=""
		title=""
		content=""
		ip=Request.ServerVariables("REMOTE_ADDR")
		addtime=now()
		reply=""
		replytime=now()
		flag=0
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.* from " & tableName & " a {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub feedbackObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into" & tableName & "(types,username,tel,fax,email,title,content,ip,addtime,reply,replytime,flag)"&_
		"values(" & types & ",'" & username & "','" & tel & "','" & fax & "','" & email & "','" & title & "','" & content & "','" & ip & "','" & addtime & "','" & reply & "','" & replytime & "'," & flag & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
		
		id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set types=" & types & ",username='" & username & "',tel='" & tel & "',fax='" & fax & "',email='" & email & "',title='" & title & "',content='" & content & "',ip='" & ip & "',addtime='" & addtime & "',reply='" & reply & "',replytime='" & replytime & "',flag=" & flag & " where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("types")<>"" then types=tempRs.Fields("types")
		if tempRs("username")<>"" then username=tempRs.Fields("username")
		if tempRs("tel")<>"" then tel=tempRs.Fields("tel")
		if tempRs("fax")<>"" then fax=tempRs.Fields("fax")
		if tempRs("email")<>"" then email=tempRs.Fields("email")
		if tempRs("title")<>"" then title=tempRs.Fields("title")
		if tempRs("content")<>"" then content=tempRs.Fields("content")
		if tempRs("ip")<>"" then ip=tempRs.Fields("ip")
		if tempRs("addtime")<>"" then addtime=tempRs.Fields("addtime")
		if tempRs("reply")<>"" then reply=tempRs.Fields("reply")
		if tempRs("replytime")<>"" then replytime=tempRs.Fields("replytime")
		if tempRs("flag")<>"" then flag=tempRs.Fields("flag")
	end function

end class


'****************************************************
' 订单类对象
'****************************************************
class ordersObj
	public id,ordernum,myuserid,fee,moneytotal,firstname,lastname,city,state,country,address,postcode,phone,mobile,email,thetext,remark,paymenttype,delivertype,begindate,inputtime,status

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[orders]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		ordernum=""
		myuserid=0
		fee=0
		moneytotal=0
		firstname=""
		lastname=""
		city=""
		state=""
		country=""
		address=""
		postcode=""
		phone=""
		mobile=""
		email=""
		thetext=""
		remark=""
		paymenttype=""
		delivertype=""
		begindate=now()
		inputtime=now()
		status=0
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.* from " & tableName & " a {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub ordersObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub
	
	'// 弱弱地 ////////////////////////////////////////////
	'// arg0: formnumber
	'// arg1: email
	public sub orderformObjByNE(arg0,arg1)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.ordernum='" & arg0 & "' and a.email='" & arg1 & "'","")
		if arg0<>"" and arg1<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into " & tableName & "(ordernum,myuserid,fee,moneytotal,firstname,lastname,city,state,country,address,postcode,phone,mobile,email,thetext,remark,paymenttype,delivertype,begindate,inputtime,status)"&_
		" values('" & ordernum & "'," & myuserid & "," & fee & "," & moneytotal & ",'" & firstname & "','" & lastname & "','" & city & "','" & state & "','" & country & "','" & address & "','" & postcode & "','" & phone & "','" & mobile & "','" & email & "','" & thetext & "','" & remark & "','" & paymenttype & "','" & delivertype & "','" & begindate & "','" & inputtime & "'," & status & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)

		id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set ordernum='" & ordernum & "',myuserid=" & myuserid & ",fee=" & fee & ",moneytotal=" & moneytotal & ",firstname='" & firstname & "',lastname='" & lastname & "',city='" & city & "',state='" & state & "',country='" & country & "',address='" & address & "',postcode='" & postcode & "',phone='" & phone & "',mobile='" & mobile & "',email='" & email & "',thetext='" & thetext & "',remark='" & remark & "',paymenttype='" & paymenttype & "',delivertype='" & delivertype & "',begindate='" & begindate & "',inputtime='" & inputtime & "',status=" & status & " where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function
	
	' // 读取最近的订单
	public function getRecentOrder()		
		dim cmdText,tempRs
		cmdText=getSelect("top 1","where a.myuserid=" & myuserid,"order by a.id desc")
		if myuserid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end function
	
	' // 读取所属用户的订单
	public function getUserOrder()		
		set getUserOrder=load("","where a.myuserid=" & myuserid,"order by a.id desc")
	end function
	
	' // 计算所属用户的订单数
	public function CountByUid()
	    dim Result
	    Result=connObj.execute("select count(id) as total from " & tableName & " where myuserid=" & myuserid)(0)
	    if not IsNumeric(Result) then Result=0
	    CountByUid=Result
	end function
	
	' // 统计某天的订单数
	public function CountByDay(idate)
	    dim Result
	    Result=connObj.execute("select count(id) as total from " & tableName & " where (inputtime between #" & idate & "# and #" & DateAdd("d",1,idate) & "#)")(0)
	    if not IsNumeric(Result) then Result=0
	    CountByDay=Result
	end function
	
	' // 统计某天的订单总金额
	public function CountTotalByDay(idate)
	    dim Result
	    Result=connObj.execute("select sum(moneytotal) as total from " & tableName & " where (inputtime between #" & idate & "# and #" & DateAdd("d",1,idate) & "#)")(0)
	    if not IsNumeric(Result) then Result=0
	    CountTotalByDay=Result
	end function
	
	' // 删除订单
	public function delete()
	    if status=0 then
	        connObj.execute("delete from " & tableName & " where id=" & id)
	        connObj.execute("delete from orderitem where orderid = " & id)
	    end if
	end function
	

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if not tempRs.eof then
			if tempRs("id")<>"" then id=tempRs.Fields("id")
			if tempRs("ordernum")<>"" then ordernum=tempRs.Fields("ordernum")
			if tempRs("myuserid")<>"" then myuserid=tempRs.Fields("myuserid")			
			if tempRs("fee")<>"" then fee=tempRs.Fields("fee")
			if tempRs("moneytotal")<>"" then moneytotal=tempRs.Fields("moneytotal")
			if tempRs("firstname")<>"" then firstname=tempRs.Fields("firstname")
			if tempRs("lastname")<>"" then lastname=tempRs.Fields("lastname")
			if tempRs("city")<>"" then city=tempRs.Fields("city")
			if tempRs("state")<>"" then state=tempRs.Fields("state")
			if tempRs("country")<>"" then country=tempRs.Fields("country")			
			if tempRs("address")<>"" then address=tempRs.Fields("address")
			if tempRs("postcode")<>"" then postcode=tempRs.Fields("postcode")
			if tempRs("phone")<>"" then phone=tempRs.Fields("phone")
			if tempRs("mobile")<>"" then mobile=tempRs.Fields("mobile")
			if tempRs("email")<>"" then email=tempRs.Fields("email")			
			if tempRs("thetext")<>"" then thetext=tempRs.Fields("thetext")
			if tempRs("remark")<>"" then remark=tempRs.Fields("remark")
			if tempRs("paymenttype")<>"" then paymenttype=tempRs.Fields("paymenttype")
			if tempRs("delivertype")<>"" then delivertype=tempRs.Fields("delivertype")
			if tempRs("begindate")<>"" then begindate=tempRs.Fields("begindate")
			if tempRs("inputtime")<>"" then inputtime=tempRs.Fields("inputtime")
			if tempRs("status")<>"" then status=tempRs.Fields("status")
		end if
	end function

end class

'****************************************************
' 订单项类对象
'****************************************************
class orderitemObj
	public orderid,ordernum,title,orderstatus,productid,productname,saletype,price_original,price,trueprice,amount,other,subtotal,begindate,remark

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[orderitem]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		orderid=0
		ordernum=""
		title=""
		orderstatus=0
		productid=0
		productname=""
		saletype=0
		price_original=0
		price=0
		trueprice=0
		amount=0
		other=""
		subtotal=0
		begindate=now()
		remark=""
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.*,b.ordernum,b.status as orderstatus,c.title as productname,c.productnumber,c.focuspic from (" & tableName & " a inner join orders b on a.orderid= b.id) inner join product c on a.productid=c.id {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function
	
	'// 取当前购物车的所有产品 ////////////////////////////////////////////////////////
	public function LoadItems()
	    set LoadItems=Load("","where a.orderid=" & orderid,"order by a.productid")
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into" & tableName & "(orderid,productid,title,saletype,price_original,price,trueprice,amount,other,subtotal,begindate,remark)"&_
		"values(" & orderid & "," & productid & ",'" & title & "'," & saletype & "," & price_original & "," & price & "," & trueprice & "," & amount & ",'" & other & "'," & subtotal & ",'" & begindate & "','" & remark & "')"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set title='" & title & "',saletype=" & saletype & ",price_original=" & price_original & ",price=" & price & ",trueprice=" & trueprice & ",amount=" & amount & ",other='" & other & "',subtotal=" & subtotal & ",begindate='" & begindate & "',remark='" & remark & "' where orderid=" & orderid & " and productid=" & productid
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("orderid")<>"" then orderid=tempRs.Fields("orderid")			
		if tempRs("ordernum")<>"" then ordernum=tempRs.Fields("ordernum")
		if tempRs("title")<>"" then title=tempRs.Fields("title")			
		if tempRs("orderstatus")<>"" then orderstatus=tempRs.Fields("orderstatus")
		if tempRs("productid")<>"" then productid=tempRs.Fields("productid")
		if tempRs("productname")<>"" then productname=tempRs.Fields("productname")
		if tempRs("saletype")<>"" then saletype=tempRs.Fields("saletype")
		if tempRs("price_original")<>"" then price_original=tempRs.Fields("price_original")
		if tempRs("price")<>"" then price=tempRs.Fields("price")
		if tempRs("trueprice")<>"" then trueprice=tempRs.Fields("trueprice")
		if tempRs("amount")<>"" then amount=tempRs.Fields("amount")
		if tempRs("other")<>"" then other=tempRs.Fields("other")
		if tempRs("subtotal")<>"" then subtotal=tempRs.Fields("subtotal")
		if tempRs("begindate")<>"" then begindate=tempRs.Fields("begindate")
		if tempRs("remark")<>"" then remark=tempRs.Fields("remark")
	end function

end class


'****************************************************
' 购物车类对象
'****************************************************
class shoppingcartsObj
	public cartid,productid,quantity,other,updatetime

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[shoppingcarts]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		cartid=""
		productid=0
		quantity=0
		other=""
		updatetime=now()
	end sub
	
	'// 设定cartid,没则赋值 ////////////////////////////////////////////////////
	'// arg0:默认值
	public function setCartID(arg0)
	    cartid=Trim(Request.Cookies("Cart")("CartID"))
	    if cartid = "" then
            cartid = arg0 
        end if
        Response.Cookies("Cart")("CartID") = cartid
        Response.Cookies("Cart").Expires = Date + 10
	end function
	
	'// 检查是否有cartid,没则跳转页 ////////////////////////////////////////////////////
	'// arg0:提示信息
	'// arg1:跳转页
	public function checkcartid(arg0,arg1)
	    cartid=Trim(Request.Cookies("Cart")("CartID"))
	    if cartid = "" or TotalItem()=0 then
	        Response.Write("<script>alert('提示:\n\r\n\r你的购物车中没有商品!');window.location='index.asp';</s"&"cript>")
	        Response.End
	    else
	        Response.Cookies("Cart").Expires = Date + 10
	    end if
	end function

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(cartid="")
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.*,b.title as productname,b.productnumber,b.saleprice,b.memberprice,b.memberprice* a.quantity as subprice,b.Price1,b.Price2,b.focuspic from " & tableName & " a inner join product b on a.productid=b.id {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub shoppingcartsObj(tcartid,tproductid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1","where a.cartid='" & tcartid & "' and a.productid=" & tproductid,"")
		if tcartid<>"" and tproductid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function
	
	public function LoadItemByCartid()
	    set LoadItemByCartid=Load("","where a.cartid='" & cartid & "'","order by a.productid")
	end function
	
	'// 是否有相同订单货品 ////////////////////////////////////////////////////////
	public function HasSame()
        dim cmdText,tempRs,Result
		cmdText=getSelect("top 1","where a.cartid='" & cartid & "' and a.productid=" & productid,"")
		set tempRs=Server.CreateObject("Adodb.RecordSet")
		tempRs.Open cmdText,connObj,1,1
		
		Result=(not tempRs.eof)
		if not tempRs.EOF then
			EvaValue(tempRs)
		end if
		tempRs.close
		set tempRs=nothing
	    HasSame=Result
	end function
	
	' // 计算当前订单总价 ////////////////////////////////////////////////////////
	' // pricename 价格字段 ///////////////
	public function countPrice(pricename)
	    dim Result
	    Result=connObj.execute("select sum(a.quantity*(b." & pricename & ")) as total from " & tableName & " a inner join product b on a.productid=b.id where cartid='" & cartid & "'")(0)
	    if not IsNumeric(Result) then Result=0
	    countPrice=Result
	end function
	
	' // 计算当前购物车的项数 ////////////////////////////////////////////////////////
	public function TotalItem()
	    dim Result
	    Result=connObj.execute("select sum(quantity) from " & tableName & " where cartid='" & cartid & "'")(0)
	    if not IsNumeric(Result) then Result=0
	    TotalItem=Result
	end function
	
	' // 清空订单 ////////////////////////////////////////////////////////
	public function Clear()
        connObj.execute("delete from shoppingcarts where cartid='" & cartid & "'")
        Response.Cookies("Cart").Expires = Date -1
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into " & tableName & "(cartid,productid,quantity,other,updatetime)" &_
		" values('" & cartid & "'," & productid & "," & quantity & ",'" & other & "','" & updatetime & "')"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set quantity=" & quantity & ",updatetime='" & updatetime & "' where cartid='" & cartid & "' and productid=" & productid
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function
	
	'// 删除 ///////////////////////////////////////////////////
	public function Delete()
	    connObj.execute("delete from " & tableName & " where cartid='" & cartid & "' and productid=" & productid)
	end function
	

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("cartid")<>"" then cartid=tempRs.Fields("cartid")
		if tempRs("productid")<>"" then productid=tempRs.Fields("productid")
		if tempRs("quantity")<>"" then quantity=tempRs.Fields("quantity")
		if tempRs("other")<>"" then other=tempRs.Fields("other")
		if tempRs("updatetime")<>"" then updatetime=tempRs.Fields("updatetime")
	end function

end class


' ***************************************************
' 登陆历史对象
' ***************************************************
class loginlogObj
	public id,usertype,username,ip,logtime,logcontent,scriptname,poststring

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[loginlog]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		usertype=""
		username=""
		ip=Request.ServerVariables("REMOTE_ADDR")
		logtime=now()
		logcontent=""
		scriptname=""
		poststring=""
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.* from " & tableName & " a {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub loginlogObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into " & tableName & "(usertype,username,ip,logtime,logcontent,scriptname,poststring)"&_
		" values('" & usertype & "','" & username & "','" & ip & "','" & logtime & "','" & logcontent & "','" & scriptname & "','" & poststring & "')"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)

		id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set usertype='" & usertype & "',username='" & username & "',ip='" & ip & "',logtime='" & logtime & "',logcontent='" & logcontent & "',scriptname='" & scriptname & "',poststring='" & poststring & "' where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("usertype")<>"" then usertype=tempRs.Fields("usertype")
		if tempRs("username")<>"" then username=tempRs.Fields("username")
		if tempRs("ip")<>"" then ip=tempRs.Fields("ip")
		if tempRs("logtime")<>"" then logtime=tempRs.Fields("logtime")
		if tempRs("logcontent")<>"" then logcontent=tempRs.Fields("logcontent")
		if tempRs("scriptname")<>"" then scriptname=tempRs.Fields("scriptname")
		if tempRs("poststring")<>"" then poststring=tempRs.Fields("poststring")
	end function

end class

' ***************************************************
' 支付平台对象
' ***************************************************
class payplatformObj
	public id,platformname,showname,description,accountsid,md5key,rate,pluspoundage,sort,isdisabled,isdefault

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[payplatform]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		platformname=""
		showname=""
		description=""
		accountsid=""
		md5key=""
		rate=5
		pluspoundage=0
		sort=0
		isdisabled=0
		isdefault=0
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.* from " & tableName & " a {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub payplatformObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into " & tableName & "(platformname,showname,description,accountsid,md5key,rate,pluspoundage,sort,isdisabled,isdefault)"&_
		" values('" & platformname & "','" & showname & "','" & description & "','" & accountsid & "','" & md5key & "'," & rate & "," & pluspoundage & "," & sort & "," & isdisabled & "," & isdefault & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)

		id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set platformname='" & platformname & "',showname='" & showname & "',description='" & description & "',accountsid='" & accountsid & "',md5key='" & md5key & "',rate=" & rate & ",pluspoundage=" & pluspoundage & ",sort=" & sort & ",isdisabled=" & isdisabled & ",isdefault=" & isdefault & " where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("platformname")<>"" then platformname=tempRs.Fields("platformname")
		if tempRs("showname")<>"" then showname=tempRs.Fields("showname")
		if tempRs("description")<>"" then description=tempRs.Fields("description")
		if tempRs("accountsid")<>"" then accountsid=tempRs.Fields("accountsid")
		if tempRs("md5key")<>"" then md5key=tempRs.Fields("md5key")
		if tempRs("rate")<>"" then rate=tempRs.Fields("rate")
		if tempRs("pluspoundage")<>"" then pluspoundage=tempRs.Fields("pluspoundage")
		if tempRs("sort")<>"" then sort=tempRs.Fields("sort")
		if tempRs("isdisabled")<>"" then isdisabled=tempRs.Fields("isdisabled")
		if tempRs("isdefault")<>"" then isdefault=tempRs.Fields("isdefault")
	end function

end class


' ***************************************************
' 支付记录对象
' ***************************************************
class payrecordObj
	public id,username,orderformid,formnumber,paymentnum,platformid,platformname,moneypay,moneytrue,paytime,status,ebankinfo,remark

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[payrecord]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		username=""
		orderformid=0
		formnumber=""
		paymentnum=""
		platformid=0
		platformname=""
		moneypay=0
		moneytrue=0
		paytime=now()
		status=0
		ebankinfo=""
		remark=""
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.*,b.ordernum,c.platformname from (" & tableName & " a inner join orders b on a.orderformid=b.id) inner join payplatform c on a.platformid=c.id  {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub payrecordObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into " & tableName & "(username,orderformid,paymentnum,platformid,moneypay,moneytrue,paytime,status,ebankinfo,remark)"&_
		" values('" & username & "'," & orderformid & ",'" & paymentnum & "'," & platformid & "," & moneypay & "," & moneytrue & ",'" & paytime & "'," & status & ",'" & ebankinfo & "','" & remark & "')"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)

        id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set username='" & username & "',orderformid=" & orderformid & ",paymentnum='" & paymentnum & "',platformid=" & platformid & ",moneypay=" & moneypay & ",moneytrue=" & moneytrue & ",paytime='" & paytime & "',status=" & status & ",ebankinfo='" & ebankinfo & "',remark='" & remark & "' where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("username")<>"" then username=tempRs.Fields("username")
		if tempRs("orderformid")<>"" then orderformid=tempRs.Fields("orderformid")
		if tempRs("ordernum")<>"" then ordernum=tempRs.Fields("ordernum")			
		if tempRs("paymentnum")<>"" then paymentnum=tempRs.Fields("paymentnum")
		if tempRs("platformid")<>"" then platformid=tempRs.Fields("platformid")
		if tempRs("platformname")<>"" then platformname=tempRs.Fields("platformname")			
		if tempRs("moneypay")<>"" then moneypay=tempRs.Fields("moneypay")
		if tempRs("moneytrue")<>"" then moneytrue=tempRs.Fields("moneytrue")
		if tempRs("paytime")<>"" then paytime=tempRs.Fields("paytime")
		if tempRs("status")<>"" then status=tempRs.Fields("status")
		if tempRs("ebankinfo")<>"" then ebankinfo=tempRs.Fields("ebankinfo")
		if tempRs("remark")<>"" then remark=tempRs.Fields("remark")
	end function

end class


' ***************************************************
' 投票类对象
' ***************************************************
class voteObj
	public id,title,select1,answer1,select2,answer2,select3,answer3,select4,answer4,select5,answer5,select6,answer6,select7,answer7,select8,answer8,votetime,votetype,startdate,enddate,isselected,status

	'// connObj:连接对象,默认:conn  tableName:表名  test:是否输出sql语句 /////////////////////////////////////////
	public connObj,tableName,test,total,ItemSet(7,2),totalitem

	'// 设置 Initialize 事件 /////////////////////////////////////////
	private sub Class_Initialize
		call init()
		tableName="[vote]"
		test=false
		set connObj=conn   ' 默认为conn
	end sub

	'// 设置 Terminate 事件 //////////////////////////////////////////
	private sub Class_Terminate
		call init()
	end sub

	'// Init 事件 ////////////////////////////////////////////////////
	private sub init()
		id=0
		title=""
		select1=""
		answer1=0
		select2=""
		answer2=0
		select3=""
		answer3=0
		select4=""
		answer4=0
		select5=""
		answer5=0
		select6=""
		answer6=0
		select7=""
		answer7=0
		select8=""
		answer8=0
		votetime=now()
		votetype=""
		startdate=now()
		enddate=now()
		isselected=0
		status=0
		
		total=0
		totalitem=0
	end sub

	'// 判断对象是否为空 /////////////////////////////////////////////
	public function isNull()
		isNull=(id=0)
	end function

	'// 组织查询语句(select) //////////////////////////////////////////
	private function getSelect(arg0,arg1,arg2)
		dim cmdText
		cmdText="select {0} a.* from " & tableName & " a {1} {2}"
		cmdText=Replace(cmdText,"{0}",arg0)
		cmdText=Replace(cmdText,"{1}",arg1)
		cmdText=Replace(cmdText,"{2}",arg2)
		if test then
			Response.Write cmdText
			Response.End()
		end if
		getSelect=cmdText
	end function

	'// 弱弱地 ////////////////////////////////////////////
	public sub voteObj(tempid)
		dim cmdText,tempRs
		cmdText=getSelect("top 1"," where a.id=" & tempid,"")
		if tempid<>"" then
			set tempRs=Server.CreateObject("Adodb.RecordSet")
			tempRs.Open cmdText,connObj,1,1
			if not tempRs.EOF then
				EvaValue(tempRs)
			end if
			tempRs.close
			set tempRs=nothing
		end if
	end sub

	'// 调用时 ////////////////////////////////////////////////////////
	'// 例子 news.load("top 20","where a.id=9","order by a.id desc") 
	public function Load(arg0,arg1,arg2)
		dim Result
		set Result=Server.CreateObject("Adodb.RecordSet")
		Result.Open getSelect(arg0,arg1,arg2),connObj,1,1
		set Load=Result
	end function

	'// 添加新数据 ///////////////////////////////////////////////////
	public function Add()
		dim cmdText
		cmdText="insert into " & tableName & "(title,select1,answer1,select2,answer2,select3,answer3,select4,answer4,select5,answer5,select6,answer6,select7,answer7,select8,answer8,votetime,votetype,startdate,enddate,isselected,status)"&_
		" values('" & title & "','" & select1 & "'," & answer1 & ",'" & select2 & "'," & answer2 & ",'" & select3 & "'," & answer3 & ",'" & select4 & "'," & answer4 & ",'" & select5 & "'," & answer5 & ",'" & select6 & "'," & answer6 & ",'" & select7 & "'," & answer7 & ",'" & select8 & "'," & answer8 & ",'" & votetime & "','" & votetype & "','" & startdate & "','" & enddate & "'," & isselected & "," & status & ")"
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)

		id=connobj.execute("select MAX(id) from " & tableName)(0)
	end function

	'// 更新数据 ///////////////////////////////////////////////////
	public function Update()
		dim cmdText
		cmdText="update " & tableName & " set title='" & title & "',select1='" & select1 & "',answer1=" & answer1 & ",select2='" & select2 & "',answer2=" & answer2 & ",select3='" & select3 & "',answer3=" & answer3 & ",select4='" & select4 & "',answer4=" & answer4 & ",select5='" & select5 & "',answer5=" & answer5 & ",select6='" & select6 & "',answer6=" & answer6 & ",select7='" & select7 & "',answer7=" & answer7 & ",select8='" & select8 & "',answer8=" & answer8 & ",votetime='" & votetime & "',votetype='" & votetype & "',startdate='" & startdate & "',enddate='" & enddate & "',isselected=" & isselected & ",status=" & status & " where id=" & id
		if test then
			Response.Write cmdText
			Response.End()
		end if
		connObj.execute(cmdText)
	end function
	
	

	'// 给变量赋值 /////////////////////////////////////////////////////
	private function EvaValue(tempRs)
		if tempRs("id")<>"" then id=tempRs.Fields("id")
		if tempRs("title")<>"" then title=tempRs.Fields("title")
		if tempRs("select1")<>"" then select1=tempRs.Fields("select1")
		if tempRs("answer1")<>"" then answer1=tempRs.Fields("answer1")
		if tempRs("select2")<>"" then select2=tempRs.Fields("select2")
		if tempRs("answer2")<>"" then answer2=tempRs.Fields("answer2")
		if tempRs("select3")<>"" then select3=tempRs.Fields("select3")
		if tempRs("answer3")<>"" then answer3=tempRs.Fields("answer3")
		if tempRs("select4")<>"" then select4=tempRs.Fields("select4")
		if tempRs("answer4")<>"" then answer4=tempRs.Fields("answer4")
		if tempRs("select5")<>"" then select5=tempRs.Fields("select5")
		if tempRs("answer5")<>"" then answer5=tempRs.Fields("answer5")
		if tempRs("select6")<>"" then select6=tempRs.Fields("select6")
		if tempRs("answer6")<>"" then answer6=tempRs.Fields("answer6")
		if tempRs("select7")<>"" then select7=tempRs.Fields("select7")
		if tempRs("answer7")<>"" then answer7=tempRs.Fields("answer7")
		if tempRs("select8")<>"" then select8=tempRs.Fields("select8")
		if tempRs("answer8")<>"" then answer8=tempRs.Fields("answer8")
		if tempRs("votetime")<>"" then votetime=tempRs.Fields("votetime")
		if tempRs("votetype")<>"" then votetype=tempRs.Fields("votetype")
		if tempRs("startdate")<>"" then startdate=tempRs.Fields("startdate")
		if tempRs("enddate")<>"" then enddate=tempRs.Fields("enddate")
		if tempRs("isselected")<>"" then isselected=tempRs.Fields("isselected")
		if tempRs("status")<>"" then status=tempRs.Fields("status")
		
		total = answer1 + answer2 + answer3 + answer4 + answer5 + answer6 + answer7 + answer8
		dim k
		for k=1 to 8
			if tempRs.Fields("select" & k) <>"" then totalitem=totalitem+1
	        ItemSet(k-1,0)=k
	        ItemSet(k-1,1)=tempRs.Fields("select" & k)
	        ItemSet(k-1,2)=tempRs.Fields("answer" & k)
	    next
	end function	
end class



' ***************************************************
' 缓存类对象
' ***************************************************
Class Cache

    '对象的声明

    Public ReloadTime    ' 过期时间（单位为分钟）
    Public CacheName     '缓存组的名称（预留功能，当一个站点中有多个缓存组时，则需要为每个缓存组设置不同的名称）。
    Private CacheData

    Private Sub Class_Initialize()
        ReloadTime = 10
        CacheName = "Abine"
    End Sub

    Private Sub Class_Terminate()

    End Sub

    '************************************************************
    '函数名：SetValue
    '作  用：设置缓存对象的值
    '参  数：MyCacheName ---- 缓存对象的名称
    '      vNewValue ----- 要给缓存对象的值
    '返回值：True ---- 设置成功，False ---- 设置失败
    '************************************************************
    Public Function SetValue(MyCacheName, vNewValue)
        If MyCacheName <> "" Then
            CacheData = Application(CacheName & "_" & MyCacheName)
            If IsArray(CacheData) Then
                CacheData(0) = vNewValue
                CacheData(1) = Now()
            Else
                ReDim CacheData(2)
                CacheData(0) = vNewValue
                CacheData(1) = Now()
            End If
            Application.Lock
            Application(CacheName & "_" & MyCacheName) = CacheData
            Application.UnLock
            SetValue = True
        Else
            SetValue = False
        End If
    End Function

    '************************************************************
    '函数名：GetValue
    '作  用：得到缓存对象的值
    '参  数：MyCacheName ---- 缓存对象的名称
    '返回值：缓存对象的值
    '************************************************************
    Public Function GetValue(MyChacheName)
        If MyChacheName <> "" Then
            CacheData = Application(CacheName & "_" & MyChacheName)
            If IsArray(CacheData) Then
                GetValue = CacheData(0)
            Else
                GetValue = ""
            End If
        Else
            GetValue = ""
        End If
    End Function

    '************************************************************
    '函数名：CacheIsEmpty
    '作  用：判断当前缓存是否过期
    '参  数：MyCacheName ---- 缓存对象的名称
    '返回值：True ---- 已经过期，False ---- 没有过期
    '************************************************************
    Public Function CacheIsEmpty(MyCacheName)
        CacheIsEmpty = True
        CacheData = Application(CacheName & "_" & MyCacheName)
        If Not IsArray(CacheData) Then Exit Function
        If Not IsDate(CacheData(1)) Then Exit Function
        If DateDiff("s", CDate(CacheData(1)), Now()) < 60 * ReloadTime Then
            CacheIsEmpty = False
        End If
    End Function

    '************************************************************
    '过程名：DelCache
    '作  用：手工删除一个缓存对象
    '参  数：MyCacheName ---- 缓存对象的名称
    '************************************************************
    Public Sub DelCache(MyCacheName)
        Application.Lock
        Application.Contents.Remove (CacheName & "_" & MyCacheName)
        Application.UnLock
    End Sub

    '************************************************************
    '过程名：DelAllCache
    '作  用：删除全部缓存对象
    '参  数：无
    '************************************************************
    Public Sub DelAllCache()
        Dim Cacheobj, strAllCache, CacheList, i
        For Each Cacheobj In Application.Contents
            If CStr(Left(Cacheobj, Len(CacheName) + 1)) = CStr(CacheName & "_") Then
                strAllCache = strAllCache & Cacheobj & ","
            End If
        Next
        CacheList = Split(strAllCache, ",")
        If UBound(CacheList) > 0 Then
            For i = 0 To UBound(CacheList)
                Application.Lock
                Application.Contents.Remove CacheList(i)
                Application.UnLock
            Next
        End If
    End Sub

    '************************************************************
    '过程名：DelChannelCache
    '作  用：删除指定频道的缓存对象
    '参  数：ChannelID ---- 频道ID
    '************************************************************
    Public Sub DelChannelCache(ChannelID)
        Dim Cacheobj, strChannelCache, CacheList, i
        regEx.Pattern = "^" & CacheName & "_" & ChannelID & "_"
        For Each Cacheobj In Application.Contents
            If regEx.Test(Cacheobj) = True Then
                strChannelCache = strChannelCache & Cacheobj & ","
            End If
        Next
        CacheList = Split(strChannelCache, ",")
        If UBound(CacheList) > 0 Then
            For i = 0 To UBound(CacheList)
                Application.Lock
                Application.Contents.Remove CacheList(i)
                Application.UnLock
            Next
        End If
    End Sub

End Class

%>