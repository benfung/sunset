﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<% GeneratedHtml conOpenAsp2Html,conDefaultPageCharset ' 生成静态页 %>
<%
dim myclass
set myclass=new classobj

dim product
set product=new productObj
product.productObj(getquerystring("id",0))

dim aryclass
aryClass=Split(myclass.Trackback(product.classid),",")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>
        <%=product.title %>:<%=conSeoTitle %>
    </title>
    <!--#include file="headmeta.asp"-->
    <meta name="keywords" content="<%=product.keywords %>" />
    <meta name="description" content="<%=product.keywords %>" />
</head>
<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td width="180">
                </td>
                <td width="12">
                </td>
                <td>
                	<div>当前位置:<a href="index.asp">首页</a>
        <% for j=0 to ubound(aryClass)%>
        	<% myclass.classobj(aryClass(j)) %>
            <% if not myclass.isnull then %>
        	>> <a href="product_<%=myclass.id%>.htm"><%=myclass.name%></a>
			<% end if %>
        <% next %>
        >> <%=product.title %>
        </div>
                    <div class="box" style="padding: 20px;">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="190"><img src="<%=product.focuspic %>" alt="<%=product.title %>" align="absmiddle"
                                    width="170" height="225" class="productimg" /></td>
                                <td class="productinfotd">【商品名称】：<span class="sp1"><%=product.title %></span><br />
                                【商品货号】：<%=product.productNumber %><br />
                                【颜色】：<%=product.other1 %><br />
                                【面料】：<%=product.other2 %><br />
                                【尺码】：<%=product.other3 %><br />
                                【批发价格】：￥ <%=product.saleprice %>&nbsp;&nbsp;&nbsp;<a href="add2cart.asp?productid=<%=product.id %>"><img class="add" src="images/addshoppingcart.gif" align="absmiddle" alt="立即订购" /></a><br />
                                <br />
                                <u>10件起批，可混批。10件以下，每件加20元。达到指定的拿货量可以享受VIP客户优惠。</u>
                             </td>
                            </tr>
                        </table>
                        <div style="padding: 10px; border-bottom:dashed 1px #ccc; margin-bottom:10px;">
                        </div>
                        <div class="productinfotd" style="width:100%;">
                        【相关图片】：<br />
                        <%
                        picset=split(product.picset,"|")
                        For Each pic In picset
                            Response.Write "<div><a href='" & pic & "' class='thickbox' rel='gallery-plants'><img src='s_" & pic & "' alt='' width='125' class='pimg' height='160' ></a></div>"
                        Next
                        %>
                        </div>
                        <div style="padding: 10px; border-bottom:dashed 1px #ccc; margin-bottom:10px;">
                        </div>
                        <%=product.content %><br />
                    </div>
                    
                </td>
            </tr>
        </table>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
