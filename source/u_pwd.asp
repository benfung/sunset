﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/md5.asp" -->
<!--#include file="Inc/getuser.asp" -->
<%
' 验证用户
call CheckUserLogin("")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>修改密码</title>
    <!--#include file="headmeta.asp"-->
</head>
<%
dim action,errormsg,oldpwd,password,password2
action=getForm("action","")
oldpwd=getForm("oldpwd","")
password=getForm("password","")
password2=getForm("password2","")
errormsg=""
if action="save" then
    if myuser.pwd<>md5(oldpwd,32) then
        errormsg="原密码输入错误！！"
    end if
    if password<>password2 then
        errormsg="密码确认错误！！"
    end if
    myuser.pwd=md5(password,32)
    if ErrorMsg="" then
        myuser.update()   
        myuser.ClearStatus()
        call alert("操作完成!!\r\n请重新登录","signin.asp?rurl=u_default.asp")
    end if
end if
%>
<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td width="180">
                    <!--#include file="u_left.asp"-->
                </td>
                <td width="12"></td>
                <td>

                    <script type="text/javascript">
                        function CheckPwdForm(form) {
                            if (!$.Validator["require"].test(form.oldpwd.value)) {
                                alert("提示:\r\n\r\n请输入旧密码!!");
                                form.oldpwd.select();
                                return false;
                            }
                            if (!$.Validator["password"].test(form.password.value)) {
                                alert("提示:\r\n\r\n请输入新密码!!");
                                form.password.select();
                                return false;
                            }
                            if ($.trim(form.password.value) != $.trim(form.password2.value)) {
                                alert("提示:\r\n\r\n密码确认错误!!");
                                form.password.value = "";
                                form.password2.value = "";
                                form.password.select();
                                return false;
                            }
                        }
                    </script>

                    <form name="webForm" id="webForm" class="appnitro" action="?" method="post" onSubmit="return CheckPwdForm(this)">
                        <input type="hidden" name="action" value="save" />
                        <ul>   
                            <li class="section_break">
                    <h3>
                        密码修改后必须重新登陆</h3>
                </li>                         
                            <% if ErrorMsg<>"" then  %><li class="imsg">
                                <%=ErrorMsg %>
                            </li>
                            <% end if %>
                            <li>
                                <label class="description" for="oldpwd">
                                    我的旧密码
                                </label>
                                <div>
                                    <input id="oldpwd" name="oldpwd" class="element text medium" type="password" maxlength="100" />
                                </div>
                                <p class="guidelines">
                                    请输入旧密码</p>
                            </li>
                            <li>
                                <hr />
                            </li>
                            <li>
                                <label class="description" for="password">
                                    新密码
                                </label>
                                <div>
                                    <input id="password" name="password" class="element text medium" type="password" maxlength="100" />
                                </div>
                                <p class="guidelines">
                                    请输入新密码,长度6－20个字符</p>
                            </li>
                            <li>
                                <label class="description" for="password2">
                                    再次输入新密码
                                </label>
                                <div>
                                    <input id="password2" name="password2" class="element text medium" type="password" maxlength="100" />
                                </div>
                            </li>
                            <li class="buttons">
                                <input class="button" type="submit" value="确 定" />
                            </li>
                        </ul>
                    </form>
                </td>
            </tr>
        </table>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
