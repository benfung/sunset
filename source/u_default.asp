﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<%
' 验证用户
call CheckUserLogin("")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>
        用户信息中心
    </title>
    
    <!--#include file="headmeta.asp"-->
</head>
<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td width="180">
                    <!--#include file="u_left.asp"-->
                </td>
                <td width="12">
                </td>
                <td>
                    <div class="singlepageTitle">
                        当前位置:<a href="index.asp" class="linkB">首页</a> > 用户信息中心
                    </div>
                    <div style="width:100%;padding: 12px 20px; text-align:center; font-size:16px; line-height: 180%; height:340px">
                            <%=myuser.loginid %>你好，欢迎您进入<%=conSiteName %>帐户管理中心;<br />
                            点击左边的链接可以查看与你相关的资料
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
