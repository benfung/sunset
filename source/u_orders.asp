﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<%
' 验证用户
call CheckUserLogin("")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>
        我的订单
    </title>    
    <!--#include file="headmeta.asp"-->
</head>
<%
dim orders
set orders=new ordersObj
orders.myuserid=myuser.id

dim action,id
action=getquerystring("action","")
id=getquerystring("id",0)
if action="cancel" and id<>"0"  then
    conn.execute("update orders set status=0 where id=" & id & " and myuserid=" & myuser.id)
    call alert("操作成功,订单已取消","?")
end if
%>
<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td width="180">
                    <!--#include file="u_left.asp"-->
                </td>
                <td width="8">
                </td>
                <td>
                    <table cellpadding="2" cellspacing="0" class="table1">
                        <tr>                        
                            <th class="th"><div>订单号</div></th>
                            <th class="th"><div>总价</div></th>
                            <th class="th"><div>状态</div></th>
                            <th class="th"><div>详情</div></th>
                            <th class="th"><div>支付</div></th>
                            <th class="th"><div>操作</div></th>
                        </tr>
                        <% set rs=orders.getUserOrder() %>
                        <% for i=1 to rs.recordcount %>
                        <tr>   
                            <td class="til" width="130"><%=rs.fields("ordernum") %></td> 
                            <td class="tic" width="80"><%=rs.fields("moneytotal") %></td>
                            <td class="tic"><%=GetOrderStatus(rs.fields("status")) %></td>
                            <td class="tic"><a href="orderdetail.asp?ordernum=<%=rs.fields("ordernum") %>&email=<%=rs.fields("email") %>" target="_blank" class="b">查看详情</a></td>  
                            <td class="tic" width="80"><% if rs.fields("status")<4 and rs.fields("status")>0 then%><a href="orderdetail.asp?ordernum=<%=rs.fields("ordernum") %>&email=<%=rs.fields("email") %>" target="_blank" class="b">支付</a><% else %>已经支付<%end if %></td> 
                            <td class="tic">
                            <% if rs.fields("status")=1 then%><a href="?action=cancel&id=<%=rs.fields("id") %>"  class="b">取消</a><% else %>不能取消<% end if %></td>                 
                        </tr>
                        <%  rs.movenext %>
                        <% next %>
                        <% rs.close:set rs=nothing %>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
