﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<% GeneratedHtml conOpenAsp2Html,conDefaultPageCharset ' 生成静态页 %>
<%
dim myclass
set myclass=new classobj

dim product
set product=new productobj

dim where,orderby
dim classid,ishot,isnew,iscommend,title
dim filePath,CurrentPage,PageSize,TotalNumber,TotalPage
classid=getquerystring("classid",0)
ishot=getquerystring("ishot",0)
isnew=getquerystring("isnew",0)
iscommend=getquerystring("iscommend",0)
title=getquerystring("title","")
CurrentPage=Cint(getQueryString("pageno",1))
filePath="?classid=" & classid & "&ishot=" & ishot & "&isnew=" & isnew & "&iscommend=" & iscommend & "&title=" & title & "&"
wtitle="商品展示"

if classid<>"0" then
    myclass.classobj(classid)
    wtitle=myclass.name
end if

if title<>"" then
    wtitle="查询关键字:" & title
end if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>
       <%=wtitle %>:<%=conSeoTitle %>
    </title>
    <!--#include file="headmeta.asp"-->
    <meta name="keywords" content="<%=conSiteKeyword %>" />
    <meta name="description" content="<%=conSiteDescription %>" />
</head>
<%
orderby="order by a.id desc"
where="where a.status=1"
if title<>"" then
    where = where & " and (a.title like '%" & title & "%' or a.productNumber like '%" & title & "%')"
end if
if classid<>"0" then
    where= where & "and a.classid in ("&myclass.GetSubs(classid)&")"
end if
if ishot<>"0" then
    orderby="order by a.ishot desc,a.id desc"
end if
if isnew<>"0" then
    orderby="order by a.isnew desc,a.id desc"
end if
if iscommend<>"0" then
    orderby="order by a.iscommend desc,a.id desc"
end if
 %>
<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td width="180">
                    
                </td>
                <td width="12">
                </td>
                <td>
                    

<div style="width: 100%;">
<% set rs=product.load("",where,orderby) %>
<% 
PageSize=12
rs.PageSize=PageSize
TotalPage=rs.PageCount
TotalNumber=rs.RecordCount
if rs.EOF then
    Response.Write ("<span style='color:red;font-size:14px;'>查询结果为空!!<span>")
else
    if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
    rs.AbsolutePage=CurrentPage
end if
 %>
<% for i=1 to CurrentRecord(rs,CurrentPage) %>
<div class="producttd"><a href="productdetail.asp?id=<%=rs.fields("id") %>"><img src="<%=rs.fields("focuspic") %>" alt="<%=rs.fields("title") %>" align="absmiddle" width="170" height="225"class="productimg" /></a><br /><a href="productdetail.asp?id=<%=rs.fields("id") %>"><%=rs.fields("title") %><br /><span style="color: #f28804;">批发价:￥<%=FormatNumber(rs.fields("memberprice")) %></span></a><br /><a href="add2cart.asp?productid=<%=rs.fields("id") %>"><img class="add" src="images/addshoppingcart.gif" align="absmiddle" alt="立即订购" /></a></div>
    <%rs.movenext %>
<% next %>
<% rs.close:set rs=nothing %>                          
</div>
<div style="margin:8px 0px;"><% call AbineShowPage(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %></div>
                </td>
            </tr>
        </table>
    </div>
    <!--#include file="flink.asp"-->
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
