﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/MailObject.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/md5.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>注册 -
        <%=conSiteTitle %>
    </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
dim myuser
set myuser=new myuserobj

dim loginid,password,action,rurl,validatecode,errormsg,email
action=getquerystring("action",getForm("action",""))
rurl=getquerystring("rurl",getForm("rurl","u_default.asp"))
loginid=TurnSql(getForm("loginid",""))
password=getForm("password","")
email=getForm("email","")
validatecode=getform("validatecode","")


errormsg=""
if action="reg" then
	myuser.loginid=loginid
	myuser.email=email
	if len(myuser.loginid)<4 then
	    'ErrorMsg="请输入用户名！长度4－20个字符"
	    call AlertPop("请输入用户名！长度4－20个字符","")
	end if
	if len(password)<6 then
	    'ErrorMsg="请输入密码！长度6－20个字符"
	    call AlertPop("请输入密码！长度6－20个字符","")
	end if
	if validatecode<>Cstr(Session("ValidateCode")) then    
        'errormsg="对不起,验证码不正确!!"  
        call AlertPop("对不起,验证码不正确!!","")  
    end if	
    
	if myuser.HasName() then
	    'ErrorMsg="该用户名已被使用，请重新输入!!"
	    call AlertPop("该用户名已被使用，请重新输入!!","") 
	end if
		
	if myuser.HasEmail() then
	    'ErrorMsg="该邮箱已被注册，请重新输入!!"
	    call AlertPop("该邮箱已被注册，请重新输入!!","") 
	end if
	myuser.pwd=md5(password,32)
	
	'myuser.question=TurnSql(getForm("question",""))
	'myuser.answer=TurnSql(getForm("answer",""))
	myuser.firstname=TurnSql(getForm("firstname",""))
	myuser.lastname=TurnSql(getForm("lastname",""))
	myuser.sex=TurnSql(getForm("sex",""))
	myuser.city=getForm("city",0)
	myuser.state=getForm("state",0)
	myuser.country=getForm("country",44)
	myuser.address=TurnSql(getForm("address",""))
	myuser.postcode=TurnSql(getForm("postcode",""))
	myuser.mobile=TurnSql(getForm("mobile",""))
	myuser.phone=TurnSql(getForm("phone",""))
	myuser.fax=TurnSql(getForm("fax",""))
	myuser.homepage=TurnSql(getForm("homepage",""))

	
	myuser.loginip=getip()
	myuser.logins=getForm("logins",0)
	myuser.lastlogintime=getForm("lastlogintime",now())
	myuser.regdate=getForm("regdate",now())
	myuser.userlevel=getForm("userlevel",1)
	myuser.lockuser=getForm("lockuser",0)
	myuser.status=getForm("status",1)

	if ErrorMsg="" then
		myuser.add()		
		
		' 发送邮件
        set Mail=new MailObject        
        Mail.MailServer=conMailServer
        Mail.MailServerUserName=conMailServerUserName
        Mail.MailServerPassWord=conMailServerPassWord
        Mail.MailDomain=conMailDomain
        subject=conSiteName &" - 会员注册"
        body= "你好:<br />网站 <a herf='"&conSiteUrl&"'>"&conSiteName&"</a> 有新会员(" & myuser.loginid & ")注册<br /><br />"        
        mail.send conWebmasterEmail,conWebmasterEmail, subject, body, conWebmasterEmail, conWebmasterEmail, 3
		
		myuser.SaveStatus(conLoginTimeOut) '登陆
		call alert("恭喜您!!" & myuser.loginid & "注册成功!!",rurl)
	end if
end if
%>
<body>
    <!--#include file="head.asp"-->
    <div class="body">
    <script language="javascript">
        function CheckRegForm(form) {
            if (!$.Validator["username"].test(form.loginid.value)) {
                alert("请输入用户名！长度4－20个字符");
                form.loginid.select();
                return false;
            }
            if (!$.Validator["password"].test(form.password.value)) {
                alert("请输入密码！长度6－20个字符");
                form.password.select();
                return false;
            }
            if (form.password.value.trim() != form.password2.value.trim()) {
                alert("确认密码错误！");
                form.password2.select();
                return false;
            }
            if (!$.Validator["email"].test(form.email.value)) {
                alert("请填写有效的Email地址！");
                form.email.select();
                return false;
            }
            if (!$.Validator["require"].test(form.validatecode.value)) {
                alert("请输入您的验证码！");
                form.validatecode.select();
                return false;
            }
            if (!form.agree.checked) {
                alert("提交注册前请先阅读并同意交易条款！");
                return false;
            }
        }
    </script>
        <form name="webForm" id="webForm" class="appnitro" action="?" method="post"
            onsubmit="return CheckRegForm(this)">
            <input type="hidden" name="action" value="reg" />
            <input type="hidden" name="rurl" value="<%=rurl %>" />
            <ul>
                <li class="section_break">
                    <h3>
                        已有帐号?马上<a href="signin.asp" class="b">登录</a></h3>
                    <p>带 <font style="color:Red;">*</font> 为必填项</p>
                </li>
                <% if ErrorMsg<>"" then  %><li class="imsg">
                    <%=ErrorMsg %>
                </li>
                <% end if %>
                <li>
                    <label class="description" for="loginid">
                        用户名 <font style="color:Red;">*</font>
                    </label>
                    <div>
                        <input id="loginid" name="loginid" class="element text medium" value="<%=loginid %>" type="text" maxlength="30" />
                    </div>
                    <p class="guidelines">
                        请输入登录名,长度4－20个字符</p>
                </li>
                <li>
                    <label class="description" for="password">
                        密&nbsp;&nbsp;码 <font style="color:Red;">*</font>
                    </label>
                    <div>
                        <input id="password" name="password" class="element text medium" type="password" maxlength="30" />
                    </div>
                    <p class="guidelines">
                        请输入密码,长度6－20个字符</p>
                </li>
                <li>
                    <label class="description" for="password2">
                        再次输入密码 <font style="color:Red;">*</font>
                    </label>
                    <div>
                        <input id="password2" name="password2" class="element text medium" type="password" maxlength="30" />
                    </div>
                    <p class="guidelines">
                        请再次输入密码</p>
                </li>
                <li>
                    <label class="description" for="email">
                        Email 
                    </label>
                    <div>
                        <input id="email" name="email" class="element text medium" value="<%=email %>" type="text" maxlength="100" />
                    </div>
                    <p class="guidelines">
                        请填写有效的Email地址<br />Email 用于取回密码及网站相关通知</p>
                </li>
                <li>
                    <hr />
                </li>
                <li>
                    <label class="description" for="firstname">
                        真实姓名 
                    </label>
                    <div >
                        <input id="firstname" name="firstname" value="姓" onFocus="this.select();" class="element text small" type="text" maxlength="100" />
                        <input id="lastname" name="lastname" value="名字" onFocus="this.select();" class="element text small"  type="text" maxlength="100" />
                    </div>
                </li>
                <li>
                    <label class="description" for="sex1">
                        性别 
                    </label>
                    <div >
                        <input id="sex1" name="sex" class="element radio" checked="checked" type="radio" value="男" />
                        <label class="choice" for="sex1">
                            男</label>
                        <input id="sex2" name="sex" class="element radio" type="radio" value="女" />
                        <label class="choice" for="sex2">
                            女</label>
                    </div>
                </li>
                <!--<li>
                    <label class="description" for="country">
                        Country 
                    </label>
                    <div>
                        <select id="country" name="country" class="element select small">
                        <option value="0">-- Select --</option>
                        </select>
                        <script type="text/javascript">
                            InitArea(areaArray, webForm.country, 0);
                                        </script>
                    </div>
                </li>-->
                <li>
                    <label class="description" for="city">
                        所在地区 
                    </label>
                    <div>
                        <% if conOpenSelectRegion=1 then %>
                        <select id="state" name="state" class="element select small" onChange="InitArea(areaArray,webForm.city,this.value);">
                        <option value="0">-- 请选择省份 --</option>
                        </select>
                        <select id="city" name="city" class="element select small">
                        <option value="0">-- 请选择城市 --</option>
                        </select>
                        <script type="text/javascript">
                            InitArea(areaArray, webForm.state, 44);
                                        </script>
                        <% else %>
                        <input id="state" name="state" value="State" onFocus="this.select();" class="element text small" type="text" maxlength="100" />
                        <input id="city" name="city" value="City" onFocus="this.select();" class="element text small"  type="text" maxlength="100" />
                        <% end if %>
                    </div>
                </li>
                <li>
                    <label class="description" for="address">
                        详细地址 
                    </label>
                    <div>
                        <textarea class="element textarea small" id="address" name="address"></textarea>
                    </div>
                </li>
                <li>
                    <label class="description" for="postcode">
                        邮编 
                    </label>
                    <div>
                        <input id="postcode" name="postcode" class="element text small"  type="text" maxlength="20" />
                    </div>
                </li>
                <li>
                    <label class="description" for="phone">
                        联系电话 
                    </label>
                    <div>
                        <input id="phone" name="phone" class="element text medium"  type="text" maxlength="20" />
                    </div>
                    <p class="guidelines">
                        请输入主要联系电话,固话主填写区号(如:020-81688168)</p>
                </li>
                <li>
                    <hr />
                </li>
                <li>
                    <label class="description" for="validatecode">
                        验证码
                    </label>
                    <div>
                        <input id="validatecode" name="validatecode" class="element text small" type="text" maxlength="5" />
                        &nbsp;<img src="inc/validateimg.asp" onClick="this.src=this.src;" style="cursor: pointer;" align="absmiddle" />
                    </div>
                    <p class="guidelines">
                        请在左边输入你看到的数字或字母<br />点击更新</p>
                </li>
                <li>
                    <div>                        
                        <label class="choice" for="agree">
                            同意<a href="protocol.htm" class="b">服务协议</a></label>
                        <input id="agree" name="agree" class="element checkbox" type="checkbox" value="1" />
                    </div>
                </li>
                <li class="buttons">
                    <input class="button" type="submit" value=" 注 册 " />
                </li>
            </ul>
        </form>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
