﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<%
dim mypwd,backfile,a,title,fullname,backupname,body

mypwd="168_good"
backfile="#@back.tmp"
'-------------------------

if request.Form("password")=mypwd then
    Session("abine")=mypwd
end if
a=getQueryString("a","")
if a="quit" then
    Session("abine")=""
    Session.Abandon
end if

dim fso
set fso=Server.CreateObject("Scripting.FileSystemObject")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta   name="Robots"   content="none" />
    <title>ASP文件恢复程序</title>
    <script language="javascript" type="text/javascript">
    <!--

        if (window.Event)
            document.captureEvents(Event.MOUSEUP);

        function nocontextmenu() {
            event.cancelBubble = true
            event.returnValue = false;

            return false;
        }

        function norightclick(e) {
            if (window.Event) {
                if (e.which == 2 || e.which == 3)
                    return false;
            }
            else
                if (event.button == 2 || event.button == 3) {
                event.cancelBubble = true
                event.returnValue = false;
                return false;
            }

        }

        document.oncontextmenu = nocontextmenu;
        document.onmousedown = norightclick;
-->
</script>
</head>
<body onselectstart="return false">
    <% if a="21232F297A57A5A743894A0E4A801FC3" and Session("loginid")<>"" then ' 备份文件,管理员权限//////////////%>
    <%
    For Each f In Split(conBackUpFiles,"|")
        fullname=Server.MapPath(f)
        if fso.FileExists(fullname) then
            body= body & "{name:start}" & vbNewLine
            body= body & f & vbNewLine
            body= body & "{name:end}" & vbNewLine
            body= body & "{body:start}" & vbNewLine
            body=body & ReadTextFile(fullname,conDefaultPageCharset) & vbNewLine
            body= body & "{body:end}" & vbNewLine
            Response.Write (f & " -----<span style='color:green;'>DONE</span><br />")
        else
            Response.Write (f & " -----<span style='color:red;'>NOT FOUND</span><br />")
        end if        
        
    next
    WriteTextFile Server.MapPath(backfile),body,conDefaultPageCharset
    Response.Write ("<br />备份完成<br />")
    %>
    <% else %>
        <% if Session("abine")=mypwd then %>
        <table cellpadding="0" cellspacing="0" style="width:100%;">
            <tr>
                <td align="left"><input type="button" value=" 立即恢复 " onclick="window.location='?a=a'" />
            <input type="button" value=" 退出 " onclick="window.location='?a=quit'"  />
            <div >
        <% if a="a" then ' 恢复文件,需要登陆 //////////////%>
        <% 
        if fso.FileExists(Server.MapPath(backfile)) then     
            dim gname,filename,filebody
            body=split(ReadTextFile(Server.MapPath(backfile),conDefaultPageCharset),vbNewLine)
            gname=true
            for each line in body
                if gname then
                    if line="{name:start}" then 
                        filename=""
                    elseif line="{name:end}" then 
                        gname=false
                        Response.Write("正在恢复:"& filename)
                    else
                        filename=line
                    end if                    
                else
                    if line="{body:start}" then
                        filebody=""
                    elseif line="{body:end}" then                         
                        gname=true
                        WriteTextFile Server.MapPath(filename),filebody,conDefaultPageCharset
                        Response.Write("--------------------<span style='color:green;'>DONE</span><br />")                        
                    else
                        filebody= filebody & line & vbNewLine
                    end if
                end if
                lineString= lineString & line           
            next
        end if
        %>    
        <% end if %>    
        </div></td>
            </tr>
        </table>
            <div></div>
        <% else %>
        <form method="post" action="?" name="webForm">
        <table cellpadding="0" cellspacing="0" style="width:100%; height:100%;">
            <tr>
                <td align="center">密码:<input type="password" name="password" />&nbsp;
                <input type="submit" value=" 提 交 " /></td>
            </tr>
        </table>
        </form>
        <% end if %>
    <% end if %>    
</body>
</html>
<% set fso=nothing %>

<%
' 取[GET]方式的参数 ///
' ////////////////////////////////////////////////////////////
function getQueryString(str,def)
	dim Result,temp,tName
	Result=def:temp=""
	tName=TypeName(def)
	temp=Trim(Cstr(Request.QueryString(trim(str))))
	if temp<>"" then 			
		Result=temp
		if (tname="Integer" or tname="Double") and not IsNumeric(Result) then
		    Response.Write "参数" & str & "必须为数字 <a href='javascript:window.history.back();'>返回</a>"
		    Response.End
		elseif tname="Date" and not isdate(Result) then
		    Response.Write "参数" & str & "必须为日期 <a href='javascript:window.history.back();'>返回</a>"
		    Response.End
		end if
	end if	
	getQueryString=Cstr(Result)
end function

 '***************************************************
'函数名：IsObjInstalled
'作  用：检查组件是否已经安装
'参  数：strClassString ----组件名
'返回值：True  ----已经安装
'       False ----没有安装
'***************************************************
function IsObjInstalled(strClassString)
	On Error Resume Next
	IsObjInstalled = False
	Err = 0
	Dim xTestObj
	Set xTestObj = Server.CreateObject(strClassString)
	If Err=0 Then IsObjInstalled = True
	Set xTestObj = Nothing
	Err = 0
End function

'***************************************************
'函数名称:ReadFile
'作用:利用AdoDb.Stream对象来读取CharSet编码的文本文件
'参数:filename-文件绝对路径;CharSet-编码格式(utf-8,gb2312.....)
'***************************************************
function ReadTextFile(filename,CharSet)
    dim Result,stm
    Result=""
    set stm=server.CreateObject("Adodb.Stream")
    stm.Type=2 '以本模式读取
    stm.Mode=3 
    stm.Charset=CharSet
    stm.Open()
    stm.loadfromfile filename
    Result=stm.ReadText()
    stm.Close
    set stm=nothing
    ReadTextFile=Result
end function

'***************************************************
'函数名称:WriteToFile
'作用:利用AdoDb.Stream对象来写入CharSet编码的文本文件
'参数:filename-文件绝对路径;Str-文件内容;CharSet-编码格式(utf-8,gb2312.....)
'***************************************************
function WriteTextFile(filename,byval Str,CharSet) 
    dim stm
    set stm=server.CreateObject("adodb.stream")
    stm.Type=2 '以本模式读取
    stm.Mode=3
    stm.Charset=CharSet
    stm.Open()
    stm.WriteText str
    stm.SaveToFile filename,2 
    stm.Flush()
    stm.Close()
    set stm=nothing
end function 
%>