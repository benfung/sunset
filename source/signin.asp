﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/Md5.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>登录 -
        <%=conSiteTitle %>
    </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
dim myuser
set myuser=new myuserobj

dim loginid,password,action,rurl,validatecode,errormsg
action=getquerystring("action",getForm("action",""))
rurl=getquerystring("rurl",getForm("rurl","index.asp"))
loginid=getForm("loginid","")
password=getForm("password","")
validatecode=getform("validatecode","")


errormsg=""
if action="login" then 
    if loginid="" then
        'errormsg="请填写用户名!!"
        call AlertPop("请填写用户名","")
    end if
    if password="" then
        'errormsg="请填写密码!!"
        call AlertPop("请填写密码","")
    end if
    if validatecode<>Cstr(Session("ValidateCode")) then    
        'errormsg="对不起,验证码不正确!!"    
    end if
    
    password=md5(password,32)
    
    
    
    myuser.myuserObjByNP loginid,password 
    
    if myuser.isnull() then
        ' 记录登陆历史 ////////////////////////    
        set loginlog=new loginlogObj
        loginlog.usertype="myuser"
        loginlog.username="<font color=red>Failing!!</font>"
        loginlog.ip=getip()
        loginlog.logcontent="<font color=red>登陆失败</font>"
        loginlog.scriptname=Trim(Request.ServerVariables("HTTP_REFERER"))
        loginlog.poststring=Replace(Request.Form,"'","''")
        loginlog.add()       
        call AlertPop("用户名或密码错误，请重新输入","")
    elseif myuser.LockUser=1 then
        call AlertPop("对不起该用户已被锁定!!如要解锁请联系客服人员","")
    else
        if errormsg="" then
            ' 更新登陆信息
            myuser.LoginIP=getip()
            myuser.LastLoginTime=now()
            myuser.logins=myuser.logins+1
            myuser.Update()
            
            ' 记录登陆历史 ////////////////////////  
            set loginlog=new loginlogObj     
            loginlog.usertype="myuser"
            loginlog.username=myuser.loginid
            loginlog.ip=getip()
            loginlog.logcontent="正常登陆"
            loginlog.scriptname=Trim(Request.ServerVariables("HTTP_REFERER"))
            loginlog.add()
            
            ' 保存状态
            myuser.SaveStatus(conLoginTimeOut)
                    
            call AlertPop("登录成功",rurl)
            'call ToPage(rurl)
        end if
    end if
end if

' 退出登陆
if action="logout" then   
    ' 清除状态 
    myuser.ClearStatus()    
    call AlertPop("安全退出",rurl)
end if
%>
<body>
    <!--#include file="head.asp"-->
    <div class="body">
    <script language="javascript">
        function CheckLoginForm(form) {
            if (!$.Validator["username"].test(form.loginid.value)) {
                alert("请输入用户名！");
                form.loginid.select();
                return false;
            }
            if (!$.Validator["require"].test(form.password.value)) {
                alert("请输入密码！");
                form.password.select();
                return false;
            }
        }
    </script>
        <form name="webForm" id="webForm" class="appnitro" action="?action=login" method="post"
            onsubmit="return CheckLoginForm(this)">
            <input type="hidden" name="action" value="login" />
            <input type="hidden" name="rurl" value="<%=rurl %>" />
            <ul>
                <li class="section_break">
                    <h3>
                        没有帐号?马上<a href="reg.asp" class="b">注册</a></h3>
                </li>
                <% if ErrorMsg<>"" then  %><li class="imsg">
                    <%=ErrorMsg %>
                </li>
                <% end if %>
                <li>
                    <label class="description" for="loginid">
                        用户名:
                    </label>
                    <div>
                        <input id="loginid" name="loginid" class="element text medium" type="text" maxlength="100" />
                    </div>
                    <p class="guidelines">
                        请输入登录名</p>
                </li>
                <li>
                    <label class="description" for="password">
                        密&nbsp;&nbsp;码:
                    </label>
                    <div>
                        <input id="password" name="password" class="element text medium" type="password" maxlength="100" />&nbsp;<a href="getpassword.asp" target="_blank" class="b">忘记密码?</a>
                    </div>
                    <p class="guidelines">
                        请输入密码</p>
                </li>
                <!--<li>
                    <label class="description" for="validatecode">
                        验证码:
                    </label>
                    <div>
                        <input id="validatecode" name="validatecode" class="element text small" type="text" maxlength="5" />
                        &nbsp;<img src="inc/validateimg.asp" onclick="this.src=this.src;" style="cursor: pointer;" align="absmiddle" />
                    </div>
                    <p class="guidelines">
                        请在左边输入你看到的数字或字母</p>
                </li>-->
                <!--<li>
                    <div>
                        <input id="save" name="save" class="element checkbox" type="checkbox" value="1" />
                        <label class="choice" for="save">
                            保存密码</label>
                    </div>
                </li>-->
                <li class="buttons">
                    <input class="button" type="submit" value=" 登 录 " />
                </li>
            </ul>
        </form>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
