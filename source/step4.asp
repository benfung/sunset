﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/MailObject.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>结算中心 </title>
    <!--#include file="headmeta.asp"-->
</head>
<%
if conOrderForUser=1 then
	' 验证用户
	call CheckUserLogin("")
end if

dim region
set region=new regionobj

dim myclass
set myclass=new classObj

dim myshopcart
set myshopcart=new shoppingcartsObj
myshopcart.checkcartid "",""

set delivery=new deliveryObj
delivery.getInfoByCookies("Cart")

dim delivertype,paytype
delivertype=Request.Cookies("Cart")("delivertype")
paytype=Request.Cookies("Cart")("paytype")

dim action
action=getform("action","")

%>

<%
if action="save" then
    set orderform=new ordersObj    
    
    ' 送货方式
    myclass.classobj(delivertype)    
    orderform.delivertype=myclass.name
    orderform.fee=myclass.subname  ' 运费
    
    orderform.ordernum=GenOrderNum() ' 生成订单号
    orderform.myuserid=myuser.id 
    orderform.moneytotal=myshopcart.countPrice("memberprice") + orderform.fee
    
    orderform.thetext=getform("thetext","")
    
    ' 送货信息
    orderform.firstname=delivery.firstname
    orderform.lastname=delivery.lastname
    
    orderform.city=delivery.city
    orderform.state=delivery.state
    orderform.country=delivery.country
    
    orderform.address=delivery.address
    orderform.postcode=delivery.postcode
    orderform.mobile=delivery.mobile
    orderform.phone=delivery.phone
    orderform.email=delivery.email    
    
    ' 付款方式
    myclass.classobj(delivertype) 
    orderform.paymenttype=myclass.name
    
    orderform.status=1
    orderform.add()
    
    ' 写入订单商品信息  ///////////////////////////////////
    set orderformitem=new orderitemObj
   set rs=myshopcart.LoadItemByCartid()                                             
   for i=1 to rs.RecordCount          
        orderformitem.orderid=orderform.id
        orderformitem.productid=rs.Fields("productid")
        orderformitem.title=rs.Fields("productname")
        orderformitem.price_original=rs.Fields("saleprice")
        orderformitem.price=rs.Fields("memberprice")
        orderformitem.trueprice=rs.Fields("memberprice")
        orderformitem.amount=rs.Fields("quantity")
        orderformitem.other=rs.Fields("other")
        orderformitem.subtotal=(rs.Fields("memberprice")*rs.Fields("quantity"))
        orderformitem.add()
        rs.MoveNext
   next
   
   Response.Cookies("Cart").Expires = Date()-1  ' 取消购物车
   
   ' 发送邮件代码  ///////////////
   set Mail=new MailObject        
   Mail.MailServer=conMailServer
   Mail.MailServerUserName=conMailServerUserName
   Mail.MailServerPassWord=conMailServerPassWord
   Mail.MailDomain=conMailDomain
    
   subject= conSiteName & " - 订单通知"  
   body= "你好:<br />网站 <a herf='"&conSiteUrl&"'>"&conSiteName&"</a> 有新订单(" & orderform.ordernum & ")<br /><br />" 
   body= body & "总金额:" & conCurrencyUnit & FormatNumber(orderform.moneytotal)  &"<br /><br />"
   body= body & "查看详细请点击以下链接<br /><a href='" & conSiteUrl & "orderdetail.asp?ordernum=" & orderform.ordernum & "&email=" & orderform.email & "'>" & SiteUrl & "orderdetail.asp?ordernum=" & orderform.ordernum & "&email=" & orderform.email & "</a><br />"
   mail.send conWebmasterEmail,conWebmasterEmail, subject, body, orderform.email, orderform.email, 3
   set Mail=nothing
           
   call topage("step5.asp?email=" & orderform.email & "&ordernum=" & orderform.ordernum )
end if
%>

<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
        <div style="font-size: 14px; font-weight: bolder; margin-bottom: 20px;width:100%;">
            订购流程:<span>1.我的购物车</span> => <span >2.填写订购信息</span> => <span style="color: Red;">3.确认订购信息</span>
            => <span>4.完成订购</span>
        </div>
            <form name="webForm" id="webForm" class="appnitro" action="?" method="post">
            <input type="hidden" name="action" value="save" />
        <div style=" margin-bottom:6px; background:#7f0019; padding:6px; color:White;">请核对以下订单信息，点击“提交订单”完成</div>        
            <ul style="border:solid 1px #999;margin-bottom:6px; ">
                <% if ErrorMsg<>"" then  %><li class="imsg">
                    <%=ErrorMsg %>
                </li>
                <% end if %>
                <li  >
                    <label class="description" for="delivertypename" >
                        送货信息 <a href="step2.asp?rurl=step4.asp" class="b">修改</a>
                    </label>
                    <div style="line-height: 120%;">
                        <%=delivery.firstname %> <%=delivery.lastname %>
                <br />
                <% region.regionObj(delivery.country):response.Write region.name  %>
                <br />
                <% if conOpenSelectRegion=1 then %>
                <% region.regionObj(delivery.state):response.Write region.name  %>&nbsp;
                <% region.regionObj(delivery.city):response.Write region.name  %>&nbsp;
                <% else %>
                <%=delivery.state %>
                <%=delivery.city %>
                <% end if %><br />
                <%=delivery.address %><br />
                <%=delivery.postcode %><br />
                <%=delivery.email %><br />
                <%=delivery.phone %><br />
                <%=delivery.mobile %>
                    </div>
                </li>  
                <hr style="border-bottom:dashed 1px #333; color:#fff;" />
                <li   >
                    <label class="description" for="delivertypename"  style="font-size:110%;  margin-bottom:8px;">
                        送货方式&支付方式 <a href="step3.asp" class="b">修改</a>
                    </label>
                    <div style="line-height: 150%;">
                        <% myclass.classobj(delivertype):response.Write myclass.name %><br />
                        <% myclass.classobj(paytype):response.Write myclass.name %>
                    </div>
                </li> 
                </ul>
                <ul style="border:solid 1px #999; margin-bottom:6px; ">
                <li   >
                    <label class="description" for="delivertypename"  style="font-size:110%;  margin-bottom:8px;">
                        商品清单 <a href="step1.asp?rurl=step4.asp" class="b">回到购物车，删除或添加商品</a>
                    </label>
                    <div style="line-height: 150%;">
                        <hr size="1" />
                        <table cellpadding="4" cellspacing="0" style="margin-bottom: 10px;">
                            <% set rs=myshopcart.LoadItemByCartid() %>
                            <% for i=1 to rs.RecordCount %>
                            <tr style="border-bottom:dashed 1px #999;">
                                <td  width="300">
                                    <a href="../productdetail.asp?id=<%=rs.fields("productid") %>" target="_blank" class="b">
                                        <%=rs.fields("productname") %>
                                    </a>
                                </td>
                                <td width="110">
                                    <%=conCurrencyUnit %> <%=FormatNumber(rs.fields("memberprice")) %>
                                </td>
                                <td style="text-align:center;">
                                    <%=rs.fields("quantity") %>         
                                </td>
                                <td  style="text-align:right;">
                                    <%=conCurrencyUnit %>
                                    <%=FormatNumber(rs.fields("quantity") * rs.fields("memberprice")) %>
                                </td>
                            </tr>
                            <%  rs.movenext %>
                            <% next %>
                            <% rs.close:set rs=nothing %>
                            <tr>
                                <td colspan="4" style="text-align:right;">
                                商品金额共计： <%=conCurrencyUnit %> <%=FormatNumber(myshopcart.countPrice("memberprice")) %> <br />
                                <% myclass.classobj(delivertype) %>
                                运费： <%=conCurrencyUnit %> <%=FormatNumber(myclass.subname) %><br /><br />
                                <b style="color:Red;">您共需要为订单支付： <%=conCurrencyUnit %> <%=FormatNumber(myshopcart.countPrice("memberprice")+cint(myclass.subname)) %> </b>
                                </td>
                                </tr>
                        </table>
                    </div>
                </li>
                </ul>
                <ul >
                <li>
                    <label class="description" for="thetext" >
                        客户留言 
                    </label>
                    <div style="line-height: 150%;">
                        <textarea class="element textarea small" id="thetext" name="thetext"></textarea>
                    </div>
                    <p class="guidelines">
                        (不超过200字)</p>
                </li>
                </ul>
                <ul >             
            <li class="buttons">
                    <input class="button" type="submit" value="提交订单" />
                </li>
            </ul>
        </form>
        
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
