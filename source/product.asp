﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<% GeneratedHtml conOpenAsp2Html,conDefaultPageCharset ' 生成静态页 %>
<%
dim myclass
set myclass=new classobj

dim product
set product=new productobj
%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>
        产品展示:<%=conSeoTitle %>
    </title>
    
    <!--#include file="headmeta.asp"-->
    <meta name="keywords" content="<%=conSiteKeyword %>" />
    <meta name="description" content="<%=conSiteDescription %>" />
</head>

<%
dim filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage
dim where,orderby,aryClass
dim classid
classid=getquerystring("classid",0)
CurrentPage=Cint(getquerystring("pageno",1))

filePath="?classid=" & classid & "&"

orderby="order by a.id desc"
where="where a.status=1"
if classid<>"0" then
	where= where & "and a.classid in ("&myclass.GetSubs(classid)&")"
	aryClass=Split(myclass.Trackback(classid),",")
end if
%>
<body>
    <!--#include file="head.asp"-->
    <div class="body">
    	
    	<div style="float:left;">
        
    	<% set rs=myclass.load("","where a.typename='product' and a.pid=0","order by a.sort,a.pid,a.id") %>
        <ul>
		<% for i=1 to rs.recordcount %>
        <li><a href="product.asp?classid=<%=rs.fields("id") %>"><%=rs.fields("name") %></a></li>
        <% rs.movenext:next %>
        </ul>
        <% rs.close:set rs=nothing %>
        </div>
        <div >
        <div><a href="index.asp">首页</a>
        <% if classid<>"0" then  %>
        <% for j=0 to ubound(aryClass)%>
        	<% myclass.classobj(aryClass(j)) %>
            <% if not myclass.isnull then %>
        	>> <a href="product_<%=myclass.id%>.htm"><%=myclass.name%></a>
			<% end if %>
        <% next %>
        <% end if %>
        </div>
        <% set rs=product.load("",where,orderby) %>
        <%
		MaxItemSize=30
                        rs.PageSize=MaxItemSize
                        TotalNumber=rs.RecordCount
                        TotalPage=rs.PageCount
                        if rs.EOF then
                            Response.Write ("<span style='color:red;font-size:14px;'>Coming soon!!<span>")
                        else
                            if(CurrentPage<1 or CurrentPage>TotalPage) then CurrentPage=1  '防止页数太大或太小
                            rs.AbsolutePage=CurrentPage
                        end if
                        %>
        <% for i=1 to CurrentRecord(rs,CurrentPage) %>
        <div style="float:left; width:33%;">
        <img src="s_<%=rs.fields("focuspic") %>" align="absmiddle"  onerror="this.src='images/000.gif'" /><br />
        <a href="productdetail.asp?id=<%=rs.fields("id") %>" class="a" target="_blank"><%=rs.fields("title") %></a></div>
        <% rs.movenext:next %>
        <% rs.close:set rs=nothing %>
        </div>
        <div><%call AbineShowPage(filePath,CurrentPage,MaxItemSize,TotalNumber,TotalPage) %></div>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
