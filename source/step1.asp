﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>
        结算中心
    </title>    
    <!--#include file="headmeta.asp"-->
</head>
<%
dim myshopcart
set myshopcart=new shoppingcartsObj
myshopcart.checkcartid "",""

%>
<body>
    <!--#include file="head.asp"-->
    <div class="body" style="margin-bottom: 10px;">
    	<div style="font-size:14px; font-weight:bolder; margin-bottom:20px;width:100%;">
            订购流程:<span style="color:Red;">1.我的购物车</span> => <span>2.填写订购信息</span> => <span>3.确认订购信息</span> => <span>4.完成订购</span>
        </div>
        <form action="add2cart.asp?action=mod" method="post" >
        <table cellpadding="4" cellspacing="0" class="table1" style="margin-bottom:1px;" >
            <tr >
                <th class="th" width="30"><div>NO.</div></th>
                <th class="th" width="60"><div>图</div></th>
                <th class="th"><div>商品名称</div></th>
                <th class="th" width="40"><div>数量</div></th>
                <th class="th" width="70"><div>单价(<%=conCurrencyUnit %>)</div></th>
                <th class="th" width="80"><div>小计(<%=conCurrencyUnit %>)</div></th>
                <th class="th" width="80"><div>操作</div></th>
            </tr>
            <% set rs=myshopcart.LoadItemByCartid() %>
            <% for i=1 to rs.RecordCount %>
            <input type="hidden" name="productidset" value="<%=rs.Fields("productid") %>" />
            <tr>
                <td class="til"><%=i %>.</td>
                <td class="tic2"><a href="productdetail.asp?id=<%=rs.fields("productid") %>" target="_blank" class="b"><img src="s_<%=rs.fields("focuspic") %>" width="50"  alt="" align="absmiddle" /></a></td>
                <td class="til"><%=rs.fields("productname") %><br />
                <%=rs.fields("productnumber") %></td>
                <td class="tic2"><input type="text" class="text" name="quantity<%=rs.fields("productid")%>" value="<%=rs.fields("quantity") %>" style="width:34px;"  /></td>
                <td class="tir"><%=FormatNumber(rs.fields("memberprice")) %></td>
                <td class="tir"><%=FormatNumber(rs.fields("quantity") * rs.fields("memberprice")) %></td>
                <td class="tic"><a href="add2cart.asp?action=del&productid=<%=rs.fields("productid") %>&other=<%=rs.fields("other") %>">×删除</a></td>
            </tr>
            <%  rs.movenext %>
            <% next %>
            <% rs.close:set rs=nothing %>
        </table>        
        <table cellpadding="4" cellspacing="0" >
            <tr>
                <td >
                    数量更改后请点击"更新数量"&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit"  class="button2" value=" 更新数量 " /></td>
                <td  width="180">
                    商品金额总计 <b style="color:red;">
                        <%=conCurrencyUnit %>
                        <%=FormatNumber(myshopcart.countPrice("memberprice")) %>
                    </b></td>
            </tr>
        </table>
        </form>
        <div style="padding:20px 10px 30px; width:100%; text-align:right;"><input type="button" class="button" value="继续购物" onClick="window.location='index.asp';" />&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="button" value="下一步 >>" onClick="window.location='step2.asp'" /></div>
        <table cellpadding="8" cellspacing="0"  style="border:solid 1px #999; margin-bottom:10px;">
        <tr>
            <td width="109"  style="line-height:150%;">关于 "我的购物车" </td>
            <td style="line-height:150%;">为方便您提交订单，您选入“我的购物车”中但尚未提交订单的商品我们将为您保留10天。<br />
                在商品保留期间，您所选择商品的价格、优惠政策、配送时间等信息可能发生变化。</td>
        </tr>
    </table>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
