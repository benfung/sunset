﻿<%@  language="vbscript" codepage="65001" %>
<!--#include file="Inc/cf.asp" -->
<!--#include file="Inc/conn.asp" -->
<!--#include file="Inc/object.asp" -->
<!--#include file="Inc/function.asp" -->
<!--#include file="Inc/getuser.asp" -->
<% GeneratedHtml conOpenAsp2Html,conDefaultPageCharset ' 生成静态页 %>
<%
set main=new mainObj
main.mainObj(getquerystring("id",0))  '生成html
%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><%=main.title %>:<%=conSeoTitle %></title>
    <!--#include file="headmeta.asp"-->
    <meta name="keywords" content="<%=conSiteKeyword %>" />
    <meta name="description" content="<%=conSiteDescription %>" />
</head>
<body>
    <!--#include file="head.asp"-->
    <div>
        <%=main.content %>
    </div>
    <!--#include file="foot.asp"-->
</body>
</html>
<%call closeconn %>
